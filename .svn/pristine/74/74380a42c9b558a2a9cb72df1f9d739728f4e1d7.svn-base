/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;

// import net.sourceforge.cobertura.CoverageIgnore;

/**
 * The class which provides a <strong>value object</strong> of patient
 * after interpreting different type of PAS-ESB messages.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9916 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:41 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public class EsbMessagePatient {

	private EsbPatientDemographics _fromPatient;
	private EsbPatientDemographics _toPatient;

	/**
	 * @return the patient to be "changed from"
	 */
	public EsbPatientDemographics getFromPatient() {
		return _fromPatient;
	}

	/**
	 * @param fromPatient the patient to be "changed from" to set
	 */
	public void setFromPatient(EsbPatientDemographics fromPatient) {
		this._fromPatient = fromPatient;
	}

	/**
	 * @return the patient to be "changed to"
	 */
	public EsbPatientDemographics getToPatient() {
		return _toPatient;
	}

	/**
	 * @param toPatient the patient to be "changed to" to set
	 */
	public void setToPatient(EsbPatientDemographics toPatient) {
		this._toPatient = toPatient;
	}

}
