/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidScaledImage.flow;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. Get cid scaled image of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_06_001 {

	@Autowired
	private IasClientService iasService;

	@Test
	public void getCidScaledImage_RetrieveNotExists_ShouldThrowException() throws Exception {

		String imageID1 = UUID.randomUUID().toString();
		
		final String expectedMessage = "Study does not exist in RR";
		final String accessionNo = "NOT_EXIST_ACCESSION";
		final String studyId = "NOT_EXIST_STUDY";
		
		try {
			CIDData cidData = iasService.generateCIDData(SampleData.getAccessionNo(), 1, 3);
			cidData.getStudyDtl().setAccessionNo(accessionNo);
			cidData.getStudyDtl().setStudyID(studyId);
			
			iasService.getCidScaledImage(cidData, "3", "3", imageID1, 500, 600);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
		
	}
}
