/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The entity class which is used for persisting a queue of PAS-ESB message subscriber.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9636 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:04:17 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_subscription_queue")
@NamedQueries({
	@NamedQuery(
		name = SubscriptionQueue.NAMED_QUERY_FIND_BY_STATUS,
		query = "SELECT q FROM SubscriptionQueue q WHERE q.status = ?1"
	)
})
public class SubscriptionQueue implements Serializable {

	public static final String NAMED_QUERY_FIND_BY_STATUS = "findByStatus";

	private static final long serialVersionUID = -4065593373568408967L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "queue_id")
	private long queueId;

	@Column(name = "version")
	private long version;

	@Column(name = "queue_name")
	private String queueName;

	@Column(name = "enqueuer_ipv4")
	private String enqueuerIpv4;

	@Column(name = "dequeuer_ipv4")
	private String dequeuerIpv4;

	@Column(name = "enqueuer_class_name")
	private String enqueuerClassName;

	@Column(name = "jndi_queue")
	private String jndiQueue;

	@Column(name = "jndi_queue_connection_factory")
	private String jndiQueueuConnectionFactory;

	@Column(name = "status")
	private String status;

	/**
	 * @return the value of <tt>queue_id</tt>
	 */
	public long getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId the <tt>queue_id</tt> to set
	 */
	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the value of <tt>version</tt>
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the <tt>version</tt> to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the value of <tt>queue_name</tt>
	 */
	public String getQueueName() {
		return queueName;
	}

	/**
	 * @param queueName the <tt>queue_name</tt> to set
	 */
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	/**
	 * @return the value of <tt>enqueuer_ipv4</tt>
	 */
	public String getEnqueuerIpv4() {
		return enqueuerIpv4;
	}

	/**
	 * @param enqueuerIpv4 the <tt>enqueuer_ipv4</tt> to set
	 */
	public void setEnqueuerIpv4(String enqueuerIpv4) {
		this.enqueuerIpv4 = enqueuerIpv4;
	}

	/**
	 * @return the value of <tt>dequeuer_ipv4</tt>
	 */
	public String getDequeuerIpv4() {
		return dequeuerIpv4;
	}

	/**
	 * @param dequeuerIpv4 the <tt>dequeuer_ipv4</tt> to set
	 */
	public void setDequeuerIpv4(String dequeuerIpv4) {
		this.dequeuerIpv4 = dequeuerIpv4;
	}

	/**
	 * @return the value of <tt>enqueuer_class_name</tt>
	 */
	public String getEnqueuerClassName() {
		return enqueuerClassName;
	}

	/**
	 * @param enqueuerClassName the value of <tt>enqueuer_class_name</tt> to set
	 */
	public void setEnqueuerClassName(String enqueuerClassName) {
		this.enqueuerClassName = enqueuerClassName;
	}

	/**
	 * @return the <tt>JNDI</tt> of a subscription queue
	 */
	public String getJndiQueue() {
		return jndiQueue;
	}

	/**
	 * @param jndiQueue the <tt>JNDI</tt> of a subscription queue to set
	 */
	public void setJndiQueue(String jndiQueue) {
		this.jndiQueue = jndiQueue;
	}

	/**
	 * @return the <tt>JNDI</tt> of a queue connection factory
	 */
	public String getJndiQueueuConnectionFactory() {
		return jndiQueueuConnectionFactory;
	}

	/**
	 * @param jndiQueueuConnectionFactory the <tt>JNDI</tt> of a queue connection factory to set
	 */
	public void setJndiQueueuConnectionFactory(String jndiQueueuConnectionFactory) {
		this.jndiQueueuConnectionFactory = jndiQueueuConnectionFactory;
	}

	/**
	 * @return the value of <tt>status</tt>
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the <tt>status</tt> to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
