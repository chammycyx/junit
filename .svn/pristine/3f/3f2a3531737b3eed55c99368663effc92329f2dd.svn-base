/**
 * 
 */
package org.ha.cid.ias.testcase.api.deleteStudy.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_11_001;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. delete the study
 * 
 * @author YKF491
 *
 */
public abstract class AUDIT_11_001 extends FLOW_11_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImageDeleteStudy_ShouldEsStudyAuditMatch() throws Exception {
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.DELETE_ES_EVENT_NAME));
	}
	
	@Test
	public void uploadMetadata_uploadImageDeleteStudy_ShouldRrStudyAuditMatch() throws Exception {
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos,
				ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME,
						AuditEventDto.DELETE_ES_EVENT_NAME, AuditEventDto.DELETE_RR_EVENT_NAME));
	}
	
}
