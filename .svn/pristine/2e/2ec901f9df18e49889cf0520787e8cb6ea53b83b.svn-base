package org.ha.cid.ias.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ha.cd2.isg.icw.tool.CidStorePathUtil;
import org.ha.cid.ias.dao.RrAuditEventDao;
import org.ha.cid.ias.dao.RrPatientDao;
import org.ha.cid.ias.dao.RrStudyDao;
import org.ha.cid.ias.entity.rr.AuditEvent;
import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.entity.rr.Patient;
import org.ha.cid.ias.entity.rr.Study;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.model.PatientDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.RemoteFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

public class IasRrInternalServiceImpl implements IasInternalService {

	private static final Logger LOG = LoggerFactory.getLogger(IasRrInternalServiceImpl.class);

	@Autowired
	private RrStudyDao rrStudyDao;
	
	@Autowired
	private RrPatientDao rrPatientDao;

	@Autowired
	private RrAuditEventDao rrAuditEventDao;
	
	@Autowired
	@Qualifier("cnRemoteFileService")
	private RemoteFileService cnRemoteFileService;

	private int delay;
	private boolean purgeImage = false;
	
	public void setDelay(int delay) {
		this.delay = delay;
	}

	public void setPurgeImage(boolean purgeImage) {
		this.purgeImage = purgeImage;
	}
	
	private void delay(long seconds) {
		try {
			LOG.info("Start delay for {} seconds...", seconds);
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<AuditEventDto> getAuditEvent(String accessionNo) throws Exception {
		if(delay>0)
			delay(delay);
		
		List<AuditEvent> auditEvents = rrAuditEventDao.findByAccessionNo(accessionNo);
		
		if(auditEvents == null || auditEvents.size() == 0)
			return null;
		
		List<AuditEventDto> auditEventDtos = new ArrayList<AuditEventDto>();
		Iterator<AuditEvent> iterator = auditEvents.iterator();
		while(iterator.hasNext()) {
			AuditEvent auditEvent = iterator.next();
			auditEventDtos.add(AuditEventDto.buildFrom(auditEvent));
		}
		return auditEventDtos;
	}

	@Override
	@Transactional("rrTransactionManager")
	public void removeStudy(String accessionNo) throws Exception {
		if(delay>0)
			delay(delay);
		
		Study study = rrStudyDao.findStudyByAccessionNo(accessionNo);
		// Remove path index from CN
		List<PathIndex> pathIndexes = study.getSeries().get(0).getPathIndexs();
		
		if (pathIndexes.size() > 1) {
			Iterator<PathIndex> iter = pathIndexes.iterator();
			while(iter.hasNext()){
				PathIndex pathIndex = iter.next();
			    if(pathIndex.getSerMotiveTag().equals("LOCAL")) {
			    	rrStudyDao.deletePathIndex(pathIndex.getId());
			    }
			}
		} else {
			throw new Exception("Image routing is not completed");
		}
		
		if (purgeImage) {
			cnRemoteFileService.deleteDirectory(CidStorePathUtil.generateStudyPath(accessionNo));
		} else {
			LOG.info("Skip delete image from directory: {}", CidStorePathUtil.generateStudyPath(accessionNo));
		}
	}

	@Override
	@Transactional("rrTransactionManager")
	public StudyDto getStudy(String accessionNo) throws Exception {
		delay(delay);
		Study study = rrStudyDao.findStudyByAccessionNo(accessionNo);
		
		if(study != null)
			return StudyDto.buildFrom(study);
		else
			return null;
	}

	@Override
	@Transactional("rrTransactionManager")
	public PatientDto getPatientByPatientKey(String patientKey)throws Exception {
		Patient patient = rrPatientDao.findPatientByPatientKey(patientKey);
		
		if(patient != null)
			return PatientDto.buildFrom(patient);
		else
			return null;
	}

	@Override
	@Transactional("rrTransactionManager")
	public void createPatient(PatientDto patientDto) throws Exception {
		Patient patient = new Patient();
			patient.setPatNo(Integer.valueOf(patientDto.getPatKey()));
			patient.setPatBirthdate(patientDto.getPatDob());
			patient.setPatGlobalId(patientDto.getPatKey());
			patient.setPatId(patientDto.getPatHkid());
			patient.setPatName(patientDto.getPatName());
			patient.setPatSex(patientDto.getPatSex());
		rrPatientDao.insert(patient);
	}

	@Override
	@Transactional("rrTransactionManager")
	public void removePatient(String patientKey) throws Exception {
		rrPatientDao.deletePatientByPatientKey(patientKey);
	}

	@Override
	@Transactional("rrTransactionManager")
	public void updateStudyWithPatient(String accessionNo, String patientKey) {
		Patient patient = rrPatientDao.findPatientByPatientKey(patientKey);
		Study study = rrStudyDao.findStudyByAccessionNo(accessionNo);
			study.setPatient(patient);
		rrStudyDao.update(study);
	}

	@Override
	@Transactional("rrTransactionManager")
	public StudyDto getRefreshedStudy(String accessionNo) {
		Study study = rrStudyDao.findStudyByAccessionNo(accessionNo);
			study = rrStudyDao.refresh(study);
		
		if(study != null)
			return StudyDto.buildFrom(study);
		else
			return null;
	}

	@Override
	@Transactional("rrTransactionManager")
	public void updateStudyWithPatientEpisode(String accessionNo, String patientKey, String hospCode, String caseNum) {
		Patient patient = rrPatientDao.findPatientByPatientKey(patientKey);
		Study study = rrStudyDao.findStudyByAccessionNo(accessionNo);
			study.setPatient(patient);
			study.setCaseNo(caseNum);
			study.setHospitalCode(hospCode);
		rrStudyDao.update(study);
	}

	@Override
	public List<AuditEventDto> getAuditEvent(String accessionNo, String auditEventName) throws Exception {
		List<AuditEventDto> auditEvents = getAuditEvent(accessionNo);
			auditEvents.removeIf(a->!a.getEvent().equals(auditEventName));
		return auditEvents;
	}

}
