package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;


/**
 * This test case intends to validate the following items:
 * 1. upload images 2 times with the same accessionNo, studyID but different caseNum 
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_EX_03_007 {
	
	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_EditDifferentCaseNo_ShouldThrowException() throws Exception {

		final String expectedMessage = "Database Error";
		
		String[] imageFiles = {	"/blazeds/flow/OGD1_v1.PNG", "/blazeds/flow/OGD1_v2.PNG", "/blazeds/flow/OGD1_v3.PNG" };
		
		String[] versions = { "1", "2", "3" };
		
		String[] imageHandlings = {	"N", "N", "N" };
		
		String imageID1 = UUID.randomUUID().toString();
		
		String[] imageIDs = { imageID1, imageID1, imageID1 };
		
		String accessionNo = SampleData.getAccessionNo();
		String studyID = UUID.randomUUID().toString();
		
		CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, accessionNo, imageHandlings, studyID);
		cidData.getVisitDtl().setCaseNum(UUID.randomUUID().toString());
		iasService.uploadMetaData(cidData);
		
		CIDData cidData2 = iasService.uploadImages(imageFiles, versions, imageIDs, accessionNo, imageHandlings, studyID);
		cidData2.getVisitDtl().setCaseNum(UUID.randomUUID().toString());
			
		try {
			iasService.uploadMetaData(cidData2);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e) {
			Assert.assertEquals(e.getStatusCode(), 500);
		} 

	}
}
