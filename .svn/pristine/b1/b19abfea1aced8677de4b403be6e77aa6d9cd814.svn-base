package org.ha.cd2.isg.icw.tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 日期工具类 默认使用 "yyyy-MM-dd HH:mm:ss" 格式化日期
 * 
 */
public final class DateUtils {
	/**
	 * 英文简写（默认）如：2010-12-01
	 */
	public static String FORMAT_SHORT = "yyyy-MM-dd";
	/**
	 * 英文全称 如：2010-12-01 23:15:06
	 */
	public static String FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 精确到毫秒的完整时间 如：yyyy-MM-dd HH:mm:ss.S
	 */
	public static String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.S";
	/**
	 * 中文简写 如：2010年12月01日
	 */
	public static String FORMAT_SHORT_CN = "yyyy年MM月dd";
	/**
	 * 中文全称 如：2010年12月01日 23时15分06秒
	 */
	public static String FORMAT_LONG_CN = "yyyy年MM月dd日  HH时mm分ss秒";
	/**
	 * 精确到毫秒的完整中文时间
	 */
	public static String FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒";
	
	public static String FORMAT_TIMESTAMP = "yyyyMMddHHmmss.SSS";
	
	public static String FORMAT_TIMESTAMP_SHORT = "yyyyMMddHHmmss";
	
	public static String FORMAT_YYYYMMDD = "yyyyMMdd";

	/**
	 * 获得默认的 date pattern
	 */
	public static String getDatePattern() {
		return FORMAT_LONG;
	}

	/**
	 * 根据预设格式返回当前日期
	 * 
	 * @return
	 */
	public static String getNow() {
		return format(new Date());
	}

	/**
	 * 根据用户格式返回当前日期
	 * 
	 * @param format
	 * @return
	 */
	public static String getNow(String format) {
		return format(new Date(), format);
	}

	/**
	 * 使用预设格式格式化日期
	 * 
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return format(date, getDatePattern());
	}

	/**
	 * 使用用户格式格式化日期
	 * 
	 * @param date
	 *            日期
	 * @param pattern
	 *            日期格式
	 * @return
	 */
	public static String format(Date date, String pattern) {
		String returnValue = "";
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(pattern);
			returnValue = df.format(date);
		}
		return (returnValue);
	}

	/**
	 * 使用预设格式提取字符串日期
	 * 
	 * @param strDate
	 *            日期字符串
	 * @return
	 */
	public static Date parse(String strDate) {
		return parse(strDate, getDatePattern());
	}

	/**
	 * 使用用户格式提取字符串日期
	 * 
	 * @param strDate
	 *            日期字符串
	 * @param pattern
	 *            日期格式
	 * @return
	 */
	public static Date parse(String strDate, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		try {
			return df.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 在日期上增加数个整月
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            要增加的月数
	 * @return
	 */
	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}

	/**
	 * 在日期上增加天数
	 * 
	 * @param date
	 *            日期
	 * @param n
	 *            要增加的天数
	 * @return
	 */
	public static Date addDay(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, n);
		return cal.getTime();
	}

	/**
	 * 获取时间戳
	 */
	public static String getTimeString() {
		SimpleDateFormat df = new SimpleDateFormat(FORMAT_FULL);
		Calendar calendar = Calendar.getInstance();
		return df.format(calendar.getTime());
	}

	/**
	 * 获取日期年份
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String getYear(Date date) {
		return format(date).substring(0, 4);
	}

	/**
	 * 按默认格式的字符串距离今天的天数
	 * 
	 * @param date
	 *            日期字符串
	 * @return
	 */
	public static int countDays(String date) {
		long t = Calendar.getInstance().getTime().getTime();
		Calendar c = Calendar.getInstance();
		c.setTime(parse(date));
		long t1 = c.getTime().getTime();
		return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
	}

	/**
	 * 按用户格式字符串距离今天的天数
	 * 
	 * @param date
	 *            日期字符串
	 * @param format
	 *            日期格式
	 * @return
	 */
	public static int countDays(String date, String format) {
		long t = Calendar.getInstance().getTime().getTime();
		Calendar c = Calendar.getInstance();
		c.setTime(parse(date, format));
		long t1 = c.getTime().getTime();
		return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
	}
	
	public static String strParseToDateTimeFormat(String str){
		if(str.length()==14 && str != null){
			StringBuilder sb = new StringBuilder();
			sb.append(str.substring(0, 4))
			.append("-").append(str.substring(4, 6))
			.append("-").append(str.substring(6, 8))
			.append(" ").append(str.substring(8, 10))
			.append(":").append(str.substring(10, 12))
			.append(":").append(str.substring(12, 14));
			return sb.toString();
		}else if(str.length()==8 && str != null){
			StringBuilder sb = new StringBuilder();
			sb.append(str.substring(0, 4)).append("-")
			.append(str.substring(4, 6))
			.append("-").append(str.substring(6, 8))
			.append(" 00:00:00");
			return sb.toString();
		}
		return null;
	}
	
	/*
	 * Check the date if it is a valid date in specific string pattern
	 * 
	 * @param date
	 *            Date in String format. Any valid format which could be accepted by object SimpleDateFormat.
	 * @param pattern
	 *            Any valid format which could be accepted by object SimpleDateFormat. 
	 * @return    Return boolean result. The result would be true if it is a valid date in specific pattern. Otherwise false.
	 *            Once the input date is invalid or empty, the return result would be the input value of dateInString.
	 *            Once the input pattern is invalid, the return result would be the input value of dateInString.
	 *  
	 */
	public static boolean isValidDate(String date, String pattern){
		if(StringUtils.isBlank(date)){
			return false;
		}
		
		SimpleDateFormat sdfrmt = new SimpleDateFormat(pattern);
		 	sdfrmt.setLenient(false);
	 	try{
	 		sdfrmt.parse(date); 
        }catch (ParseException e){
            return false;
        }
        return true;
	}
	
	/*
	 * Format a date to specific pattern
	 * 
	 * @param dateInString
	 *            Date in String format. Accepted types of pattern are "yyyyMMddHHmmss.SSS", "yyyyMMddHHmmss" and "yyyyMMdd"
	 * @param pattern
	 *            Any valid format which could be accepted by object SimpleDateFormat. 
	 * @return    Return the formated date. 
	 *            Once the input dateInString is invalid or empty, the return result would be the input value of dateInString.
	 *            Once the input pattern is invalid, the return result would be the input value of dateInString.
	 *  
	 */
	public static String toFormat(String dateInString, String pattern){
		if(StringUtils.isBlank(dateInString)){
			return dateInString;
		}
		
		Date date = null;
		SimpleDateFormat df = new SimpleDateFormat();
		df.setLenient(false);
		try {
			if(dateInString.length()==FORMAT_YYYYMMDD.length()){
				df.applyPattern(FORMAT_YYYYMMDD);
				date = df.parse(dateInString);
			}else if(dateInString.length()==FORMAT_TIMESTAMP_SHORT.length()){
				df.applyPattern(FORMAT_TIMESTAMP_SHORT);
				date = df.parse(dateInString);
			}else if(dateInString.length()==FORMAT_TIMESTAMP.length()){
				df.applyPattern(FORMAT_TIMESTAMP);
				date = df.parse(dateInString);
			}
		} catch (ParseException e) {
			return dateInString;
		}
		
		if(date!=null){
			return format(date,pattern);
		}
		return dateInString;
	}
}
