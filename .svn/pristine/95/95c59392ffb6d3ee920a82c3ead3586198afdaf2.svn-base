package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.basic;

import java.util.Iterator;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. getCidStudyFirstLastImage with optional parameters
 * 
 * @author YKF491
 *
 */
public abstract class API_09_002 {

	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = {	"1", "2", "3" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	imageID1, imageID1, imageID1 };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	protected void ignoreImageDtlsBySeqNVersion(String imageSeq, String versionNo) {
		
		if (imageSeq != null || versionNo != null) {		
			Iterator<ImageDtl> iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
			while(iterator.hasNext()) {
				ImageDtl imageDtl = iterator.next();
				if (imageSeq != null) {
					if (imageDtl.getImageSequence().intValue() == Integer.parseInt(imageSeq)) {
						continue;
					}
				}
				if (versionNo != null) {
					if (imageDtl.getImageVersion().equals(versionNo)) {
						continue;
					}
				}
				
				iterator.remove();
			}
		}
	}
	
	protected void assertHa7(String ha7Result) {
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}
		
		iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			if("2".equals(imageDtl.getImageVersion())) {
				iterator.remove();
			}
		}
		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
		
	}

	@Test
	public void getCidStudyFirstLastImage_WorkstationIdNull_ShouldReturnFirstLast() throws Exception {
		
		String workstationId = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(),
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				null,
				null,
				SampleData.getUserId(),
				workstationId,
				SampleData.getRequestSystem()
		);
		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_WorkstationIdEmpty_ShouldReturnFirstLast() throws Exception {
		
		String workstationId = "";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(),
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				null,
				null,
				SampleData.getUserId(),
				workstationId,
				SampleData.getRequestSystem()
		);
		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_SeriesNoNull_ShouldReturnFirstLast() throws Exception {
		
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				null,
				null
		);
		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_ImageSeqNotNull_ShouldReturnSeq3() throws Exception {
		
		String imageSeq = "3";
		String versionNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);
		
		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
		
	@Test
	public void getCidStudyFirstLastImage_VersionNoNotNull_ShouldReturnVer3() throws Exception {
		
		String imageSeq = null;
		String versionNo = "3";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}

	@Test
	public void getCidStudyFirstLastImage_ImageSeqAndVersionNoNotNull_ShouldReturnSeq3Ver3() throws Exception {
		
		String imageSeq = "3";
		String versionNo = "3";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullAndImageSeqAndVersionNoNotNull_ShouldReturnSeq3Ver3() throws Exception {
		
		String imageSeq = "3";
		String versionNo = "3";
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullAndImageSeqNotNull_ShouldReturnSeq3() throws Exception {
		
		String imageSeq = "3";
		String versionNo = null;
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}

	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullAndVersionNoNotNull_ShouldReturnVer3() throws Exception {
		
		String imageSeq = null;
		String versionNo = "3";
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	/** Exception cases
	 * 
	 * @throws Exception
	 */
	/*
	@Test
	public void getCidStudyFirstLastImage_ImageSeqNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = "2";
		String versionNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}
		
	@Test
	public void getCidStudyFirstLastImage_VersionNoNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = null;
		String versionNo = "2";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}

	@Test
	public void getCidStudyFirstLastImage_ImageSeqVersionNoNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = "2";
		String versionNo = "2";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullImageSeqVersionNoNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = "2";
		String versionNo = "2";
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullImageSeqNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = "2";
		String versionNo = null;
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}

	@Test
	public void getCidStudyFirstLastImage_SeriesNoNullVersionNoNotNull_ShouldSuccess() throws Exception {
		
		String imageSeq = null;
		String versionNo = "2";
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				imageSeq,
				versionNo
		);
		
		assertHa7(ha7Result);
		
	}
	*/
	/**
	 * Exceptional Case
	 *  - Wrong Series No
	 *  - Move to EX Test Case
	 * @throws Exception
	 */
	/*
	@Test
	public void getCidStudyFirstLastImage_SeriesNoWrong_ShouldSuccess() throws Exception {
		
		String seriesNo = "NOT_EXIST_SERIES_NO";
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData,
				seriesNo,
				null,
				null
		);
		
		assertHa7(ha7Result);
		
	}
	*/
	
	/*
	@Test
	public void getCidStudyFirstLastImage_HospCdeNull_ShouldSuccess() throws Exception {
		
		String hospCde = null;
		
		String ha7Result = iasService.getCidStudyFirstLastImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(),
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				null,
				null,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);
		
		assertHa7(ha7Result);
		
	}
	*/
}

