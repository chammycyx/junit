/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;

import org.ha.cid.ias.enumeration.EsbMessageType;

// import net.sourceforge.cobertura.CoverageIgnore;

/**
 * The class which provides a <strong>value object</strong> of the message payload
 * after interpreting different type of PAS-ESB messages.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9916 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:41 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public class EsbMessageHeader {

	private String transactionId;
	private String evn2Dt;
	private EsbMessageType messageType;

	/**
	 * @return the transaction ID of PAS event
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param pasTransactionId the transaction ID of PAS event to set
	 */
	public void setTransactionId(String pasTransactionId) {
		this.transactionId = pasTransactionId;
	}

	/**
	 * @return the event date-time of PAS event
	 */
	public String getEvn2Dt() {
		return evn2Dt;
	}

	/**
	 * @param pasEventDt the event date-time of PAS event to set
	 */
	public void setEvn2Dt(String pasEventDt) {
		this.evn2Dt = pasEventDt;
	}

	public EsbMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(EsbMessageType messageType) {
		this.messageType = messageType;
	}

}
