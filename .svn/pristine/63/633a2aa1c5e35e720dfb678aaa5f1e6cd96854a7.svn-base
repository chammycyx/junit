package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends focus on 'duplicated ESB message checking', the following items would be validated:
 * 1. Duplicated A08/A40/A45/A47 message smaller than Day Back value 
 * 2. Duplicated A08/A40/A45/A47 message equal to Day Back value
 * 3. Duplicated A08/A40/A45/A47 message greater than Day Back value
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_006 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo1;
	private String accessionNo2;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";
	private String cutoffTimeInMinute = "1";
	private String hospCode = "VH";
	private String caseNum1 = "HN01000101X";
	private String caseNum2 = "HN01000102X";
	private String duplicationDayback = "1";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("false");
		iasEsbService.enableDuplicationChecker("true");
		iasEsbService.enableDisorderChecker("true");
		iasEsbService.setCutoffTime(cutoffTimeInMinute);
		iasEsbService.setDuplicationDayback(duplicationDayback);
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo1 = createStudy();
		accessionNo2 = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
		iasEsInternalService.removeStudy(accessionNo1);
		iasEsInternalService.removeStudy(accessionNo2);
	}
	
	/**
	 * 1. Send A08
	 * 2. Send A08 again with same transaction id and the set the received time to that before the day back value
	 * 3. Assert the second A08 should be classified as 'Duplicated message'
	 * 
	 */
	@Test
	public void duplicated_msg_before_day_back_value(){
		//Arrange
		Calendar evn2Calendar = Calendar.getInstance();
		String evn2 = DateUtils.format(evn2Calendar.getTime(), DateUtils.FORMAT_TIMESTAMP);
		Calendar firstMsgReceivedDt = Calendar.getInstance();
			firstMsgReceivedDt.add(Calendar.DATE, -1);
		Calendar duplicatedMsgReceivedDt = (Calendar) firstMsgReceivedDt.clone();
			duplicatedMsgReceivedDt.add(Calendar.DATE, Integer.valueOf(duplicationDayback));
			duplicatedMsgReceivedDt.add(Calendar.MILLISECOND, -1);
		
		//Act
		String a08TxnId1 = sendA08(evn2, fromPatientKey);
		TimerUtil.delay(1);
		String a08TxnId2 = sendA08(evn2, fromPatientKey);
		
		List<StagingDto> stagings = iasEsbService.findStagingByOrderingIdentifierSortByReceivedDtAsc(evn2Calendar.getTime());
		iasEsbService.setReceivedDt(stagings.get(0).getStagingMessageId(), firstMsgReceivedDt.getTime());
		iasEsbService.setReceivedDt(stagings.get(1).getStagingMessageId(), duplicatedMsgReceivedDt.getTime());
		
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto staging1 = iasEsbService.findStagingByMsgId(stagings.get(0).getStagingMessageId());
		StagingDto staging2 = iasEsbService.findStagingByMsgId(stagings.get(1).getStagingMessageId());
		
		assertEquals(MessageProcessStatus.SUCCESS, staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.DUPLICATE, staging2.getProcessStatus());
	}
	
	/**
	 * 1. Send A08
	 * 2. Send A08 again with same transaction id and the set the received time to that equal to the day back value
	 * 3. Assert the second A08 should be classified as 'Duplicated message'
	 * 
	 */
	@Test
	public void duplicated_msg_equal_to_day_back_value(){
		//Arrange
		Calendar evn2Calendar = Calendar.getInstance();
		String evn2 = DateUtils.format(evn2Calendar.getTime(), DateUtils.FORMAT_TIMESTAMP);
		Calendar firstMsgReceivedDt = Calendar.getInstance();
			firstMsgReceivedDt.add(Calendar.DATE, -1);
		Calendar duplicatedMsgReceivedDt = (Calendar) firstMsgReceivedDt.clone();
			duplicatedMsgReceivedDt.add(Calendar.DATE, Integer.valueOf(duplicationDayback));
//			duplicatedMsgReceivedDt.add(Calendar.MILLISECOND, -1);
		
		//Act
		String a40TxnId1 = sendA40(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay(1);
		String a40TxnId2 = sendA40(evn2, fromPatientKey, toPatientKey);
		
		List<StagingDto> stagings = iasEsbService.findStagingByOrderingIdentifierSortByReceivedDtAsc(evn2Calendar.getTime());
		iasEsbService.setReceivedDt(stagings.get(0).getStagingMessageId(), firstMsgReceivedDt.getTime());
		iasEsbService.setReceivedDt(stagings.get(1).getStagingMessageId(), duplicatedMsgReceivedDt.getTime());
		
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto staging1 = iasEsbService.findStagingByMsgId(stagings.get(0).getStagingMessageId());
		StagingDto staging2 = iasEsbService.findStagingByMsgId(stagings.get(1).getStagingMessageId());
		
		assertEquals(MessageProcessStatus.SUCCESS, staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.DUPLICATE, staging2.getProcessStatus());
	}
	
	/**
	 * 1. Send A08
	 * 2. Send A08 again with same transaction id and the set the received time to that after the day back value
	 * 3. Assert the second A08 should be classified as 'disordered message'
	 * 
	 */
	@Test
	public void duplicated_msg_after_day_back_value(){
		//Arrange
		Calendar evn2Calendar = Calendar.getInstance();
		String evn2 = DateUtils.format(evn2Calendar.getTime(), DateUtils.FORMAT_TIMESTAMP);
		Calendar firstMsgReceivedDt = Calendar.getInstance();
			firstMsgReceivedDt.add(Calendar.DATE, -1);
		Calendar duplicatedMsgReceivedDt = (Calendar) firstMsgReceivedDt.clone();
			duplicatedMsgReceivedDt.add(Calendar.DATE, Integer.valueOf(duplicationDayback));
			duplicatedMsgReceivedDt.add(Calendar.MILLISECOND, 1);
		
		//Act
		String a45TxnId1 = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		TimerUtil.delay(1);
		String a45TxnId2 = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		List<StagingDto> stagings = iasEsbService.findStagingByOrderingIdentifierSortByReceivedDtAsc(evn2Calendar.getTime());
		iasEsbService.setReceivedDt(stagings.get(0).getStagingMessageId(), firstMsgReceivedDt.getTime());
		iasEsbService.setReceivedDt(stagings.get(1).getStagingMessageId(), duplicatedMsgReceivedDt.getTime());
		
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto staging1 = iasEsbService.findStagingByMsgId(stagings.get(0).getStagingMessageId());
		StagingDto staging2 = iasEsbService.findStagingByMsgId(stagings.get(1).getStagingMessageId());
		
		assertEquals(MessageProcessStatus.SUCCESS, staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.OTHERS, staging2.getProcessStatus());
	}

}
