package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * Prerequisite : Please enable PAS services in ES
 * 
 * 1. upload 1 image with wrong hospital / case number
 * 2. Void the study
 * 3. getCidStudyFirstLastImage should return patient not exists
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_VS_EX_09_003 {

	@Autowired
	private IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService internalService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	
	//Prerequisite : Please enable PAS services in ES
	protected void assertResult(String expectedMessage) throws Exception {
		try {
			iasService.getCidStudyFirstLastImage(
					cidData.getPatientDtl().getPatKey(),
					cidData.getVisitDtl().getVisitHosp(),
					cidData.getVisitDtl().getCaseNum(),
					cidData.getStudyDtl().getAccessionNo(),
					cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
					null,
					null,
					SampleData.getUserId(),
					SampleData.getWorkstationId(), 
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e) {
			Assert.assertEquals(e.getStatusCode(), 500);
		}
	}
	
	@Test
	public void getCidStudyFirstLastImage_WrongHospitalFromRR_ShouldPatientNotExist() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getVisitDtl().setVisitHosp("ABC");
		iasService.uploadMetaData(cidData);
		
		internalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
		
		assertResult("Patient info not matched");
	}

	@Test
	public void getCidStudyFirstLastImage_WrongCaseNumberFromRR_ShouldPatientNotExist() throws Exception {

		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getVisitDtl().setCaseNum(SampleData.getCaseNo());
		iasService.uploadMetaData(cidData);

		assertResult("Patient info not matched");
	}
}
