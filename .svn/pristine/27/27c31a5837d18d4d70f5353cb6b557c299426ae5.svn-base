package org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic;

import java.util.Arrays;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;

public abstract class API_EX_07_001 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasSerivce;
	
	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String HOSPITAL_CODE = SampleData.getHospCde();
	protected static final String CASE_NO = SampleData.getCaseNo();
	protected static final String ACCESSION_NO = SampleData.getAccessionNo();
	protected static final String SERIES_NO = SampleData.getSeriesNo();
	protected static final String IMAGE_SEQ_NO = "1";
	protected static final String VERSION_NO = "1";
	protected static final String IMAGE_ID = SampleData.getImageID();
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	protected String fieldName;
	protected String patientKey;
	protected String hospCde;
	protected String caseNo;
	protected String accessionNo;
	protected String seriesNo;
	protected String imageSeqNo;
	protected String versionNo;
	protected String imageId;
	protected String userId;
	protected String workstationId;
	protected String requestSys;

	public API_EX_07_001(String fieldName, String patientKey, String hospCde, String caseNo, 
			String accessionNo, String seriesNo, String imageSeqNo, String versionNo, String imageId,  
			String userId, String workstationId, String requestSys) {
		this.fieldName = fieldName;
		this.patientKey = patientKey;
		this.hospCde = hospCde;
		this.caseNo = caseNo;
		this.accessionNo = accessionNo;
		this.seriesNo = seriesNo;
		this.imageSeqNo = imageSeqNo;
		this.versionNo = versionNo;
		this.imageId = imageId;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	@Parameters
	public static Iterable<Object[]> dataset() {
		
		return Arrays.asList(new Object[][] { 
//				{ "patientKey", 
//						"", HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
//				{ "patientKey", 
//						null, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
//				{ "hospCde", 
//						PATIENT_KEY, "", CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
//				{ "hospCde", 
//						PATIENT_KEY, null, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
//				{ "caseNo", 
//						PATIENT_KEY, HOSPITAL_CODE, "", ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
//				{ "caseNo", 
//						PATIENT_KEY, HOSPITAL_CODE, null, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
//						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "accessionNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, "", SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "accessionNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, null, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "seriesNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, "", IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "seriesNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, null, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "imageSeqNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, "",
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "imageSeqNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, null,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "versionNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						"", IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "versionNo", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						null, IMAGE_ID, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "imageId", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, "", USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "imageId", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, null, USER_ID, WORKSTATION_ID, REQUEST_SYS},
				{ "userId", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, "", WORKSTATION_ID, REQUEST_SYS},
				{ "userId", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, null, WORKSTATION_ID, REQUEST_SYS},
				{ "requestSys", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, ""},
				{ "requestSys", 
						PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, SERIES_NO, IMAGE_SEQ_NO,
						VERSION_NO, IMAGE_ID, USER_ID, WORKSTATION_ID, null},
			});
	}

	@Test
	public void getCidImageThumbnail_MissingParameter_ShouldThrowException() throws Exception {

		try {
			iasSerivce.getCidImageThumbnail(patientKey, hospCde, caseNo, accessionNo, seriesNo, imageSeqNo, 
					versionNo, imageId, userId, workstationId, requestSys);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(this.fieldName));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(this.fieldName));
		}
	}
}
