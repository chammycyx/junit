/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image thumbnail of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_CN_EX_07_001 {

	@Autowired
	protected IasClientService iasService;

	protected String imageID1 = UUID.randomUUID().toString();
	
	protected CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		final String accessionNo = "NOT_EXIST_ACCESSION";
		final String studyId = "NOT_EXIST_STUDY";
		
		cidData = iasService.generateCIDData(SampleData.getAccessionNo(), 1, 3);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyId);
	}
	
	@Test
	public void getCidImageThumbnail_RetrieveNotExistsFromCN_ShouldThrowException() throws Exception {

		final String expectedMessage = "Cannot find the Study";
		
		try {
			iasService.getCidImageThumbnail(cidData, "3", "3", imageID1);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			System.out.println(error.faultString);
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e) {
			Assert.assertEquals(e.getStatusCode(), 500);
		}
		
	}
}
