package org.ha.cid.ias.loadtest;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class is for uploading a set of studies that have continuous accession no, study id and series id for load test
 * studyCount the starting suffix of accession no, series ID and images ID
 * studyTotal total no of studies
 * 
 * e.g. studyCount=0, studyTotal=3
 * The studies to be uploaded are like the following:
 * 1. accessionNo=VH HC31087000000, seriesID=1b9aedf6-e8e6-4e50-b381-series000000, imageID=8e15782e-a187-485f-aebf-images000000
 * 2. accessionNo=VH HC31087000001, seriesID=1b9aedf6-e8e6-4e50-b381-series000001, imageID=8e15782e-a187-485f-aebf-images000001
 * 3. accessionNo=VH HC31087000002, seriesID=1b9aedf6-e8e6-4e50-b381-series000002, imageID=8e15782e-a187-485f-aebf-images000002
 * 
 * @author LSM131
 *
 */
public abstract class LoadTestPreparation {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	String[] imageFiles2 = {	"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG" };
	
	String[] versions = { "1", "2" };
	String[] versions2 = { "1", "2" };
	
	String[] imageHandlings = {	"N", "N" };
	
	String studyID = UUID.randomUUID().toString();
	
	String seriesID = UUID.randomUUID().toString();
	
	String imageID = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID, imageID };
	
	String accNo = SampleData.getAccessionNo();
	
	int studyCount = 0;
	
	int studyTotal = 3;
	
	CIDData cidData = null;
	
	@Test
	public void uploadStudies() throws Exception {
		if(studyTotal<=1000000) {
			System.out.println("========== Batch upload studies start ==========");
			for(; studyCount<studyTotal; studyCount++) {
				accNo = accNo.substring(0,10) + String.format("%06d", studyCount);
				
				seriesID = seriesID.substring(0, 24) + "series" + String.format("%06d", studyCount);
				
				imageID = imageID.substring(0, 24) + "images" + String.format("%06d", studyCount);
				String[] imageIDs = { imageID, imageID };
				
				cidData = iasService.uploadImages(imageFiles, versions, imageIDs, accNo, imageHandlings, studyID, seriesID);
				iasService.uploadMetaData(cidData);
			}
			System.out.println("========== Batch upload studies end ==========");
			System.out.println("accNoPrefix    = " + accNo.substring(0,10));
			System.out.println("seriesIdPrefix = " + seriesID.substring(0, 24) + "series");
			System.out.println("imageIdPrefix  = " + imageID.substring(0,24) + "images");
		}
	}
}
