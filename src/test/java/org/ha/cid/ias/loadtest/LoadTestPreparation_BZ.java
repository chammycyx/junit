/**
 * 
 */
package org.ha.cid.ias.loadtest;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This class is for uploading a set of studies that have continuous accession no, study id and series id for load test
 * studyCount the starting suffix of accession no, series ID and images ID
 * studyTotal total no of studies
 * 
 * e.g. studyCount=0, studyTotal=3
 * The studies to be uploaded are like the following:
 * 1. accessionNo=VH HC31087000000, seriesID=1b9aedf6-e8e6-4e50-b381-series000000, imageID=8e15782e-a187-485f-aebf-images000000
 * 2. accessionNo=VH HC31087000001, seriesID=1b9aedf6-e8e6-4e50-b381-series000001, imageID=8e15782e-a187-485f-aebf-images000001
 * 3. accessionNo=VH HC31087000002, seriesID=1b9aedf6-e8e6-4e50-b381-series000002, imageID=8e15782e-a187-485f-aebf-images000002
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class LoadTestPreparation_BZ extends LoadTestPreparation {
}
