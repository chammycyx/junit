package org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001a;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidStudyImageCount should return 1 (each image has 3 versions)
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_VS_10_001a extends FLOW_VS_10_001a {

	@Autowired
	@Qualifier("iasRrInternalService")
	protected IasInternalService rrInternalService;
	
	@Test
	public void getCidStudyImageCount_UploadImageVoidRetrieveImageCount_ShouldAuditMatch() throws Exception {
		iasService.getCidStudyImageCount(cidData, false);
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME,
				AuditEventDto.RETRIEVE_ES_EVENT_NAME, AuditEventDto.RETRIEVE_RR_EVENT_NAME));
	}
	
	@Test
	public void getCidStudyImageCount_UploadImagePrintedCountRetrieveImageCount_ShouldAuditMatch() throws Exception {
		iasService.getCidStudyImageCount(cidData, true);
		
		// Validate Audit Events
		rrInternalService.setDelay(120);
		List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME,
				AuditEventDto.RETRIEVE_ES_EVENT_NAME, AuditEventDto.RETRIEVE_RR_EVENT_NAME));
	}
}
