/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_001;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_EX_05_001 extends FLOW_EX_05_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Test
	public void getCidImage_RetrieveNotExistsFromCN_ShouldAuditMatch() throws Exception {
		final String auditEventName = "getCidImage";
		final String expectedMessage = "Study does not exist in ES";
		
		try {
			iasService.getCidImage(cidData, "3", "3", imageID1);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception e) {
			// Validate Audit Events
			List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), auditEventName);
			AuditEventDto auditEvent = auditEventDtos.get(0);
			Assert.assertTrue(auditEvent.getStatus().equals(AuditEventDto.STATUS_FAILURE));
			Assert.assertTrue(auditEvent.getResponse().equals(expectedMessage));
		}
		
	}
}
