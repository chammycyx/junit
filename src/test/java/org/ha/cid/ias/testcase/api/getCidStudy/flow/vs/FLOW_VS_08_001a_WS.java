/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. getCidStudy should return HA7 with 1 image (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_08_001a_WS extends FLOW_VS_08_001a {
}
