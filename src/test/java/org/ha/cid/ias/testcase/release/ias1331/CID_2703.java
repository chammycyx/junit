package org.ha.cid.ias.testcase.release.ias1331;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.RemoteFileService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
* This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. upload 1 metadata with escape data in remark
 * 3. check audit log event for escape char
 * 4. check remark in CID data by getCidStudy.
 *  
 * Refer to CID-2703
 * 
 * @author CYC014
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:ws-config-context.xml",
		"classpath:internal-context.xml"
})
public class CID_2703 {
	
	@Autowired
	protected IasClientService iasService;

	@Autowired
	@Qualifier("iasRrInternalService")
	protected IasInternalService rrInternalService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;
	
	@Autowired
	@Qualifier("cnRemoteFileService")
	private RemoteFileService cnRemoteFileService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };

	String[] versions2 = { "1", "3", "4" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	Integer result = null;
	
	String escapeString = "&<>"; 
	String noEscapeString = "!@#$%^*()";
	String accessionNo = "";
	
	@Before
	public void prepare() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
	}
	
	@Test
	public void uploadMetadata_RemoveEscapeCharStudyRetrieveFromRR_ShouldSuccess() throws Exception {
		
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		String remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(escapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		//Check audit logs
		List<AuditEventDto> auditEvents = esInternalService.getAuditEvent(accessionNo,"ITI-41");
		for(AuditEventDto event : auditEvents){
			Assert.assertEquals("Success", event.getStatus());
			Assert.assertTrue(event.getRequest().contains(xmlEscapeText(escapeString)));
		
		}
		
		//skip index.xml
		
		//getStudy from ES
		StudyDto study = esInternalService.getStudy(accessionNo);
		Assert.assertEquals(escapeString, study.getRemark());
		
		//voidStudy from ES
		esInternalService.removeStudy(accessionNo);
		
		//getStudy from RR
		study =  rrInternalService.getStudy(accessionNo);
		Assert.assertEquals(escapeString, study.getRemark());
		
	}
	
	
	@Test
	public void uploadMetadata_RemoveNoEscapeCharStudyRetrieveFromRR_ShouldSuccess() throws Exception {
		
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		String remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(noEscapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		//Check audit logs
		List<AuditEventDto> auditEvents = esInternalService.getAuditEvent(accessionNo,"ITI-41");
		for(AuditEventDto event : auditEvents){
			Assert.assertEquals("Success", event.getStatus());
			Assert.assertTrue(event.getRequest().contains(xmlEscapeText(noEscapeString)));
		}
		
		//skip index.xml
		
		//getStudy from ES
		StudyDto study = esInternalService.getStudy(accessionNo);
		Assert.assertEquals(noEscapeString, study.getRemark());
		
		//voidStudy from ES
		esInternalService.removeStudy(accessionNo);
		
		//getStudy from RR
		study =  rrInternalService.getStudy(accessionNo);
		Assert.assertEquals(noEscapeString, study.getRemark());
		
	}
	
	@Test
	public void uploadMetadata_ReuploadEscapeCharStudyRetrieveFromES_ShouldSuccess() throws Exception {
		//upload metadata
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		String remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(noEscapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		//Check audit logs
		List<AuditEventDto> auditEvents = esInternalService.getAuditEvent(accessionNo, "ITI-41");
		for(AuditEventDto event : auditEvents){
			Assert.assertEquals("Success", event.getStatus());
			Assert.assertTrue(event.getRequest().contains(xmlEscapeText(noEscapeString)));
		
		}
		
		//getCidStudy
		String ha7Result = iasService.getCidStudy(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Assert.assertEquals(noEscapeString, cidDataResult.getStudyDtl().getRemark());
		
		//reupload with escape data
		cidData = iasService.reuploadImages(cidData, imageFiles, versions2, imageIDs, imageHandlings);
		remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(escapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		//Check audit logs
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		auditEvents = esInternalService.getAuditEvent(accessionNo, "ITI-41");
		
		if(auditEvents.size() > 1){
			Collections.sort(auditEvents, new DateComparator());
		}
		
		Assert.assertEquals("Success", auditEvents.get(0).getStatus());
		Assert.assertTrue(auditEvents.get(0).getRequest().contains(xmlEscapeText(escapeString)));

		
		//getCidStudy
		ha7Result = iasService.getCidStudy(cidData);
		cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		
		Assert.assertEquals(escapeString, cidDataResult.getStudyDtl().getRemark());

	}
	
	
	private String xmlEscapeText(String t) {
	   StringBuilder sb = new StringBuilder();
	   for(int i = 0; i < t.length(); i++){
	      char c = t.charAt(i);
	      switch(c){
	      case '<': sb.append("&lt;"); break;
	      case '>': sb.append("&gt;"); break;
	      case '\"': sb.append("&quot;"); break;
	      case '&': sb.append("&amp;"); break;
	      case '\'': sb.append("&apos;"); break;
	      default:
	         if(c>0x7e) {
	            sb.append("&#"+((int)c)+";");
	         }else
	            sb.append(c);
	      }
	   }
	   return sb.toString();
	}
	
	class DateComparator implements Comparator<AuditEventDto> {
	    @Override
	    public int compare(AuditEventDto a, AuditEventDto b) {
	    	long aa = Long.parseLong(a.getDate());
	    	long bb = Long.parseLong(b.getDate());
	        return  aa > bb ? -1 : aa == bb ? 0 : 1; 
	    }
	}
	
	
}
