package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;


/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. uploadMetadata with duplicated sequence number
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_03_004 {
	
	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_DuplicatedSeqNo_ShouldThrowException() throws Exception {

		final String expectedMessage = "Duplicate Metadata Exception!";
		
		String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
		
		String[] versions = { "1", "2", "3" };
		
		String[] imageHandlings = {	"N", "N", "N" };
		
		String imageID1 = UUID.randomUUID().toString();
		
		String[] imageIDs = { imageID1, imageID1, imageID1 };
		
		CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		
		// Add 1 more images to cid data
		CIDData cidData2 = iasService.generateCIDData(SampleData.getAccessionNo(), 1, 3);
		for (ImageDtl imageDtl : cidData2.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl()) {
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}
		
		try {
			iasService.uploadMetaData(cidData);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e) {
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} 

	}
}
