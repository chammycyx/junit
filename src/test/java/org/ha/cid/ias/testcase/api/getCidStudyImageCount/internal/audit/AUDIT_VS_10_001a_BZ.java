/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidStudyImageCount should return 1 (each image has 3 versions)
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_VS_10_001a_BZ extends AUDIT_VS_10_001a {
}
