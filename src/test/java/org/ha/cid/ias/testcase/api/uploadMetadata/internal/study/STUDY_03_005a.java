package org.ha.cid.ias.testcase.api.uploadMetadata.internal.study;

import org.ha.cd2.isg.icw.tool.StudyCompareUtil;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.uploadMetadata.flow.FLOW_03_005a;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with version 1, 3, 4
 * 3. modify and upload the image with version 1, 4, 5
 * 4. validate ES study
 * 5. validate audit log
 * 
 * @author LSM131
 *
 */
public abstract class STUDY_03_005a extends FLOW_03_005a {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImage3VersionEditTwice_ShouldEsStudyMatch() throws Exception {
		// Validate ES Study
		StudyDto rrStudyDto = esInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(rrStudyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(rrStudyDto, cidData);
	}
	
	@Test
	public void uploadMetadata_uploadImage3VersionEditTwice_ShouldRrStudyMatch() throws Exception {
		// Validate RR Study
		StudyDto rrStudyDto = rrInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(rrStudyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(rrStudyDto, cidData);
		
	}
}
