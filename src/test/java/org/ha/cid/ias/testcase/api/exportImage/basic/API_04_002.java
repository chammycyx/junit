package org.ha.cid.ias.testcase.api.exportImage.basic;

import java.util.Arrays;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images with missing optional parameters (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
public abstract class API_04_002 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasService;
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	protected String[] versions = { "1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected CIDData cidData = null;
	
	// Parameterize
	protected static final String PASSWORD = SampleData.getStringWithChar(8);
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	protected String fieldName;
	protected String workstationId;

	public API_04_002(String fieldName, String workstationId) {
		this.fieldName = fieldName;
		this.workstationId = workstationId;
	}
	
	@Parameters
	public static Iterable<Object[]> dataset() {
		
		return Arrays.asList(new Object[][] { 
				{ "workstationId", null },
				{ "workstationId", "" }
			});
	}
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void exportImage_uploadExportByOptionalFields_ShouldSuccess() throws Exception {

		try {
			iasService.exportImage(
					Ha7Util.convertToHa7xml(this.cidData), 
					PASSWORD, 
					USER_ID, 
					workstationId, 
					REQUEST_SYS);
			Assert.assertTrue(true);
			
		} catch (Exception e) {
			Assert.assertTrue("Exception is not expected.", false);
			
			
		}
	}
}
