/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload a BMP image
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_02_005_WS extends API_02_005 {
}
