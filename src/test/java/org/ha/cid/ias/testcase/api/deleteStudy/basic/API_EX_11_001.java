package org.ha.cid.ias.testcase.api.deleteStudy.basic;

import java.util.Arrays;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;


public abstract class API_EX_11_001 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasService;

	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String HOSPITAL_CODE = SampleData.getHospCde();
	protected static final String CASE_NO = SampleData.getCaseNo();
	protected static final String ACCESSION_NO = SampleData.getAccessionNo();
	protected static final String STUDY_ID = SampleData.getStudyId();
	
	protected String fieldName;
	protected String patientKey;
	protected String hospCde;
	protected String caseNo;
	protected String accessionNo;
	protected String studyID;

	public API_EX_11_001(String fieldName, String patientKey, String hospCde, String caseNo, 
			String accessionNo, String studyID) {
		this.fieldName = fieldName;
		this.patientKey = patientKey;
		this.hospCde = hospCde;
		this.caseNo = caseNo;
		this.accessionNo = accessionNo;
		this.studyID = studyID;
	}
	
	@Parameters
	public static Iterable<Object[]> dataset() {
		
		return Arrays.asList(new Object[][] { 
				{ "patientKey", "", HOSPITAL_CODE, CASE_NO, ACCESSION_NO, STUDY_ID },
				{ "patientKey", null, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, STUDY_ID },
				{ "hospCde", PATIENT_KEY, "", CASE_NO, ACCESSION_NO, STUDY_ID },
				{ "hospCde", PATIENT_KEY, null, CASE_NO, ACCESSION_NO, STUDY_ID },
				{ "caseNo", PATIENT_KEY, HOSPITAL_CODE, "", ACCESSION_NO, STUDY_ID },
				{ "caseNo", PATIENT_KEY, HOSPITAL_CODE, null, ACCESSION_NO, STUDY_ID },
				{ "accessionNo", PATIENT_KEY, HOSPITAL_CODE, CASE_NO, "", STUDY_ID },
				{ "accessionNo", PATIENT_KEY, HOSPITAL_CODE, CASE_NO, null, STUDY_ID },
				{ "studyID", PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, "" },
				{ "studyID", PATIENT_KEY, HOSPITAL_CODE, CASE_NO, ACCESSION_NO, null },
			});
	}
	
	@Test
	public void deleteStudy_MissingParameter_ShouldThrowException() throws Exception {
		try {
			iasService.deleteStudy(patientKey, hospCde, caseNo, accessionNo, studyID);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(this.fieldName));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(this.fieldName));
		}
	}

}
