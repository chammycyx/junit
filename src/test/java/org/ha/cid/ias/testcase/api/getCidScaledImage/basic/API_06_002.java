package org.ha.cid.ias.testcase.api.getCidScaledImage.basic;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidScaledImage with optional parameters
 * 
 * @author HSY914
 *
 */
public abstract class API_06_002 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	protected void assertImage(byte[] result) {
		InputStream input = getClass().getResourceAsStream("/blazeds/flow/OGD1_v3_scaled.JPG");
		
		try {
			byte[] expectedImage = IOUtils.toByteArray(input);
			Assert.assertArrayEquals(expectedImage, result);
		} catch (IOException e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(input);
		}
	}
	
	@Test
	public void getCidScaledImage_PatientKeyNull_ShouldSuccess() throws Exception {
		
		final String patientKey = null;
		
		byte[] result = iasService.getCidScaledImage(
				patientKey, 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_PatientKeyWrong_ShouldSuccess() throws Exception {
		
		final String patientKey = "NOT_EXIST_PATIENT";
		
		byte[] result = iasService.getCidScaledImage(
				patientKey, 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_HospCdeNull_ShouldSuccess() throws Exception {
		
		final String hospCde = null;
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				hospCde, 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_HospCdeWrong_ShouldSuccess() throws Exception {
		
		final String hospCde = "ABC";
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				hospCde, 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_CaseNoNull_ShouldSuccess() throws Exception {
		
		final String caseNo = null;
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(),
				caseNo,
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_CaseNoWrong_ShouldSuccess() throws Exception {
		
		final String caseNo = "NOT_EXIST_CASE_NO";
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(),
				caseNo,
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_WorkstationIdNull_ShouldSuccess() throws Exception {
		
		final String workstationId = null;
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				workstationId,
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
	
	@Test
	public void getCidScaledImage_WorkstationIdEmpty_ShouldSuccess() throws Exception {
		
		final String workstationId = "";
		
		byte[] result = iasService.getCidScaledImage(
				cidData.getPatientDtl().getPatKey(),
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(),
				"3", 
				"3", 
				imageID1, 
				500, 
				600, 
				SampleData.getUserId(), 
				workstationId,
				SampleData.getRequestSystem()
		);
		
		assertImage(result);
	}
}
