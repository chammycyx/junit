package org.ha.cid.ias.testcase.release.ias150;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.List;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cd2.isg.icw.tool.StudyCompareUtil;
import org.ha.cid.ias.enumeration.StudyStatusEnum;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. upload 1 metadata
 * 3. delink study
 * 4. validate no of image records in t_image
 * 
 * Refer to CID-2199
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class CID_2199 {
	@Autowired
	private IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService internalService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };

	String[] versions = { "1", "2", "3" };

	String[] imageHandlings = {	"N", "N", "N" };

	String imageID1 = UUID.randomUUID().toString();

	String[] imageIDs = { imageID1, imageID1, imageID1 };

	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}

	@Test
	public void delinkErsRecord_uploadStudyDelinkStudy_ShouldNoDuplicatedImages() throws Exception {
		iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = internalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		
		Assert.assertEquals(imageFiles.length, studyDto.getSeriesDtos().get(0).getImageDtos().size());
	}
	
	@Test
	public void delinkErsRecord_uploadStudyDelinkStudyWithInvalidHospitalCode_ShouldNoDuplicatedImages() throws Exception {
		cidData.getVisitDtl().setVisitHosp("INVALID_HOSP");
		iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = internalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		
		Assert.assertEquals(imageFiles.length, studyDto.getSeriesDtos().get(0).getImageDtos().size());
	}
	
	@Test
	public void delinkErsRecord_uploadStudyDelinkStudyWithInvalidPatKey_ShouldNoDuplicatedImages() throws Exception {
		cidData.getPatientDtl().setPatKey("INVALID_PAT_KEY");
		iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = internalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		
		Assert.assertEquals(imageFiles.length, studyDto.getSeriesDtos().get(0).getImageDtos().size());
	}
	
	@Test
	public void delinkErsRecord_uploadStudyDelinkStudyWithInvalidCaseNo_ShouldNoDuplicatedImages() throws Exception {
		cidData.getVisitDtl().setCaseNum("INVALID_CASE_NO");
		iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = internalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		
		Assert.assertEquals(imageFiles.length, studyDto.getSeriesDtos().get(0).getImageDtos().size());
	}
	
	@Test
	public void delinkErsRecord_uploadStudyDelinkStudyWithInvalidStudyId_ShouldNoDuplicatedImages() throws Exception {
		cidData.getStudyDtl().setStudyID("INVALID_STUDY_ID");
		iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = internalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		
		Assert.assertEquals(imageFiles.length, studyDto.getSeriesDtos().get(0).getImageDtos().size());
	}
}
