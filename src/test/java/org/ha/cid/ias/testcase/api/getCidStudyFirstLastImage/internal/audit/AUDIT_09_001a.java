package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001a;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. getCidStudyFirstLastImage should return HA7 with 1 image (each image has 3 versions)
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_09_001a extends FLOW_09_001a {

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService internalService;

	@Test
	public void getCidStudyFirstLastImage_UploadImageRetrieve1stLast_ShouldAuditMatch() throws Exception {
		iasService.getCidStudyFirstLastImage(cidData);
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = internalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.RETRIEVE_ES_EVENT_NAME));
	}
}
