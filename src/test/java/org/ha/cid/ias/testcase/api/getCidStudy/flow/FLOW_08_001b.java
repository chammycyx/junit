package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import java.util.Iterator;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;


/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. getCidStudy should return HA7 with 1 image (each image has 2 versions)
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_08_001b {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	
	String[] versions = { "1", "2" };
	
	String[] imageHandlings = {	"N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType("ES"); // RTC
		iasService.uploadMetaData(cidData);
		
	}
	
	@Test
	public void getCidStudy_Upload1Image_ShouldReturnHA7() throws Exception {
		String ha7Result = iasService.getCidStudy(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}
		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
	}
}
