package org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_001;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidImage using seq 3 and ver 3 should return the image of seq 3 and ver 3
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_CN_06_001 extends FLOW_CN_06_001 {

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;

	@Test
	public void getCidScaledImage_UploadImageVoidRetrieveScaled_ShouldAuditMatch() throws Exception {
		for (int i = 0; i < imageFiles.length; i++) {
			iasService.getCidScaledImage(cidData, String.valueOf(i+1), versions[i], imageIDs[i], 500, 600);
		}
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME,
				AuditEventDto.RETRIEVE_ES_EVENT_NAME, AuditEventDto.RETRIEVE_RR_EVENT_NAME));
	}
}
