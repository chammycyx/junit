/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.flow.cn;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Export image of a image which does not exist from CN
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_CN_EX_04_001_WS extends FLOW_CN_EX_04_001 {
}
