/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. getCidStudy should return HA7 with 1 image (each image has 2 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_08_001b_RS extends FLOW_08_001b {
}
