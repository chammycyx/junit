/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidScaledImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. modify and upload the image with updated versions (v1, v(N-1), v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 3. getCidScaledImage with version v12 should return HA7 with 1 image (version v12)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_06_006_BZ extends FLOW_06_006 {
}
