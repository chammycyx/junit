/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image thumbnail of a image which does not exist
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_CN_EX_07_002_WS extends AUDIT_CN_EX_07_002 {
}
