package org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic;

import org.apache.http.client.HttpResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_EX_07_002_RS extends API_EX_07_002 {
	
	@Test
	public void getCidImageThumbnail_NoParameter_ShouldThrowException() throws Exception {
		try{
			iasService.getCidImageThumbnail(null, null, null, null);
			Assert.assertTrue("Exception is expected.", false);
		}catch (HttpResponseException e){
			//Assert.assertEquals(e.getStatusCode(), 500);
			Assert.assertTrue(e.getMessage().contains("The parameter accessionNo is mandatory in the getCidImageThumbnail transaction"));
		}
		
	}
	
}
