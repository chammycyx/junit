/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_001;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. getCidStudyImageCount of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_EX_10_001 extends FLOW_EX_10_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Test
	public void getCidStudyImageCount_RetrieveNotExists_AuditShouldMatch() throws Exception {

		final String expectedMessage = "Cannot find the Study";
		final String auditEventName = "getCidStudyImageCount";
		
		try {
			iasService.getCidStudyImageCount(cidData, false);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception e) {
			// Validate Audit Events
			List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), auditEventName);
			AuditEventDto auditEventDto = auditEventDtos.get(0);
			Assert.assertTrue(auditEventDto.getStatus().equals(AuditEventDto.STATUS_FAILURE));
			Assert.assertTrue(auditEventDto.getResponse().equals(expectedMessage));
			Assert.assertTrue(auditEventDto.getException().contains(expectedMessage));
		}
	}
}
