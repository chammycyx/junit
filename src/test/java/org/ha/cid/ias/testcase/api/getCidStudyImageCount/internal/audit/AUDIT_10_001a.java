package org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001a;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. getCidStudyImageCount should return 1 (each image has 3 versions)
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_10_001a extends FLOW_10_001a {

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService internalService;

	@Test
	public void getCidStudyImageCount_UploadImageRetrieveImageCount_ShouldAuditMatch() throws Exception {
		iasService.getCidStudyImageCount(cidData, false);
		
		// Validate Audit Events
		List<AuditEventDto> auditEventDtos = internalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.RETRIEVE_ES_EVENT_NAME));
	}
	
	@Test
	public void getCidStudyImageCount_UploadImagePrintedCountRetrieveImageCount_ShouldAuditMatch() throws Exception {
		iasService.getCidStudyImageCount(cidData, true);
		
		// Validate Audit Events
		internalService.setDelay(120);
		List<AuditEventDto> auditEventDtos = internalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.RETRIEVE_ES_EVENT_NAME));
	}
}
