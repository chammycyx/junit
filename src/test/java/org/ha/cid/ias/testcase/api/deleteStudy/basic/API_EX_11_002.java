package org.ha.cid.ias.testcase.api.deleteStudy.basic;

import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. deleteStudy with wrong parameter
 * 
 * @author CFT545
 *
 */
public abstract class API_EX_11_002 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void deleteStudy_PatientKeyWrong_ShouldFail() throws Exception {

		final String patientKey = "NOT_EXIST_PATIENT";
		
		String result = iasService.deleteStudy(
				patientKey, 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getStudyID());
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm("");
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
	
	@Test
	public void deleteStudy_HospitalWrong_ShouldFail() throws Exception {

		final String hospCde = "NOT_EXIST_HOSPITAL";
		
		String result = iasService.deleteStudy(
				cidData.getPatientDtl().getPatKey(), 
				hospCde, 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getStudyID());
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getMessageDtl().setServerHosp(hospCde);
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm("");
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
	
	@Test
	public void deleteStudy_CaseNumWrong_ShouldFail() throws Exception {

		final String caseNum = "NOT_EXIST_CASE_NUM";
		
		String result = iasService.deleteStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(),
				caseNum,
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getStudyID());
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm("");
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
	
	@Test
	public void deleteStudy_AccessionNoWrong_ShouldFail() throws Exception {
		
		final String accessionNo = "NOT_EXIST_ACCESSION_NO";
		
		String result = iasService.deleteStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				accessionNo,
				cidData.getStudyDtl().getStudyID());
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(accessionNo);
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm("");
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
	}
	
	@Test
	public void deleteStudy_StudyIdWrong_ShouldFail() throws Exception {
		
		final String studyID = "NOT_EXIST_STUDY_ID";
		
		String result = iasService.deleteStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(),
				cidData.getStudyDtl().getAccessionNo(),
				studyID);
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(studyID);
		expected.getAckDtl().setStudyDtm("");
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
	}
	
}
