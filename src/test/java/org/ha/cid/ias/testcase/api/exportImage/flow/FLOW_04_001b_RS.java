/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 2 versions)
 * 2. export all images (each image has 2 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_04_001b_RS extends FLOW_04_001b {
}
