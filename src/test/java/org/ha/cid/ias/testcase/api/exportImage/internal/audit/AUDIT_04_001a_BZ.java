/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml",
})
public class AUDIT_04_001a_BZ extends AUDIT_04_001a{
}
