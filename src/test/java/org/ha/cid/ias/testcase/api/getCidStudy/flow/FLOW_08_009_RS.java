/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 2 images (each image has 2 version)
 * 2. add 1 image and rearrange and upload the images
 * 3. getCidStudy should return HA7 (each image has 2 versions)
 * 
 * @author CFT545
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_08_009_RS extends FLOW_08_009 {
}
