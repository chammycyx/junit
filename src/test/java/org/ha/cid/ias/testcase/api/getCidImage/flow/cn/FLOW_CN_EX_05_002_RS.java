/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.flow.cn;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image of a image which does not exist from RR
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_CN_EX_05_002_RS extends FLOW_CN_EX_05_002 {
}
