/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. upload 1 metadata
 * 3. delete the study
 * 4. upload 1 metadata again
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_03_008 {

	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_StudyDeleted_ShouldThrowException() throws Exception {

		final String expectedMessage = "Found study was deleted";
		
		String[] imageFiles = {	"/blazeds/flow/OGD1_v1.PNG", "/blazeds/flow/OGD1_v2.PNG", "/blazeds/flow/OGD1_v3.PNG" };
		
		String[] imageFiles2 = { "/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v3.JPG", "/blazeds/flow/OGD1_v4.JPG" };
		
		String[] versions = { 	"1", "2", "3" };

		String[] versions2 = {	"1", "3", "4" };
		
		String[] imageHandlings = {	"N", "N", "N" };
		
		String imageID1 = UUID.randomUUID().toString();
		
		String[] imageIDs = { imageID1, imageID1, imageID1 };
		
		CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		CIDData cidData2 =iasService.reuploadImages(cidData, imageFiles2, versions2, imageIDs, imageHandlings);
		try {
			iasService.uploadMetaData(cidData2);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e) {
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} 
		
	}
}
