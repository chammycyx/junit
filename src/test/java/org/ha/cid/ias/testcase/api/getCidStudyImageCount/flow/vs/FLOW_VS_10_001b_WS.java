/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. remove the study from ES
 * 3. getCidStudyImageCount should return 1 (each image has 2 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_10_001b_WS extends FLOW_VS_10_001b {
}
