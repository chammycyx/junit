package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Calendar;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.enumeration.EsbMessageType;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.EsbPatientDemographics;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;


/**
 * This test case intends to validate the following items:
 * 1. A08 esb msg successfully send to ES
 * 2. A08 esb msg successfully send to RR
 * 3. Patient demo successfully update to ES DB
 * 4. Patient demo successfully update to RR DB
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_001 extends FLOW_ESB_13_BASE{
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo;
	private String toPatientKey = "80050200";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("false");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("false");
		iasEsbService.setCutoffTime("0");
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatient(accessionNo, toPatientKey);
		iasRrInternalService.updateStudyWithPatient(accessionNo, toPatientKey);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(toPatientKey);
//		StudyDto esStudy = iasEsInternalService.getRefreshedStudy(accessionNo);
//		iasClientService.deleteStudy(esStudy.getPatKey(), esStudy.getVisitHosp(), esStudy.getCaseNo(), accessionNo, esStudy.getStudyId());
		iasEsInternalService.removeStudy(accessionNo);
	}

	/**
	 * 1. Send A08 ESB
	 * 2. Assert status 'A'
	 * 3. Assert status 'I'/'C' as staging being/got processed
	 * 4. Assert two subscription added as esb message send to ES and RR 
	 * 5. Assert Patient/Study info updated in ES
	 * 6. Assert Patient/Study info updated in RR
	 * @throws Exception 
	 */
	@Test
	public void send_A08() throws Exception {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A08);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		//Assert
		TimerUtil.delay(2);//wait for web service of message receiver to turn ESB msg to staging and store to DB 
		long numStagingWithStatusA = iasEsbService.findNumStagingWithStatus(Arrays.asList(MessageProcessStatus.INITIAL));
		assertEquals(1, numStagingWithStatusA);
		
		iasEsbService.enableStagingPoller("true");//Enable poller
		TimerUtil.delay(5);//wait for staging poller to pick staging from DB and process it
		StagingDto staging = iasEsbService.findStaging(esbMessageHeader.getTransactionId());
		assertEquals(true, Arrays.asList(MessageProcessStatus.PROCESSING,MessageProcessStatus.SUCCESS).contains(staging.getProcessStatus()));
		assertEquals(2, (staging.getSubscriptions()==null)?0:staging.getSubscriptions().size());
		
		TimerUtil.delay(10);//wait for ES and RR to update patient/study info from IAS-ESB notification
		StudyDto esStudyDto = iasEsInternalService.getRefreshedStudy(accessionNo);
		StudyDto rrStudyDto = iasRrInternalService.getRefreshedStudy(accessionNo);
		
		assertEquals(toPatient.getPatientKey(), esStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", esStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), esStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), esStudyDto.getPatName());
		assertEquals(toPatient.getSex(), esStudyDto.getPatSex());
		assertEquals(toPatient.getPatientKey(), rrStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", rrStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), rrStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), rrStudyDto.getPatName());
		assertEquals(toPatient.getSex(), rrStudyDto.getPatSex());
	}

}
