/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidImageThumbnail using seq 3 and ver 3 should return the image thumbnail of seq 3 and ver 3
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_07_001_RS extends AUDIT_07_001 {
}
