package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow;

import java.util.Iterator;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with updated versions (v1, v(N-1), v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 4. getCidStudyFirstLastImage should return HA7 with 1 image (v1, v11, v12) (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_09_005a {

	@Autowired
	protected IasClientService iasService;
	
	final int uploadCount = 10;
	final int imageCount = 1;
	final int vesionCount = 3;
	String templateFile = "/blazeds/flow/OGD1.JPG";
	String tempFile = "/blazeds/flow/OGD1_temp1.JPG";
	
	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.repeatUploadImages(templateFile, tempFile, 
				SampleData.getAccessionNo(), imageCount, vesionCount, uploadCount, "HADRW");
	}
	
	@Test
	public void getCidStudyFirstLastImage_UploadEditNTimes_ShouldSuccess() throws Exception {
		String ha7Result = iasService.getCidStudyFirstLastImage(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}
		
		iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			if("11".equals(imageDtl.getImageVersion())) {
				iterator.remove();
			}
		}
		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
	}
}
