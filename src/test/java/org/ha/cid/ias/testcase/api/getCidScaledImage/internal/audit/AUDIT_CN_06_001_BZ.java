/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidImage using seq 3 and ver 3 should return the image of seq 3 and ver 3
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_CN_06_001_BZ extends AUDIT_CN_06_001 {
}
