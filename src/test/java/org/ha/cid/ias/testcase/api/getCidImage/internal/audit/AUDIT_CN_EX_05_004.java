/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_004;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete all images
 * 3. Get cid image
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_CN_EX_05_004 extends FLOW_CN_EX_05_004 {

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;

	@Test
	public void getCidImage_RetrieveDeletedAllImageFromCN_ShouldAuditMatch() throws Exception {

		final String auditEventName = "getCidImageInRR";
		final String expectedMessage = "Study is mark-deleted in RR";
		
		try {
			iasService.getCidImage(cidData, "3", "3", imageID1);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception e) {
			// Validate Audit Events
			List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
			for (AuditEventDto auditEventDto: auditEventDtos) {
				if (auditEventName.equals(auditEventDto.getEvent())) {
					Assert.assertTrue(auditEventDto.getStatus().equals(AuditEventDto.STATUS_FAILURE));
					Assert.assertTrue(auditEventDto.getResponse().equals(expectedMessage));
					Assert.assertTrue(auditEventDto.getException().contains(expectedMessage));
				}
			}
		}
		
	}
}
