package org.ha.cid.ias.testcase.data;

import icw.wsdl.xjc.pojo.CIDData.PatientDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class SampleData { 
    // 4 set of patient 
    static String[] PatSex = {"M", "F", "F", "M"};
    static String[] PatDOB = {"19460107000000.000", "19760131000000.000", "19800528000000.000", "19891128000000.000"};
    static String[] PATNAME = {"TSANG, AH OI", "PAT, A TEST", "PAT, B TEST", "PAT, C TEST"}; 
    
    static String[] ALPHABET = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    static Random generator = new Random();
    public static final String[] HOSPCDE = {"VH"};
    public static final String[] EXAMTYPE = {"EX21","EX31","EX22","EX32","EX23","EX33","EX24","EX34","EX25","EX35"};
	public static final String SAMPLE_CDATA = "<![CDATA["
		+"<sample>"
		  +"<normal>qwertyuioopasdfgghjklzxcvbnm</normal>"
		  +"<special>`1234567890-=~!@#$%^()_+{}[]:\";'?,./>|\\&*\\<</special>"
			+"<chin>中文字</chin>"
		+"</sample>"
		+"]]>"; 
    
    public static String getTransactionDtm(){
    	Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
        String nowStr = sdf.format(now);
        
    	return nowStr;
    }
    
    public static String getStudyDtm(){
    	Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String nowStr = sdf.format(now);
    	return nowStr;
    }
    
    public static String getExamDtm() {
    	Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String nowStr = sdf.format(now);
    	return nowStr;
	}
    
    public static String getImageFile(){
    	return "TEST_IMAGE_"+getCaseNo()+".jpg";
    }
    public static String getImageID(){
    	return UUID.randomUUID().toString().toUpperCase();
    }

    public static PatientDtl getPatientDtl(){
        PatientDtl dtl = new PatientDtl();
        int index = generator.nextInt(PATNAME.length - 1);
        dtl.setHKID(getHkid());
        dtl.setPatDOB(PatDOB[index]);
        dtl.setPatKey(getPatientKey());
        dtl.setPatName(PATNAME[index]);
        dtl.setPatSex(PatSex[index]);
        dtl.setDeathIndicator("");
        return dtl;
    }
    
    /**
     * get a new patient object with patientKey with 9 digit
     * */
    public static PatientDtl getSpecialPatientDtl(){
        PatientDtl dtl = new PatientDtl();
        int index = generator.nextInt(PATNAME.length - 1);
        dtl.setHKID(getHkid());
        dtl.setPatDOB(PatDOB[index]);
        dtl.setPatKey(getRandomPatientKey());
        dtl.setPatName(PATNAME[index]);
        dtl.setPatSex(PatSex[index]);
        dtl.setDeathIndicator("");
        return dtl;
    }
    
    /**
     * Accession Number format: HOSPCDE + <2ALPHABET> +  <10 DIGIT> + <1ALPHABET>
     * Example: VH XR1234567890A
     * @return Accession number
     */
    public static String getAccessionNo(){
        String hospCde = HOSPCDE[0];
        
        if(hospCde.length() < 3){
            hospCde = hospCde + " "; 
        } 
        return hospCde + getChar() + getChar() + numberSet(10) + getChar();
    }
    
    /**
     * Case Number format: <2ALPHABET> +  <8 DIGIT> + <1ALPHABET>
     * Example: HN05000207X
     * @return Accession number
     */
    public static String getCaseNo(){
        return getChar() + getChar() + numberSet(8) + getChar(); 
    }
    
    public static String getHospCde() {
    	String hospCde = HOSPCDE[0];
        
        return hospCde;
    }
    
    public static String getPatName(){
        return PATNAME[generator.nextInt(PATNAME.length - 1)];
    }
    
    // 8 digit
    public static String getPatientKey(){
        return numberSet(8);
    }
    
    /*
     * patientKey must not match with valid patient key
     * */
    public static String getRandomPatientKey(){
    	return numberSet(9);
    }
    
    public static String getHkid(){
        return getChar() + numberSet(7);
    }
    
    public static String getChar(){
        return ALPHABET[generator.nextInt(ALPHABET.length - 1)];
    }
    
    public static String getStudyId(){
        return UUID.randomUUID().toString().toUpperCase();
    }
    
    public static String getSeriesNo(){
        return UUID.randomUUID().toString().toUpperCase();
    }
    
    public static String getTransactionID(){
        return UUID.randomUUID().toString().toUpperCase();
    }
    
    public static String getHospCdeByAccNo(String accNo){
    	
    	return accNo.substring(0, 3).trim();
    	
    }
    
    public static String getStringWithChar(Integer n){
    	
    	StringBuffer sb = new StringBuffer();
    	
    	for (int i = 0; i < n ; i++){
    		sb.append("A");
    	}
    	
    	return sb.toString();
    }
    
    public static String getFormattedImageFilename(ImageDtl dtl, int imgVersion){
    	String seq1_2 = dtl.getImageSequence().toString();
		return seq1_2+"."+dtl.getImageID()+"_v"+imgVersion+".JPEG";
    }
    
    static String numberSet(int size) {  
        if (size > 10) {  
            return null;  
        }  
        int[] myNumbers = new int[10];  
        String result = "";  
        int index;  
        for (int i = 0; i < myNumbers.length; i++) {  
            myNumbers[i] = i;  
        }  
        for (int i = 0; i < size; i++) {  
            index = (int) Math.floor(Math.random() * 10);  
            if (myNumbers[index] >= 0) {  
                result = result + myNumbers[index] + "";  
                myNumbers[index] = -1;  
            } else {  
                i--;  
            }  
        }  
        return result;  
    }

	public static String getImageFormat() {

		return "IM";
	}

	public static String getHash() {
		
		return String.valueOf("abc".hashCode());
	}
	
	public static String getUserId() {
		return "TEST_USER";
	}
	
	public static String getWorkstationId() {
		return "TEST_WORKSTATION_ID";
	}
	
	public static String getRequestSystem() {
		return "TEST_REQUEST_SYS";
	}

    public static void main(String[] args)
    {
//    	System.out.println(getChar());
//    	System.out.println("6 dig:" + numberSet(6));
//    	System.out.println("7 dig:" + numberSet(7));
//    	System.out.println("10 dig:" + numberSet(10));
//    	System.out.println("12 dig:" + numberSet(12));
    	
//        System.out.println("getPatName            " + getPatName());
//        System.out.println("getPatientKey         " + getPatientKey());
//        System.out.println("getHkid               " + getHkid());
//        System.out.println("getAccessionNo        " + getAccessionNo());
//        System.out.println("getStudyId            " + getStudyId());
//        System.out.println("getCaseNo             " + getCaseNo());
//        System.out.println("getSeriesNo           " + getSeriesNo());
//        System.out.println("getTransactionID      " + getTransactionID());
//        
//        String acc = getAccessionNo();
//        System.out.println("getAccessionNo(orig)  " + acc);
//        System.out.println("getHospCdeByAccNo     " + getHospCdeByAccNo(acc));
//        
//        PatientDtl patientDtl = getPatientDtl();
//        System.out.println("getPatientDtl getHKID     " + patientDtl.getHKID());
//        System.out.println("getPatientDtl getPatDOB   " + patientDtl.getPatDOB());
//        System.out.println("getPatientDtl getPatName  " + patientDtl.getPatName());
//        System.out.println("getPatientDtl getPatKey   " + patientDtl.getPatKey());
//        System.out.println("getPatientDtl getPatSex   " + patientDtl.getPatSex());
    	
    	// get number of char 
    	String aaa = getStringWithChar(100);
    	System.out.println("100 A : " + aaa);
    	System.out.println("length : " + aaa.length());
    }
  
}


