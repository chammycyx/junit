package org.ha.cid.ias.testcase.api.getCidStudy.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;


@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class API_EX_08_001_BZ extends API_EX_08_001 {

	public API_EX_08_001_BZ(String fieldName, String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, String userId, String workstationId,
			String requestSys) {
		super(fieldName, patientKey, hospCde, caseNo, accessionNo, seriesNo, imageSeqNo, versionNo, userId, workstationId,
				requestSys);
	}
}
