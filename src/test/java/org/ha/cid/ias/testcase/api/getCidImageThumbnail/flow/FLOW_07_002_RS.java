/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 PNG image (each image has 3 versions)
 * 2. getCidImageThumbnail for all images
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_07_002_RS extends FLOW_07_002 {
}
