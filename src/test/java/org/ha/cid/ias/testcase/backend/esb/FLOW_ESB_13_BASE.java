package org.ha.cid.ias.testcase.backend.esb;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.Calendar;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.enumeration.EsbMessageType;
import org.ha.cid.ias.model.EsbEpisode;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.EsbPatientDemographics;
import org.ha.cid.ias.model.PatientDto;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class FLOW_ESB_13_BASE {
	private String[] imageFiles = {	"/blazeds/flow/300KB.JPG", "/blazeds/flow/300KB.JPG" };
	
	private String[] versions = { "1", "2" };
	
	private String imageID1 = UUID.randomUUID().toString();
	
	private String[] imageIDs = { imageID1, imageID1 };
	
	private String[] imageHandlings = {	"N", "N" };

	protected static final Integer CUTOFF_TIME_IN_SECOND = 60;
	
	protected static final Integer PROCESS_TIME_IN_SECOND = 10;
	
	protected void setup(){
		iasEsbService.enableNullOrderingIdentifierStagingPolling("false");
	}
	
	@Autowired
	protected IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasRrInternalService")
	protected IasInternalService iasRrInternalService;
	
	@Autowired
	protected IasClientService iasClientService;
	
	protected PatientDto createPatient(String patientKey) throws Exception{
		iasRrInternalService.removePatient(patientKey);
		PatientDto patientDto = new PatientDto();
			patientDto.setPatKey(patientKey);
		iasRrInternalService.createPatient(patientDto);
		return patientDto;
	}
	
	protected String createStudy() throws Exception{
		CIDData cidData = iasClientService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		Integer result = iasClientService.uploadMetaData(cidData);
		if(result.intValue()!=0){throw new Exception("Study created unsuccessfully");}
		return cidData.getStudyDtl().getAccessionNo();
	}
	
	protected String sendA08(String evn2, String toPatientKey){
		//Arrange
		Calendar now = Calendar.getInstance();
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A08);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TXN_ID" + evn2);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		return esbMessageHeader.getTransactionId();
	}
	
	protected String sendA40(String evn2, String fromPatientKey, String toPatientKey) {
		//Arrange
		Calendar now = Calendar.getInstance();
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A40);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TXN_ID" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		return esbMessageHeader.getTransactionId();
	}
	
	protected String sendA45(String evn2, String fromPatientKey, String toPatientKey, String hospCode, String caseNum) {
		//Arrange
		Calendar now = Calendar.getInstance();
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A45);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TXN_ID" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		EsbEpisode esbEpisode = new EsbEpisode();
			esbEpisode.setCaseNumber(caseNum);
			esbEpisode.setHospitalCode(hospCode);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, esbEpisode);
		
		return esbMessageHeader.getTransactionId();
	}
	
	protected String sendA47(String evn2, String fromPatientKey,String toPatientKey) {
		//Arrange
		Calendar now = Calendar.getInstance();
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A47);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TXN_ID" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		return esbMessageHeader.getTransactionId();
	}
	
	
}
