package org.ha.cid.ias.testcase.api.getCidImage.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;


@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_EX_05_001_WS extends API_EX_05_001 {

	public API_EX_05_001_WS(String fieldName, String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, String imageId, String userId, String workstationId,
			String requestSys) {
		super(fieldName, patientKey, hospCde, caseNo, accessionNo, seriesNo, imageSeqNo, versionNo, imageId, userId,
				workstationId, requestSys);
	}
}
