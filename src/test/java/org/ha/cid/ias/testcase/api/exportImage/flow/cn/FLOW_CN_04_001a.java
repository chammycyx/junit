package org.ha.cid.ias.testcase.api.exportImage.flow.cn;

import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. export all images (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_CN_04_001a {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;

	@Autowired
	protected ResourceService resourceService;

	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	protected String[] versions = { "1", "2", "3"};
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String exportPath = "/blazeds/flow/exportImage.7z";
	
	protected String password = "password";
	
	protected CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		esInternalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
	}
	
	@Test
	public void exportImage_UploadExportFromCN_ShouldSuccess() throws Exception {
		
		byte[] result = iasService.exportImage(Ha7Util.convertToHa7xml(cidData), password);

		ImageDtls imageDtls = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls();
		
		for(int i = 0; i < imageFiles.length; i++) {
			byte[] resultImage = resourceService.extractZipFile(exportPath, result, imageDtls.getImageDtl().get(i).getImageFile(), password);
			byte[] expectedImage = null;
			try {
				expectedImage = IOUtils.toByteArray(getClass().getResourceAsStream(imageFiles[i]));
			} catch (IOException e) {
				Assert.assertTrue(false);
				e.printStackTrace();
			}
			Assert.assertArrayEquals(expectedImage, resultImage);
		}
		
	}
}
