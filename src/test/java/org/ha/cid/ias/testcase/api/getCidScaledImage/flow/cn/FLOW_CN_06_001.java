package org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidImage using seq 3 and ver 3 should return the image of seq 3 and ver 3
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_CN_06_001 {
	
	@Autowired
	protected IasClientService iasService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;
	
	@Autowired	
	protected ResourceService resourceService;
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };

	protected String[] scaledImageFiles = {	"/blazeds/flow/OGD1_v1_scaled.JPG", "/blazeds/flow/OGD1_v2_scaled.JPG", "/blazeds/flow/OGD1_v3_scaled.JPG" };
	
	protected String[] versions = { "1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		esInternalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
	}
	
	@Test
	public void getCidScaledImage_UploadRetrieveFromCN_ShouldSuccess() throws Exception {
		
		for (int i = 0; i < imageFiles.length; i++) {

			byte[] result = iasService.getCidScaledImage(cidData, String.valueOf(i+1), versions[i], imageIDs[i], 500, 600);

			byte[] expected = resourceService.loadFileAsByteArray(scaledImageFiles[i]);
			
			Assert.assertArrayEquals(expected, result);
			
		}
		
	}
}
