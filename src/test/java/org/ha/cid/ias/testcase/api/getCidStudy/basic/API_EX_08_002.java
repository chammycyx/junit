package org.ha.cid.ias.testcase.api.getCidStudy.basic;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

public abstract class API_EX_08_002 {

	@Autowired
	protected IasClientService iasService;
	
	protected static final String IMAGE_SEQ_NO = "1";
	protected static final String VERSION_NO = "1";
	protected static final Integer WIDTH = 100;
	protected static final Integer HEIGHT = 100;
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	protected String patientKey = null;
	protected String accessionNo = null; 
	protected String seriesNo = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		patientKey = cidData.getPatientDtl().getPatKey();
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
	}
	
	protected void assertResult(String expectedMessage) throws Exception {
		try {
			iasService.getCidStudy(
					patientKey,
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					accessionNo, 
					seriesNo,
					null,
					null,
					SampleData.getUserId(), 
					SampleData.getWorkstationId(), 
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
			
			
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			System.out.println("Returned result: " + e.getMessage());
			System.out.println("Expected result: " + expectedMessage);
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidStudy_PatientKeyWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "Patient info not matched";
		patientKey = "NOT_EXIST_PATIENT";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidStudy_AccessionNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "Cannot find the Study";
		accessionNo = "NOT_EXIST_ACCESSION_NO";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidStudy_SeriesNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "No Records in the Axon Edge Server";
		seriesNo = "NOT_EXIST_SERIES_NO";
		
		assertResult(expectedMessage);
		
	}

}
