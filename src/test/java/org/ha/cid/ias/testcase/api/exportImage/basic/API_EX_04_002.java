package org.ha.cid.ias.testcase.api.exportImage.basic;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;


public abstract class API_EX_04_002 extends AbstractSpringTestCase {

	@Autowired
	protected IasClientService iasService;
	
	protected static final String HA7_FILE = "/blazeds/imageretrivalds/HA7.xml";
	protected static final String PASSWORD = SampleData.getStringWithChar(8);
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	@Test
	public void exportImage_Ha7MessageInvalid_ShouldThrowException() throws Exception {

		final String expectedMessage = "The incoming HA7 Message is incorrect, cann't process";
		try {
			iasService.exportImage(
					"<abc>Invalid XML</abc>", 
					PASSWORD, 
					USER_ID, 
					WORKSTATION_ID, 
					REQUEST_SYS
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
}
