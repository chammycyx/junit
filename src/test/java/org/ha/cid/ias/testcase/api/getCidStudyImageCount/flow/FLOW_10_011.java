package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. update image handling, rearrange and upload metadata
 * 3. getCidStudyImageCount should return 3 (each image has 2 versions)
 * 
 * @author HSY914
 * 
 */
public abstract class FLOW_10_011 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG" 
	};
	
	String[] versions = {
			"1", "2", 
			"1", "2", 
			"1", "2"
	};
	
	String[] versions2 = {
			"1", "3", 
			"1", "3", 
			"1", "3"
	};
	
	String[] imageHandlings = {
			"N", "N",
			"N", "N",
			"N", "N"
	};

	String[] imageHandlings2 = {
			"N", "N",
			"N", "Y",
			"N", "Y"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	
	String[] sequences = {
			"1", "2",
			"3", "4",
			"5", "6"
	};
	
	String[] sequences2 = {
			"3", "4",
			"5", "6",
			"1", "2"
	};
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {

		cidData = iasService.uploadImages(
				imageFiles,
				versions,
				imageIDs, 
				SampleData.getAccessionNo(),
				imageHandlings,
				sequences
				);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions2,
				imageIDs,
				imageHandlings2,
				sequences2
				);
		iasService.uploadMetaData(cidData);

	}
	
	@Test
	public void getCidStudyImageCount_UpdateHandlingRearrangeImage_ShouldReturn3() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, false);
		
		Assert.assertEquals(3, imageCount);
	}

	@Test
	public void getCidStudyImageCount_UpdateHandlingRearrangeImagePrintedCount_ShouldReturn2() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, true);
		
		Assert.assertEquals(2, imageCount);
	}
}


