package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.math.BigInteger;
import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 version)
 * 2. update annotation and upload metadata
 * 
 * @author HSY914
 * 
 */
public abstract class FLOW_03_015 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] versions2 = { "1", "3", "4" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	@Test
	public void uploadMetadata_AddAnnotation_ShouldSuccess() throws Exception {
		
		CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		
		AnnotationDtls annotationDtls = new AnnotationDtls();
		AnnotationDtl annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("0"));
		annotationDtl.setAnnotationType("ImageCreateTimeStamp");
		annotationDtl.setAnnotationText("20150101123456.00");
		annotationDtl.setAnnotationCoordinate("0^0~0^0");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("N");
		annotationDtl.setAnnotationUpdDtm("");
		annotationDtls.getAnnotationDtl().add(annotationDtl);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(1).setAnnotationDtls(annotationDtls);
		
		iasService.uploadMetaData(cidData);
		
		cidData = iasService.reuploadImages(cidData, imageFiles, versions2, imageIDs, imageHandlings);

		annotationDtls = new AnnotationDtls();
		annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("0"));
		annotationDtl.setAnnotationType("ImageCreateTimeStamp");
		annotationDtl.setAnnotationText("20160210123456.00");
		annotationDtl.setAnnotationCoordinate("0^0~0^0");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("N");
		annotationDtl.setAnnotationUpdDtm("");
		annotationDtls.getAnnotationDtl().add(annotationDtl);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(1).setAnnotationDtls(annotationDtls);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}
}
