package org.ha.cid.ias.testcase.api.uploadMetadata.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;

/**
 * This test case intends to validate the following items:
 * 1. Mandatory parameters (null + empty) check
 * 
 * @author cft545
 *
 */
@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_EX_03_001_WS extends API_EX_03_001 {
	
	public API_EX_03_001_WS(String fieldName, String ha7File, String userId, String workstationId, String requestSys) {
		super(fieldName, ha7File, userId, workstationId, requestSys);
	}
}
