package org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.uploadMetadata.flow.FLOW_03_005a;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with version 1, 3, 4
 * 3. modify and upload the image with version 1, 4, 5
 * 4. validate ES study
 * 5. validate audit log
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_03_005a extends FLOW_03_005a {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService internalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImage3VersionEditTwice_ShouldEsAuditMatch() throws Exception {
		
		// Validate ES Audit Events
		List<AuditEventDto> esAuditEventDtos = internalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(esAuditEventDtos, AuditEventDto.UPLOAD_ES_EVENT_NAME);
	}

	@Test
	public void uploadMetadata_uploadImage3VersionEditTwice_ShouldRrAuditMatch() throws Exception {
	
		// Validate RR Audit Events
		rrInternalService.setDelay(120);
		List<AuditEventDto> rrAuditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(rrAuditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME));
	}
}
