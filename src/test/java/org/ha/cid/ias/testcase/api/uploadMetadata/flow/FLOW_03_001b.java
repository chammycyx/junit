package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. upload 1 metadata
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_03_001b {
	
	@Autowired
	protected IasClientService iasService;
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	
	protected String[] versions = { "1", "2" };
	
	protected String[] imageHandlings = {	"N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1 };
	
	protected CIDData cidData = null;
	
	protected Integer result = null;
	
	@Before
	public void prepare() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType("ES"); // RTC
		result = iasService.uploadMetaData(cidData);
		
	}
	
	@Test
	public void uploadMetadata_Upload1Image_ShouldReturnZero() throws Exception {
		Assert.assertEquals(0, result.intValue());
	}
}
