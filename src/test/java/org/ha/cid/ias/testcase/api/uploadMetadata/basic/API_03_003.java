package org.ha.cid.ias.testcase.api.uploadMetadata.basic;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

public abstract class API_03_003 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
	}
	
	@Test
	// TODO: Should throw exception
	public void uploadMetadata_PatHKIDMissing_ShouldSuccess() throws Exception {
		cidData.getPatientDtl().setHKID(null);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}
	
	@Test
	// TODO: Should throw exception
	public void uploadMetadata_PatNameMissing_ShouldSuccess() throws Exception {
		cidData.getPatientDtl().setPatName(null);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}
	
	@Test
	// TODO: Should throw exception
	public void uploadMetadata_PatDOBMissing_ShouldSuccess() throws Exception {
		cidData.getPatientDtl().setPatDOB(null);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}
	
	@Test
	// TODO: Should throw exception
	public void uploadMetadata_PatSexMissing_ShouldSuccess() throws Exception {
		cidData.getPatientDtl().setPatSex(null);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}
	
	@Test
	public void uploadMetadata_DeathIndicatorMissing_ShouldSuccess() throws Exception {
		cidData.getPatientDtl().setDeathIndicator(null);
		
		Integer result = iasService.uploadMetaData(cidData);

		Assert.assertEquals(0, result.intValue());
	}

}
