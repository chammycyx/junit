/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidScaledImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. Delete 1 images
 * 3. Get cid scaled image
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_EX_06_005_BZ extends FLOW_EX_06_005 {
}
