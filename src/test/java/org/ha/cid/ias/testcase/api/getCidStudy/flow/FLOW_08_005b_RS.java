/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. modify and upload the image with updated versions (v1, v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 4. getCidStudy should return HA7 with 1 image (v1, v11) (each image has 2 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_08_005b_RS extends FLOW_08_005b {
}
