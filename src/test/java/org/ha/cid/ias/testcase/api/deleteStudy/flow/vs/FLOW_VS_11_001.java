/**
 * 
 */
package org.ha.cid.ias.testcase.api.deleteStudy.flow.vs;

import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. remove the study from ES
 * 3. delete the study
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_VS_11_001 {

	@Autowired
	private IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService internalService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.PNG", "/blazeds/flow/OGD1_v2.PNG", "/blazeds/flow/OGD1_v3.PNG" };
	
	String[] versions = { 	"1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		internalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
	}
	
	@Test
	public void deleteStudy_UploadDeleteFromRR_ShouldSuccess() throws Exception {
		
		String result = iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm(cidData.getStudyDtl().getStudyDtm());
		expected.getAckDtl().setActionStatus("Success");
		expected.getAckDtl().setActionRemarks("1 record(s) deleted");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
}
