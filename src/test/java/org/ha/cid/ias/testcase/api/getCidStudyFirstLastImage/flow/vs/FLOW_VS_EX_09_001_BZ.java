/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete the study
 * 3. Void the study
 * 4. Get cid first last image
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_EX_09_001_BZ extends FLOW_VS_EX_09_001 {
}
