package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 version)
 * 2. delete annotation and upload metadata
 * 3. getCidStudyFirstLastImage should return HA7 (each image has 3 versions)
 * 
 * @author CFT545
 * 
 */
public abstract class FLOW_09_015 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };

	String[] versions2 = { "1", "3", "4" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		
		AnnotationDtls annotationDtls = new AnnotationDtls();
		AnnotationDtl annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("0"));
		annotationDtl.setAnnotationType("ImageCreateTimeStamp");
		annotationDtl.setAnnotationText("20150101123456.00");
		annotationDtl.setAnnotationCoordinate("0^0~0^0");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("N");
		annotationDtl.setAnnotationUpdDtm("");
		annotationDtls.getAnnotationDtl().add(annotationDtl);

		annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("1"));
		annotationDtl.setAnnotationType("MARKER");
		annotationDtl.setAnnotationText("ONE");
		annotationDtl.setAnnotationCoordinate("100.15^17.55~150.15^67.55");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("Y");
		annotationDtl.setAnnotationUpdDtm("");
		annotationDtls.getAnnotationDtl().add(annotationDtl);
		
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(1).setAnnotationDtls(annotationDtls);
		
		iasService.uploadMetaData(cidData);
		
		cidData = iasService.reuploadImages(cidData, imageFiles, versions2, imageIDs, imageHandlings);

		annotationDtls = new AnnotationDtls();
		annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("0"));
		annotationDtl.setAnnotationType("ImageCreateTimeStamp");
		annotationDtl.setAnnotationText("20150101123456.00");
		annotationDtl.setAnnotationCoordinate("0^0~0^0");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("N");
		annotationDtl.setAnnotationUpdDtm("");
		annotationDtls.getAnnotationDtl().add(annotationDtl);
		
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(1).setAnnotationDtls(annotationDtls);
		
		iasService.uploadMetaData(cidData);

	}
	
	@Test
	public void getCidStudyFirstLastImage_DeleteAnnotation_ShouldSuccess() throws Exception {
		String ha7Result = iasService.getCidStudyFirstLastImage(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}

		iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			if("3".equals(imageDtl.getImageVersion())) {
				iterator.remove();
			}
		}
		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
	}
}
