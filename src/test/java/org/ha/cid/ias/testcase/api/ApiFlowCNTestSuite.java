package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001a_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001a_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001a_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001b_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001b_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001b_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_002_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_002_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_001_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_001_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_001_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_002_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_002_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_003_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_003_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_003_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_004_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_004_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_EX_04_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_003_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_004_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_005_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_006_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_003_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_004_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_004_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_005_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_005_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_006_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_001_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_002_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_002_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_002_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_002_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_003_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_003_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_003_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_004_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_004_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_004_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_005_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_005_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_EX_06_005_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	
	// Blaze DS
	FLOW_CN_04_001a_BZ.class,
	FLOW_CN_04_001b_BZ.class,
	FLOW_CN_04_002_BZ.class,
	FLOW_CN_EX_04_001_BZ.class,
	FLOW_CN_EX_04_002_BZ.class,
	FLOW_CN_EX_04_003_BZ.class,
	FLOW_CN_EX_04_004_BZ.class,
	FLOW_CN_05_001_BZ.class,
	FLOW_CN_05_002_BZ.class,
	FLOW_CN_EX_05_001_BZ.class,
	FLOW_CN_EX_05_002_BZ.class,
	FLOW_CN_EX_05_003_BZ.class,
	FLOW_CN_EX_05_004_BZ.class,
	FLOW_CN_EX_05_005_BZ.class,
	FLOW_CN_EX_05_006_BZ.class,
	FLOW_CN_07_001_BZ.class,
	FLOW_CN_07_002_BZ.class,
	FLOW_CN_EX_07_001_BZ.class,
	FLOW_CN_EX_07_002_BZ.class,
	FLOW_CN_EX_07_003_BZ.class,
	FLOW_CN_EX_07_004_BZ.class,
	FLOW_CN_EX_07_005_BZ.class,
	FLOW_CN_EX_07_006_BZ.class,
	FLOW_CN_06_001_BZ.class,
	FLOW_CN_06_002_BZ.class,
	FLOW_CN_EX_06_002_BZ.class,
	FLOW_CN_EX_06_003_BZ.class,
	FLOW_CN_EX_06_004_BZ.class,
	FLOW_CN_EX_06_005_BZ.class,
	
	// Web Service
	FLOW_CN_04_001a_WS.class,
	FLOW_CN_04_001b_WS.class,
	FLOW_CN_04_002_WS.class,
	FLOW_CN_EX_04_001_WS.class,
	FLOW_CN_EX_04_002_WS.class,
	FLOW_CN_EX_04_003_WS.class,
	FLOW_CN_EX_04_004_WS.class,
	FLOW_CN_05_001_WS.class,
	FLOW_CN_05_002_WS.class,
	FLOW_CN_EX_05_001_WS.class,
	FLOW_CN_EX_05_002_WS.class,
	FLOW_CN_EX_05_003_WS.class,
	FLOW_CN_EX_05_004_WS.class,
	FLOW_CN_EX_05_005_WS.class,
	FLOW_CN_EX_05_006_WS.class,
	FLOW_CN_07_001_WS.class,
	FLOW_CN_07_002_WS.class,
	FLOW_CN_EX_07_001_WS.class,
	FLOW_CN_EX_07_002_WS.class,
	FLOW_CN_EX_07_003_WS.class,
	FLOW_CN_EX_07_004_WS.class,
	FLOW_CN_EX_07_005_WS.class,
	FLOW_CN_EX_07_006_WS.class,
	FLOW_CN_06_001_WS.class,
	FLOW_CN_06_002_WS.class,
	FLOW_CN_EX_06_002_WS.class,
	FLOW_CN_EX_06_003_WS.class,
	FLOW_CN_EX_06_004_WS.class,
	FLOW_CN_EX_06_005_WS.class,
		
	
	//RS
	FLOW_CN_04_001a_RS.class,
	FLOW_CN_04_001b_RS.class,
	FLOW_CN_04_002_RS.class,
	FLOW_CN_EX_04_001_RS.class,
	FLOW_CN_EX_04_002_RS.class,
	FLOW_CN_EX_04_003_RS.class,
	FLOW_CN_EX_04_004_RS.class,
	FLOW_CN_05_001_RS.class,
	FLOW_CN_05_002_RS.class,
	FLOW_CN_EX_05_001_RS.class,
	FLOW_CN_EX_05_002_RS.class,
	FLOW_CN_EX_05_003_RS.class,
	FLOW_CN_EX_05_004_RS.class,
	FLOW_CN_EX_05_005_RS.class,
	FLOW_CN_EX_05_006_RS.class,
	FLOW_CN_07_001_RS.class,
	FLOW_CN_07_002_RS.class,
	FLOW_CN_EX_07_001_RS.class,
	FLOW_CN_EX_07_002_RS.class,
	FLOW_CN_EX_07_003_RS.class,
	FLOW_CN_EX_07_004_RS.class,
	FLOW_CN_EX_07_005_RS.class,
	FLOW_CN_EX_07_006_RS.class,
	FLOW_CN_06_001_RS.class,
	FLOW_CN_06_002_RS.class,
	FLOW_CN_EX_06_002_RS.class,
	FLOW_CN_EX_06_003_RS.class,
	FLOW_CN_EX_06_004_RS.class,
	FLOW_CN_EX_06_005_RS.class,
	

})
public class ApiFlowCNTestSuite {

}
