package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. modify and upload the image with updated versions (v1, v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_03_006b {

	@Autowired
	protected IasClientService iasService;
	
	final int uploadCount = 10;
	final int imageCount = 1;
	final int vesionCount = 2;
	String templateFile = "/blazeds/flow/OGD1.JPG";
	String tempFile = "/blazeds/flow/OGD1_temp1.JPG";
	
	CIDData cidData = null;
	
	@Test
	public void uploadMetadata_UploadEditNTimes_ShouldSuccess() throws Exception {
		try {
			cidData = iasService.repeatUploadImages(templateFile, tempFile, 
					SampleData.getAccessionNo(), imageCount, vesionCount, uploadCount, "ES");
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected", false);
		}
		
	}
}
