package org.ha.cid.ias.testcase.release.ias150;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.model.ImageDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. getCidStudy should return HA7 with 1 image (each image has 3 versions)
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class CID_2856 {
	private static final Logger LOG = LoggerFactory.getLogger(CID_2856.class);
	
	@Autowired
	private IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;
	
	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };

	String[] versions = { "1", "2", "3" };

	String imageID1 = UUID.randomUUID().toString();

	String[] imageIDs = { imageID1, imageID1, imageID1 };

	CIDData cidData = null;
	
	int delay = 0;

	@Test
	public void getCidStudy_UploadImageEmptyImageHandlingNFormat_ShouldValueMatch() throws Exception {
		String[] imageHandlings = {	"", "", "" };
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		Iterator<ImageDtl> iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImageFormat("");
		}
		iasService.uploadMetaData(cidData);
		
		checkDBImageHandlingNFormat(cidData, null, null);
		
		checkRetrievedImageHandlingNFormat(cidData, "", "");
	}
	
	@Test
	public void getCidStudy_UploadImageEmptyImageHandling_ShouldValueMatch() throws Exception {
		String[] imageHandlings = {	"", "", "" };
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		checkDBImageHandlingNFormat(cidData, null, "IM");
		
		checkRetrievedImageHandlingNFormat(cidData, "", "IM");
	}
	
	@Test
	public void getCidStudy_UploadImageEmptyImageFormat_ShouldValueMatch() throws Exception {
		String[] imageHandlings = {	"N", "N", "N" };
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		Iterator<ImageDtl> iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImageFormat("");
		}
		iasService.uploadMetaData(cidData);
		
		checkDBImageHandlingNFormat(cidData, "N", null);
		
		checkRetrievedImageHandlingNFormat(cidData, "N", "");
	}
	
	@Test
	public void getCidStudy_UploadImageNotEmptyImageHandlingNFormat_ShouldValueMatch() throws Exception {
		String[] imageHandlings = {	"N", "N", "N" };
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		checkDBImageHandlingNFormat(cidData, "N", "IM");
		
		checkRetrievedImageHandlingNFormat(cidData, "N", "IM");
	}
	
	private void verifyImageHandlingNFormat(String ha7Result, String handlingValue, String formatValue) throws Exception {
		
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while (iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			Assert.assertEquals(handlingValue, imageDtl.getImageHandling());
			Assert.assertEquals(formatValue, imageDtl.getImageFormat());
		}
	}
	
	private void checkDBImageHandlingNFormat(CIDData cidData, String handlingValue, String formatValue) throws Exception {
		StudyDto studyDto = null;
		
		studyDto = esInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		verifyImageHandlingNFormat(studyDto, handlingValue, formatValue);
		
		studyDto = rrInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		verifyImageHandlingNFormat(studyDto, handlingValue, formatValue);
	}
	
	private void checkRetrievedImageHandlingNFormat(CIDData cidData, String handlingValue, String formatValue) throws Exception {
		String ha7Result = null;
		
		ha7Result = iasService.getCidStudy(cidData);
		verifyImageHandlingNFormat(ha7Result, handlingValue, formatValue);
		
		delay();
		esInternalService.setDelay(120);
		esInternalService.removeStudy(cidData.getStudyDtl().getAccessionNo());

		ha7Result = iasService.getCidStudy(cidData);
		verifyImageHandlingNFormat(ha7Result, handlingValue, formatValue);
		
		delay();
	}
	
	private void verifyImageHandlingNFormat(StudyDto studyDto, String handlingValue, String formatValue) throws Exception {
		
		List<ImageDto> imageDtoList = studyDto.getSeriesDtos().get(0).getImageDtos();
		
		Iterator<ImageDto> iterator = imageDtoList.iterator();
		while (iterator.hasNext()) {
			ImageDto imageDto = iterator.next();
			Assert.assertEquals(handlingValue, imageDto.getImageHandling());
			Assert.assertEquals(formatValue, imageDto.getImageFormat());
		}
	}
	
	private void delay() {
		if (delay > 0) {
			try {
				LOG.info("Start delay for {} seconds...", delay);
				TimeUnit.SECONDS.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
