package org.ha.cid.ias.testcase.api.getCidScaledImage.basic;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;

public abstract class API_EX_06_002 {

	@Autowired
	private IasClientService iasService;
	
	protected static final String IMAGE_SEQ_NO = "1";
	protected static final String VERSION_NO = "1";
	protected static final Integer WIDTH = 100;
	protected static final Integer HEIGHT = 100;
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	protected String accessionNo = null; 
	protected String seriesNo = null;
	protected String imageSeqNo = null; 
	protected String versionNo = null; 
	protected String imageId = null;
	protected int width = 0;
	protected int height = 0;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		imageSeqNo = "1";
		versionNo = "1";
		imageId = imageID1;
		width = 100;
		height = 100;
	}
	
	protected void assertResult(String expectedMessage) throws Exception {
		try {
			iasService.getCidScaledImage(
					cidData.getPatientDtl().getPatKey(),
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					accessionNo, 
					seriesNo, 
					imageSeqNo, 
					versionNo, 
					imageId,
					width, 
					height, 
					SampleData.getUserId(), 
					SampleData.getWorkstationId(), 
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			System.out.println("Returned : " + e.getMessage());
			System.out.println("Expected : " + expectedMessage);
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidScaledImage_AccessionNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "Study does not exist in RR";
		accessionNo = "NOT_EXIST_ACCESSION_NO";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_SeriesNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "NullPointerException";
		seriesNo = "NOT_EXIST_SERIES_NO";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_ImageSeqNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "No image existing";
		imageSeqNo = "0";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_VersionNoWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "No image existing";
		versionNo = "0";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_ImageIdWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "No image existing";
		imageId = "NOT_EXIST_IMAGE_ID";
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_WidthWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "cannot be <= 0";
		width = -100;
		
		assertResult(expectedMessage);
		
	}
	
	@Test
	public void getCidScaledImage_HeightWrong_ShouldThrowException() throws Exception {
		
		String expectedMessage = "cannot be <= 0";
		height = -100;
		
		assertResult(expectedMessage);
		
	}

}
