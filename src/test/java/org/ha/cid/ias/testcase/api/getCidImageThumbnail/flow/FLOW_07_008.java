package org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 10 images (each image has 3 versions)
 * 2. modify and upload the image with updated versions (v1, v(N-1), v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 3. getCidImageThumbnail with version v12 should return HA7 with 10 images (version v12)
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_07_008 {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	protected ResourceService resourceService;
	
	@Test
	public void getCidImage_Upload3ImagesEditNTimes_ShouldSuccess() throws Exception {
		
		final int uploadCount = 10;
		final int imageCount = 10;
		final int vesionCount = 3;
		final String version = "12";
		
		String templateFile = "/blazeds/flow/OGD1.JPG";
		String tempFile = "/blazeds/flow/OGD1_temp1.JPG";

		CIDData cidData = iasService.repeatUploadImages(templateFile, tempFile, 
				SampleData.getAccessionNo(), imageCount, vesionCount, uploadCount, "EX_3_VERSIONS");

		for(int i = 0; i < imageCount; i++) {
			byte[] result = iasService.getCidImageThumbnail(cidData, String.valueOf((i+1)*3), version, cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get((i+1)*3-1).getImageID());
			
			byte[] expected = resourceService.loadFileAsByteArray("/blazeds/flow/OGD1_thumbnail" + String.valueOf(i+1) + "_v12.JPG");
			
			Assert.assertArrayEquals(expected, result);
		}
	}
}
