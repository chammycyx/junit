package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.enumeration.EsbMessageType;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.EsbPatientDemographics;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends to validate the following items:
 * 1. A47 esb msg successfully send to ES
 * 2. A47 esb msg successfully send to RR
 * 3. Patient demo successfully update to ES DB
 * 4. Patient demo successfully update to RR DB
 * 5. Study successfully moved from one patient to another patient in ES
 * 6. Study successfully moved from one patient to another patient in RR
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_004 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("false");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("false");
		iasEsbService.setCutoffTime("0");
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create patients for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatient(accessionNo, fromPatientKey);
		iasRrInternalService.updateStudyWithPatient(accessionNo, fromPatientKey);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
//		StudyDto esStudy = iasEsInternalService.getRefreshedStudy(accessionNo);
//		iasClientService.deleteStudy(esStudy.getPatKey(), esStudy.getVisitHosp(), esStudy.getCaseNo(), accessionNo, esStudy.getStudyId());
		iasEsInternalService.removeStudy(accessionNo);
	}

	@Test
	public void send_A47_diff_patieny_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A47);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		//Assert
		TimerUtil.delay(2);//wait for web service of message receiver to turn ESB msg to staging and store to DB
		long numStagingWithStatusA = iasEsbService.findNumStagingWithStatus(Arrays.asList(MessageProcessStatus.INITIAL));
		assertEquals(1, numStagingWithStatusA);
		
		//Enable poller
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay(5);//wait for staging poller to pick staging from DB and process it
		StagingDto staging = iasEsbService.findStaging(esbMessageHeader.getTransactionId());
		assertEquals(true, Arrays.asList(MessageProcessStatus.PROCESSING,MessageProcessStatus.SUCCESS).contains(staging.getProcessStatus()));
		assertEquals(2, (staging.getSubscriptions()==null)?0:staging.getSubscriptions().size());
		
		TimerUtil.delay(10);//wait for ES and RR to update patient/study info from IAS-ESB notification
		StudyDto esStudyDto = iasEsInternalService.getRefreshedStudy(accessionNo);
		StudyDto rrStudyDto = iasRrInternalService.getRefreshedStudy(accessionNo);
		
		assertEquals(toPatient.getPatientKey(), esStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", esStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), esStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), esStudyDto.getPatName());
		assertEquals(toPatient.getSex(), esStudyDto.getPatSex());
		assertEquals(toPatient.getPatientKey(), rrStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", rrStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), rrStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), rrStudyDto.getPatName());
		assertEquals(toPatient.getSex(), rrStudyDto.getPatSex());
	}
	
	@Test
	public void send_A47_same_patieny_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A47);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid("X0123456X");
			toPatient.setPatientKey(fromPatient.getPatientKey());
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
		//Assert
		TimerUtil.delay(2);//wait for web service of message receiver to turn ESB msg to staging and store to DB 
		long numStagingWithStatusA = iasEsbService.findNumStagingWithStatus(Arrays.asList(MessageProcessStatus.INITIAL));
		assertEquals(1, numStagingWithStatusA);
		
		iasEsbService.enableStagingPoller("true");//Enable poller
		TimerUtil.delay(5);//wait for staging poller to pick staging from DB and process it
		StagingDto staging = iasEsbService.findStaging(esbMessageHeader.getTransactionId());
		assertEquals(true, Arrays.asList(MessageProcessStatus.PROCESSING,MessageProcessStatus.SUCCESS).contains(staging.getProcessStatus()));
		assertEquals(2, (staging.getSubscriptions()==null)?0:staging.getSubscriptions().size());
		
		TimerUtil.delay(10);//wait for ES and RR to update patient/study info from IAS-ESB notification
		StudyDto esStudyDto = iasEsInternalService.getRefreshedStudy(accessionNo);
		StudyDto rrStudyDto = iasRrInternalService.getRefreshedStudy(accessionNo);
		
		assertEquals(toPatient.getPatientKey(), esStudyDto.getPatKey());
		assertEquals(fromPatient.getPatientKey(), esStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", esStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), esStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), esStudyDto.getPatName());
		assertEquals(toPatient.getSex(), esStudyDto.getPatSex());
		assertEquals(toPatient.getPatientKey(), rrStudyDto.getPatKey());
		assertEquals(toPatient.getDob()+"000000.000", rrStudyDto.getPatDob());
		assertEquals(toPatient.getHkid(), rrStudyDto.getPatHkid());
		assertEquals(toPatient.getPatientName(), rrStudyDto.getPatName());
		assertEquals(toPatient.getSex(), rrStudyDto.getPatSex());
	}

}
