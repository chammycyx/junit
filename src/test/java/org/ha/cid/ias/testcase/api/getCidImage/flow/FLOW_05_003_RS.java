/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 GIF image (each image has 3 versions)
 * 2. getCidImage for all images
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_05_003_RS extends FLOW_05_003 {
}
