package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.basic;

import org.apache.http.client.HttpResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_EX_09_002_RS extends API_EX_09_002 {
	@Test
	public void getCidStudyFirstLastImage_NoParameter_ShouldThrowException() throws Exception {
		try{
			iasService.getCidStudyFirstLastImage(null);
			Assert.assertTrue("Exception is expected.", false);
		}catch (HttpResponseException e){
			Assert.assertEquals(e.getStatusCode(), 500);
			//Assert.assertTrue(e.getMessage().contains("The parameter patientKey is mandatory in the getCidStudyFirstLastImage transaction"));
		}
		
	}
	
}
