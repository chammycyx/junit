package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. rearrange and delete and upload the images
 * 3. getCidStudyImageCount should return 2 (each image has 2 versions)
 * 
 * @author CFT545
 * 
 */
public abstract class FLOW_10_007 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG" 
	};
	
	String[] versions = {
			"1", "2", 
			"1", "2", 
			"1", "2"
	};

	String[] versions2 = {
			"1", "3", 
			"1", "3", 
			"1", "2"
	};
	
	String[] imageHandlings = {
			"N", "N",
			"N", "N",
			"N", "N"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	
	String[] sequences = {
			"1", "2",
			"3", "4",
			"5", "6"
	};
	
	String[] sequences2 = {
			"3", "4",
			"5", "6",
			"1", "2"
	};
	
	String[] status2 = {
			"1", "1",
			"1", "1",
			"0", "0"
	};
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {

		cidData = iasService.uploadImages(
				imageFiles,
				versions,
				imageIDs, 
				SampleData.getAccessionNo(),
				imageHandlings,
				sequences
				);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions2,
				imageIDs,
				imageHandlings,
				sequences2,
				status2
				);
		iasService.uploadMetaData(cidData);

	}
	
	@Test
	public void getCidStudyImageCount_RearrangeDeleteImage_ShouldReturn2() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, false);
		
		Assert.assertEquals(2, imageCount);
	}

	@Test
	public void getCidStudyImageCount_RearrangeDeleteImagePrintedCount_ShouldReturn0() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, true);
		
		Assert.assertEquals(0, imageCount);
	}
}


