package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 2 versions)
 * 2. add 2 new images
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_03_007b {

	@Autowired
	protected IasClientService iasService;
	
	@Autowired
	protected ResourceService resourceService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG"
	};

	String[] imageFiles2 = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v3.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v3.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};
	
	String[] versions = {	
			"1", "2" 
	};
	String[] versions2 = {	
			"1", "2",
			"1", "2",
			"1", "2"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs1 = {	
			imageID1, imageID1 
	};
	String[] imageIDs2 = {	
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	

	String[] imageHandlings1 = {	
			"N", "N" 
	};
	String[] imageHandlings2 = {	
			"N", "N",  
			"N", "N", 
			"N", "N" 
	};
	
	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs1, SampleData.getAccessionNo(), imageHandlings1);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void uploadMetadata_Add2Images_ShouldSuccess() throws Exception {
		try {
			cidData = iasService.reuploadImages(cidData, imageFiles2, versions2, imageIDs2, imageHandlings2);
			Integer result = iasService.uploadMetaData(cidData);
			Assert.assertEquals(0, result.intValue());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected", false);
		}
		
	}
}
