/**
 * 
 */
package org.ha.cid.ias.testcase.api.common;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_01_001_WS extends API_01_001 {
	
	
	@Autowired
	Wss4jSecurityInterceptor securityInterceptor;
	
	@Value("${ws.userName}")
	String username;
	@Value("${ws.password}")
	String password;
	
	@Before
	public void prepareTest() throws Exception {
		
		changeCredentials("cid2user", "abcd3###");
	}
	
	@After
	public void afterTest() throws Exception {
		
		changeCredentials(username, password);
		
	}
	
	private void changeCredentials(String username, String password){
		securityInterceptor.setSecurementUsername(username);	
		securityInterceptor.setSecurementPassword(password);
	}
}
