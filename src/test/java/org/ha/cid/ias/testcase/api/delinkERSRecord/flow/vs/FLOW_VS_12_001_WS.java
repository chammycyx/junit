/**
 * 
 */
package org.ha.cid.ias.testcase.api.delinkERSRecord.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. Delete the study
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_12_001_WS extends FLOW_VS_12_001 {
}
