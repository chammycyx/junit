/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. add 2 new images
 * 3. getCidStudyImageCount should return 3 (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_10_005a_RS extends FLOW_10_005a {
}
