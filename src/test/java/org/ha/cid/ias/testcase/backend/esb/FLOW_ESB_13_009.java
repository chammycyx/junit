package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends focus on 'process staging one by one', the following items would be validated:
 * 1. No staging polled if there is an staging being processing(status 'I')
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_009 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo;
	private String toPatientKey = "80050200";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("false");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("false");
		iasEsbService.setCutoffTime("0");
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatient(accessionNo, toPatientKey);
		iasRrInternalService.updateStudyWithPatient(accessionNo, toPatientKey);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(toPatientKey);
//		StudyDto esStudy = iasEsInternalService.getRefreshedStudy(accessionNo);
//		iasClientService.deleteStudy(esStudy.getPatKey(), esStudy.getVisitHosp(), esStudy.getCaseNo(), accessionNo, esStudy.getStudyId());
		iasEsInternalService.removeStudy(accessionNo);
	}

	/**
	 * Test staging get processed one by one
	 * 
	 * Steps
	 * 1. Stop polling
	 * 2. Send the first A08, set the status to 'I'
	 * 3. Send the second A08, leave the status to 'A'
	 * 4. Resume polling
	 * 5. The second A08 would not get polled as the first one is in progress('I')
	 * 6. Set the status of first A08 to 'E'
	 * 7. Assert the second A08 will get polled and processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void process_staging_one_by_one() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		Date evn2TxnId1 = new Date(now.getTime().getTime());
		String a08TxnId1 = sendA08(DateUtils.format(evn2TxnId1, DateUtils.FORMAT_TIMESTAMP), toPatientKey);
		now.add(Calendar.MILLISECOND, 1);
		Date evn2TxnId2 = new Date(now.getTime().getTime());
		String a08TxnId2 = sendA08(DateUtils.format(evn2TxnId2, DateUtils.FORMAT_TIMESTAMP), toPatientKey);
		iasEsbService.setStagingStatus(evn2TxnId1, MessageProcessStatus.PROCESSING);
		
		//Act
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay(5);//wait for staging poller to trigger polling
		
		//Assert
		StagingDto a08Staging1 = iasEsbService.findStagingByOrderingIdentifier(evn2TxnId1);
		StagingDto a08Staging2 = iasEsbService.findStagingByOrderingIdentifier(evn2TxnId2);
		
		assertEquals(MessageProcessStatus.PROCESSING, a08Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.INITIAL, a08Staging2.getProcessStatus());
		
		//Change the first staging to E, the second staging would then starting get polled
		iasEsbService.setStagingStatus(evn2TxnId1, MessageProcessStatus.OTHERS);
		TimerUtil.delay(5);//wait for staging poller to trigger polling and process staging
		
		StagingDto a08Staging4 = iasEsbService.findStagingByOrderingIdentifier(evn2TxnId2);
		
		assertEquals(true, Arrays.asList(MessageProcessStatus.PROCESSING,MessageProcessStatus.SUCCESS).contains(a08Staging4.getProcessStatus()));
	}

}
