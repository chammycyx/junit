/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.basic;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. CID study image count with wrong parameter
 * 
 * @author CFT545
 *
 */
public abstract class API_EX_10_002 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};
	
	String[] versions = {	
			"1", "2", "3",
			"1", "2", "3",
			"1", "2", "3"
	};
	
	String[] imageHandlings = {	
			"N", "N", "N",
			"N", "N", "N",
			"N", "N", "N"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	
			imageID1, imageID1, imageID1,
			imageID2, imageID2, imageID2,
			imageID3, imageID3, imageID3
	};
	
	CIDData cidData = null;
	protected String patientKey = null;
	protected String hospCde = null;
	protected String caseNo = null;
	protected String accessionNo = null; 
	protected String seriesNo = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		patientKey = cidData.getPatientDtl().getPatKey();
		hospCde = cidData.getVisitDtl().getVisitHosp();
		caseNo = cidData.getVisitDtl().getCaseNum();
		accessionNo = cidData.getStudyDtl().getAccessionNo();
		seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
	}
	
	protected void assertResult(String expectedMessage) throws Exception {
		try {
			iasService.getCidStudyImageCount(
					patientKey,
					hospCde, 
					caseNo, 
					accessionNo, 
					seriesNo,
					null,
					null,
					false,
					SampleData.getUserId(), 
					SampleData.getWorkstationId(), 
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			System.out.println("Returned result: " + e.getMessage());
			System.out.println("Expected result: " + expectedMessage);
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
//	@Test
//	public void getCidStudyImageCount_PatientKeyWrong_ShouldThrowException() throws Exception {
//		String expectedMessage = "Patient info not matched";
//		patientKey = "NOT_EXIST_PATIENT";
//		
//		assertResult(expectedMessage);
//	}
//	
//	@Test
//	public void getCidStudyImageCount_HospCdeWrong_ShouldThrowException() throws Exception {
//		String expectedMessage = "Patient info not matched";
//		hospCde = "ABC";
//		
//		assertResult(expectedMessage);
//	}
//	
//	@Test
//	public void getCidStudyImageCount_CaseNoWrong_ShouldThrowException() throws Exception {
//		String expectedMessage = "Patient info not matched";
//		caseNo = "NOT_EXIST_CASE_NO";
//		
//		assertResult(expectedMessage);
//	}	

	@Test
	public void getCidStudyImageCount_AccessionNumberWrong_ShouldThrowException() throws Exception {
		String expectedMessage = "Cannot find the Study";
		accessionNo = "NOT_EXIST_ACCESSION_NO";
		
		assertResult(expectedMessage);
	}

	@Test
	public void getCidStudyImageCount_SeriesNoWrong_ShouldReturn0() throws Exception {
		String expectedMessage = "No Records in the Axon Edge Server";
		seriesNo = "NOT_EXIST_SERIES_NO";
		
		assertResult(expectedMessage);
	}
}
