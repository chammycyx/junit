package org.ha.cid.ias.testcase.release.ias150;

import icw.wsdl.xjc.pojo.CIDData;

import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.enumeration.EsbMessageType;
import org.ha.cid.ias.model.EsbEpisode;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.EsbPatientDemographics;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_BASE;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends focus on several issues in CID-3027, the following items would be validated:
 * 1. Masked HKID(normal) in log in IAS-ES when receiving A08 request
 * 2. Masked HKID(Space as prefix) in log in IAS-ES when receiving A08 request
 * 3. Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A08 request
 * 4. Masked HKID(normal) in log in IAS-ES when receiving A40 request
 * 5. Masked HKID(Space as prefix) in log in IAS-ES when receiving A40 request
 * 6. Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A40 request
 * 7. Masked HKID(normal) in log in IAS-ES when receiving A45 request
 * 8. Masked HKID(Space as prefix) in log in IAS-ES when receiving A45 request
 * 9. Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A45 request
 * 10. Masked HKID(normal) in log in IAS-ES when receiving A47(A08&A40) request
 * 11. Masked HKID(Space as prefix) in log in IAS-ES when receiving A47(A08&A40) request
 * 12. Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A47(A08&A40) request
 * 13. Masked HKID(Exception data 'A123456') in log in IAS-ES when receiving A40 request
 * 14. Masked HKID(Exception data 'A1234') in log in IAS-ES when receiving A40 request
 * 15. Masked HKID(Exception data ' UV987654A') in log in IAS-ES when uploading metadata
 * 16. Masked HKID(Exception data 'XjV98765ABCDDDD') in log in IAS-ES when uploading metadata
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class CID_3027 extends FLOW_ESB_13_BASE{
	
	private static final String HKID_EXCEPTIONAL_TYPE4 = " XV78765ABCDDDD";
	private static final String HKID_EXCEPTIONAL_TYPE3 = " UV987654A";
	private static final String HKID_EXCEPTIONAL_TYPE2 = "T3646";
	private static final String HKID_EXCEPTIONAL_TYPE1 = "T364646";
	private static final String HKID_DOUBLE_ALPHABETS_PREFIX = "UU6851342";
	private static final String HKID_SPACE_PREFIX = " U6851342";
	private static final String HKID_NORMAL = "U6851342";

	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
//	@Autowired
//	@Qualifier("esRemoteLogFileService")
//	private RemoteFileService esRemoteLogFileService;
	
	private String accessionNo1;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";
	private String cutoffTimeInMinute = "0";
	private String hospCode = "VH";
	private String caseNum1 = "HN01000101X";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("true");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("false");
		iasEsbService.setCutoffTime(cutoffTimeInMinute);
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo1 = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
		iasEsInternalService.removeStudy(accessionNo1);
	}

	/**
	 * Masked HKID(normal) in log in IAS-ES when receiving A08 request
	 * @throws Exception
	 */
	@Test
	public void a08_normal_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA08(HKID_NORMAL);
		
		//Assert
		//'U6851342'->'U6851***'
	}
	
	/**
	 * Masked HKID(Space as prefix) in log in IAS-ES when receiving A08 request
	 * @throws Exception
	 */
	@Test
	public void a08_space_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA08(HKID_SPACE_PREFIX);
		
		//Assert
		//' U6851342'->' U6851***'
	}
	
	/**
	 * Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A08 request
	 * @throws Exception
	 */
	@Test
	public void a08_double_alphabets_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA08(HKID_DOUBLE_ALPHABETS_PREFIX);
		
		//Assert
		//'UU6851342'->'UU6851***'
	}
	
	/**
	 * Masked HKID(normal) in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void a40_normal_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA40(HKID_NORMAL);
		
		//Assert
		//'U6851342'->'U6851***'
	}
	
	/**
	 * Masked HKID(Space as prefix) in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void a40_space_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA40(HKID_SPACE_PREFIX);
		
		//Assert
		//' U6851342'->' U6851***'	
	}
	
	/**
	 * Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void a40_double_alphabets_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA40(HKID_DOUBLE_ALPHABETS_PREFIX);
		
		//Assert
		//'UU6851342'->'UU6851***'
	}
	
	/**
	 * Masked HKID(normal) in log in IAS-ES when receiving A45 request
	 * @throws Exception
	 */
	@Test
	public void a45_normal_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA45(HKID_NORMAL);
		
		//Assert
		//'U6851342'->'U6851***'	
	}
	
	/**
	 * Masked HKID(Space as prefix) in log in IAS-ES when receiving A45 request
	 * @throws Exception
	 */
	@Test
	public void a45_Space_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA45(HKID_SPACE_PREFIX);
		
		//Assert
		//' U6851342'->' U6851***'	
	}
	
	/**
	 * Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A45 request
	 * @throws Exception
	 */
	@Test
	public void a45_double_alphabets_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA45(HKID_DOUBLE_ALPHABETS_PREFIX);
		
		//Assert
		//'UU6851342'->'UU6851***'	
	}
	
	/**
	 * Masked HKID(normal) in log in IAS-ES when receiving A47 request
	 * @throws Exception
	 */
	@Test
	public void a47_normal_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA47DiffPatienyKey(HKID_NORMAL);
		sendA47SamePatienyKey(HKID_NORMAL);
		
		//Assert
		//'U6851342'->'U6851***'	
	}
	
	/**
	 * Masked HKID(Space as prefix) in log in IAS-ES when receiving A47 request
	 * @throws Exception
	 */
	@Test
	public void a47_space_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA47DiffPatienyKey(HKID_SPACE_PREFIX);
		sendA47SamePatienyKey(HKID_SPACE_PREFIX);
		
		//Assert
		//' U6851342'->' U6851***'	
	}
	
	/**
	 * Masked HKID(Double alphabets as prefix) in log in IAS-ES when receiving A47 request
	 * @throws Exception
	 */
	@Test
	public void a47_double_alphabets_prefix_hkid() throws Exception {
		//Arrange
		
		//Act
		sendA47DiffPatienyKey(HKID_DOUBLE_ALPHABETS_PREFIX);
		sendA47SamePatienyKey(HKID_DOUBLE_ALPHABETS_PREFIX);
		
		//Assert
		//'UU6851342'->'UU6851***'	
	}
	
	/**
	 * Masked HKID(Exception data 'A123456') in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void exceptional_hkid_type1() throws Exception {
		//Arrange
		
		//Act
		sendA40(HKID_EXCEPTIONAL_TYPE1);
		
		//Assert
		//'T364646'->'T3646**'
	}
	
	
	/**
	 * Masked HKID(Exception data 'A1234') in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void exceptional_hkid_type2() throws Exception {
		//Arrange
		
		//Act
		sendA40(HKID_EXCEPTIONAL_TYPE2);
		
		//Assert
		//'T3646'->'T3646'
	}
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	protected String[] versions = { "1", "2", "3" };
	protected String[] imageHandlings = {	"N", "N", "N" };
	protected String imageID1 = UUID.randomUUID().toString();
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	/**
	 * Masked HKID(Exception data 'A1234') in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void exceptional_hkid_type3() throws Exception {
		//Arrange
		
		//Act
//		sendA40(HKID_EXCEPTIONAL_TYPE3);
		CIDData cidData = iasClientService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
			cidData.getPatientDtl().setHKID(HKID_EXCEPTIONAL_TYPE3);
		iasClientService.uploadMetaData(cidData);
		
		//Assert
		//' UV987654A'->' UV98*****'
	}
	
	/**
	 * Masked HKID(Exception data 'A1234') in log in IAS-ES when receiving A40 request
	 * @throws Exception
	 */
	@Test
	public void exceptional_hkid_type4() throws Exception {
		//Arrange
		
		//Act
//		sendA40(HKID_EXCEPTIONAL_TYPE4);
		CIDData cidData = iasClientService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
			cidData.getPatientDtl().setHKID(HKID_EXCEPTIONAL_TYPE4);
		iasClientService.uploadMetaData(cidData);
		
		//Assert
		//' XV78765ABCDDDD'->' XV78**********'
	}
	
	public void sendA47DiffPatienyKey(String hkid) throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A47);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid(hkid);
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
	}
	
	public void sendA47SamePatienyKey(String hkid) throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A47);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid(hkid);
			toPatient.setPatientKey(fromPatient.getPatientKey());
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
		
	}
	
	public void sendA45(String hkid) throws Exception {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A45);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid(hkid);
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		EsbEpisode esbEpisode = new EsbEpisode();
			esbEpisode.setCaseNumber(caseNum1);
			esbEpisode.setHospitalCode(hospCode);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, esbEpisode);
	}
	
	public void sendA40(String hkid) throws Exception {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A40);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics fromPatient = new EsbPatientDemographics();
			fromPatient.setPatientKey(fromPatientKey);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid(hkid);
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setFromPatient(fromPatient);
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
	}
	
	private void sendA08(String hkid){
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		EsbMessageHeader esbMessageHeader = new EsbMessageHeader();
			esbMessageHeader.setMessageType(EsbMessageType.A08);
			esbMessageHeader.setEvn2Dt(evn2);
			esbMessageHeader.setTransactionId("TxnId" + evn2);
		EsbPatientDemographics toPatient = new EsbPatientDemographics();
			toPatient.setDob("19880203");
			toPatient.setHkid(hkid);
			toPatient.setPatientKey(toPatientKey);
			toPatient.setPatientName("DEV Patient 01");
			toPatient.setSex("M");
		EsbMessagePatient esbMessagePatient = new EsbMessagePatient();
			esbMessagePatient.setToPatient(toPatient);
		
		//Act
		int actual = iasEsbService.sendEsbMsg(esbMessageHeader, esbMessagePatient, null);
	}

}
