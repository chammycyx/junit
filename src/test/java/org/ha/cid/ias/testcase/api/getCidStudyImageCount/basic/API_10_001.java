/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.basic;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. CID study image count should be 1
 * 
 * @author YKF491
 *
 */
public abstract class API_10_001 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void getCidStudyImageCount_uploadRetrievePcFalse_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(cidData, false);
		
		Assert.assertEquals(1, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_uploadRetrievePcTrue_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(cidData, true);
		
		Assert.assertEquals(0, imageCount);
	}
}
