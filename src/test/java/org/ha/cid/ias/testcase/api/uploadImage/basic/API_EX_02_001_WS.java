package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;


@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_EX_02_001_WS extends API_EX_02_001 {
	
	public API_EX_02_001_WS(String fieldName, String imageFile, String requestSys, String filename, String patientKey,
			String studyId, String seriesNo, String userId, String workstationId) {
		super(fieldName, imageFile, requestSys, filename, patientKey, studyId, seriesNo, userId, workstationId);
	}
	
}
