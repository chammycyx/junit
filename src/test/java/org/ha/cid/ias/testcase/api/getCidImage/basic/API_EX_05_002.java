package org.ha.cid.ias.testcase.api.getCidImage.basic;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidImage with wrong parameter
 * 
 * @author CFT545
 *
 */
public abstract class API_EX_05_002 {
	
	@Autowired
	protected IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void getCidImage_AccessionNoWrong_ShouldFail() throws Exception {
		
		final String accessionNo = "NOT_EXIST_ACCESSION_NO";
		final String expectedMessage = "Cannot find the Study";
		try {
			iasService.getCidImage(
					cidData.getPatientDtl().getPatKey(), 
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					accessionNo, 
					cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
					"3", 
					"3", 
					imageID1, 
					SampleData.getUserId(),
					SampleData.getWorkstationId(),
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidImage_SeriesNoWrong_ShouldFail() throws Exception {
		
		final String seriesNo = "NOT_EXIST_SERIES_NO";
		final String expectedMessage = ""; // No error message?
		try {
			iasService.getCidImage(
					cidData.getPatientDtl().getPatKey(), 
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					cidData.getStudyDtl().getAccessionNo(), 
					seriesNo, 
					"3", 
					"3", 
					imageID1, 
					SampleData.getUserId(),
					SampleData.getWorkstationId(),
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidImage_ImageSeqNoWrong_ShouldFail() throws Exception {
		
		final String imageSeqNo = "0";
		final String expectedMessage = "No image existing";
		try {
			iasService.getCidImage(
					cidData.getPatientDtl().getPatKey(), 
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					cidData.getStudyDtl().getAccessionNo(), 
					cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
					imageSeqNo, 
					"3", 
					imageID1, 
					SampleData.getUserId(),
					SampleData.getWorkstationId(),
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidImage_VersionNoWrong_ShouldFail() throws Exception {
		
		final String versionNo = "0";
		final String expectedMessage = "No image existing";
		try {
			iasService.getCidImage(
					cidData.getPatientDtl().getPatKey(), 
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					cidData.getStudyDtl().getAccessionNo(), 
					cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
					"3", 
					versionNo, 
					imageID1, 
					SampleData.getUserId(),
					SampleData.getWorkstationId(),
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void getCidImage_ImageIdWrong_ShouldFail() throws Exception {
		
		final String imageId = "NOT_EXIST_IMAGE_ID";
		final String expectedMessage = "No image existing";
		try {
			iasService.getCidImage(
					cidData.getPatientDtl().getPatKey(), 
					cidData.getVisitDtl().getVisitHosp(), 
					cidData.getVisitDtl().getCaseNum(), 
					cidData.getStudyDtl().getAccessionNo(), 
					cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
					"3", 
					"3", 
					imageId, 
					SampleData.getUserId(),
					SampleData.getWorkstationId(),
					SampleData.getRequestSystem()
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
}
