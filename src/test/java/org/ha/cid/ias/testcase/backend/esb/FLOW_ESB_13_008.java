package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Calendar;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends focus on 'patient independent dis-ordered(exceeds cut-off time) ESB message', the following items would be validated:
 * 1. A08<->A08 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 2. A08<->A40 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 3. A08<->A45 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 4. A08<->A47 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 5. A40<->A40 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 6. A40<->A45 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 7. A40<->A47 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 8. A45<->A45 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 9. A45<->A47 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 10. A47<->A47 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 11. Combined test of A08/A40/A45/A47 patient independent dis-ordered ESB message should be processed and mark status 'I'
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_008 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo1;
	private String accessionNo2;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";
	private String fromPatientKey2 = "80050202";
	private String toPatientKey2 = "80050203";
	private String fromPatientKey3 = "80050204";
	private String toPatientKey3 = "80050205";
	private String fromPatientKey4 = "80050206";
	private String toPatientKey4 = "80050207";
	private String fromPatientKey5 = "80050208";
	private String toPatientKey5 = "80050209";
//	private String fromPatientKey6 = "80050210";
//	private String toPatientKey6 = "80050211";
//	private String fromPatientKey7 = "80050212";
//	private String toPatientKey7 = "80050213";
//	private String fromPatientKey8 = "80050214";
//	private String toPatientKey8 = "80050215";
	private String cutoffTimeInMinute = "1";
	private String hospCode = "VH";
	private String caseNum1 = "HN01000101X";
	private String caseNum2 = "HN01000102X";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
		iasEsbService.enableStagingPoller("true");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("true");
		iasEsbService.setCutoffTime(cutoffTimeInMinute);
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		createPatient(fromPatientKey2);
		createPatient(toPatientKey2);
		createPatient(fromPatientKey3);
		createPatient(toPatientKey3);
		createPatient(fromPatientKey4);
		createPatient(toPatientKey4);
		createPatient(fromPatientKey5);
		createPatient(toPatientKey5);
//		createPatient(fromPatientKey6);
//		createPatient(toPatientKey6);
//		createPatient(fromPatientKey7);
//		createPatient(toPatientKey7);
//		createPatient(fromPatientKey8);
//		createPatient(toPatientKey8);
		
		//Create a study for testing
		accessionNo1 = createStudy();
		accessionNo2 = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
		iasRrInternalService.removePatient(fromPatientKey2);
		iasRrInternalService.removePatient(toPatientKey2);
		iasRrInternalService.removePatient(fromPatientKey3);
		iasRrInternalService.removePatient(toPatientKey3);
		iasRrInternalService.removePatient(fromPatientKey4);
		iasRrInternalService.removePatient(toPatientKey4);
		iasRrInternalService.removePatient(fromPatientKey5);
		iasRrInternalService.removePatient(toPatientKey5);
//		iasRrInternalService.removePatient(fromPatientKey6);
//		iasRrInternalService.removePatient(toPatientKey6);
//		iasRrInternalService.removePatient(fromPatientKey7);
//		iasRrInternalService.removePatient(toPatientKey7);
//		iasRrInternalService.removePatient(fromPatientKey8);
//		iasRrInternalService.removePatient(toPatientKey8);
		iasEsInternalService.removeStudy(accessionNo1);
		iasEsInternalService.removeStudy(accessionNo2);
	}

	/**
	 * 
	 * 1. Send A08 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A08 with smaller EVN.2
	 * 4. Assert the A08 with higher ENV.2 is successfully processed
	 * 5. Assert the A08 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A08() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId1 = sendA08(evn2, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a08TxnId2 = sendA08(evn2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging1 = iasEsbService.findStaging(a08TxnId1);
		StagingDto a08Staging2 = iasEsbService.findStaging(a08TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging2.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A08 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A40 with smaller EVN.2
	 * 4. Assert the A08 with higher ENV.2 is successfully processed
	 * 5. Assert the A40 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A40() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId = sendA08(evn2, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A08 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A45 with smaller EVN.2
	 * 4. Assert the A08 with higher ENV.2 is successfully processed
	 * 5. Assert the A45 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A45() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId = sendA08(evn2, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey2, toPatientKey2, hospCode, caseNum1);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A08 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A40-different patient key) with smaller EVN.2
	 * 4. Assert the A08 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A47_diff_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId = sendA08(evn2, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId = sendA47(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a47Staging = iasEsbService.findStaging(a47TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A08 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A08-same patient key) with smaller EVN.2
	 * 4. Assert the A08 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A47_same_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId = sendA08(evn2, fromPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId = sendA47(evn2, fromPatientKey2, fromPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a47Staging = iasEsbService.findStaging(a47TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A40 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A40 with smaller EVN.2
	 * 4. Assert the A40 with higher ENV.2 is successfully processed
	 * 5. Assert the A40 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A40_A40() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a40TxnId1 = sendA40(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId2 = sendA40(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a40Staging1 = iasEsbService.findStaging(a40TxnId1);
		StagingDto a40Staging2 = iasEsbService.findStaging(a40TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging2.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A40 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A45 with smaller EVN.2
	 * 4. Assert the A40 with higher ENV.2 is successfully processed
	 * 5. Assert the A45 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A40_A45() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey2, toPatientKey2, hospCode, caseNum1);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging.getProcessStatus());
	}
	
	
	/**
	 * 
	 * 1. Send A40 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A40-different patient key) with smaller EVN.2
	 * 4. Assert the A40 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A40_A47_diff_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId = sendA47(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a47Staging = iasEsbService.findStaging(a47TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging.getProcessStatus());
	}
	
	
	/**
	 * 
	 * 1. Send A40 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A08-same patient key) with smaller EVN.2
	 * 4. Assert the A40 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A40_A47_same_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId = sendA47(evn2, fromPatientKey2, fromPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a47Staging = iasEsbService.findStaging(a47TxnId);
		
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging.getProcessStatus());
	}
	
	
	/**
	 * 
	 * 1. Send A45 with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A45 with smaller EVN.2
	 * 4. Assert the A45 with higher ENV.2 is successfully processed
	 * 5. Assert the A45 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A45_A45() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a45TxnId1 = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId2 = sendA45(evn2, fromPatientKey2, toPatientKey2, hospCode, caseNum1);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a45Staging1 = iasEsbService.findStaging(a45TxnId1);
		StagingDto a45Staging2 = iasEsbService.findStaging(a45TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging2.getProcessStatus());
	}
	
	
	
	/**
	 * 
	 * 1. Send A47(A08-same patient key) with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A40-different patient key) with smaller EVN.2
	 * 4. Assert the A47 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A47_same_patient_key_A47_diff_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a47TxnId1 = sendA47(evn2, fromPatientKey, fromPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
	}
	
	
	/**
	 * 
	 * 1. Send A47(A08-same patient key) with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A08-same patient key) with smaller EVN.2
	 * 4. Assert the A47 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A47_same_patient_key_A47_same_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a47TxnId1 = sendA47(evn2, toPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, toPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A47(A40-different patient key) with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A08-same patient key) with smaller EVN.2
	 * 4. Assert the A47 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A47_diff_patient_key_A47_same_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a47TxnId1 = sendA47(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, toPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
	}
	
	
	/**
	 * 
	 * 1. Send A47(A40-different patient key) with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A40-different patient key) with smaller EVN.2
	 * 4. Assert the A47 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A47_diff_patient_key_A47_diff_patient_key() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a47TxnId1 = sendA47(evn2, fromPatientKey, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
	}
	
	/**
	 * 
	 * 1. Send A47(A40-different patient key) with higher EVN.2
	 * 2. Wait unit cut-off time is passed
	 * 3. Send A47(A40-different patient key) with smaller EVN.2
	 * 4. Assert the A47 with higher ENV.2 is successfully processed
	 * 5. Assert the A47 with smaller ENV.2 is successfully processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_A08_A40_A45_A47() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		String a08TxnId = sendA08(evn2, toPatientKey);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);
		
		now.add(Calendar.MILLISECOND, -4);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey2, toPatientKey2);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey3, toPatientKey3, hospCode, caseNum1);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId1 = sendA47(evn2, fromPatientKey4, toPatientKey4);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, fromPatientKey5, toPatientKey5);
		TimerUtil.delay((Integer.valueOf(cutoffTimeInMinute)*60)+5);//wait until cut-off time passed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
	}

}
