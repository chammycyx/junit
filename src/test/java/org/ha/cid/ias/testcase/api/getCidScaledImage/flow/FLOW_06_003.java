package org.ha.cid.ias.testcase.api.getCidScaledImage.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 GIF image (each image has 3 versions)
 * 2. getCidScaledImage for all images
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_06_003 {

	@Autowired
	private IasClientService iasService;

	@Autowired
	protected ResourceService resourceService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.GIF", "/blazeds/flow/OGD1_v2.GIF", "/blazeds/flow/OGD1_v3.GIF" };

	String[] scaledImageFiles = {	"/blazeds/flow/OGD1_v1_scaled.GIF", "/blazeds/flow/OGD1_v2_scaled.GIF", "/blazeds/flow/OGD1_v3_scaled.GIF" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void getCidScaledImage_UploadRetrieveGIF_ShouldSuccess() throws Exception {
		
		for (int i = 0; i < imageFiles.length; i++) {

			byte[] result = iasService.getCidScaledImage(cidData, String.valueOf(i+1), versions[i], imageIDs[i], 500, 600);

			byte[] expected = resourceService.loadFileAsByteArray(scaledImageFiles[i]);
			
			Assert.assertArrayEquals(expected, result);
			
		}
		
	}
	
}
