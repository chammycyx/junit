/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.flow.cn;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete the study
 * 3. Remove the study from ES
 * 4. Export image
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_CN_EX_04_002_BZ extends FLOW_CN_EX_04_002 {
}
