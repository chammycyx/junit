/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. rearrange and upload metadata
 * 3. getCidStudyImageCount should return 3 (each image has 2 versions)
 * 
 * @author HSY914
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_10_006_BZ extends FLOW_10_006 {
}
