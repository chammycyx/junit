package org.ha.cid.ias.testcase.api.getCidImage.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_002;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image of a image which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_CN_EX_05_002 extends FLOW_CN_EX_05_002 {

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;

	@Test
	public void getCidImage_RetrieveImageNotExistsFromCN_ShouldAuditMatch() throws Exception {
		final String auditEventName = "getCidImageInRR";
		final String imageId = "NOT_EXIST_IMAGE";
		final String expectedMessage = "No image existing";

		try {
			iasService.getCidImage(cidData, "3", "3", imageId);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception ex) {
			// Validate Audit Events
			rrInternalService.setDelay(10);
			List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), auditEventName);
			AuditEventDto auditEventDto = auditEventDtos.get(0);
			Assert.assertTrue(auditEventDto.getStatus().equals(AuditEventDto.STATUS_FAILURE));
			Assert.assertTrue(auditEventDto.getResponse().equals(expectedMessage));
			Assert.assertTrue(auditEventDto.getException().contains(expectedMessage));
		}
	}

}
