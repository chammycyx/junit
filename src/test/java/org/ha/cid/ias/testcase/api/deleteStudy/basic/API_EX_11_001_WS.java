package org.ha.cid.ias.testcase.api.deleteStudy.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;


@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_EX_11_001_WS extends API_EX_11_001 {

	public API_EX_11_001_WS(String fieldName, String patientKey, String hospCde, 
			String caseNo, String accessionNo, String studyID) {
		super(fieldName, patientKey, hospCde, caseNo, accessionNo, studyID);
	}
	
}
