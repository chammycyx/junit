/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. modify and upload the image with version 1, 3, 4
 * 4. modify and upload the image with version 1, 4, 5
 * 5. getCidStudy should return HA7 with 2 images (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_08_003a_RS extends FLOW_VS_08_003a {
}
