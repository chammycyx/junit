/**
 * 
 */
package org.ha.cid.ias.testcase.api.common;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpResponseException;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.service.RestfulService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_01_001_RS extends API_01_001 {
	
	@Autowired
	public RestfulService restfulService;
	
	@Value("${rs.userName}")
	String username;
	@Value("${rs.password}")
	String password;
	
	@Before
	public void prepareTest() throws Exception {
		
		changeCredentials("cid2user", "abcd3###");
	}
	
	@After
	public void afterTest() throws Exception {
		
		changeCredentials(username, password);
		
	}
	
	private void changeCredentials(String username, String password){
		restfulService.changeCredentials(username, password);
	}
}
