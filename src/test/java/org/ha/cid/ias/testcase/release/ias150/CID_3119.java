package org.ha.cid.ias.testcase.release.ias150;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_BASE;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;

/**
 * This test case intends focus on several issues in CID-3119, the following items would be validated:
 * 1. Solve the issue of ‘out of memory exception caused by a very delayed ESB’ 
 * 2. Poll old staging(ordering_identifier==null) only when flag 'null.ordering.Identifier.data.polling.enable' is on
 * 3. Poll old and new staging only when flag 'null.ordering.Identifier.data.polling.enable' is on 
 * 4. Poll new staging(ordering_identifier!=null) only when flag 'null.ordering.Identifier.data.polling.enable' is on
 * 5. No staging polled if there is an staging being processing(status 'I'). Staging should process one by one.
 * 
 * @author Patrick YAU
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class CID_3119 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo1;
	private String accessionNo2;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";
	private String fromPatientKey2 = "80050202";
	private String toPatientKey2 = "80050203";
	private String fromPatientKey3 = "80050204";
	private String toPatientKey3 = "80050205";
	private String fromPatientKey4 = "80050206";
	private String toPatientKey4 = "80050207";
	private String fromPatientKey5 = "80050208";
	private String toPatientKey5 = "80050209";
	private String cutoffTimeInMinute = "1";
	private String hospCode = "VH";
	private String caseNum1 = "HN01000101X";
	private String caseNum2 = "HN01000102X";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		super.setup();
		
		//Setup ESB env
//		iasEsbService.enableStagingPoller("true");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("true");
		iasEsbService.setCutoffTime(cutoffTimeInMinute);
		iasEsbService.enableNullOrderingIdentifierStagingPolling("true");
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create a patient for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		createPatient(fromPatientKey2);
		createPatient(toPatientKey2);
		createPatient(fromPatientKey3);
		createPatient(toPatientKey3);
		createPatient(fromPatientKey4);
		createPatient(toPatientKey4);
		createPatient(fromPatientKey5);
		createPatient(toPatientKey5);
		
		//Create a study for testing
		accessionNo1 = createStudy();
		accessionNo2 = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
		iasRrInternalService.removePatient(fromPatientKey2);
		iasRrInternalService.removePatient(toPatientKey2);
		iasRrInternalService.removePatient(fromPatientKey3);
		iasRrInternalService.removePatient(toPatientKey3);
		iasRrInternalService.removePatient(fromPatientKey4);
		iasRrInternalService.removePatient(toPatientKey4);
		iasRrInternalService.removePatient(fromPatientKey5);
		iasRrInternalService.removePatient(toPatientKey5);
		iasEsInternalService.removeStudy(accessionNo1);
		iasEsInternalService.removeStudy(accessionNo2);
	}

	
//	@Test
	public void stage1_esb_performance() {
		//Arrange
		Calendar now = Calendar.getInstance();
//		int numEsb = 400000;
		int numEsb = 200;
		iasEsbService.enableStagingPoller("true");
		
		String testingFromPatientKey = "1000000";
		String testingToPatientKey = "1500000";
		
		//insert patient to table for increasing data volume
		iasEsbService.createPatient(testingFromPatientKey, testingToPatientKey);
		
		//Act
		for(int i=0;i<numEsb;i++){
			now.add(Calendar.MILLISECOND, 1);
			String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
			String a08TxnId1 = sendA08(evn2, fromPatientKey);
			
			now.add(Calendar.MILLISECOND, 1);
			evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
			String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
			
			now.add(Calendar.MILLISECOND, 1);
			evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
			String a45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
			
			now.add(Calendar.MILLISECOND, 1);
			evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
			String a47TxnId1 = sendA47(evn2, fromPatientKey, toPatientKey);
			
			now.add(Calendar.MILLISECOND, 1);
			evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
			String a47TxnId2 = sendA47(evn2, fromPatientKey, fromPatientKey);
		}
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//Wait for cut-off time
		TimerUtil.delay(numEsb*5*5);//Wait for all message get processed
		
		//Assert
		//1000 records in total in 30sec, including A08, A40, A45, A47
		//Whole process time: 18 mins 1 second 562 millisecond = 1081 seconds
		//Avg process time(I->C): 0.076168 second 
		//Max process time(I->C): 0.356 second 
		//Min process time(I->C): 0.054 second
		
		//1000 records in total in 30sec, including A08, A40, A45, A47
		//Whole process time: 18 mins 6 second 31 millisecond = 1086 seconds
		//Avg process time(I->C): 0.080615 second 
		//Max process time(I->C): 0.226 second 
		//Min process time(I->C): 0.056 second
	}
	
	@Test
	public void stage2_out_memory_by_delayed_message() {
		//Arrange
		Date minOrderingIdentifier = iasEsbService.findMinimumOrderingIdentifier();
		Calendar cal = Calendar.getInstance();
			cal.setTime(minOrderingIdentifier);
			cal.add(Calendar.MILLISECOND, -1);
		String evn2 = DateUtils.format(cal.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		iasEsbService.enableStagingPoller("true");
		String a08TxnId = sendA08(evn2, fromPatientKey);
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//Wait for cut-off time
		TimerUtil.delay(10);//Wait for message get processed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		long millisecond = getDateDiff(a08Staging.getStagingStartDt(), a08Staging.getStagingEndDt(), TimeUnit.MILLISECONDS);
		assertEquals(true, millisecond<3000);
	}
	
	@Test
	public void poll_old_data_only() {
		//Arrange
		iasEsbService.enableStagingPoller("false");
		
		Calendar now = Calendar.getInstance();
		Date a08Evn2 = new Date(now.getTime().getTime());
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a08TxnId = sendA08(evn2, fromPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		Date a40Evn2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		Date a45Evn2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, -1);
		Date a47Txn1Evn2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId1 = sendA47(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		Date a47Txn2Evn2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47TxnId2 = sendA47(evn2, fromPatientKey, fromPatientKey);
		
		TimerUtil.delay(5);//wait for message receiver to turn esb into staging
		
		iasEsbService.setOrderingIdentifierNull(a08Evn2);
		iasEsbService.setOrderingIdentifierNull(a40Evn2);
		iasEsbService.setOrderingIdentifierNull(a45Evn2);
		iasEsbService.setOrderingIdentifierNull(a47Txn1Evn2);
		iasEsbService.setOrderingIdentifierNull(a47Txn2Evn2);
		
		//Act
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//Wait for cut-off time
		TimerUtil.delay(30);//wait for all esb get processed
		
		//Assert
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		StagingDto a47Staging1 = iasEsbService.findStaging(a47TxnId1);
		StagingDto a47Staging2 = iasEsbService.findStaging(a47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, a08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a45Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, a47Staging2.getProcessStatus());
		assertEquals(true, a08Staging.getStagingEndDt().before(a40Staging.getStagingStartDt()));
		assertEquals(true, a40Staging.getStagingEndDt().before(a45Staging.getStagingStartDt()));
		assertEquals(true, a45Staging.getStagingEndDt().before(a47Staging1.getStagingStartDt()));
		assertEquals(true, a47Staging1.getStagingEndDt().before(a47Staging2.getStagingStartDt()));
		
	}
	
	@Test
	public void poll_new_data_only() {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A08SubTypeTxnId = sendA47(evn2, fromPatientKey, fromPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A40SubTypeTxnId = sendA47(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a08TxnId = sendA08(evn2, toPatientKey);
		
		//Assert
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//cutoff time in second
		TimerUtil.delay(120);//wait for all ESB msg get processed
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		StagingDto a47A40SubTypeStaging = iasEsbService.findStaging(a47A40SubTypeTxnId);
		StagingDto a47A08SubTypeStaging = iasEsbService.findStaging(a47A08SubTypeTxnId);
		
		assertEquals(true, a08Staging.getStagingEndDt().before(a40Staging.getStagingStartDt()));
		assertEquals(true, a40Staging.getStagingEndDt().before(a45Staging.getStagingStartDt()));
		assertEquals(true, a45Staging.getStagingEndDt().before(a47A40SubTypeStaging.getStagingStartDt()));
		assertEquals(true, a47A40SubTypeStaging.getStagingEndDt().before(a47A08SubTypeStaging.getStagingStartDt()));
	}
	
	@Test
	public void poll_old_new_data() {
		//Arrange
		iasEsbService.enableStagingPoller("false");
		
		Calendar now = Calendar.getInstance();
		
		//old data
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		Date oldA08EVN2 = new Date(now.getTime().getTime());
		String oldA08TxnId = sendA08(evn2, fromPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		Date oldA40EVN2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String oldA40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		Date oldA45EVN2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String oldA45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, 1);
		Date oldA47Txn1EVN2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String oldA47TxnId1 = sendA47(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		Date oldA47Txn2EVN2 = new Date(now.getTime().getTime());
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String oldA47TxnId2 = sendA47(evn2, fromPatientKey, fromPatientKey);
		
		TimerUtil.delay(5);//wait for message receiver to turn esb into staging
		
		iasEsbService.setOrderingIdentifierNull(oldA08EVN2);
		iasEsbService.setOrderingIdentifierNull(oldA40EVN2);
		iasEsbService.setOrderingIdentifierNull(oldA45EVN2);
		iasEsbService.setOrderingIdentifierNull(oldA47Txn1EVN2);
		iasEsbService.setOrderingIdentifierNull(oldA47Txn2EVN2);
		
		//new data
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String newA08TxnId = sendA08(evn2, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String newA40TxnId = sendA40(evn2, fromPatientKey2, toPatientKey2);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String newA45TxnId = sendA45(evn2, fromPatientKey3, toPatientKey3, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String newA47TxnId1 = sendA47(evn2, fromPatientKey4, toPatientKey4);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String newA47TxnId2 = sendA47(evn2, fromPatientKey5, toPatientKey5);
		
		//Act
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//Wait for cut-off time
		TimerUtil.delay(120);//wait for all esb get processed
		
		//Assert
		StagingDto oldA08Staging = iasEsbService.findStaging(oldA08TxnId);
		StagingDto oldA40Staging = iasEsbService.findStaging(oldA40TxnId);
		StagingDto oldA45Staging = iasEsbService.findStaging(oldA45TxnId);
		StagingDto oldA47Staging1 = iasEsbService.findStaging(oldA47TxnId1);
		StagingDto oldA47Staging2 = iasEsbService.findStaging(oldA47TxnId2);
		StagingDto newA08Staging = iasEsbService.findStaging(newA08TxnId);
		StagingDto newA40Staging = iasEsbService.findStaging(newA40TxnId);
		StagingDto newA45Staging = iasEsbService.findStaging(newA45TxnId);
		StagingDto newA47Staging1 = iasEsbService.findStaging(newA47TxnId1);
		StagingDto newA47Staging2 = iasEsbService.findStaging(newA47TxnId2);
		
		assertEquals(MessageProcessStatus.SUCCESS, oldA08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, oldA40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, oldA45Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, oldA47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, oldA47Staging2.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, newA08Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, newA40Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, newA45Staging.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, newA47Staging1.getProcessStatus());
		assertEquals(MessageProcessStatus.SUCCESS, newA47Staging2.getProcessStatus());
		assertEquals(true, oldA08Staging.getStagingEndDt().before(oldA40Staging.getStagingStartDt()));
		assertEquals(true, oldA40Staging.getStagingEndDt().before(oldA45Staging.getStagingStartDt()));
		assertEquals(true, oldA45Staging.getStagingEndDt().before(oldA47Staging1.getStagingStartDt()));
		assertEquals(true, oldA47Staging1.getStagingEndDt().before(oldA47Staging2.getStagingStartDt()));
		assertEquals(true, oldA47Staging2.getStagingEndDt().before(newA08Staging.getStagingStartDt()));
		assertEquals(true, newA08Staging.getStagingEndDt().before(newA40Staging.getStagingStartDt()));
		assertEquals(true, newA40Staging.getStagingEndDt().before(newA45Staging.getStagingStartDt()));
		assertEquals(true, newA45Staging.getStagingEndDt().before(newA47Staging1.getStagingStartDt()));
		assertEquals(true, newA47Staging1.getStagingEndDt().before(newA47Staging2.getStagingStartDt()));
	}
	
	/**
	 * Test staging get processed one by one
	 * 
	 * Steps
	 * 1. Stop polling
	 * 2. Send the first A08, set the status to 'I'
	 * 3. Send the second A08, leave the status to 'A'
	 * 4. Resume polling
	 * 5. The second A08 would not get polled as the first one is in progress('I')
	 * 6. Set the status of first A08 to 'E'
	 * 7. Assert the second A08 will get polled and processed
	 * 
	 * @throws IOException
	 */
	@Test
	public void process_staging_one_by_one() throws IOException {
		//Arrange
		iasEsbService.enableStagingPoller("false");
		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		iasEsbService.setCutoffTime(cutoffTimeInMinute);
		
		Calendar now = Calendar.getInstance();
		Date evn2TxnId1 = new Date(now.getTime().getTime());
		String a08TxnId1 = sendA08(DateUtils.format(evn2TxnId1, DateUtils.FORMAT_TIMESTAMP), toPatientKey);
		now.add(Calendar.MILLISECOND, 1);
		Date evn2TxnId2 = new Date(now.getTime().getTime());
		String a08TxnId2 = sendA08(DateUtils.format(evn2TxnId2, DateUtils.FORMAT_TIMESTAMP), toPatientKey);
		iasEsbService.setStagingStatus(evn2TxnId1, MessageProcessStatus.PROCESSING);
		iasEsbService.setOrderingIdentifierNull(evn2TxnId1);
		iasEsbService.setOrderingIdentifierNull(evn2TxnId2);
		
		//Act
		iasEsbService.enableStagingPoller("true");
		TimerUtil.delay(5);//wait for staging poller to trigger polling
		
		//Assert
		assertEquals(1, iasEsbService.findNumStagingWithStatus(Arrays.asList(MessageProcessStatus.PROCESSING)));
		assertEquals(1, iasEsbService.findNumStagingWithStatus(Arrays.asList(MessageProcessStatus.INITIAL)));
		
		//Change the first staging to E, the second staging would then starting get polled
		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.PROCESSING));
		TimerUtil.delay(Integer.valueOf(cutoffTimeInMinute)*60);//wait for cut-off time
		TimerUtil.delay(5);//wait for staging poller to trigger polling and process staging
		
		StagingDto a08Staging2 = iasEsbService.findStaging(a08TxnId2);
		
		assertEquals(true, Arrays.asList(MessageProcessStatus.PROCESSING,MessageProcessStatus.SUCCESS).contains(a08Staging2.getProcessStatus()));
	}
	
	/**
	 * Get a diff between two dates
	 * @param date1 the oldest date
	 * @param date2 the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,timeUnit);
	}

}
