package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image with patient no exist in PAS(each image has 3 versions)
 * 2. getCidStudyFirstLastImage
 * 
 * @author WYT752
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:rs-config-context.xml"
})
public class FLOW_EX_09_004_RS extends FLOW_EX_09_004{

}
