/**
 * 
 */
package org.ha.cid.ias.testcase.api.deleteStudy.flow;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This test case intends to validate the following items:
 * 1. Delete a study twice
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_11_002 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.PNG", "/blazeds/flow/OGD1_v2.PNG", "/blazeds/flow/OGD1_v3.PNG" };
	
	String[] versions = { 	"1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = { 	imageID1, imageID1, imageID1,
							imageID2, imageID2, imageID2,
							imageID3, imageID3, imageID3,
						};
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);	
	}
	
	@Test
	public void deleteStudy_DeleteTwice_ShouldGetFailedAck() throws Exception {
		
		String result;
		result = iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		result = iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");
		
		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
}
