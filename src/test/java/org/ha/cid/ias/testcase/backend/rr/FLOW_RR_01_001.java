package org.ha.cid.ias.testcase.backend.rr;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.junit.Assert;
import org.ha.cd2.isg.icw.tool.CidStorePathUtil;
import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.RemoteFileService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import common.utils.TimerUtil;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. update pathindex update_date to (today-89) day and wait for cron job 
 * 3. check files and local record exist (should not be removed)
 * 
 * @author CYC014
 *
 */
public abstract class FLOW_RR_01_001 {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	@Qualifier("iasRrInternalService")
	protected IasInternalService rrInternalService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;
	
	@Autowired
	@Qualifier("cnRemoteFileService")
	private RemoteFileService cnRemoteFileService;
	
	@Autowired
	protected ResourceService resourceService;
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	protected String[] versions = { "1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		esInternalService.removeStudy(accessionNo);
		
		Calendar calendar = Calendar.getInstance(); 
		calendar.add(Calendar.DATE, -89);
		Date date =  calendar.getTime();
		
		PathIndex pathIndex = rrInternalService.findPathIndex(accessionNo,"LOCAL");
		rrInternalService.updatePathIndex(pathIndex,date);
		System.out.println("============PathIndex Updated Date changed to "+ date +"========");
		
		
	}
	
	@Test
	public void PurgeCNImage_89Days_ShouldNotPurge() throws Exception {
		
		TimerUtil.delay(300);
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		PathIndex pathIndex = rrInternalService.findPathIndex(accessionNo,"LOCAL");
		boolean fileExistFlag = cnRemoteFileService.checkFileExist(CidStorePathUtil.generateStudyPath(accessionNo));
    	
		Assert.assertNotNull(accessionNo+" local record in tg_pathindex should not be removed", pathIndex);
		Assert.assertEquals("Files should not be removed", true, fileExistFlag);
	}
	
}
