package org.ha.cid.ias.testcase.api.exportImage.basic;

import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.http.util.EntityUtils;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
public abstract class API_04_001 {

	@Autowired
	protected IasClientService iasService;
	
	@Autowired
	protected ResourceService resourceService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3"};
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String exportPath = "/blazeds/flow/exportImage.7z";
	
	String password = "password";
	
	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void exportImage_uploadExport_ShouldSuccess() throws Exception {
		
		ImageDtls imageDtls = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls();
		
		byte[] result = iasService.exportImage(Ha7Util.convertToHa7xml(cidData), password);
		
		for(int i = 0; i < imageFiles.length; i++) {
			byte[] resultImage = resourceService.extractZipFile(exportPath, result, imageDtls.getImageDtl().get(i).getImageFile(), password);
			byte[] expectedImage = null;
			try {
				expectedImage = IOUtils.toByteArray(getClass().getResourceAsStream(imageFiles[i]));
			} catch (IOException e) {
				Assert.assertTrue(false);
				e.printStackTrace();
			}
			Assert.assertArrayEquals(expectedImage, resultImage);
		}
		
	}
}
