/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 2 images (each image has 2 version)
 * 2. add 1 image and rearrange and upload the images
 * 
 * @author CFT545
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class FLOW_03_010_WS extends FLOW_03_010 {
}
