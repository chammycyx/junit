/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload a image below 1 MB(each image has 2 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_02_001_RS extends FLOW_02_001 {

}
