/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 30 images (each image has 3 version)
 * 2. uploadMetaData is successful 
 * 
 * @author YKF491
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_02_002_BZ extends FLOW_02_002 {
}
