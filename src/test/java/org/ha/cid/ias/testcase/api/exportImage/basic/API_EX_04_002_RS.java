package org.ha.cid.ias.testcase.api.exportImage.basic;

import javax.xml.bind.UnmarshalException;

import org.apache.http.client.HttpResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_EX_04_002_RS extends API_EX_04_002 {
	
	@Test
	public void exportImage_NoParameter_ShouldThrowException() throws Exception {
	    final String expectedMessage = "The parameter ha7Msg is mandatory in the exportImage transaction";
	    
		try{
			iasService.exportImage(null, null);
			Assert.assertTrue("Exception is expected.", false);
		}catch (HttpResponseException e){
		    //Assert.assertEquals(e.getStatusCode(), 500);
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
		
	}
	
	@Override
	public void exportImage_Ha7MessageInvalid_ShouldThrowException() throws Exception {

		final String expectedMessage = "The incoming HA7 Message is incorrect, cann't process";
		try {
			iasService.exportImage(
					"{ \"abc\": \"Invalid XML\" }", 
					PASSWORD, 
					USER_ID, 
					WORKSTATION_ID, 
					REQUEST_SYS
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		   // Assert.assertTrue(e.getMessage().contains(expectedMessage));
		    Assert.assertEquals(e.getStatusCode(),500);
		} 
	}
	
	
}
