package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;


/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 version)
 * 2. edit and upload the image with the same accession no but different study ID
 * 
 * @author YKF491
 * 
 */
public abstract class FLOW_EX_03_006 {
	
	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_EditDifferentStudy_ShouldThrowException() throws Exception {

		final String expectedMessage = "Database Error";
		
		String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
		
		String[] versions = { "1", "2", "3" };
		
		String imageID1 = UUID.randomUUID().toString();
		
		String[] imageIDs = { imageID1, imageID1, imageID1 };
		
		String[] imageHandlings = {	"N", "N", "N" };
		
		CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		String[] versions2 = { "1", "3", "4" };
		
		cidData.getStudyDtl().setStudyID(UUID.randomUUID().toString());
		
		cidData = iasService.reuploadImages(cidData, imageFiles, versions2, imageIDs, imageHandlings);
			
		try {
			iasService.uploadMetaData(cidData);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e) {
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} 

	}
}
