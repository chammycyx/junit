package org.ha.cid.ias.testcase;

import org.ha.cid.ias.testcase.api.ApiBasicTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowCNTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowESBTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowRRTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowSNTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowTestSuite;
import org.ha.cid.ias.testcase.api.ApiFlowVSTestSuite;
import org.ha.cid.ias.testcase.api.ApiInternalAuditTestSuite;
import org.ha.cid.ias.testcase.api.ApiInternalStudyTestSuite;
import org.ha.cid.ias.testcase.api.ReleaseTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ApiBasicTestSuite.class,
	ApiFlowTestSuite.class,
	ApiFlowCNTestSuite.class,
	ApiInternalAuditTestSuite.class,
	ApiInternalStudyTestSuite.class,
	ApiFlowSNTestSuite.class,
	ApiFlowVSTestSuite.class,
	ApiFlowESBTestSuite.class,
	ApiFlowRRTestSuite.class,
	ReleaseTestSuite.class
	
})
public class MainTestSuite {

}
