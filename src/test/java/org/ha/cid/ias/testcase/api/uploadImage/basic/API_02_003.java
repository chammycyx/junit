/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This test case intends to validate the following items:
 * 1. upload GIF image with different size
 * 
 * @author CFT545
 *
 */
public abstract class API_02_003 {

	@Autowired
	private IasClientService iasService;

	@Autowired
	private ResourceService resourceService;
	
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	protected static final String FILE_NAME = "BRONCH1.JPG";
	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String STUDY_ID = SampleData.getStudyId();
	protected static final String SERIES_NO = SampleData.getSeriesNo();
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	
	@Test
	public void uploadImage_upload1MbGif_WillReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray("/blazeds/flow/gif/1MB.GIF");
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
	
	@Test
	public void uploadImage_upload2MbGif_WillReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray("/blazeds/flow/gif/2MB.GIF");
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
	
	@Test
	public void uploadImage_upload5MbGif_WillReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray("/blazeds/flow/gif/5MB.GIF");
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
	
	@Test
	public void uploadImage_upload10MbGif_WillReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray("/blazeds/flow/gif/10MB.GIF");
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
}
