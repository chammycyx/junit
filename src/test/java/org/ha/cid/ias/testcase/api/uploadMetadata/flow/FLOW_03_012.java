package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. update image handling, edit and upload metadata
 * 
 * @author HSY914
 * 
 */
public abstract class FLOW_03_012 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG" 
	};
	
	String[] versions = {
			"1", "2", 
			"1", "2", 
			"1", "2"
	};
	
	String[] versions2 = {
			"1", "3", 
			"1", "3", 
			"1", "3"
	};

	String[] versions3 = {
			"1", "4", 
			"1", "4", 
			"1", "4"
	};
	
	String[] imageHandlings = {
			"N", "N",
			"N", "N",
			"N", "N"
	};

	String[] imageHandlings2 = {
			"N", "N",
			"N", "Y",
			"N", "Y"
	};

	String[] imageHandlings3 = {
			"N", "N",
			"N", "N",
			"N", "Y"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	
	@Test
	public void uploadMetadata_UpdateHandlingRearrangeImage_ShouldSuccess() throws Exception {

		CIDData cidData = iasService.uploadImages(
				imageFiles,
				versions,
				imageIDs, 
				SampleData.getAccessionNo(),
				imageHandlings
				);
		iasService.uploadMetaData(cidData);

		CIDData cidData2 = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions2,
				imageIDs,
				imageHandlings2
				);
		iasService.uploadMetaData(cidData2);

		CIDData cidData3 = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions3,
				imageIDs,
				imageHandlings3
				);
		Integer result = iasService.uploadMetaData(cidData3);
		
		Assert.assertEquals(0, result.intValue());
	}
}


