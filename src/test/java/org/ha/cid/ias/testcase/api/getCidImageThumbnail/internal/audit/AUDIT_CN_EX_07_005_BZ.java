/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. Delete 1 images
 * 3. Get cid image thumbnail
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_CN_EX_07_005_BZ extends AUDIT_CN_EX_07_005 {
}
