package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.release.ias1331.CID_2701;
import org.ha.cid.ias.testcase.release.ias1331.CID_2703;
import org.ha.cid.ias.testcase.release.ias1331.CID_2704;
import org.ha.cid.ias.testcase.release.ias150.CID_2133;
import org.ha.cid.ias.testcase.release.ias150.CID_2199;
import org.ha.cid.ias.testcase.release.ias150.CID_2856;
import org.ha.cid.ias.testcase.release.ias150.CID_3027;
import org.ha.cid.ias.testcase.release.ias150.CID_3119;
import org.ha.cid.ias.testcase.release.ias150.CID_3128;
import org.ha.cid.ias.testcase.release.ias160.CID_3354;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	CID_2701.class,
	CID_2703.class,
	CID_2704.class,
	CID_2199.class,
	CID_2856.class,
	CID_3027.class,
	CID_3119.class,
	CID_3128.class,
	CID_3354.class
 
})
public class ReleaseTestSuite {

}
