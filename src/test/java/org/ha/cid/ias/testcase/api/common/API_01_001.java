package org.ha.cid.ias.testcase.api.common;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
public abstract class API_01_001 {

	@Autowired
	protected IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3"};
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String exportPath = "/blazeds/flow/exportImage.7z";
	
	
	CIDData cidData = null;

	String ha7Password ="password";

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void exportImage_changeCredentials_ShouldException() throws Exception {
		
		try{
			iasService.exportImage(Ha7Util.convertToHa7xml(cidData), ha7Password);
			Assert.assertTrue(false);
			
		}catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains("Invalid login"));
			
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains("Authentication of Username Password Token Failed"));
			
		} catch(HttpResponseException e){
			Assert.assertEquals(e.getStatusCode(),401);
		} 
		
	}
}
