package org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_EX_07_002_WS extends API_EX_07_002 {
	
}
