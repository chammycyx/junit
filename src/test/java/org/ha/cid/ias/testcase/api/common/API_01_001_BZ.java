/**
 * 
 */
package org.ha.cid.ias.testcase.api.common;

import java.io.InputStream;
import java.util.Properties;

import org.ha.cd2.isg.blazeds.client.retrieval.ExportImageClient;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class API_01_001_BZ extends API_01_001 {

	@Autowired
	ExportImageClient exportImageClient;
	
	@Value("${bz.credential}")
	String credential;
	
	@Before
	public void prepareTest() throws Exception {
		
		changeCredentials("cid2user:abcd3###");
	}
	
	@After
	public void afterTest() throws Exception {
		changeCredentials(credential);
		
	}
	
	private void changeCredentials(String credential){
		exportImageClient.setCredential(credential);
	}
}
