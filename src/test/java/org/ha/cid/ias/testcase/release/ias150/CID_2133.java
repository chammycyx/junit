package org.ha.cid.ias.testcase.release.ias150;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.List;
import java.util.UUID;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. upload 1 metadata
 * 3. validate patient DOB in audit logs
 * 
 * Refer to CID-2133
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:ws-config-context.xml",
		"classpath:internal-context.xml"
})
public class CID_2133 {
	
	@Autowired
	private IasClientService iasService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	
	String[] versions = { "1", "2" };
	
	String[] imageHandlings = {	"N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1 };
	
	CIDData cidData = null;
	
	Integer result = null;
	
	private void assertEsDobFormatMatch() throws Exception {
		esInternalService.setDelay(180);
		List<AuditEventDto> esAuditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		
		for(AuditEventDto audDto : esAuditEventDtos) {
			if("ExtSubImageUpdateFilter".equals(audDto.getEvent())) {
				Assert.assertEquals("19460101", audDto.getPatDob());
				break;
			}
		}
	}

	private void assertRrDobFormatMatch() throws Exception {
		rrInternalService.setDelay(180);
		List<AuditEventDto> rrAuditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), "ExtSubImageUpdateFilter");
		AuditEventDto audDto = rrAuditEventDtos.get(0);
		Assert.assertEquals("19460101", audDto.getPatDob());
	}
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType("ES"); // RTC
	}
	
	@Test
	public void uploadMetadata_Upload1ImageWithTimestampDob_ShouldAuditEventDobMatchFormat() throws Exception {
		cidData.getPatientDtl().setPatDOB("19460101000000.000");
		iasService.uploadMetaData(cidData);
		assertEsDobFormatMatch();
		assertRrDobFormatMatch();
	}
	
	@Test
	public void uploadMetadata_Upload1ImageWithTimestampShortDob_ShouldAuditEventDobMatchFormat() throws Exception {
		cidData.getPatientDtl().setPatDOB("19460101000000");
		iasService.uploadMetaData(cidData);
		assertEsDobFormatMatch();
		assertRrDobFormatMatch();
	}
	
	@Test
	public void uploadMetadata_Upload1ImageWithYyyymmddDob_ShouldAuditEventDobMatchFormat() throws Exception {
		cidData.getPatientDtl().setPatDOB("19460101");
		iasService.uploadMetaData(cidData);
		assertEsDobFormatMatch();
		assertRrDobFormatMatch();
	}
}
