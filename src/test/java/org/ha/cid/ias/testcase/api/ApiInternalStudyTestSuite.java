package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.deleteStudy.internal.study.STUDY_11_001_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.internal.study.STUDY_11_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.internal.study.STUDY_12_001_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001a_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001a_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001a_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001b_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001b_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_001b_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005a_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005a_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005a_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005b_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005b_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.study.STUDY_03_005b_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({

	// Blaze DS
	STUDY_03_001a_BZ.class,
	STUDY_03_001b_BZ.class,
	STUDY_03_005a_BZ.class,
	STUDY_03_005b_BZ.class,
	STUDY_11_001_BZ.class,
	
	// Web Service
	STUDY_03_001a_WS.class,
	STUDY_03_001b_WS.class,
	STUDY_03_005a_WS.class,
	STUDY_03_005b_WS.class,
	STUDY_11_001_WS.class,
	STUDY_12_001_WS.class,
	
	//RS
	STUDY_03_001a_RS.class,
	STUDY_03_001b_RS.class,
	STUDY_03_005a_RS.class,
	STUDY_03_005b_RS.class,
})
public class ApiInternalStudyTestSuite {

}
