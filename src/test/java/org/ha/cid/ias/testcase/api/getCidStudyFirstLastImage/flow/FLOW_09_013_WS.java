/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 version)
 * 2. add annotation and upload metadata
 * 3. getCidStudyFirstLastImage should return HA7 (each image has 3 versions)
 * 
 * @author CFT545
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class FLOW_09_013_WS extends FLOW_09_013 {
}
