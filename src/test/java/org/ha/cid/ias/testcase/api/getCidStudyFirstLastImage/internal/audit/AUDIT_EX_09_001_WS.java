/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. getCidStudyFirstLastImage of a study which does not exist
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_EX_09_001_WS extends AUDIT_EX_09_001 {
}
