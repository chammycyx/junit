package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. upload 1 metadata
 * 3. delete 2nd image
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_03_003b {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG"
	};
	
	String[] versions = {	
			"1", "2",
			"1", "2",
			"1", "2"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	
			imageID1, imageID1, 
			imageID2, imageID2, 
			imageID3, imageID3
	};
	
	String[] imageHandlings = {	
			"N", "N",
			"N", "N",
			"N", "N"
	};
	
	String deleteRemark = "Delete remark";
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType("ES"); // RTC
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void uploadMetadata_Delete2ndImage_ShouldSuccess() throws Exception {
		try {
			cidData = iasService.deleteImages(cidData, new String[] { imageID2 }, imageFiles, deleteRemark);
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected", false);
		}
	}
}
