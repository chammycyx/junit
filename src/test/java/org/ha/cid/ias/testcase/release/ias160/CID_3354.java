package org.ha.cid.ias.testcase.release.ias160;

import org.ha.cid.ias.testcase.api.deleteStudy.internal.study.STUDY_11_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.internal.study.STUDY_12_001_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_001_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_003_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_004_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.basic.API_EX_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.cn.FLOW_CN_EX_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic.API_EX_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn.FLOW_CN_EX_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.cn.FLOW_CN_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_003_WS;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_001;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_002;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_003;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_004;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_005;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_006;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_007;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_008;
import org.ha.cid.ias.testcase.backend.esb.FLOW_ESB_13_009;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	
	
	
	/* Web Service */
	// ApiFlowVSTestSuite
	API_EX_05_002_WS.class,
	API_EX_07_002_WS.class,
	FLOW_EX_04_003_WS.class,
	FLOW_EX_04_004_WS.class,
	FLOW_EX_05_001_WS.class,
	FLOW_EX_07_001_WS.class,
	FLOW_CN_04_002_WS.class,
	FLOW_CN_05_002_WS.class,
	FLOW_SN_05_002_WS.class,
	FLOW_CN_07_002_WS.class,
	FLOW_SN_07_002_WS.class,
	FLOW_CN_06_002_WS.class,
	FLOW_SN_06_002_WS.class,
	FLOW_VS_EX_08_003_WS.class,
	FLOW_VS_EX_09_003_WS.class,
	
	/* Others need changes */
	FLOW_EX_04_001_WS.class,
	FLOW_CN_EX_05_001_WS.class,
	FLOW_CN_EX_05_006_WS.class,
	FLOW_CN_EX_07_006_WS.class,
	STUDY_11_001_WS.class,
	STUDY_12_001_WS.class,
	AUDIT_EX_05_001_WS.class,
	AUDIT_EX_07_001_WS.class,
	AUDIT_CN_EX_05_001_WS.class,
	AUDIT_CN_EX_05_005_WS.class,
	FLOW_SN_04_002_WS.class,
//	FLOW_ESB_13_001.class,
//	FLOW_ESB_13_002.class,
//	FLOW_ESB_13_003.class,
//	FLOW_ESB_13_004.class,
//	FLOW_ESB_13_005.class,
//	FLOW_ESB_13_006.class,
//	FLOW_ESB_13_007.class,
//	FLOW_ESB_13_008.class,
//	FLOW_ESB_13_009.class,



})
public class CID_3354 {

}
