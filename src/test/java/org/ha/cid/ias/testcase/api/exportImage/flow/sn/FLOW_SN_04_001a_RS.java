/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.flow.sn;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Remove the study from ES and CN
 * 3. export all images (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml",
})
public class FLOW_SN_04_001a_RS extends FLOW_SN_04_001a {
}
