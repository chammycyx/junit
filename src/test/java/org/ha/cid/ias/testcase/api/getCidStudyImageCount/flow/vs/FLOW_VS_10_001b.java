package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. remove the study from ES
 * 3. getCidStudyImageCount should return 1 (each image has 2 versions)
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_VS_10_001b {
	
	@Autowired
	private IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService internalService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	
	String[] versions = { "1", "2" };
	
	String[] imageHandlings = {	"N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);

		internalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
	}
	
	@Test
	public void getCidStudyImageCount_Upload1ImageFromRR_ShouldReturn1() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, false);
		
		Assert.assertEquals(1, imageCount);
	}

	@Test
	public void getCidStudyImageCount_Upload1ImagePrintedCountFromRR_ShouldReturn0() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, true);
		
		Assert.assertEquals(0, imageCount);
	}
}
