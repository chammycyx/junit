package org.ha.cid.ias.testcase.api.exportImage.flow;

import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. export 2nd image (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_04_003 {

	@Autowired
	protected IasClientService iasService;
	
	@Autowired
	protected ResourceService resourceService;

	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};
	
	String[] expectedimagesFiles = { 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG"
	};
	
	String[] versions = {	"1", "2", "3",
				"1", "2", "3",
				"1", "2", "3"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	imageID1, imageID1, imageID1,
				imageID2, imageID2, imageID2,
				imageID3, imageID3, imageID3
	};
	
	String[] imageHandlings = {	"N", "N", "N",  
					"N", "N", "N", 
					"N", "N", "N" 
	};
	
	String exportPath = "/blazeds/flow/exportImage.7z";
	
	String password = "password";
	
	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void exportImage_UploadExport2ndImage_ShouldSuccess() throws Exception {
		
		// Remove image 1 and image 3 from CID data
		ImageDtls imageDtls = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls();
		Iterator<ImageDtl> iter = imageDtls.getImageDtl().iterator();
		while(iter.hasNext()) {
			ImageDtl imageDtl = iter.next();
			if(imageID1.equals(imageDtl.getImageID()) || imageID3.equals(imageDtl.getImageID())) {
				iter.remove();
			}
		}
		
		byte[] result = iasService.exportImage(Ha7Util.convertToHa7xml(cidData), password);
		
		for(int i = 0; i < expectedimagesFiles.length; i++) {
			byte[] resultImage = resourceService.extractZipFile(exportPath, result, imageDtls.getImageDtl().get(i).getImageFile(), password);
			byte[] expectedImage = null;
			try {
				expectedImage = IOUtils.toByteArray(getClass().getResourceAsStream(expectedimagesFiles[i]));
			} catch (IOException e) {
				Assert.assertTrue(false);
				e.printStackTrace();
			}
			Assert.assertArrayEquals(expectedImage, resultImage);
		}
		
	}
}
