package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_11_001_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_11_001_WS;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_EX_11_001_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_EX_11_001_WS;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_EX_11_002_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_EX_11_002_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.flow.FLOW_12_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.flow.FLOW_EX_12_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.flow.FLOW_EX_12_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001a_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001a_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001a_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001b_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001b_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_001b_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_002_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_002_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_003_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_003_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_003_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_004_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_004_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_04_004_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_001_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_001_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_001_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_002_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_002_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_002_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_003_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_003_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_003_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_004_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_004_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_004_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_005_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_005_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.FLOW_EX_04_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_003_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_004_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_005_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_006_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_007_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_007_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_007_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_008_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_008_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_008_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_003_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_004_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_005_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_006_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_EX_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_003_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_004_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_004_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_005_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_005_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_006_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_007_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_007_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_007_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_008_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_008_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_07_008_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_003_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_004_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_004_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_005_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_005_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_006_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.FLOW_EX_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_001_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_002_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_002_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_003_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_003_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_003_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_004_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_004_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_004_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_005_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_005_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_005_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_006_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_006_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_006_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_007_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_007_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_007_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_008_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_008_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_06_008_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_001_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_002_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_002_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_003_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_003_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_003_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_004_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_004_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_004_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_005_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_005_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.FLOW_EX_06_005_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_004b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_005b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_006b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_007_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_007_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_007_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_008_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_008_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_008_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_009_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_009_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_009_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_010_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_010_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_010_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_011_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_011_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_011_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_012_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_012_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_012_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_013_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_013_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_013_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_014_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_014_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_014_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_015_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_015_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_08_015_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_003_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_003_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_004_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_004_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_004_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_004b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_005b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_006b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_007_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_007_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_007_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_008_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_008_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_008_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_009_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_009_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_009_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_010_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_010_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_010_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_011_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_011_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_011_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_012_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_012_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_012_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_013_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_013_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_013_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_014_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_014_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_014_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_015_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_015_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_09_015_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_003_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_003_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_004_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_004_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_004_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_005_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_005_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_005_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_004b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_005b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_006_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_006_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_006_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_007_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_007_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_007_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_008_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_008_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_008_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_009_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_009_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_009_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_010_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_010_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_010_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_011_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_011_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_10_011_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_003_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_003_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.FLOW_EX_10_003_WS;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_001_BZ;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_001_RS;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_001_WS;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_002_BZ;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_002_RS;
import org.ha.cid.ias.testcase.api.uploadImage.flow.FLOW_02_002_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.flow.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	
	// Blaze DS
	FLOW_02_001_BZ.class,
	FLOW_02_002_BZ.class,
	FLOW_03_001a_BZ.class,
	FLOW_03_001b_BZ.class,
	FLOW_03_002a_BZ.class,
	FLOW_03_002b_BZ.class,
	FLOW_03_003a_BZ.class,
	FLOW_03_003b_BZ.class,
	FLOW_03_004a_BZ.class,
	FLOW_03_004b_BZ.class,
	FLOW_03_005a_BZ.class,
	FLOW_03_005b_BZ.class,
	FLOW_03_006a_BZ.class,
	FLOW_03_006b_BZ.class,
	FLOW_03_007a_BZ.class,
	FLOW_03_007b_BZ.class,
	FLOW_03_008_BZ.class,
	FLOW_03_009_BZ.class,
	FLOW_03_010_BZ.class,
	FLOW_03_011_BZ.class,
	FLOW_03_012_BZ.class,
	FLOW_03_013_BZ.class,
	FLOW_03_014_BZ.class,
	FLOW_03_015_BZ.class,
	FLOW_03_016_BZ.class,
	FLOW_04_001a_BZ.class,
	FLOW_04_001b_BZ.class,
	FLOW_04_002_BZ.class,
	FLOW_04_003_BZ.class,
	FLOW_04_004_BZ.class,
	FLOW_05_001_BZ.class,
	FLOW_05_002_BZ.class,
	FLOW_05_003_BZ.class,
	FLOW_05_004_BZ.class,
	FLOW_05_005_BZ.class,
	FLOW_05_006_BZ.class,
	FLOW_05_007_BZ.class,
	FLOW_05_008_BZ.class,
	FLOW_06_001_BZ.class,
	FLOW_06_002_BZ.class,
	FLOW_06_003_BZ.class,
	FLOW_06_004_BZ.class,
	FLOW_06_005_BZ.class,
	FLOW_06_006_BZ.class,
	FLOW_06_007_BZ.class,
	FLOW_06_008_BZ.class,
	FLOW_07_001_BZ.class,
	FLOW_07_002_BZ.class,
	FLOW_07_003_BZ.class,
	FLOW_07_004_BZ.class,
	FLOW_07_005_BZ.class,
	FLOW_07_006_BZ.class,
	FLOW_07_007_BZ.class,
	FLOW_07_008_BZ.class,
	FLOW_08_001a_BZ.class,
	FLOW_08_001b_BZ.class,
	FLOW_08_002a_BZ.class,
	FLOW_08_002b_BZ.class,
	FLOW_08_003a_BZ.class,
	FLOW_08_003b_BZ.class,
	FLOW_08_004a_BZ.class,
	FLOW_08_004b_BZ.class,
	FLOW_08_005a_BZ.class,
	FLOW_08_005b_BZ.class,
	FLOW_08_006a_BZ.class,
	FLOW_08_006b_BZ.class,
	FLOW_08_007_BZ.class,
	FLOW_08_008_BZ.class,
	FLOW_08_009_BZ.class,
	FLOW_08_010_BZ.class,
	FLOW_08_011_BZ.class,
	FLOW_08_012_BZ.class,
	FLOW_08_013_BZ.class,
	FLOW_08_014_BZ.class,
	FLOW_08_015_BZ.class,
	FLOW_09_001a_BZ.class,
	FLOW_09_001b_BZ.class,
	FLOW_09_002a_BZ.class,
	FLOW_09_002b_BZ.class,
	FLOW_09_003a_BZ.class,
	FLOW_09_003b_BZ.class,
	FLOW_09_004a_BZ.class,
	FLOW_09_004b_BZ.class,
	FLOW_09_005a_BZ.class,
	FLOW_09_005b_BZ.class,
	FLOW_09_006a_BZ.class,
	FLOW_09_006b_BZ.class,
	FLOW_09_007_BZ.class,
	FLOW_09_008_BZ.class,
	FLOW_09_009_BZ.class,
	FLOW_09_010_BZ.class,
	FLOW_09_011_BZ.class,
	FLOW_09_012_BZ.class,
	FLOW_09_013_BZ.class,
	FLOW_09_014_BZ.class,
	FLOW_09_015_BZ.class,
	FLOW_10_001a_BZ.class,
	FLOW_10_001b_BZ.class,
	FLOW_10_002a_BZ.class,
	FLOW_10_002b_BZ.class,
	FLOW_10_003a_BZ.class,
	FLOW_10_003b_BZ.class,
	FLOW_10_004a_BZ.class,
	FLOW_10_004b_BZ.class,
	FLOW_10_005a_BZ.class,
	FLOW_10_005b_BZ.class,
	FLOW_10_006_BZ.class,
	FLOW_10_007_BZ.class,
	FLOW_10_008_BZ.class,
	FLOW_10_009_BZ.class,
	FLOW_10_010_BZ.class,
	FLOW_10_011_BZ.class,
	FLOW_11_001_BZ.class,
	FLOW_EX_03_001_BZ.class,
	FLOW_EX_03_002_BZ.class,
	FLOW_EX_03_003_BZ.class,
	FLOW_EX_03_004_BZ.class,
	FLOW_EX_03_005_BZ.class,
	FLOW_EX_03_006_BZ.class,
	FLOW_EX_03_007_BZ.class,
	FLOW_EX_03_008_BZ.class,
	FLOW_EX_03_009_BZ.class,
	FLOW_EX_03_010_BZ.class,
	FLOW_EX_03_011_BZ.class,
	FLOW_EX_03_012_BZ.class,
	FLOW_EX_03_013a_BZ.class,
	FLOW_EX_03_013b_BZ.class,
	FLOW_EX_03_014_BZ.class,
	FLOW_EX_03_015_BZ.class,
	FLOW_EX_04_001_BZ.class,
	FLOW_EX_04_002_BZ.class,
	FLOW_EX_04_003_BZ.class,
	FLOW_EX_04_004_BZ.class,
	FLOW_EX_04_005_BZ.class,
	FLOW_EX_05_001_BZ.class,
	FLOW_EX_05_002_BZ.class,
	FLOW_EX_05_003_BZ.class,
	FLOW_EX_05_004_BZ.class,
	FLOW_EX_05_005_BZ.class,
	FLOW_EX_05_006_BZ.class,
	FLOW_EX_06_001_BZ.class,
	FLOW_EX_06_002_BZ.class,
	FLOW_EX_06_003_BZ.class,
	FLOW_EX_06_004_BZ.class,
	FLOW_EX_06_005_BZ.class,
	FLOW_EX_07_001_BZ.class,
	FLOW_EX_07_002_BZ.class,
	FLOW_EX_07_003_BZ.class,
	FLOW_EX_07_004_BZ.class,
	FLOW_EX_07_005_BZ.class,
	FLOW_EX_07_006_BZ.class,
	FLOW_EX_08_001_BZ.class,
	FLOW_EX_08_002_BZ.class,
	FLOW_EX_08_003_BZ.class,
	FLOW_EX_08_004_BZ.class, //need to turn on PAS
	FLOW_EX_09_001_BZ.class,
	FLOW_EX_09_002_BZ.class,
	FLOW_EX_09_003_BZ.class,
	FLOW_EX_09_004_BZ.class, //need to turn on PAS
	FLOW_EX_09_005_BZ.class,
	FLOW_EX_10_001_BZ.class,
	FLOW_EX_10_002_BZ.class,
	FLOW_EX_10_003_BZ.class,
	FLOW_EX_11_001_BZ.class,
	FLOW_EX_11_002_BZ.class,
	

	// Web Service
	FLOW_02_001_WS.class,
	FLOW_02_002_WS.class,
	FLOW_03_001a_WS.class,
	FLOW_03_001b_WS.class,
	FLOW_03_002a_WS.class,
	FLOW_03_002b_WS.class,
	FLOW_03_003a_WS.class,
	FLOW_03_003b_WS.class,
	FLOW_03_004a_WS.class,
	FLOW_03_004b_WS.class,
	FLOW_03_005a_WS.class,
	FLOW_03_005b_WS.class,
	FLOW_03_006a_WS.class,
	FLOW_03_006b_WS.class,
	FLOW_03_007a_WS.class,
	FLOW_03_007b_WS.class,
	FLOW_03_008_WS.class,
	FLOW_03_009_WS.class,
	FLOW_03_010_WS.class,
	FLOW_03_011_WS.class,
	FLOW_03_012_WS.class,
	FLOW_03_013_WS.class,
	FLOW_03_014_WS.class,
	FLOW_03_015_WS.class,
	FLOW_03_016_WS.class,
	FLOW_04_001a_WS.class,
	FLOW_04_001b_WS.class,
	FLOW_04_002_WS.class,
	FLOW_04_003_WS.class,
	FLOW_04_004_WS.class,
	FLOW_05_001_WS.class,
	FLOW_05_002_WS.class,
	FLOW_05_003_WS.class,
	FLOW_05_004_WS.class,
	FLOW_05_005_WS.class,
	FLOW_05_006_WS.class,
	FLOW_05_007_WS.class,
	FLOW_05_008_WS.class,
	FLOW_06_001_WS.class,
	FLOW_06_002_WS.class,
	FLOW_06_003_WS.class,
	FLOW_06_004_WS.class,
	FLOW_06_005_WS.class,
	FLOW_06_006_WS.class,
	FLOW_06_007_WS.class,
	FLOW_06_008_WS.class,
	FLOW_07_001_WS.class,
	FLOW_07_002_WS.class,
	FLOW_07_003_WS.class,
	FLOW_07_004_WS.class,
	FLOW_07_005_WS.class,
	FLOW_07_006_WS.class,
	FLOW_07_007_WS.class,
	FLOW_07_008_WS.class,
	FLOW_08_001a_WS.class,
	FLOW_08_001b_WS.class,
	FLOW_08_002a_WS.class,
	FLOW_08_002b_WS.class,
	FLOW_08_003a_WS.class,
	FLOW_08_003b_WS.class,
	FLOW_08_004a_WS.class,
	FLOW_08_004b_WS.class,
	FLOW_08_005a_WS.class,
	FLOW_08_005b_WS.class,
	FLOW_08_006a_WS.class,
	FLOW_08_006b_WS.class,
	FLOW_08_007_WS.class,
	FLOW_08_008_WS.class,
	FLOW_08_009_WS.class,
	FLOW_08_010_WS.class,
	FLOW_08_011_WS.class,
	FLOW_08_012_WS.class,
	FLOW_08_013_WS.class,
	FLOW_08_014_WS.class,
	FLOW_08_015_WS.class,
	FLOW_09_001a_WS.class,
	FLOW_09_001b_WS.class,
	FLOW_09_002a_WS.class,
	FLOW_09_002b_WS.class,
	FLOW_09_003a_WS.class,
	FLOW_09_003b_WS.class,
	FLOW_09_004a_WS.class,
	FLOW_09_004b_WS.class,
	FLOW_09_005a_WS.class,
	FLOW_09_005b_WS.class,
	FLOW_09_006a_WS.class,
	FLOW_09_006b_WS.class,
	FLOW_09_007_WS.class,
	FLOW_09_008_WS.class,
	FLOW_09_009_WS.class,
	FLOW_09_010_WS.class,
	FLOW_09_011_WS.class,
	FLOW_09_012_WS.class,
	FLOW_09_013_WS.class,
	FLOW_09_014_WS.class,
	FLOW_09_015_WS.class,
	FLOW_10_001a_WS.class,
	FLOW_10_001b_WS.class,
	FLOW_10_002a_WS.class,
	FLOW_10_002b_WS.class,
	FLOW_10_003a_WS.class,
	FLOW_10_003b_WS.class,
	FLOW_10_004a_WS.class,
	FLOW_10_004b_WS.class,
	FLOW_10_005a_WS.class,
	FLOW_10_005b_WS.class,
	FLOW_10_006_WS.class,
	FLOW_10_007_WS.class,
	FLOW_10_008_WS.class,
	FLOW_10_009_WS.class,
	FLOW_10_010_WS.class,
	FLOW_10_011_WS.class,
	FLOW_11_001_WS.class,
	FLOW_12_001_WS.class,
	FLOW_EX_03_001_WS.class,
	FLOW_EX_03_002_WS.class,
	FLOW_EX_03_003_WS.class,
	FLOW_EX_03_004_WS.class,
	FLOW_EX_03_005_WS.class,
	FLOW_EX_03_006_WS.class,
	FLOW_EX_03_007_WS.class,
	FLOW_EX_03_008_WS.class,
	FLOW_EX_03_009_WS.class,
	FLOW_EX_03_010_WS.class,
	FLOW_EX_03_011_WS.class,
	FLOW_EX_03_012_WS.class,
	FLOW_EX_03_013a_WS.class,
	FLOW_EX_03_013b_WS.class,
	FLOW_EX_03_014_WS.class,
	FLOW_EX_03_015_WS.class,
	FLOW_EX_04_001_WS.class,
	FLOW_EX_04_002_WS.class,
	FLOW_EX_04_003_WS.class,
	FLOW_EX_04_004_WS.class,
	FLOW_EX_04_005_WS.class,
	FLOW_EX_05_001_WS.class,
	FLOW_EX_05_002_WS.class,
	FLOW_EX_05_003_WS.class,
	FLOW_EX_05_004_WS.class,
	FLOW_EX_05_005_WS.class,
	FLOW_EX_05_006_WS.class,
	FLOW_EX_06_001_WS.class,
	FLOW_EX_06_002_WS.class,
	FLOW_EX_06_003_WS.class,
	FLOW_EX_06_004_WS.class,
	FLOW_EX_06_005_WS.class,
	FLOW_EX_07_001_WS.class,
	FLOW_EX_07_002_WS.class,
	FLOW_EX_07_003_WS.class,
	FLOW_EX_07_004_WS.class,
	FLOW_EX_07_005_WS.class,
	FLOW_EX_07_006_WS.class,
	FLOW_EX_08_001_WS.class,
	FLOW_EX_08_002_WS.class,
	FLOW_EX_08_003_WS.class,
	FLOW_EX_08_004_WS.class, //need turn on PAS
	FLOW_EX_09_001_WS.class,
	FLOW_EX_09_002_WS.class,
	FLOW_EX_09_003_WS.class,
	FLOW_EX_09_004_WS.class, //need turn on PAS
	FLOW_EX_09_005_WS.class,
	FLOW_EX_10_001_WS.class,
	FLOW_EX_10_002_WS.class,
	FLOW_EX_10_003_WS.class,
	FLOW_EX_11_001_WS.class,
	FLOW_EX_11_002_WS.class,
	FLOW_EX_12_001_WS.class,
	FLOW_EX_12_002_WS.class,

	
	// Restful 
	FLOW_02_001_RS.class,
	FLOW_02_002_RS.class,
	FLOW_03_001a_RS.class,
	FLOW_03_001b_RS.class,
	FLOW_03_002a_RS.class,
	FLOW_03_002b_RS.class,
	FLOW_03_003a_RS.class,
	FLOW_03_003b_RS.class,
	FLOW_03_004a_RS.class,
	FLOW_03_004b_RS.class,
	FLOW_03_005a_RS.class,
	FLOW_03_005b_RS.class,
	FLOW_03_006a_RS.class,
	FLOW_03_006b_RS.class,
	FLOW_03_007a_RS.class,
	FLOW_03_007b_RS.class,
	FLOW_03_008_RS.class,
	FLOW_03_009_RS.class,
	FLOW_03_010_RS.class,
	FLOW_03_011_RS.class,
	FLOW_03_012_RS.class,
	FLOW_03_013_RS.class,
	FLOW_03_014_RS.class,
	FLOW_03_015_RS.class,
	FLOW_03_016_RS.class,
	FLOW_04_001a_RS.class,
	FLOW_04_001b_RS.class,
	FLOW_04_002_RS.class,
	FLOW_04_003_RS.class,
	FLOW_04_004_RS.class,
	FLOW_05_001_RS.class,
	FLOW_05_002_RS.class,
	FLOW_05_003_RS.class,
	FLOW_05_004_RS.class,
	FLOW_05_005_RS.class,
	FLOW_05_006_RS.class,
	FLOW_05_007_RS.class,
	FLOW_05_008_RS.class,
	FLOW_06_001_RS.class,
	FLOW_06_002_RS.class,
	FLOW_06_003_RS.class,
	FLOW_06_004_RS.class,
	FLOW_06_005_RS.class,
	FLOW_06_006_RS.class,
	FLOW_06_007_RS.class,
	FLOW_06_008_RS.class,
	FLOW_07_001_RS.class,
	FLOW_07_002_RS.class,
	FLOW_07_003_RS.class,
	FLOW_07_004_RS.class,
	FLOW_07_005_RS.class,
	FLOW_07_006_RS.class,
	FLOW_07_007_RS.class,
	FLOW_07_008_RS.class,
	FLOW_08_001a_RS.class,
	FLOW_08_001b_RS.class,
	FLOW_08_002a_RS.class,
	FLOW_08_002b_RS.class,
	FLOW_08_003a_RS.class,
	FLOW_08_003b_RS.class,
	FLOW_08_004a_RS.class,
	FLOW_08_004b_RS.class,
	FLOW_08_005a_RS.class,
	FLOW_08_005b_RS.class,
	FLOW_08_006a_RS.class,
	FLOW_08_006b_RS.class,
	FLOW_08_007_RS.class,
	FLOW_08_008_RS.class,
	FLOW_08_009_RS.class,
	FLOW_08_010_RS.class,
	FLOW_08_011_RS.class,
	FLOW_08_012_RS.class,
	FLOW_08_013_RS.class,
	FLOW_08_014_RS.class,
	FLOW_08_015_RS.class,
	FLOW_09_001a_RS.class,
	FLOW_09_001b_RS.class,
	FLOW_09_002a_RS.class,
	FLOW_09_002b_RS.class,
	FLOW_09_003a_RS.class,
	FLOW_09_003b_RS.class,
	FLOW_09_004a_RS.class,
	FLOW_09_004b_RS.class,
	FLOW_09_005a_RS.class,
	FLOW_09_005b_RS.class,
	FLOW_09_006a_RS.class,
	FLOW_09_006b_RS.class,
	FLOW_09_007_RS.class,
	FLOW_09_008_RS.class,
	FLOW_09_009_RS.class,
	FLOW_09_010_RS.class,
	FLOW_09_011_RS.class,
	FLOW_09_012_RS.class,
	FLOW_09_013_RS.class,
	FLOW_09_014_RS.class,
	FLOW_09_015_RS.class,
	FLOW_10_001a_RS.class,
	FLOW_10_001b_RS.class,
	FLOW_10_002a_RS.class,
	FLOW_10_002b_RS.class,
	FLOW_10_003a_RS.class,
	FLOW_10_003b_RS.class,
	FLOW_10_004a_RS.class,
	FLOW_10_004b_RS.class,
	FLOW_10_005a_RS.class,
	FLOW_10_005b_RS.class,
	FLOW_10_006_RS.class,
	FLOW_10_007_RS.class,
	FLOW_10_008_RS.class,
	FLOW_10_009_RS.class,
	FLOW_10_010_RS.class,
	FLOW_10_011_RS.class,
	FLOW_EX_03_001_RS.class,
	FLOW_EX_03_002_RS.class,
	FLOW_EX_03_003_RS.class,
	FLOW_EX_03_004_RS.class,
	FLOW_EX_03_005_RS.class,
	FLOW_EX_03_006_RS.class,
	FLOW_EX_03_007_RS.class,
	FLOW_EX_03_008_RS.class,
	FLOW_EX_03_009_RS.class,
	FLOW_EX_03_010_RS.class,
	FLOW_EX_03_011_RS.class,
	FLOW_EX_03_012_RS.class,
	FLOW_EX_03_013a_RS.class,
	FLOW_EX_03_013b_RS.class,
	FLOW_EX_03_014_RS.class,
	FLOW_EX_03_015_RS.class,
	FLOW_EX_04_001_RS.class,
	FLOW_EX_04_002_RS.class,
	FLOW_EX_04_003_RS.class,
	FLOW_EX_04_004_RS.class,
	FLOW_EX_04_005_RS.class,
	FLOW_EX_05_001_RS.class,
	FLOW_EX_05_002_RS.class,
	FLOW_EX_05_003_RS.class,
	FLOW_EX_05_004_RS.class,
	FLOW_EX_05_005_RS.class,
	FLOW_EX_05_006_RS.class,
	FLOW_EX_06_001_RS.class,
	FLOW_EX_06_002_RS.class,
	FLOW_EX_06_003_RS.class,
	FLOW_EX_06_004_RS.class,
	FLOW_EX_06_005_RS.class,
	FLOW_EX_07_001_RS.class,
	FLOW_EX_07_002_RS.class,
	FLOW_EX_07_003_RS.class,
	FLOW_EX_07_004_RS.class,
	FLOW_EX_07_005_RS.class,
	FLOW_EX_07_006_RS.class,
	FLOW_EX_08_001_RS.class,
	FLOW_EX_08_002_RS.class,
	FLOW_EX_08_003_RS.class,
	FLOW_EX_08_004_RS.class, //need turn on PAS
	FLOW_EX_09_001_RS.class,
	FLOW_EX_09_002_RS.class,
	FLOW_EX_09_003_RS.class,
	FLOW_EX_09_004_RS.class, //need turn on PAS
	FLOW_EX_09_005_RS.class,
	FLOW_EX_10_001_RS.class,
	FLOW_EX_10_002_RS.class,
	FLOW_EX_10_003_RS.class
	
})
public class ApiFlowTestSuite {

}
