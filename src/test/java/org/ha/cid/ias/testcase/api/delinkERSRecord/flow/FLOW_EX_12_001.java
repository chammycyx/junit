/**
 * 
 */
package org.ha.cid.ias.testcase.api.delinkERSRecord.flow;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This test case intends to validate the following items:
 * 1. Delete a study which does not exist
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_EX_12_001 {

	@Autowired
	private IasClientService iasService;

	@Test
	public void deLinkERSRecord_NotExists_ShouldGetFailedAck() throws Exception {
		
		final String accessionNo = "NOT_EXIST_ACCESSION";
		final String studyId = "NOT_EXIST_STUDY";
		
		CIDData cidData = iasService.generateCIDData(SampleData.getAccessionNo(), 1, 3);
		String result = iasService.delinkStudy(cidData, accessionNo, studyId);
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(accessionNo);
		expected.getAckDtl().setStudyID(studyId);
		expected.getAckDtl().setActionStatus("Failed");
		expected.getAckDtl().setActionRemarks("No record(s) found for delink operation");
		
		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
	}
}
