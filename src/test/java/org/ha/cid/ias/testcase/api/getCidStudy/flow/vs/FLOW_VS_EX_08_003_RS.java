package org.ha.cid.ias.testcase.api.getCidStudy.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image with wrong hospital / case number
 * 2. Void the study
 * 3. getCidStudy should return patient not exists
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_EX_08_003_RS extends FLOW_VS_EX_08_003 {

}
