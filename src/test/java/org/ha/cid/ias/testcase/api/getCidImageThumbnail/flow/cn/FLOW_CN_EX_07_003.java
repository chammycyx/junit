/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.cn;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete the study
 * 3. Remove the study from ES
 * 4. Get cid image thumbnail
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_CN_EX_07_003 {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;
	
	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	protected String[] versions = { "1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());

		esInternalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
	}
	
	@Test
	public void getCidImageThumbnail_RetrieveDeletedFromCN_ShouldThrowException() throws Exception {
		
		final String expectedMessage = "Study is mark-deleted in RR";
		
		try {
			iasService.getCidImageThumbnail(cidData, "3", "3", imageID1);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e) {
	         Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
		
	}
}
