package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. update image handling, edit and upload metadata
 * 3. getCidStudy should return HA7 (each image has 2 versions)
 * 
 * @author HSY914
 * 
 */
public abstract class FLOW_08_011 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG" 
	};
	
	String[] versions = {
			"1", "2", 
			"1", "2", 
			"1", "2"
	};
	
	String[] versions2 = {
			"1", "3", 
			"1", "3", 
			"1", "3"
	};
	
	String[] versions3 = {
			"1", "4", 
			"1", "4", 
			"1", "4"
	};
	
	String[] imageHandlings = {
			"N", "N",
			"N", "N",
			"N", "N"
	};

	String[] imageHandlings2 = {
			"N", "Y",
			"N", "N",
			"N", "Y"
	};

	String[] imageHandlings3 = {
			"N", "N",
			"N", "N",
			"N", "Y"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {

		cidData = iasService.uploadImages(
				imageFiles,
				versions,
				imageIDs, 
				SampleData.getAccessionNo(),
				imageHandlings
				);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions2,
				imageIDs,
				imageHandlings2
				);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(
				cidData,
				imageFiles,
				versions3,
				imageIDs,
				imageHandlings3
				);
		iasService.uploadMetaData(cidData);

	}
	
	@Test
	public void getCidStudy_UpdateHandlingEditImage_ShouldSuccess() throws Exception {
		String ha7Result = iasService.getCidStudy(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		
		sortImageDtl(cidData);
		sortImageDtl(cidDataResult);
		
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}
		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
	}
	
	private void sortImageDtl(CIDData cidData) {
		Collections.sort(cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl(), new Comparator<ImageDtl>() {
            @Override
            public int compare(ImageDtl lhs, ImageDtl rhs) {
                return lhs.getImageSequence().compareTo(rhs.getImageSequence());
            }
        });
	}
}


