package org.ha.cid.ias.testcase.api.getCidStudy.basic;

import java.util.Iterator;
import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. getCidStudy with optional parameters
 * 
 * @author YKF491
 *
 */
public abstract class API_08_002 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	protected void ignoreImageDtlsBySeqNVersion(String imageSeq, String versionNo) {
		
		if (imageSeq != null || versionNo != null) {		
			Iterator<ImageDtl> iterator = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
			while(iterator.hasNext()) {
				ImageDtl imageDtl = iterator.next();
				if (imageSeq != null) {
					if (imageDtl.getImageSequence().intValue() == Integer.parseInt(imageSeq)) {
						continue;
					}
				}
				if (versionNo != null) {
					if (imageDtl.getImageVersion().equals(versionNo)) {
						continue;
					}
				}
				
				iterator.remove();
			}
		}
	}
	
	protected void assertHa7(String ha7Result) {
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		Iterator<ImageDtl> iterator = cidDataResult.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			imageDtl.setImagePath("");
		}

		
		Ha7Util.assertCIDDataSkipTransactionDataEqual(cidData, cidDataResult);
		
	}
	
	@Test
	public void getCidStudy_HospCdeNull_ShouldSuccess() throws Exception {
		String hospCde = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				hospCde, 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_HospCdeWrong_ShouldSuccess() throws Exception {
		String hospCde = "ABC";
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				hospCde, 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_CaseNoNull_ShouldSuccess() throws Exception {
		String caseNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				caseNo,
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_CaseNoWrong_ShouldSuccess() throws Exception {
		String caseNo = "NOT_EXIST_CASE_NO";
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				caseNo,
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_WorkstationIdNull_ShouldSuccess() throws Exception {
		String workstationId = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				workstationId, 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_WorkstationIdEmpty_ShouldSuccess() throws Exception {
		String workstationId = "";
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				SampleData.getUserId(), 
				workstationId, 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_SeriesNoNull_ShouldSuccess() throws Exception {
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				seriesNo, 
				null, 
				null, 
				SampleData.getUserId(), 
				SampleData.getWorkstationId(), 
				SampleData.getRequestSystem()
		);

		
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_ImageSeqNotNull_ShouldSuccess() throws Exception {
		String imageSeq = "3";
		String versionNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_VersionNoNotNull_ShouldSuccess() throws Exception {
		String imageSeq = null;
		String versionNo = "3";
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_ImageSeqAndVersionNoNotNull_ShouldSuccess() throws Exception {
		String imageSeq = "3";
		String versionNo = "3";
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_SeriesNoNullAndImageSeqAndVersionNoNotNull_ShouldSuccess() throws Exception {
		String imageSeq = "3";
		String versionNo = "3";
		
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				seriesNo, 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
	
	@Test
	public void getCidStudy_SeriesNoNullAndImageSeqNotNull_ShouldSuccess() throws Exception {
		String imageSeq = "3";
		String versionNo = null;
		
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				seriesNo, 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}

	@Test
	public void getCidStudy_SeriesNoNullAndVersionNoNotNull_ShouldSuccess() throws Exception {
		String imageSeq = null;
		String versionNo = "3";
		
		String seriesNo = null;
		
		String ha7Result = iasService.getCidStudy(
				cidData, 
				seriesNo, 
				imageSeq, 
				versionNo
		);

		ignoreImageDtlsBySeqNVersion(imageSeq, versionNo);
		assertHa7(ha7Result);
		
	}
}
