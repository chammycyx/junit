package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with version 1, 3, 4
 * 3. modify and upload the image with version 1, 4, 5
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_03_005a {

	@Autowired
	protected IasClientService iasService;
	
	@Autowired
	protected ResourceService resourceService;

	protected String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};

	protected String[] imageFiles2 = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v3.JPG", "/blazeds/flow/OGD1_v4.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v3.JPG", "/blazeds/flow/OGD2_v4.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v3.JPG", "/blazeds/flow/OGD3_v4.JPG"
	};
	
	protected String[] imageFiles3 = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v4.JPG", "/blazeds/flow/OGD1_v5.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v4.JPG", "/blazeds/flow/OGD2_v5.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v4.JPG", "/blazeds/flow/OGD3_v5.JPG"
	};
	
	protected String[] expectedimagesFiles = { 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v4.JPG", "/blazeds/flow/OGD3_v5.JPG"
	};
	
	protected String[] versions = {	
			"1", "2", "3",
			"1", "2", "3",
			"1", "2", "3"
	};
	protected String[] versions2 = {	
			"1", "3", "4",
			"1", "3", "4",
			"1", "3", "4"
	};
	protected String[] versions3 = {	
			"1", "4", "5",
			"1", "4", "5",
			"1", "4", "5"
	};
	
	protected String imageID1 = UUID.randomUUID().toString();
	protected String imageID2 = UUID.randomUUID().toString();
	protected String imageID3 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = {	imageID1, imageID1, imageID1,
				imageID2, imageID2, imageID2,
				imageID3, imageID3, imageID3
	};
	
	protected String[] imageHandlings = {	"N", "N", "N",  
					"N", "N", "N", 
					"N", "N", "N" 
	};
	
	protected CIDData cidData = null;
	
	protected Integer result;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(cidData, imageFiles2, versions2, imageIDs, imageHandlings);
		iasService.uploadMetaData(cidData);

		cidData = iasService.reuploadImages(cidData, imageFiles3, versions3, imageIDs, imageHandlings);
		result = iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void uploadMetadata_UploadEditTwice_ShouldSuccess() throws Exception {
		try {
			Assert.assertEquals(0, result.intValue());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected", false);
		}
		
	}
}
