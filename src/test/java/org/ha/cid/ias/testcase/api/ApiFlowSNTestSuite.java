package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001a_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001a_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001a_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001b_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001b_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_001b_WS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_002_BZ;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_002_RS;
import org.ha.cid.ias.testcase.api.exportImage.flow.sn.FLOW_SN_04_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.flow.sn.FLOW_SN_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.flow.sn.FLOW_SN_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_001_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_002_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_002_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.flow.sn.FLOW_SN_06_002_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	
	
	// BlazeDS
	FLOW_SN_04_001a_BZ.class,
	FLOW_SN_04_001b_BZ.class,
	FLOW_SN_04_002_BZ.class,
	FLOW_SN_05_001_BZ.class,
	FLOW_SN_05_002_BZ.class,
	FLOW_SN_07_001_BZ.class,
	FLOW_SN_07_002_BZ.class,
	FLOW_SN_06_001_BZ.class,
	FLOW_SN_06_002_BZ.class,
	
	// Web Service
	FLOW_SN_04_001a_WS.class,
	FLOW_SN_04_001b_WS.class,
	FLOW_SN_04_002_WS.class,
	FLOW_SN_05_001_WS.class,
	FLOW_SN_05_002_WS.class,
	FLOW_SN_07_001_WS.class,
	FLOW_SN_07_002_WS.class,
	FLOW_SN_06_001_WS.class,
	FLOW_SN_06_002_WS.class,
	
	//RS
	FLOW_SN_04_001a_RS.class,
	FLOW_SN_04_001b_RS.class,
	FLOW_SN_04_002_RS.class,
	FLOW_SN_05_001_RS.class,
	FLOW_SN_05_002_RS.class,
	FLOW_SN_07_001_RS.class,
	FLOW_SN_07_002_RS.class,
	FLOW_SN_06_001_RS.class,
	FLOW_SN_06_002_RS.class,
})
public class ApiFlowSNTestSuite {

}
