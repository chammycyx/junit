/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Get cid image of a image which does not exist
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_EX_05_002_RS extends FLOW_EX_05_002 {
}
