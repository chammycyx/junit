/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.basic;

import org.apache.http.client.HttpResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidImage with wrong parameter
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_EX_05_002_RS extends API_EX_05_002 {
	
	@Test
	public void getCidImage_NoParameter_ShouldThrowException() throws Exception {
		try{
			iasService.getCidImage(null, null, null, null);
			Assert.assertTrue("Exception is expected.", false);
		}catch (HttpResponseException e){
			Assert.assertEquals(e.getStatusCode(), 500);
			//Assert.assertTrue(e.getMessage().contains("The parameter accessionNo is mandatory in the getCidImage transaction"));
		}
		
	}
	
}
