/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. getCidStudyFirstLastImage should return HA7 with 1 images (each image has version 1 & 3)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class API_09_001_BZ extends API_09_001 {
}
