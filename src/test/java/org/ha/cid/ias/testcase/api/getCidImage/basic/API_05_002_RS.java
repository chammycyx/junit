/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidImage with optional parameters
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_05_002_RS extends API_05_002 {
}
