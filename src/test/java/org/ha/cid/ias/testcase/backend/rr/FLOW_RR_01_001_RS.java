/**
 * 
 */
package org.ha.cid.ias.testcase.backend.rr;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. update pathindex update_date to (today-89) day and wait for cron job
 * 3. check files and local record exist (should not be removed)
 * 
 * @author CYC014
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_RR_01_001_RS extends FLOW_RR_01_001 {
}
