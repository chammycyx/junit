/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. upload 1 metadata
 * 3. validate ES study
 * 4. validate audit log
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_03_001a_RS extends AUDIT_03_001a {
}
