/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 version)
 * 2. delete 1 image
 * 3. upload 3 images again
 * 
 * @author CFT545
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_EX_03_011_RS extends FLOW_EX_03_011 {
}
