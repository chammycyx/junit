/**
 * 
 */
package org.ha.cid.ias.testcase.api.delinkERSRecord.flow;

import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. delete the study
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_12_001 {

	@Autowired
	protected IasClientService iasService;

	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.PNG", "/blazeds/flow/OGD1_v2.PNG", "/blazeds/flow/OGD1_v3.PNG" };
	
	protected String[] versions = { 	"1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	protected String imageID2 = UUID.randomUUID().toString();
	protected String imageID3 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { 	imageID1, imageID1, imageID1,
							imageID2, imageID2, imageID2,
							imageID3, imageID3, imageID3,
						};
	
	protected CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void deLinkStudy_UploadDelink_ShouldSuccess() throws Exception {
		
		String result = iasService.delinkStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		CIDAck actual = Ha7Util.convertToCIDAck(result);
		
		CIDAck expected = iasService.generateCIDAck();
		expected.getAckDtl().setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		expected.getAckDtl().setStudyID(cidData.getStudyDtl().getStudyID());
		expected.getAckDtl().setStudyDtm(cidData.getStudyDtl().getStudyDtm());
		expected.getAckDtl().setActionStatus("Success");
		expected.getAckDtl().setActionRemarks("1 record(s) deleted");

		Ha7Util.assertCIDAckSkipTransactionDataEqual(expected, actual);
		
	}
}
