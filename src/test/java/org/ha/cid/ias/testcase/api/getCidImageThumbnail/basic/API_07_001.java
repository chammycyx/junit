package org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. Configure thumbnail width to 400
 * 2. upload 1 JPG image (each image has 3 versions)
 * 3. getCidImageThumbnail using seq 3 and ver 3 should return the image thumbnail of seq 3 and ver 3
 * 
 * @author YKF491
 *
 */
public abstract class API_07_001 {
	
	@Autowired
	private IasClientService iasService;
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	String[] versions = { "1", "2", "3" };
	
	String[] imageHandlings = {	"N", "N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	CIDData cidData;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void getCidImageThumnail_UploadRetrieve_ShouldSuccess() throws Exception {
		
		
		byte[] result = iasService.getCidImageThumbnail(cidData, "3", "3", imageID1);
		
		InputStream input = getClass().getResourceAsStream("/blazeds/flow/OGD1_v3_thumbnail.JPG");
		
		try {
			byte[] expectedImage = IOUtils.toByteArray(input);
			input.close();
			Assert.assertArrayEquals(expectedImage, result);
			
		} catch (IOException e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}
}
