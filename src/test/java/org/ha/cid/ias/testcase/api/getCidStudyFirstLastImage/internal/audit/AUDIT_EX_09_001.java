/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.FLOW_EX_09_001;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. getCidStudyFirstLastImage of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_EX_09_001 extends FLOW_EX_09_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Test
	public void getCidStudyFirstLastImage_RetrieveNotExists_AuditShouldMatch() throws Exception {

		final String expectedMessage = "Cannot find the Study";
		final String auditEventName = "getCidStudyFirstLastImage";
		
		try {
			iasService.getCidStudyFirstLastImage(cidData);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception e) {
			// Validate Audit Events
			List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), auditEventName);
			AuditEventDto auditEventDto = auditEventDtos.get(0);
			Assert.assertTrue(auditEventDto.getStatus().equals(AuditEventDto.STATUS_FAILURE));
			Assert.assertTrue(auditEventDto.getResponse().equals(expectedMessage));
			Assert.assertTrue(auditEventDto.getException().contains(expectedMessage));
		}
	}
}
