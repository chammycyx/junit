/**
 * 
 */
package org.ha.cid.ias.testcase.api.exportImage.flow;

import org.apache.http.client.HttpResponseException;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. Export image of a study which does not exist
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_04_001 {

	@Autowired
	private IasClientService iasService;

	@Test
	public void exportImage_RetrieveNotExists_ShouldThrowException() throws Exception {

		final String password = "password";
		
		final String expectedMessage = "Cannot find the Study";
		final String accessionNo = "NOT_EXIST_ACCESSION";
		final String studyId = "NOT_EXIST_STUDY";
		
		try {
			CIDData cidData = iasService.generateCIDData(SampleData.getAccessionNo(), 1, 3);
			cidData.getStudyDtl().setAccessionNo(accessionNo);
			cidData.getStudyDtl().setStudyID(studyId);
			
			iasService.exportImage(Ha7Util.convertToHa7xml(cidData), password);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			//System.out.println("Returned result : " + e.getMessage());
			//System.out.println("Expected result : " + expectedMessage);
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
			//Assert.assertEquals(e.getStatusCode(), 500);
		    //System.out.println("Returned result : " + e.getMessage());
            //System.out.println("Expected result : " + expectedMessage);
            Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
		
	}
}
