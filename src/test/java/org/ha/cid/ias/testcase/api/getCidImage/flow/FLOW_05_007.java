package org.ha.cid.ias.testcase.api.getCidImage.flow;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with updated versions (v1, v(N-1), v(N))
 * 3. repeat above step 9 times until N equals to 12
 * 3. getCidImage with version v12 should return HA7 with 3 images (version v12)
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_05_007 {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	protected ResourceService resourceService;
	
	@Test
	public void getCidImage_Upload3ImagesEditNTimes_ShouldSuccess() throws Exception {
		
		final int uploadCount = 10;
		final int imageCount = 3;
		final int vesionCount = 3;
		String templateFile = "/blazeds/flow/OGD1.JPG";
		String tempFile = "/blazeds/flow/OGD1_temp1.JPG";

		CIDData cidData = iasService.repeatUploadImages(templateFile, tempFile, SampleData.getAccessionNo(), 
				imageCount, vesionCount, uploadCount, "EX_3_VERSIONS");

		byte[] result1 = iasService.getCidImage(cidData, "3", "12", cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(2).getImageID());
		byte[] result2 = iasService.getCidImage(cidData, "6", "12", cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(5).getImageID());
		byte[] result3 = iasService.getCidImage(cidData, "9", "12", cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(8).getImageID());

		byte[] expected1 = resourceService.loadFileAsByteArray("/blazeds/flow/OGD1_image1_v12.JPG");
		byte[] expected2 = resourceService.loadFileAsByteArray("/blazeds/flow/OGD1_image2_v12.JPG");
		byte[] expected3 = resourceService.loadFileAsByteArray("/blazeds/flow/OGD1_image3_v12.JPG");
		
		Assert.assertArrayEquals(expected1, result1);
		Assert.assertArrayEquals(expected2, result2);
		Assert.assertArrayEquals(expected3, result3);
	}
}
