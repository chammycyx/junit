package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. upload 1 metadata
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_03_002b {
	
	@Autowired
	protected IasClientService iasService;
	
	protected String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG"
	};
	
	protected String[] versions = {	
			"1", "2",
			"1", "2",
			"1", "2"
	};
	
	protected String[] imageHandlings = {	
			"N", "N",
			"N", "N",
			"N", "N"
	};
	
	protected String imageID1 = UUID.randomUUID().toString();
	protected String imageID2 = UUID.randomUUID().toString();
	protected String imageID3 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = {	
			imageID1, imageID1,
			imageID2, imageID2,
			imageID3, imageID3
	};
	
	protected CIDData cidData = null;
	

	protected Integer result = null;
	
	@Before
	public void setUp() throws Exception {
		
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType("ES"); // RTC
		result = iasService.uploadMetaData(cidData);

	}
	
	@Test
	public void uploadMetadata_Upload3Images_ShouldReturnZero() throws Exception {
		Assert.assertEquals(0, result.intValue());
	}
}
