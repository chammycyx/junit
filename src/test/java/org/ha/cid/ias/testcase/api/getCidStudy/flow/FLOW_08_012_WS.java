/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 version)
 * 2. update image handling, rearrange and upload metadata
 * 3. getCidStudy should return HA7 (each image has 2 versions)
 * 
 * @author HSY914
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class FLOW_08_012_WS extends FLOW_08_011 {
}
