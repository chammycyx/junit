/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. remove the study from ES
 * 3. modify and upload the image with version 1, 3, 4
 * 4. modify and upload the image with version 1, 4, 5
 * 5. getCidStudyImageCount should return 3 (each image has 2 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class FLOW_VS_10_002b_WS extends FLOW_VS_10_002b {
}
