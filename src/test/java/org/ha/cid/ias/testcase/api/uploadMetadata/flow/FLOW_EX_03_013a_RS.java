/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 version)
 * 2. reupload image 2 times (v1, v4, v5)
 * 3. reupload image (v1, v2, v3)
 * 3. reupload image (v1, v6, v7)
 * 
 * @author YKF491
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class FLOW_EX_03_013a_RS extends FLOW_EX_03_013a {
}
