/**
 * 
 */
package org.ha.cid.ias.testcase.api.deleteStudy.internal.study;

import org.ha.cd2.isg.icw.tool.StudyCompareUtil;
import org.ha.cid.ias.enumeration.StudyStatusEnum;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.FLOW_11_001;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. delete the study
 * 
 * @author YKF491
 *
 */
public abstract class STUDY_11_001 extends FLOW_11_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImageDeleteStudy_ShouldEsStudyAuditMatch() throws Exception {
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate ES Study
		StudyDto studyDto = esInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(studyDto, cidData);
		Assert.assertEquals(studyDto.getStudyStatus(), StudyStatusEnum.DELETED.getLookup());
	}

	@Test
	public void uploadMetadata_uploadImageDeleteStudy_ShouldRrStudyAuditMatch() throws Exception {
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
		
		// Validate RR Study
		StudyDto studyDto = rrInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(studyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(studyDto, cidData);
		Assert.assertEquals(studyDto.getStudyStatus(), StudyStatusEnum.DELETED.getLookup());
	}
	
}
