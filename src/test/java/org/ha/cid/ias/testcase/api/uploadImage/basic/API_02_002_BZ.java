/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload JPG image with different size
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class API_02_002_BZ extends API_02_002 {
}
