/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. getCidStudyFirstLastImage should return HA7 with 1 image (each image has 3 versions)
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_09_002a_BZ extends FLOW_09_002a {
}
