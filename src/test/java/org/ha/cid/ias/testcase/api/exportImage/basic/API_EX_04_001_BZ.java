package org.ha.cid.ias.testcase.api.exportImage.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;


@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class API_EX_04_001_BZ extends API_EX_04_001 {

	public API_EX_04_001_BZ(String fieldName, String ha7File, String password, 
			String userId, String workstationId, String requestSys) {
		super(fieldName, ha7File, password, userId, workstationId, requestSys);
	}
}
