/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImageThumbnail.basic;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Configure thumbnail width to 400
 * 2. upload 1 JPG image (each image has 3 versions)
 * 3. getCidImageThumbnail with optional parameters
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_07_002_RS extends API_07_002 {
}
