package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.deleteStudy.flow.vs.FLOW_VS_11_001_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.flow.vs.FLOW_VS_11_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.flow.vs.FLOW_VS_12_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_08_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_003_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_003_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.vs.FLOW_VS_EX_08_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_09_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_003_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_003_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.flow.vs.FLOW_VS_EX_09_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_001b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_002b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003b_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003b_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_10_003b_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs.FLOW_VS_EX_10_002_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({


	// Blaze DS
	FLOW_VS_08_001a_BZ.class,
	FLOW_VS_08_001b_BZ.class,
	FLOW_VS_08_002a_BZ.class,
	FLOW_VS_08_002b_BZ.class,
	FLOW_VS_08_003a_BZ.class,
	FLOW_VS_08_003b_BZ.class,
	FLOW_VS_09_001a_BZ.class,
	FLOW_VS_09_001b_BZ.class,
	FLOW_VS_09_002a_BZ.class,
	FLOW_VS_09_002b_BZ.class,
	FLOW_VS_09_003a_BZ.class,
	FLOW_VS_09_003b_BZ.class,
	FLOW_VS_10_001a_BZ.class,
	FLOW_VS_10_001b_BZ.class,
	FLOW_VS_10_002a_BZ.class,
	FLOW_VS_10_002b_BZ.class,
	FLOW_VS_10_003a_BZ.class,
	FLOW_VS_10_003b_BZ.class,
	FLOW_VS_11_001_BZ.class,
	FLOW_VS_EX_08_001_BZ.class,
	FLOW_VS_EX_08_002_BZ.class,
	FLOW_VS_EX_08_003_BZ.class,
	FLOW_VS_EX_09_001_BZ.class,
	FLOW_VS_EX_09_002_BZ.class,
	FLOW_VS_EX_09_003_BZ.class,
	FLOW_VS_EX_10_001_BZ.class,
	FLOW_VS_EX_10_002_BZ.class,

	// Web Service
	FLOW_VS_08_001a_WS.class,
	FLOW_VS_08_001b_WS.class,
	FLOW_VS_08_002a_WS.class,
	FLOW_VS_08_002b_WS.class,
	FLOW_VS_08_003a_WS.class,
	FLOW_VS_08_003b_WS.class,
	FLOW_VS_09_001a_WS.class,
	FLOW_VS_09_001b_WS.class,
	FLOW_VS_09_002a_WS.class,
	FLOW_VS_09_002b_WS.class,
	FLOW_VS_09_003a_WS.class,
	FLOW_VS_09_003b_WS.class,
	FLOW_VS_10_001a_WS.class,
	FLOW_VS_10_001b_WS.class,
	FLOW_VS_10_002a_WS.class,
	FLOW_VS_10_002b_WS.class,
	FLOW_VS_10_003a_WS.class,
	FLOW_VS_10_003b_WS.class,
	FLOW_VS_11_001_WS.class,
	FLOW_VS_12_001_WS.class,
	FLOW_VS_EX_08_001_WS.class,
	FLOW_VS_EX_08_002_WS.class,
	FLOW_VS_EX_08_003_WS.class,
	FLOW_VS_EX_09_001_WS.class,
	FLOW_VS_EX_09_002_WS.class,
	FLOW_VS_EX_09_003_WS.class,
	FLOW_VS_EX_10_001_WS.class,
	FLOW_VS_EX_10_002_WS.class,

	//RS
	FLOW_VS_08_001a_RS.class,
	FLOW_VS_08_001b_RS.class,
	FLOW_VS_08_002a_RS.class,
	FLOW_VS_08_002b_RS.class,
	FLOW_VS_08_003a_RS.class,
	FLOW_VS_08_003b_RS.class,
	FLOW_VS_09_001a_RS.class,
	FLOW_VS_09_001b_RS.class,
	FLOW_VS_09_002a_RS.class,
	FLOW_VS_09_002b_RS.class,
	FLOW_VS_09_003a_RS.class,
	FLOW_VS_09_003b_RS.class,
	FLOW_VS_10_001a_RS.class,
	FLOW_VS_10_001b_RS.class,
	FLOW_VS_10_002a_RS.class,
	FLOW_VS_10_002b_RS.class,
	FLOW_VS_10_003a_RS.class,
	FLOW_VS_10_003b_RS.class,
	FLOW_VS_EX_08_001_RS.class,
	FLOW_VS_EX_08_002_RS.class,
	FLOW_VS_EX_08_003_RS.class,
	FLOW_VS_EX_09_001_RS.class,
	FLOW_VS_EX_09_002_RS.class,
	FLOW_VS_EX_09_003_RS.class,
	FLOW_VS_EX_10_001_RS.class,
	FLOW_VS_EX_10_002_RS.class,

})
public class ApiFlowVSTestSuite {

}
