/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. getCidStudyImageCount should return 1 (each image has 3 versions)
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_10_001a_WS extends AUDIT_10_001a {
}
