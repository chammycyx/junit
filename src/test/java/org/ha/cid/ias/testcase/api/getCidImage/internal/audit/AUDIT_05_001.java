package org.ha.cid.ias.testcase.api.getCidImage.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidImage.flow.FLOW_05_001;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 JPG image (each image has 3 versions)
 * 2. getCidImage using seq 3 and ver 3 should return the image of seq 3 and ver 3
 * 3. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_05_001 extends FLOW_05_001 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Test
	public void getCidImage_UploadImageRetrieveImage_ShouldAuditMatch() throws Exception {
		for (int i = 0; i < imageFiles.length; i++) {
			iasService.getCidImage(cidData, String.valueOf(i+1), versions[i], imageIDs[i]);
		}
		
		// Validate Audit Events
		esInternalService.setDelay(120);
		List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.RETRIEVE_ES_EVENT_NAME));
	}

}
