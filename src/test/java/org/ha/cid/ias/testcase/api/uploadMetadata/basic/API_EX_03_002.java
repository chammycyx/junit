package org.ha.cid.ias.testcase.api.uploadMetadata.basic;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;

/**
 * This test case intends to validate the following items:
 * 1. Upload invalid metadata (Malformat XML)
 * 
 * @author cft545
 *
 */
public abstract class API_EX_03_002 extends AbstractSpringTestCase {

	protected static final String HA7_MESSAGE = "/blazeds/flow/HA7_ERROR.xml";
	
	@Autowired
	private IasClientService iasService;

	@Autowired
	private ResourceService resourceService;
	
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	@Test
	public void uploadMetadata_Ha7MessageInvalid_ShouldThrowException() throws Exception {

		final String expectedMessage = "The incoming HA7 Message is incorrect, cann't process";
		
		try {
			iasService.uploadMetaData(
					"<abc>Invalid XML</abc>", 
					USER_ID, 
					WORKSTATION_ID, 
					REQUEST_SYS
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	}
	
	@Test
	public void uploadMetadata_Ha7MessageTagInvalid_ShouldThrowException() throws Exception {

		final String expectedMessage = "Database Error";
		
		try {
			iasService.uploadMetaData(
					resourceService.loadFile(HA7_MESSAGE),
					USER_ID, 
					WORKSTATION_ID, 
					REQUEST_SYS
			);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} catch (HttpResponseException e){
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
	} 
	
}
