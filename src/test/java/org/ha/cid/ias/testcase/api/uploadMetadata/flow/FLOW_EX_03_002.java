package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl;

import java.math.BigInteger;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;


/**
 * This test case intends to validate the following items:
 * 1. uploadMetaData with annotation without uploadImage
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_EX_03_002 {
	
	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_WithoutUploadImage_ShouldThrowException() throws Exception {
		
		final String expectedMessage = "Lost the image file!";
		
		CIDData cidData = iasService.generateCIDData(SampleData.getAccessionNo(), 1,3);
		
		AnnotationDtl annotationDtl = new AnnotationDtl();
		annotationDtl.setAnnotationSeq(new BigInteger("0"));
		annotationDtl.setAnnotationType("ImageCreateTimeStamp");
		annotationDtl.setAnnotationText(new DateTime().toString());
		annotationDtl.setAnnotationCoordinate("0^0~0^0");
		annotationDtl.setAnnotationStatus("F");
		annotationDtl.setAnnotationEditable("N");
		annotationDtl.setAnnotationUpdDtm("");
		
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0)
				.getImageDtls().getImageDtl().get(0).getAnnotationDtls()
				.getAnnotationDtl().add(annotationDtl);
		
		try {
			iasService.uploadMetaData(cidData);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e) {
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		} 
	}
}
