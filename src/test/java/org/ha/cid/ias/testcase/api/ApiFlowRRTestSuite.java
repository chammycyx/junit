package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_001_BZ;
import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_001_RS;
import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_001_WS;
import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_002_BZ;
import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_002_RS;
import org.ha.cid.ias.testcase.backend.rr.FLOW_RR_01_002_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({

	FLOW_RR_01_001_BZ.class,
	FLOW_RR_01_002_BZ.class,
	FLOW_RR_01_001_WS.class,
	FLOW_RR_01_002_WS.class,
	FLOW_RR_01_001_RS.class,
	FLOW_RR_01_002_RS.class,

})
public class ApiFlowRRTestSuite {

}
