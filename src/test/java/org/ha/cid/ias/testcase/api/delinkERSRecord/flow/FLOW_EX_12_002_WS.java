/**
 * 
 */
package org.ha.cid.ias.testcase.api.delinkERSRecord.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. Delink a study twice
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class FLOW_EX_12_002_WS extends FLOW_EX_12_002 {
}
