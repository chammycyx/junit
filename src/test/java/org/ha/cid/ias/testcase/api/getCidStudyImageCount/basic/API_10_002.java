/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.basic;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. CID study image count with optional parameter
 * 
 * @author CFT545
 *
 */
public abstract class API_10_002 {

	@Autowired
	private IasClientService iasService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG", 
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG", 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};
	
	String[] versions = {	
			"1", "2", "3",
			"1", "2", "3",
			"1", "2", "3"
	};
	
	String[] imageHandlings = {	
			"N", "N", "N",
			"N", "N", "N",
			"N", "N", "N"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	
			imageID1, imageID1, imageID1,
			imageID2, imageID2, imageID2,
			imageID3, imageID3, imageID3
	};
	
	CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	
	
	@Test
	public void getCidStudyImageCount_SeriesNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				null, 
				"3", 
				"3", 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_ImageSeqNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				"3", 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_VersionNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				"3", 
				null, 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	

	@Test
	public void getCidStudyImageCount_ImageSeqNoAndVersionNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				null, 
				null, 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_SeriesNoAndImageSeqNoAndVersionNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				null, 
				null, 
				null, 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_SeriesNoAndImageSeqNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				null, 
				null, 
				"3", 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_SeriesNoAndVersionNoNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(),
				null, 
				"3", 
				null, 
				false,
				SampleData.getUserId(),
				SampleData.getWorkstationId(),
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_WorkstationIdNull_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				"3", 
				"3", 
				false,
				SampleData.getUserId(),
				null,
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
	
	@Test
	public void getCidStudyImageCount_WorkstationIdEmpty_ShouldReturnCount() throws Exception {
		
		int imageCount = iasService.getCidStudyImageCount(
				cidData.getPatientDtl().getPatKey(), 
				cidData.getVisitDtl().getVisitHosp(), 
				cidData.getVisitDtl().getCaseNum(), 
				cidData.getStudyDtl().getAccessionNo(), 
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo(), 
				"3", 
				"3", 
				false,
				SampleData.getUserId(),
				"",
				SampleData.getRequestSystem()
		);

		Assert.assertEquals(3, imageCount);
	}
}
