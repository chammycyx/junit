package org.ha.cid.ias.testcase.release.ias1331;

import icw.wsdl.xjc.pojo.CIDData;

import java.util.UUID;

import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. upload 1 metadata with escape data in remark
 * 3. check remark in CID data by getCidStudy.
 * 
 * Refer to CID-2701
 * 
 * @author CYC014
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:ws-config-context.xml",
		"classpath:internal-context.xml"
})
public class CID_2701 {
	
	@Autowired
	private IasClientService iasService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;
//
	
	String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG" };
	
	String[] versions = { "1", "2" };
	
	String[] imageHandlings = {	"N", "N" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1 };
	
	CIDData cidData = null;
	
	Integer result = null;
	
	String escapeString = "&<>\"\'";
	
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		
	}
	
	@Test
	public void uploadMetadata_UploadEscapedRetrieveFromES_ShouldSuccess() throws Exception {
		
		String remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(escapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		String ha7Result = iasService.getCidStudy(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		
		Assert.assertEquals(escapeString, cidDataResult.getStudyDtl().getRemark());
		
	}
	
	
	@Test
	public void uploadMetadata_UploadEscapedRetrieveFromVS_ShouldSuccess() throws Exception {
		
		String remark = cidData.getStudyDtl().getRemark() + xmlEscapeText(escapeString);
		cidData.getStudyDtl().setRemark(remark);
		iasService.uploadMetaData(cidData);
		
		esInternalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
		
		String ha7Result = iasService.getCidStudy(cidData);
		CIDData cidDataResult = Ha7Util.convertToCIDData(ha7Result);
		
		Assert.assertEquals(escapeString, cidDataResult.getStudyDtl().getRemark());
		
	}
	
	
	private String xmlEscapeText(String t) {
	   StringBuilder sb = new StringBuilder();
	   for(int i = 0; i < t.length(); i++){
	      char c = t.charAt(i);
	      switch(c){
	      case '<': sb.append("&lt;"); break;
	      case '>': sb.append("&gt;"); break;
	      case '\"': sb.append("&quot;"); break;
	      case '&': sb.append("&amp;"); break;
	      case '\'': sb.append("&apos;"); break;
	      default:
	         if(c>0x7e) {
	            sb.append("&#"+((int)c)+";");
	         }else
	            sb.append(c);
	      }
	   }
	   return sb.toString();
	}
	
}
