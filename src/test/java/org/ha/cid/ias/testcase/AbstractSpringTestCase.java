package org.ha.cid.ias.testcase;

import org.junit.Before;
import org.springframework.test.context.TestContextManager;

/**
 * This abstract test case inject spring context into test class
 * Use only for test cases with RunWith(value = Parameterized.class)
 * 
 * @author CFT545
 *
 */
public abstract class AbstractSpringTestCase {

	protected TestContextManager testContextManager;

    @Before
    public void setUpContext() throws Exception {
      this.testContextManager = new TestContextManager(getClass());
      this.testContextManager.prepareTestInstance(this);
    }
    
}
