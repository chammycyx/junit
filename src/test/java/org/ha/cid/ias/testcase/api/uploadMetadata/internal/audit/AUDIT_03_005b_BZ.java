/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 2 versions)
 * 2. modify and upload the image with version 1, 3
 * 3. modify and upload the image with version 1, 4
 * 4. validate ES study
 * 5. validate audit log
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_03_005b_BZ extends AUDIT_03_005b {
}
