package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow.vs;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. remove the study from ES
 * 3. modify and upload the image with version 1, 3, 4
 * 4. modify and upload the image with version 1, 4, 5
 * 5. getCidStudyImageCount should return 3 (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_VS_10_003a {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService internalService;
	
	@Autowired
	protected ResourceService resourceService;

	String[] imageFiles = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v2.JPG", "/blazeds/flow/OGD2_v3.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v2.JPG", "/blazeds/flow/OGD3_v3.JPG"
	};

	String[] imageFiles2 = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v3.JPG", "/blazeds/flow/OGD1_v4.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v3.JPG", "/blazeds/flow/OGD2_v4.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v3.JPG", "/blazeds/flow/OGD3_v4.JPG"
	};
	
	String[] imageFiles3 = {	
			"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v4.JPG", "/blazeds/flow/OGD1_v5.JPG",
			"/blazeds/flow/OGD2_v1.JPG", "/blazeds/flow/OGD2_v4.JPG", "/blazeds/flow/OGD2_v5.JPG",
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v4.JPG", "/blazeds/flow/OGD3_v5.JPG"
	};
	
	String[] expectedimagesFiles = { 
			"/blazeds/flow/OGD3_v1.JPG", "/blazeds/flow/OGD3_v4.JPG", "/blazeds/flow/OGD3_v5.JPG"
	};
	
	String[] versions = {	
			"1", "2", "3",
			"1", "2", "3",
			"1", "2", "3"
	};
	String[] versions2 = {	
			"1", "3", "4",
			"1", "3", "4",
			"1", "3", "4"
	};
	String[] versions3 = {	
			"1", "4", "5",
			"1", "4", "5",
			"1", "4", "5"
	};
	
	String imageID1 = UUID.randomUUID().toString();
	String imageID2 = UUID.randomUUID().toString();
	String imageID3 = UUID.randomUUID().toString();
	
	String[] imageIDs = {	imageID1, imageID1, imageID1,
				imageID2, imageID2, imageID2,
				imageID3, imageID3, imageID3
	};
	
	String[] imageHandlings = {	"N", "N", "N",  
					"N", "N", "N", 
					"N", "N", "N" 
	};
	
	CIDData cidData = null;

	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		
		internalService.removeStudy(cidData.getStudyDtl().getAccessionNo());
		
		cidData = iasService.reuploadImages(cidData, imageFiles2, versions2, imageIDs, imageHandlings);
		iasService.uploadMetaData(cidData);
		cidData = iasService.reuploadImages(cidData, imageFiles3, versions3, imageIDs, imageHandlings);
		iasService.uploadMetaData(cidData);
	}
	
	@Test
	public void getCidStudyImageCount_UploadDownloadFromRREditTwice_ShouldReturn3() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, false);
		
		Assert.assertEquals(3, imageCount);
	}

	@Test
	public void getCidStudyImageCount_UploadDownloadFromRREditTwicePrintedCount_ShouldReturn0() throws Exception {
		int imageCount = iasService.getCidStudyImageCount(cidData, true);
		
		Assert.assertEquals(0, imageCount);
	}
}
