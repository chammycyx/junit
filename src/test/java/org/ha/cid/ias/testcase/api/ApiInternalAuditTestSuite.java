package org.ha.cid.ias.testcase.api;

import org.ha.cid.ias.testcase.api.deleteStudy.internal.audit.AUDIT_11_001_BZ;
import org.ha.cid.ias.testcase.api.deleteStudy.internal.audit.AUDIT_11_001_WS;
import org.ha.cid.ias.testcase.api.delinkERSRecord.internal.audit.AUDIT_12_001_WS;
import org.ha.cid.ias.testcase.api.exportImage.internal.audit.AUDIT_04_001a_BZ;
import org.ha.cid.ias.testcase.api.exportImage.internal.audit.AUDIT_04_001a_RS;
import org.ha.cid.ias.testcase.api.exportImage.internal.audit.AUDIT_04_001a_WS;
import org.ha.cid.ias.testcase.api.exportImage.internal.audit.AUDIT_CN_04_001a_BZ;
import org.ha.cid.ias.testcase.api.exportImage.internal.audit.AUDIT_CN_04_001a_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_003_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_004_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_005_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_006_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_002_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_002_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_003_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_004_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_004_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_005_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_005_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_006_RS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_003_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_004_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_004_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_005_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_005_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_006_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_002_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_002_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_002_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_003_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_004_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_004_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_004_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_005_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_005_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_005_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_006_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_006_RS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_006_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit.AUDIT_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit.AUDIT_06_001_RS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit.AUDIT_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit.AUDIT_CN_06_001_BZ;
import org.ha.cid.ias.testcase.api.getCidScaledImage.internal.audit.AUDIT_CN_06_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_08_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_08_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_08_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_VS_08_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_VS_08_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_09_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_09_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_09_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_001_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_002_RS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_VS_09_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_VS_09_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_10_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_10_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_10_001a_WS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_VS_10_001a_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_VS_10_001a_RS;
import org.ha.cid.ias.testcase.api.getCidStudyImageCount.internal.audit.AUDIT_VS_10_001a_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001a_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001a_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001a_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001b_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001b_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_001b_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005a_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005a_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005a_WS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005b_BZ;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005b_RS;
import org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit.AUDIT_03_005b_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({


	// Blaze DS
	AUDIT_03_001a_BZ.class,
	AUDIT_03_001b_BZ.class,
	AUDIT_03_005a_BZ.class,
	AUDIT_03_005b_BZ.class,
	AUDIT_04_001a_BZ.class,
	AUDIT_05_001_BZ.class,
	AUDIT_07_001_BZ.class,
	AUDIT_06_001_BZ.class,
	AUDIT_08_001a_BZ.class,
	AUDIT_09_001a_BZ.class,
	AUDIT_10_001a_BZ.class,
	AUDIT_11_001_BZ.class,
	AUDIT_EX_05_001_BZ.class,
	AUDIT_EX_05_002_BZ.class,
	AUDIT_EX_05_003_BZ.class,
	AUDIT_EX_05_004_BZ.class,
	AUDIT_EX_05_005_BZ.class,
	AUDIT_EX_05_006_BZ.class,
	AUDIT_EX_07_001_BZ.class,
	AUDIT_EX_07_002_BZ.class,
	AUDIT_EX_07_003_BZ.class,
	AUDIT_EX_07_004_BZ.class,
	AUDIT_EX_07_005_BZ.class,
	AUDIT_EX_07_006_BZ.class,
	AUDIT_EX_08_001_BZ.class,
	AUDIT_EX_08_002_BZ.class,
	AUDIT_EX_09_001_BZ.class,
	AUDIT_EX_09_002_BZ.class,
	AUDIT_CN_EX_05_001_BZ.class,
	AUDIT_CN_EX_05_002_BZ.class,
	AUDIT_CN_EX_05_003_BZ.class,
	AUDIT_CN_EX_05_004_BZ.class,
	AUDIT_CN_EX_05_005_BZ.class,
	AUDIT_CN_EX_05_006_BZ.class,
	AUDIT_CN_EX_07_001_BZ.class,
	AUDIT_CN_EX_07_002_BZ.class,
	AUDIT_CN_EX_07_003_BZ.class,
	AUDIT_CN_EX_07_004_BZ.class,
	AUDIT_CN_EX_07_005_BZ.class,
	AUDIT_CN_EX_07_006_BZ.class,
	AUDIT_VS_10_001a_BZ.class,

	// Web Service
	AUDIT_03_001a_WS.class,
	AUDIT_03_001b_WS.class,
	AUDIT_03_005a_WS.class,
	AUDIT_03_005b_WS.class,
	AUDIT_04_001a_WS.class,
	AUDIT_05_001_WS.class,
	AUDIT_07_001_WS.class,
	AUDIT_06_001_WS.class,
	AUDIT_08_001a_WS.class,
	AUDIT_09_001a_WS.class,
	AUDIT_10_001a_WS.class,
	AUDIT_11_001_WS.class,
	AUDIT_12_001_WS.class,
	AUDIT_EX_05_001_WS.class,
	AUDIT_EX_05_002_WS.class,
	AUDIT_EX_05_003_WS.class,
	AUDIT_EX_05_004_WS.class,
	AUDIT_EX_05_005_WS.class,
	AUDIT_EX_05_006_WS.class,
	AUDIT_EX_07_001_WS.class,
	AUDIT_EX_07_002_WS.class,
	AUDIT_EX_07_003_WS.class,
	AUDIT_EX_07_004_WS.class,
	AUDIT_EX_07_005_WS.class,
	AUDIT_EX_07_006_WS.class,
	AUDIT_EX_08_001_WS.class,
	AUDIT_EX_08_002_WS.class,
	AUDIT_EX_09_001_WS.class,
	AUDIT_EX_09_002_WS.class,
	AUDIT_CN_EX_05_001_WS.class,
	AUDIT_CN_EX_05_002_WS.class,
	AUDIT_CN_EX_05_003_WS.class,
	AUDIT_CN_EX_05_004_WS.class,
	AUDIT_CN_EX_05_005_WS.class,
	AUDIT_CN_EX_05_006_WS.class,
	AUDIT_CN_EX_07_001_WS.class,
	AUDIT_CN_EX_07_002_WS.class,
	AUDIT_CN_EX_07_003_WS.class,
	AUDIT_CN_EX_07_004_WS.class,
	AUDIT_CN_EX_07_005_WS.class,
	AUDIT_CN_EX_07_006_WS.class,
	AUDIT_VS_10_001a_WS.class,

	// Restful
	AUDIT_03_001a_RS.class,
	AUDIT_03_001b_RS.class,
	AUDIT_03_005a_RS.class,
	AUDIT_03_005b_RS.class,
	AUDIT_04_001a_RS.class,
	AUDIT_05_001_RS.class,
	AUDIT_07_001_RS.class,
	AUDIT_06_001_RS.class,
	AUDIT_08_001a_RS.class,
	AUDIT_09_001a_RS.class,
	AUDIT_10_001a_RS.class,
	AUDIT_EX_05_001_RS.class,
	AUDIT_EX_05_002_RS.class,
	AUDIT_EX_05_003_RS.class,
	AUDIT_EX_05_004_RS.class,
	AUDIT_EX_05_005_RS.class,
	AUDIT_EX_05_006_RS.class,
	AUDIT_EX_07_001_RS.class,
	AUDIT_EX_07_002_RS.class,
	AUDIT_EX_07_003_RS.class,
	AUDIT_EX_07_004_RS.class,
	AUDIT_EX_07_005_RS.class,
	AUDIT_EX_07_006_RS.class,
	AUDIT_EX_08_001_RS.class,
	AUDIT_EX_08_002_RS.class,
	AUDIT_EX_09_001_RS.class,
	AUDIT_EX_09_002_RS.class,
	AUDIT_CN_EX_05_001_RS.class,
	AUDIT_CN_EX_05_002_RS.class,
	AUDIT_CN_EX_05_003_RS.class,
	AUDIT_CN_EX_05_004_RS.class,
	AUDIT_CN_EX_05_005_RS.class,
	AUDIT_CN_EX_05_006_RS.class,
	AUDIT_CN_EX_07_001_RS.class,
	AUDIT_CN_EX_07_002_RS.class,
	AUDIT_CN_EX_07_003_RS.class,
	AUDIT_CN_EX_07_004_RS.class,
	AUDIT_CN_EX_07_005_RS.class,
	AUDIT_CN_EX_07_006_RS.class,
	AUDIT_VS_10_001a_RS.class,

	AUDIT_CN_04_001a_BZ.class,
	AUDIT_CN_05_001_BZ.class,
	AUDIT_CN_06_001_BZ.class,
	AUDIT_CN_07_001_BZ.class,
	AUDIT_VS_08_001a_BZ.class,
	AUDIT_VS_09_001a_BZ.class,
	AUDIT_CN_04_001a_WS.class,
	AUDIT_CN_05_001_WS.class,
	AUDIT_CN_06_001_WS.class,
	AUDIT_CN_07_001_WS.class,
	AUDIT_VS_08_001a_WS.class,
	AUDIT_VS_09_001a_WS.class

})
public class ApiInternalAuditTestSuite {

}
