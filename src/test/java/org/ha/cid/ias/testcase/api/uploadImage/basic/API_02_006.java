/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This test case intends to validate the following items:
 * 1. upload a TIF image
 * 
 * @author YKF491
 *
 */
public abstract class API_02_006 {

	@Autowired
	private IasClientService iasService;
	
	@Autowired
	private ResourceService resourceService;
	
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	protected static final String FILE_NAME = "BRONCH1.JPG";
	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String STUDY_ID = SampleData.getStudyId();
	protected static final String SERIES_NO = SampleData.getSeriesNo();
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	
	@Test
	public void uploadImage_uploadTif_WillReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray("/blazeds/flow/sample.tif");
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
}
