/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.basic;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This test case intends to validate the following items:
 * 1. upload a image 
 * 
 * @author CFT545
 *
 */
public abstract class API_02_001 {

	@Autowired
	protected IasClientService iasService;

	@Autowired
	private ResourceService resourceService;
	
	protected static final String IMAGE_FILE = "/blazeds/imageuploadds/BRONCH1.JPG";
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	protected static final String FILE_NAME = "BRONCH1.JPG";
	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String STUDY_ID = SampleData.getStudyId();
	protected static final String SERIES_NO = SampleData.getSeriesNo();
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	
	@Test
	public void uploadImage_uploadJpeg_ShouldReturnImagePath() throws Exception {
		byte[] imgBinaryArray = resourceService.loadFileAsByteArray(IMAGE_FILE);
		
		String imagePath = iasService.uploadImage(
				imgBinaryArray, 
				REQUEST_SYS, 
				FILE_NAME, 
				PATIENT_KEY, 
				STUDY_ID,
				SERIES_NO, 
				USER_ID, 
				WORKSTATION_ID);
		
		Assert.assertNotNull(imagePath);
		Assert.assertNotEquals(0, imagePath.length());
	}
	
}
