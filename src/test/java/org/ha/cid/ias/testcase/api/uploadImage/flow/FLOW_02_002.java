/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.flow;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;


/**
 * This test case intends to validate the following items:
 * 1. upload 30 images (each image has 3 version)
 * 2. uploadMetaData is successful 
 * 
 * @author YKF491
 * 
 */
public abstract class FLOW_02_002 {

	@Autowired
	private IasClientService iasService;

	@Test
	public void uploadMetadata_upload30images_ShouldSuccess() throws Exception {

		final int imageCount = 30;
		final int versionCount = 3;

		List<String> imageFileList = new ArrayList<String>();
		List<String> versionList = new ArrayList<String>();
		List<String> imageIDList = new ArrayList<String>();
		List<String> imageHandlingList = new ArrayList<String>();

		String imageID = "";
		for (int i = 0; i < imageCount * versionCount; i++) {
			imageFileList.add("/blazeds/flow/OGD1.JPG");
			versionList.add(String.valueOf(i % versionCount + 1));
			if (i % versionCount == 0) {
				imageID = UUID.randomUUID().toString();
			}
			imageIDList.add(imageID);
			imageHandlingList.add("N");
		}

		try {
			CIDData cidData = iasService.uploadImages(imageFileList.toArray(new String[imageFileList.size()]),
					versionList.toArray(new String[versionList.size()]),
					imageIDList.toArray(new String[imageIDList.size()]), SampleData.getAccessionNo(),
					imageHandlingList.toArray(new String[imageHandlingList.size()]));
			Integer result = iasService.uploadMetaData(cidData);
			Assert.assertEquals(0, result.intValue());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected.", false);
		}
	}
}
