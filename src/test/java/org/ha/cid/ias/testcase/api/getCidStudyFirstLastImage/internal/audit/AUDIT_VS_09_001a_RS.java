/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. remove the study from ES
 * 3. getCidStudyFirstLastImage should return HA7 with 1 image (each image has 3 versions)
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml",
	"classpath:internal-context.xml"
})
public class AUDIT_VS_09_001a_RS extends AUDIT_VS_09_001a {
}
