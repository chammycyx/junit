package org.ha.cid.ias.testcase.api.uploadMetadata.basic;

import java.util.Arrays;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;


/**
 * This test case intends to validate the following items:
 * 1. Mandatory parameters (null + empty) check
 * 
 * @author cft545
 *
 */
public abstract class API_EX_03_001 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasService;
	
	@Autowired
	private ResourceService resourceService;
	
	protected static final String HA7_MESSAGE = "/blazeds/imageretrivalds/HA7.xml";
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	
	protected String fieldName;
	protected String ha7File;
	protected String userId;
	protected String workstationId;
	protected String requestSys;

	public API_EX_03_001(String fieldName, String ha7File, String userId, 
			String workstationId, String requestSys) {
		this.fieldName = fieldName;
		this.ha7File = ha7File;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	@Parameters
	public static Iterable<Object[]> dataset() {
		
		return Arrays.asList(new Object[][] { 
				{ "ha7Message", 
						"", USER_ID, WORKSTATION_ID, REQUEST_SYS },
				{ "userId", 
						HA7_MESSAGE, "", WORKSTATION_ID, REQUEST_SYS },
				{ "userId", 
							HA7_MESSAGE, null, WORKSTATION_ID, REQUEST_SYS },
				{ "workstationId", 
						HA7_MESSAGE, USER_ID, "", REQUEST_SYS },
				{ "workstationId", 
							HA7_MESSAGE, USER_ID, null, REQUEST_SYS },
				{ "requestSys", 
						HA7_MESSAGE, USER_ID, WORKSTATION_ID, "" },
				{ "requestSys", 
							HA7_MESSAGE, USER_ID, WORKSTATION_ID, null },
			});
	}
	
	@Test
	public void uploadMetadata_MissingParameter_ShouldThrowException() throws Exception {

		try {
			iasService.uploadMetaData(loadHa7File(ha7File), userId, workstationId, requestSys);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(this.fieldName));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(this.fieldName));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(this.fieldName));
		}
	}
	
	protected String loadHa7File(String ha7File) throws Exception {
		if (ha7File == null) {
			return null;
		} else if (ha7File.isEmpty()) {
			return "";
		} else {
			return resourceService.loadFile(ha7File);
		} 
	}
}
