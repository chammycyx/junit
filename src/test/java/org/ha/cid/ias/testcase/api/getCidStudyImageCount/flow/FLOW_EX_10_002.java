/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudyImageCount.flow;

import java.util.UUID;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;
import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete the study
 * 3. Get image count
 * 
 * @author CFT545
 *
 */
public abstract class FLOW_EX_10_002 {

	@Autowired
	protected IasClientService iasService;

	protected String[] imageFiles = {	"/blazeds/flow/OGD1_v1.JPG", "/blazeds/flow/OGD1_v2.JPG", "/blazeds/flow/OGD1_v3.JPG" };
	
	protected String[] versions = { "1", "2", "3" };
	
	protected String[] imageHandlings = {	"N", "N", "N" };
	
	protected String imageID1 = UUID.randomUUID().toString();
	
	protected String[] imageIDs = { imageID1, imageID1, imageID1 };
	
	protected CIDData cidData = null;
	
	@Before
	public void prepare() throws Exception {
		cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
		iasService.uploadMetaData(cidData);
		iasService.deleteStudy(cidData, cidData.getStudyDtl().getAccessionNo(), cidData.getStudyDtl().getStudyID());
	}
	
	@Test
	public void getCidStudyImageCount_RetrieveDeleted_ShouldThrowException() throws Exception {
		
		String expectedMessage = "Study is mark-deleted in ES";
		
		try {
			iasService.getCidStudyImageCount(cidData, false);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(expectedMessage));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(expectedMessage));
		}
		
	}
}
