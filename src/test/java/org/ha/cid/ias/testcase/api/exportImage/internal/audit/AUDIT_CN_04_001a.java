package org.ha.cid.ias.testcase.api.exportImage.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.exportImage.flow.cn.FLOW_CN_04_001a;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Remove the study from ES
 * 3. export all images (each image has 3 versions)
 * 4. validate audit events
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_CN_04_001a extends FLOW_CN_04_001a {

	@Autowired
	@Qualifier("iasEsInternalService")
	protected IasInternalService esInternalService;
	
	@Autowired
	@Qualifier("iasRrInternalService")
	protected IasInternalService rrInternalService;

	@Test
	public void exportImage_UploadExportFromCN_ShouldAuditMatch() throws Exception {
		iasService.exportImage(Ha7Util.convertToHa7xml(cidData), password);
		
		// Validate Audit Events
		rrInternalService.setDelay(120);
		List<AuditEventDto> auditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(auditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME,
				AuditEventDto.RETRIEVE_ES_EVENT_NAME, AuditEventDto.RETRIEVE_RR_EVENT_NAME));	
	}
}
