/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadMetadata.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. upload metadata by changing seq 3 to seq 2 with all image exam type
 * 
 * @author YKF491
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_EX_03_015_BZ extends FLOW_EX_03_015 {
}
