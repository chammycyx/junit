package org.ha.cid.ias.testcase.api.exportImage.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. export all images with missing optional parameters (each image has 3 versions)
 * 
 * @author CFT545
 *
 */
@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml"
})
public class API_04_002_WS extends API_04_002 {
	
	public API_04_002_WS(String fieldName, String workstationId) {
		super(fieldName, workstationId);
	}
}
