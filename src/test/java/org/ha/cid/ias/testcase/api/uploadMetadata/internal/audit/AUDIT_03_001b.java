package org.ha.cid.ias.testcase.api.uploadMetadata.internal.audit;

import java.util.List;

import org.ha.cd2.isg.icw.tool.ArrayUtil;
import org.ha.cd2.isg.icw.tool.AuditEventCompareUtil;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.uploadMetadata.flow.FLOW_03_001b;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 2 versions)
 * 2. upload 1 metadata
 * 3. validate ES study
 * 4. validate audit log
 * 
 * @author LSM131
 *
 */
public abstract class AUDIT_03_001b extends FLOW_03_001b {
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImage2Version_ShouldEsAuditMatch() throws Exception {
		
		// Validate ES Audit Events
		List<AuditEventDto> esAuditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(esAuditEventDtos, AuditEventDto.UPLOAD_ES_EVENT_NAME);
	}

	@Test
	public void uploadMetadata_uploadImage2Version_ShouldRrAuditMatch() throws Exception {
		
		// Validate RR Audit Events
		rrInternalService.setDelay(120);
		List<AuditEventDto> rrAuditEventDtos = rrInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo());
		AuditEventCompareUtil.assertAuditEventsSuccess(rrAuditEventDtos, ArrayUtil.addAll(AuditEventDto.UPLOAD_ES_EVENT_NAME, AuditEventDto.UPLOAD_RR_EVENT_NAME));
	}
}
