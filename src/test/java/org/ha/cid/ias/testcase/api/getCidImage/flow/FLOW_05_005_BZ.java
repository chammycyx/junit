/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidImage.flow;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * This test case intends to validate the following items:
 * 1. upload 3 images (each image has 3 versions)
 * 2. modify and upload the image with version 1, 3, 4
 * 3. modify and upload the image with version 1, 4, 5
 * 4. getCidImage for all images
 * 
 * @author CFT545
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:blazeds-config-context.xml"
})
public class FLOW_05_005_BZ extends FLOW_05_005 {
}
