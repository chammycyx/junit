package org.ha.cid.ias.testcase.release.ias150;

import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_CN_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_001_WS;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImage.internal.audit.AUDIT_EX_05_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_CN_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_001_WS;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_003_BZ;
import org.ha.cid.ias.testcase.api.getCidImageThumbnail.internal.audit.AUDIT_EX_07_003_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudy.internal.audit.AUDIT_EX_08_002_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_001_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_001_WS;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_002_BZ;
import org.ha.cid.ias.testcase.api.getCidStudyFirstLastImage.internal.audit.AUDIT_EX_09_002_WS;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	// Web Service
	AUDIT_EX_08_001_WS.class,
	AUDIT_EX_09_001_WS.class,
	AUDIT_EX_05_001_WS.class,
	AUDIT_EX_07_001_WS.class,
	AUDIT_EX_08_002_WS.class,
	AUDIT_EX_09_002_WS.class,
	AUDIT_EX_05_003_WS.class,
	AUDIT_EX_07_003_WS.class,
	AUDIT_CN_EX_05_003_WS.class,
	AUDIT_CN_EX_07_003_WS.class,

	// Blaze DS
	AUDIT_EX_08_001_BZ.class,
	AUDIT_EX_09_001_BZ.class,
	AUDIT_EX_05_001_BZ.class,
	AUDIT_EX_07_001_BZ.class,
	AUDIT_EX_08_002_BZ.class,
	AUDIT_EX_09_002_BZ.class,
	AUDIT_EX_05_003_BZ.class,
	AUDIT_EX_07_003_BZ.class,
	AUDIT_CN_EX_05_003_BZ.class,
	AUDIT_CN_EX_07_003_BZ.class,
})
public class CID_3128 {

}
