package org.ha.cid.ias.testcase.api.getCidStudyImageCount.basic;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.ContextConfiguration;



@RunWith(value = Parameterized.class)
@ContextConfiguration(locations = {
	"classpath:rs-config-context.xml"
})
public class API_EX_10_001_RS extends API_EX_10_001 {

	public API_EX_10_001_RS(String fieldName, String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, Boolean isPrintedCount, String userId, String workstationId,
			String requestSys) {
		super(fieldName, patientKey, hospCde, caseNo, accessionNo, seriesNo, imageSeqNo, versionNo, 
				isPrintedCount, userId, workstationId, requestSys);
	}
}
