package org.ha.cid.ias.testcase.api.uploadImage.basic;

import java.util.Arrays;

import org.apache.http.client.HttpResponseException;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.testcase.AbstractSpringTestCase;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.soap.client.SoapFaultClientException;

import flex.messaging.io.amf.client.exceptions.ServerStatusException;
import flex.messaging.messages.ErrorMessage;

public abstract class API_EX_02_001 extends AbstractSpringTestCase {

	@Autowired
	private IasClientService iasSerivce;

	@Autowired
	private ResourceService resourceService;
	
	protected static final String IMAGE_FILE = "/blazeds/imageuploadds/BRONCH1.JPG";
	protected static final String REQUEST_SYS = SampleData.getRequestSystem();
	protected static final String FILE_NAME = "BRONCH1.JPG";
	protected static final String PATIENT_KEY = SampleData.getPatientKey();
	protected static final String STUDY_ID = SampleData.getStudyId();
	protected static final String SERIES_NO = SampleData.getSeriesNo();
	protected static final String USER_ID = SampleData.getUserId();
	protected static final String WORKSTATION_ID = SampleData.getWorkstationId();
	
	protected String fieldName;
	protected String imageFile;
	protected String requestSys;
	protected String filename;
	protected String patientKey;
	protected String studyId;
	protected String seriesNo;
	protected String userId;
	protected String workstationId;

	public API_EX_02_001(String fieldName, String imageFile, String requestSys, String filename, 
			String patientKey, String studyId, String seriesNo, String userId, String workstationId) {
		this.fieldName = fieldName;
		this.imageFile = imageFile;
		this.requestSys = requestSys;
		this.filename = filename;
		this.patientKey = patientKey;
		this.studyId = studyId;
		this.seriesNo = seriesNo;
		this.userId = userId;
		this.workstationId = workstationId;
	}
	
	@Parameters
	public static Iterable<Object[]> dataset() {
		
		return Arrays.asList(new Object[][] { 
				{ "imgBinaryArray", 
						null, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "requestSys", 
						IMAGE_FILE, "", FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "requestSys", 
						IMAGE_FILE, null, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "filename", 
						IMAGE_FILE, REQUEST_SYS, "", PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "filename", 
						IMAGE_FILE, REQUEST_SYS, null, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "patientKey", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, "", STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "patientKey", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, null, STUDY_ID, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "studyId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, "", 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "studyId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, null, 
						SERIES_NO, USER_ID, WORKSTATION_ID },
				{ "seriesNo", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						"", USER_ID, WORKSTATION_ID },
				{ "seriesNo", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						null, USER_ID, WORKSTATION_ID },
				{ "userId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, "", WORKSTATION_ID },
				{ "userId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, null, WORKSTATION_ID },
				{ "workstationId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, "" },
				{ "workstationId", 
						IMAGE_FILE, REQUEST_SYS, FILE_NAME, PATIENT_KEY, STUDY_ID, 
						SERIES_NO, USER_ID, null },
			});
	}
	
	@Test
	public void uploadImage_MissingParameter_ShouldThrowException() throws Exception {

		try {
			byte[] imgBinaryArr = null;
			
			if (imageFile != null) {
				imgBinaryArr = resourceService.loadFileAsByteArray(imageFile);
			}
			
			iasSerivce.uploadImage(imgBinaryArr, 
					requestSys, filename, patientKey, studyId, 
					seriesNo, userId, workstationId);
			Assert.assertTrue("Exception is expected.", false);
		} catch (ServerStatusException e) {
			ErrorMessage error = (ErrorMessage) e.getData();
			Assert.assertTrue(error.faultString.contains(this.fieldName));
		} catch (SoapFaultClientException e) {
			Assert.assertTrue(e.getMessage().contains(this.fieldName));
		} catch (HttpResponseException e){
		    Assert.assertTrue(e.getMessage().contains(this.fieldName));
		}
	}

}
