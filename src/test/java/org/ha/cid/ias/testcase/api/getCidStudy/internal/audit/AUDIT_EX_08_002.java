/**
 * 
 */
package org.ha.cid.ias.testcase.api.getCidStudy.internal.audit;

import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.getCidStudy.flow.FLOW_EX_08_002;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 images (each image has 3 versions)
 * 2. Delete the study
 * 3. Get cid study
 * 
 * @author CFT545
 *
 */
public abstract class AUDIT_EX_08_002 extends FLOW_EX_08_002 {

	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Test
	public void getCidStudy_RetrieveDeleted_ShouldAuditMatch() throws Exception {

		final String expectedMessage = "Study is mark-deleted in ES";
		final String auditEventName = "getCidStudy";
		
		try {
			iasService.getCidStudy(cidData);
			Assert.assertTrue("Exception is expected.", false);
		} catch (Exception e) {
			// Validate Audit Events
			List<AuditEventDto> auditEventDtos = esInternalService.getAuditEvent(cidData.getStudyDtl().getAccessionNo(), auditEventName);
			AuditEventDto auditEventDto = auditEventDtos.get(0);
			Assert.assertTrue(auditEventDto.getStatus().equals(AuditEventDto.STATUS_FAILURE));
			Assert.assertTrue(auditEventDto.getResponse().equals(expectedMessage));
			Assert.assertTrue(auditEventDto.getException().contains(expectedMessage));
		}
	}
}
