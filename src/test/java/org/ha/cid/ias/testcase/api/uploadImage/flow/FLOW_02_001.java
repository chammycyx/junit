/**
 * 
 */
package org.ha.cid.ias.testcase.api.uploadImage.flow;

import java.util.UUID;

import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.testcase.data.SampleData;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import icw.wsdl.xjc.pojo.CIDData;

/**
 * This test case intends to validate the following items:
 * 1. upload a image below 1 MB(each image has 2 versions)
 * 2. uploadMetaData is successful 
 * 
 * @author YKF491
 *
 */
public abstract class FLOW_02_001 {

	@Autowired
	protected IasClientService iasService;

	String[] imageFiles = {	"/blazeds/flow/300KB.JPG", "/blazeds/flow/300KB.JPG" };
	
	String[] versions = { "1", "2" };
	
	String imageID1 = UUID.randomUUID().toString();
	
	String[] imageIDs = { imageID1, imageID1 };
	
	String[] imageHandlings = {	"N", "N" };
	
	@Test
	public void uploadImage_Upload_ShouldSuccess() throws Exception {
		
		try {
			CIDData cidData = iasService.uploadImages(imageFiles, versions, imageIDs, SampleData.getAccessionNo(), imageHandlings);
			Integer result = iasService.uploadMetaData(cidData);
			Assert.assertEquals(0, result.intValue());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception is not expected.", false);
		}
	}
}
