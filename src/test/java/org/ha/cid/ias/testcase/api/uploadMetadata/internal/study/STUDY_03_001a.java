package org.ha.cid.ias.testcase.api.uploadMetadata.internal.study;

import org.ha.cd2.isg.icw.tool.StudyCompareUtil;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.testcase.api.uploadMetadata.flow.FLOW_03_001a;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This test case intends to validate the following items:
 * 1. upload 1 image (each image has 3 versions)
 * 2. upload 1 metadata
 * 3. validate ES study
 * 4. validate audit log
 * 
 * @author LSM131
 *
 */
public abstract class STUDY_03_001a extends FLOW_03_001a {
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService esInternalService;

	@Autowired
	@Qualifier("iasRrInternalService")
	private IasInternalService rrInternalService;
	
	@Test
	public void uploadMetadata_uploadImage3Version_ShouldEsStudyMatch() throws Exception {
		// Validate ES Study
		StudyDto rrStudyDto = esInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(rrStudyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(rrStudyDto, cidData);
	}
	
	@Test
	public void uploadMetadata_uploadImage3Version_ShouldRrStudyMatch() throws Exception {
		// Validate RR Study
		StudyDto rrStudyDto = rrInternalService.getStudy(cidData.getStudyDtl().getAccessionNo());
		if(rrStudyDto == null)
			Assert.fail("No study was found.");
		StudyCompareUtil.assertStudyAndCidDataEquals(rrStudyDto, cidData);
		
	}
	
}
