package org.ha.cid.ias.testcase.backend.esb;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;

import org.ha.cd2.isg.icw.tool.DateUtils;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasEsbService;
import org.ha.cid.ias.service.IasInternalService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import common.utils.TimerUtil;
import static org.junit.Assert.assertEquals;

/**
 * This test case intends focus on 're-ordering with cut-off time feature', the following items would be validated:
 * 1. In-ordered A08/A40/A45/A47 message after 're-ordering within cut-off time' should kept in-order 
 * 2. Dis-ordered A08/A40/A45/A47 message after 're-ordering within cut-off time' should be re-ordered
 * 
 * @author Patrick YAU
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:ws-config-context.xml",
	"classpath:internal-context.xml",
	"classpath:esb-config-context.xml"
})
public class FLOW_ESB_13_005 extends FLOW_ESB_13_BASE{
	
	@Autowired
	private IasEsbService iasEsbService;
	
	@Autowired
	@Qualifier("iasEsInternalService")
	private IasInternalService iasEsInternalService;
	
	private String accessionNo1;
	private String accessionNo2;
	private String fromPatientKey = "80050200";
	private String toPatientKey = "80050201";
	private String hospCode = "VH";
	private String caseNum1 = "HN01000101X";
	private String caseNum2 = "HN01000102X";
	
	private String cutOffTimeInMinute = "1";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		//Setup ESB env
		super.setup();
		iasEsbService.enableStagingPoller("true");
		iasEsbService.enableDuplicationChecker("false");
		iasEsbService.enableDisorderChecker("true");
		iasEsbService.setCutoffTime(cutOffTimeInMinute);
		
//		iasEsbService.markStagingToErrWithStatus(Arrays.asList(MessageProcessStatus.INITIAL,MessageProcessStatus.PROCESSING));
		
		//Create patients for testing
		createPatient(fromPatientKey);
		createPatient(toPatientKey);
		
		//Create a study for testing
		accessionNo1 = createStudy();
		accessionNo2 = createStudy();
		
		//Bind Study and Patient
		TimerUtil.delay(10);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo1, fromPatientKey, hospCode, caseNum1);
		iasEsInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
		iasRrInternalService.updateStudyWithPatientEpisode(accessionNo2, fromPatientKey, hospCode, caseNum2);
	}

	@After
	public void tearDown() throws Exception {
		iasRrInternalService.removePatient(fromPatientKey);
		iasRrInternalService.removePatient(toPatientKey);
//		StudyDto esStudy1 = iasEsInternalService.getRefreshedStudy(accessionNo1);
//		iasClientService.deleteStudy(esStudy1.getPatKey(), esStudy1.getVisitHosp(), esStudy1.getCaseNo(), accessionNo1, esStudy1.getStudyId());
		iasEsInternalService.removeStudy(accessionNo1);
//		StudyDto esStudy2 = iasEsInternalService.getRefreshedStudy(accessionNo2);
//		iasClientService.deleteStudy(esStudy2.getPatKey(), esStudy2.getVisitHosp(), esStudy2.getCaseNo(), accessionNo2, esStudy2.getStudyId());
		iasEsInternalService.removeStudy(accessionNo2);
	}

	/**
	 * 1. Send in-ordered ESB messages. A08(EVN2 #1)->A40(EVN2 #2)->A45(EVN2 #3)->A47(EVN2 #4)
	 * 2. The processing sequence should keep unchanged (A08->A40->A45->A47)
	 * 3. Assert the processing end time of staging that should earlier than latter one.
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_in_ordered_A08_A40_A45_A47() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		
		//Act
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a08TxnId = sendA08(evn2, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A40SubTypeTxnId = sendA47(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, 1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A08SubTypeTxnId = sendA47(evn2, fromPatientKey, fromPatientKey);
		
		//Assert
		TimerUtil.delay(Integer.valueOf(cutOffTimeInMinute)*60);//cutoff time in second
		TimerUtil.delay(120);//wait for all ESB msg get processed
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		StagingDto a47A40SubTypeStaging = iasEsbService.findStaging(a47A40SubTypeTxnId);
		StagingDto a47A08SubTypeStaging = iasEsbService.findStaging(a47A08SubTypeTxnId);
		
		assertEquals(true, a08Staging.getStagingEndDt().before(a40Staging.getStagingStartDt()));
		assertEquals(true, a40Staging.getStagingEndDt().before(a45Staging.getStagingStartDt()));
		assertEquals(true, a45Staging.getStagingEndDt().before(a47A40SubTypeStaging.getStagingStartDt()));
		assertEquals(true, a47A40SubTypeStaging.getStagingEndDt().before(a47A08SubTypeStaging.getStagingStartDt()));
	}
	
	/**
	 * 1. Send dis-ordered ESB messages. A47(EVN2 #4)->A45(EVN2 #3)->A40(EVN2 #2)->A08(EVN2 #1)
	 * 2. The processing sequence should be re-ordered to A08->A40->A45->A47.
	 * 3. Assert the processing end time of staging that should earlier than latter one.
	 * 
	 * @throws IOException
	 */
	@Test
	public void send_disordered_A08_A40_A45_A47() throws IOException {
		//Arrange
		Calendar now = Calendar.getInstance();
		String evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		
		//Act
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A08SubTypeTxnId = sendA47(evn2, fromPatientKey, fromPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a47A40SubTypeTxnId = sendA47(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a45TxnId = sendA45(evn2, fromPatientKey, toPatientKey, hospCode, caseNum1);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a40TxnId = sendA40(evn2, fromPatientKey, toPatientKey);
		
		now.add(Calendar.MILLISECOND, -1);
		evn2 = DateUtils.format(now.getTime(), DateUtils.FORMAT_TIMESTAMP);
		String a08TxnId = sendA08(evn2, toPatientKey);
		
		//Assert
		TimerUtil.delay(Integer.valueOf(cutOffTimeInMinute)*60);//cutoff time in second
		TimerUtil.delay(120);//wait for all ESB msg get processed
		StagingDto a08Staging = iasEsbService.findStaging(a08TxnId);
		StagingDto a40Staging = iasEsbService.findStaging(a40TxnId);
		StagingDto a45Staging = iasEsbService.findStaging(a45TxnId);
		StagingDto a47A40SubTypeStaging = iasEsbService.findStaging(a47A40SubTypeTxnId);
		StagingDto a47A08SubTypeStaging = iasEsbService.findStaging(a47A08SubTypeTxnId);
		
		assertEquals(true, a08Staging.getStagingEndDt().before(a40Staging.getStagingStartDt()));
		assertEquals(true, a40Staging.getStagingEndDt().before(a45Staging.getStagingStartDt()));
		assertEquals(true, a45Staging.getStagingEndDt().before(a47A40SubTypeStaging.getStagingStartDt()));
		assertEquals(true, a47A40SubTypeStaging.getStagingEndDt().before(a47A08SubTypeStaging.getStagingStartDt()));
	}
	

}
