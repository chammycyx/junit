//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.05.06 at 11:33:04 AM CST 
//


package wsclient.icw.wsdl.xjc.pojo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the icw.wsdl.xjc.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: icw.wsdl.xjc.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CIDData }
     * 
     */
    public CIDData createCIDData() {
        return new CIDData();
    }

    /**
     * Create an instance of {@link CIDAck }
     * 
     */
    public CIDAck createCIDAck() {
        return new CIDAck();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl }
     * 
     */
    public CIDData.StudyDtl createCIDDataStudyDtl() {
        return new CIDData.StudyDtl();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls createCIDDataStudyDtlSeriesDtls() {
        return new CIDData.StudyDtl.SeriesDtls();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls.SeriesDtl }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls.SeriesDtl createCIDDataStudyDtlSeriesDtlsSeriesDtl() {
        return new CIDData.StudyDtl.SeriesDtls.SeriesDtl();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls createCIDDataStudyDtlSeriesDtlsSeriesDtlImageDtls() {
        return new CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl createCIDDataStudyDtlSeriesDtlsSeriesDtlImageDtlsImageDtl() {
        return new CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls createCIDDataStudyDtlSeriesDtlsSeriesDtlImageDtlsImageDtlAnnotationDtls() {
        return new CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls();
    }

    /**
     * Create an instance of {@link CIDData.MessageDtl }
     * 
     */
    public CIDData.MessageDtl createCIDDataMessageDtl() {
        return new CIDData.MessageDtl();
    }

    /**
     * Create an instance of {@link CIDData.PatientDtl }
     * 
     */
    public CIDData.PatientDtl createCIDDataPatientDtl() {
        return new CIDData.PatientDtl();
    }

    /**
     * Create an instance of {@link CIDData.VisitDtl }
     * 
     */
    public CIDData.VisitDtl createCIDDataVisitDtl() {
        return new CIDData.VisitDtl();
    }

    /**
     * Create an instance of {@link CIDAck.MessageDtl }
     * 
     */
    public CIDAck.MessageDtl createCIDAckMessageDtl() {
        return new CIDAck.MessageDtl();
    }

    /**
     * Create an instance of {@link CIDAck.AckDtl }
     * 
     */
    public CIDAck.AckDtl createCIDAckAckDtl() {
        return new CIDAck.AckDtl();
    }

    /**
     * Create an instance of {@link CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl }
     * 
     */
    public CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl createCIDDataStudyDtlSeriesDtlsSeriesDtlImageDtlsImageDtlAnnotationDtlsAnnotationDtl() {
        return new CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl();
    }

}
