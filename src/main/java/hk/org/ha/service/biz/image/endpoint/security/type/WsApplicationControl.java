package hk.org.ha.service.biz.image.endpoint.security.type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "applicationControl", propOrder = { "applicationVersion",
		"enabled", "maxPanelWidth", "maxPanelHeight", "minPanelWidth",
		"minPanelHeight", "maxUploadFileSize", "profileId",
		"maxUploadFileCount", "maxMarkerCount", "defaultTextboxFontSize",
		"defaultMarkerWidth", "defaultMarkerHeight", "enableMarkerHistory",
		"arrayOfServiceFunction", "enableUploadUi" })
public class WsApplicationControl {

	@XmlElement(name = "applicationVersion")
	protected String applicationVersion;

	@XmlElement(name = "enabled")
	protected String enabled;

	@XmlElement(name = "maxPanelWidth")
	protected int maxPanelWidth;

	@XmlElement(name = "maxPanelHeight")
	protected int maxPanelHeight;

	@XmlElement(name = "minPanelWidth")
	protected int minPanelWidth;

	@XmlElement(name = "minPanelHeight")
	protected int minPanelHeight;

	@XmlElement(name = "maxUploadFileSize")
	protected int maxUploadFileSize;

	@XmlElement(name = "profileId")
	protected int profileId;

	@XmlElement(name = "maxUploadFileCount")
	protected int maxUploadFileCount;

	@XmlElement(name = "maxMarkerCount")
	protected int maxMarkerCount;

	@XmlElement(name = "defaultTextboxFontSize")
	protected int defaultTextboxFontSize;

	@XmlElement(name = "defaultMarkerWidth")
	protected int defaultMarkerWidth;
	
	@XmlElement(name = "defaultMarkerHeight")
	protected int defaultMarkerHeight;
	
	@XmlElement(name = "enableMarkerHistory")
	protected boolean enableMarkerHistory;
	
	@XmlElement(name = "arrayOfServiceFunction")
	protected ArrayOfServiceFunction arrayOfServiceFunction;
	
	@XmlElement(name = "enableUploadUi")
	protected boolean enableUploadUi;
	
	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String value) {
		this.applicationVersion = value;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String value) {
		this.enabled = value;
	}

	public int getMaxPanelWidth() {
		return maxPanelWidth;
	}

	public void setMaxPanelWidth(int value) {
		this.maxPanelWidth = value;
	}

	public int getMaxPanelHeight() {
		return maxPanelHeight;
	}

	public void setMaxPanelHeight(int value) {
		this.maxPanelHeight = value;
	}

	public int getMinPanelWidth() {
		return minPanelWidth;
	}

	public void setMinPanelWidth(int value) {
		this.minPanelWidth = value;
	}

	public int getMinPanelHeight() {
		return minPanelHeight;
	}

	public void setMinPanelHeight(int value) {
		this.minPanelHeight = value;
	}

	public int getMaxUploadFileSize() {
		return maxUploadFileSize;
	}

	public void setMaxUploadFileSize(int value) {
		this.maxUploadFileSize = value;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int value) {
		this.profileId = value;
	}

	public int getMaxUploadFileCount() {
		return maxUploadFileCount;
	}

	public void setMaxUploadFileCount(int value) {
		this.maxUploadFileCount = value;
	}

	public int getMaxMarkerCount() {
		return maxMarkerCount;
	}

	public void setMaxMarkerCount(int value) {
		this.maxMarkerCount = value;
	}

	public int getDefaultTextboxFontSize() {
		return defaultTextboxFontSize;
	}

	public void setDefaultTextboxFontSize(int defaultTextboxFontSize) {
		this.defaultTextboxFontSize = defaultTextboxFontSize;
	}

	public int getDefaultMarkerWidth() {
		return defaultMarkerWidth;
	}

	public void setDefaultMarkerWidth(int defaultMarkerWidth) {
		this.defaultMarkerWidth = defaultMarkerWidth;
	}

	public int getDefaultMarkerHeight() {
		return defaultMarkerHeight;
	}

	public void setDefaultMarkerHeight(int defaultMarkerHeight) {
		this.defaultMarkerHeight = defaultMarkerHeight;
	}

	public boolean getEnableMarkerHistory() {
		return enableMarkerHistory;
	}

	public void setEnableMarkerHistory(boolean enableMarkerHistory) {
		this.enableMarkerHistory = enableMarkerHistory;
	}
	
	public ArrayOfServiceFunction getArrayOfServiceFunction() {
		return this.arrayOfServiceFunction;
	}

	public void setArrayOfServiceFunction(
			ArrayOfServiceFunction arrayOfServiceFunction) {
		this.arrayOfServiceFunction = arrayOfServiceFunction;
	}

	public boolean getEnableUploadUi() {
		return enableUploadUi;
	}

	public void setEnableUploadUi(boolean enableUploadUi) {
		this.enableUploadUi = enableUploadUi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((applicationVersion == null) ? 0 : applicationVersion
						.hashCode());
		result = prime
				* result
				+ ((arrayOfServiceFunction == null) ? 0
						: arrayOfServiceFunction.hashCode());
		result = prime * result + defaultMarkerHeight;
		result = prime * result + defaultMarkerWidth;
		result = prime * result + defaultTextboxFontSize;
		result = prime * result + (enableMarkerHistory ? 1231 : 1237);
		result = prime * result + (enableUploadUi ? 1231 : 1237);
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + maxMarkerCount;
		result = prime * result + maxPanelHeight;
		result = prime * result + maxPanelWidth;
		result = prime * result + maxUploadFileCount;
		result = prime * result + maxUploadFileSize;
		result = prime * result + minPanelHeight;
		result = prime * result + minPanelWidth;
		result = prime * result + profileId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WsApplicationControl other = (WsApplicationControl) obj;
		if (applicationVersion == null) {
			if (other.applicationVersion != null)
				return false;
		} else if (!applicationVersion.equals(other.applicationVersion))
			return false;
		if (arrayOfServiceFunction == null) {
			if (other.arrayOfServiceFunction != null)
				return false;
		} else if (!arrayOfServiceFunction.equals(other.arrayOfServiceFunction))
			return false;
		if (defaultMarkerHeight != other.defaultMarkerHeight)
			return false;
		if (defaultMarkerWidth != other.defaultMarkerWidth)
			return false;
		if (defaultTextboxFontSize != other.defaultTextboxFontSize)
			return false;
		if (enableMarkerHistory != other.enableMarkerHistory)
			return false;
		if (enableUploadUi != other.enableUploadUi)
			return false;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (maxMarkerCount != other.maxMarkerCount)
			return false;
		if (maxPanelHeight != other.maxPanelHeight)
			return false;
		if (maxPanelWidth != other.maxPanelWidth)
			return false;
		if (maxUploadFileCount != other.maxUploadFileCount)
			return false;
		if (maxUploadFileSize != other.maxUploadFileSize)
			return false;
		if (minPanelHeight != other.minPanelHeight)
			return false;
		if (minPanelWidth != other.minPanelWidth)
			return false;
		if (profileId != other.profileId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WsApplicationControl [applicationVersion=" + applicationVersion
				+ ", enabled=" + enabled + ", maxPanelWidth=" + maxPanelWidth
				+ ", maxPanelHeight=" + maxPanelHeight + ", minPanelWidth="
				+ minPanelWidth + ", minPanelHeight=" + minPanelHeight
				+ ", maxUploadFileSize=" + maxUploadFileSize + ", profileId="
				+ profileId + ", maxUploadFileCount=" + maxUploadFileCount
				+ ", maxMarkerCount=" + maxMarkerCount
				+ ", defaultTextboxFontSize=" + defaultTextboxFontSize
				+ ", defaultMarkerWidth=" + defaultMarkerWidth
				+ ", defaultMarkerHeight=" + defaultMarkerHeight
				+ ", enableMarkerHistory=" + enableMarkerHistory
				+ ", arrayOfServiceFunction=" + arrayOfServiceFunction
				+ ", enableUploadUi=" + enableUploadUi + "]";
	}

	
}
