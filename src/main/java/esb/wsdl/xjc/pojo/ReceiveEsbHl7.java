package esb.wsdl.xjc.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "receiveEsbHl7", namespace = "http://ha.org.hk/cd2/ias/esbecosys/v1")
//@XmlRootElement(name = "receiveEsbHl7")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "receiveEsbHl7", namespace = "http://ha.org.hk/cd2/ias/esbecosys/v1")
//@XmlType()
//@XmlType(name="", propOrder={"esbHl7"})
public class ReceiveEsbHl7 {
	
	@XmlElement(name = "esbHl7", namespace = "http://ha.org.hk/cd2/ias/esbecosys/v1")
//	@XmlElement(name = "esbHl7")
	private String esbHl7;
	
	public ReceiveEsbHl7(){
		super();
	}
	
	public ReceiveEsbHl7(String esbHl7){
		super();
		this.esbHl7 = esbHl7;
	}

	public String getEsbHl7() {
		return esbHl7;
	}

	public void setEsbHl7(String esbHl7) {
		this.esbHl7 = esbHl7;
	}
	
}
