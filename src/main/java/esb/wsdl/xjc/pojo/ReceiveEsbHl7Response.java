package esb.wsdl.xjc.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "receiveEsbHl7Response", namespace = "http://ha.org.hk/cd2/ias/esbecosys/v1")
//@XmlRootElement(name = "receiveEsbHl7Response")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "receiveEsbHl7Response", namespace = "http://ha.org.hk/cd2/ias/esbecosys/v1")
//@XmlType(name = "", propOrder = {"receiveEsbHl7Result"})
public class ReceiveEsbHl7Response {
//	@XmlElement(name = "receiveEsbHl7Result", namespace = "")
	@XmlElement(name = "receiveEsbHl7Result")
    private int receiveEsbHl7Result;

    /**
     *
     * @return
     *     returns int
     */
    public int getReceiveEsbHl7Result() {
        return this.receiveEsbHl7Result;
    }

    /**
     *
     * @param receiveEsbHl7Result
     *     the value for the receiveEsbHl7Result property
     */
    public void setReceiveEsbHl7Result(int receiveEsbHl7Result) {
        this.receiveEsbHl7Result = receiveEsbHl7Result;
    }
	
}
