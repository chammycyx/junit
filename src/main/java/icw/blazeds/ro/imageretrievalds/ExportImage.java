/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

import icw.blazeds.ro.AbstractRemoteObject;

/**
 * @author YKF491
 *
 */
public class ExportImage extends AbstractRemoteObject {

	// fields ordering has to be the same as parameter ordering with public modifier
	public String ha7Msg;
	public String password;
	public String userId;
	public String workstationId;
	public String requestSys;
	
	public ExportImage() {
		super();
	}
	
	public ExportImage(String ha7Msg, String password, String userId, String workstationId, String requestSys) {
		super();
		this.ha7Msg = ha7Msg;
		this.password = password;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	public String getHa7Msg() {
		return ha7Msg;
	}
	public void setHa7Msg(String ha7Msg) {
		this.ha7Msg = ha7Msg;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	
}
