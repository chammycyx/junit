/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;


/**
 * @author YKF491
 *
 */
public class GetCidImageThumbnailResponse {

	private byte[] getCidImageThumbnailResult;

	public byte[] getGetCidImageThumbnailResult() {
		return (getCidImageThumbnailResult != null)?getCidImageThumbnailResult.clone():new byte[0];
	}

	public void setGetCidImageThumbnailResult(byte[] value) {
		this.getCidImageThumbnailResult = (value == null)?null:value.clone();
	}

}
