/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

/**
 * @author YKF491
 *
 */
public class ExportImageExResponse {

	private byte[] exportImageExResult;

	public byte[] getExportImageExResult() {
		return (exportImageExResult != null)?exportImageExResult.clone():new byte[0];
	}

	public void setExportImageExResult(byte[] value) {
		this.exportImageExResult = (value == null)?null:value.clone();
	}
}
