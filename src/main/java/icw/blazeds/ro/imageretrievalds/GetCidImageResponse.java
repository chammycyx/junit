/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;


/**
 * @author YKF491
 *
 */
public class GetCidImageResponse {

	private byte[] getCidImageResult;

	public byte[] getGetCidImageResult() {
		return (getCidImageResult != null)?getCidImageResult.clone():new byte[0];
	}

	public void setGetCidImageResult(byte[] value) {
		this.getCidImageResult = (value == null)?null:value.clone();
	}

}
