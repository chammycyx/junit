/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

import icw.blazeds.ro.AbstractRemoteObject;


/**
 * @author YKF491
 *
 */
public class GetCidStudy extends AbstractRemoteObject {

	// fields ordering has to be the same as parameter ordering with public modifier
	public String patientKey;
	public String hospCde;
	public String caseNo;
	public String accessionNo;
	public String seriesNo;
	public String imageSeqNo;
	public String versionNo;
	public String userId;
	public String workstationId;
	public String requestSys;
	
	public GetCidStudy() {
		super();
	}
	
	public GetCidStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String userId, String workstationId, String requestSys) {
		super();
		this.patientKey = patientKey;
		this.hospCde = hospCde;
		this.caseNo = caseNo;
		this.accessionNo = accessionNo;
		this.seriesNo = seriesNo;
		this.imageSeqNo = imageSeqNo;
		this.versionNo = versionNo;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	public String getPatientKey() {
		return patientKey;
	}
	public void setPatientKey(String patientKey) {
		this.patientKey = patientKey;
	}
	public String getHospCde() {
		return hospCde;
	}
	public void setHospCde(String hospCde) {
		this.hospCde = hospCde;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getAccessionNo() {
		return accessionNo;
	}
	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}
	public String getSeriesNo() {
		return seriesNo;
	}
	public void setSeriesNo(String seriesNo) {
		this.seriesNo = seriesNo;
	}
	public String getImageSeqNo() {
		return imageSeqNo;
	}
	public void setImageSeqNo(String imageSeqNo) {
		this.imageSeqNo = imageSeqNo;
	}
	public String getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	
}
