/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

/**
 * @author YKF491
 *
 */
public class GetCidStudyFirstLastImageResponse {

	private String ha7Result;

	public String getHa7Result() {
		return ha7Result;
	}

	public void setHa7Result(String ha7Result) {
		this.ha7Result = ha7Result;
	}

	public String getGetCidStudyFirstLastImageResult() {
		// TODO Auto-generated method stub
		return ha7Result;
	}
}
