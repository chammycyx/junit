/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;


/**
 * @author YKF491
 *
 */
public class GetCidScaledImageResponse {

	private byte[] getCidScaledImageResult;

	public byte[] getCidScaledImageResult() {
		return (getCidScaledImageResult != null)?getCidScaledImageResult.clone():new byte[0];
	}

	public void setCidScaledImageResult(byte[] value) {
		this.getCidScaledImageResult = (value == null)?null:value.clone();
	}

	public byte[] getGetCidScaledImageResult() {
		// TODO Auto-generated method stub
		return (getCidScaledImageResult != null)?getCidScaledImageResult.clone():new byte[0];
	}

}
