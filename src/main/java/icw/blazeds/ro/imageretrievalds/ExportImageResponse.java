/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

/**
 * @author YKF491
 *
 */
public class ExportImageResponse {

	private byte[] exportImageResult;

	public byte[] getExportImageResult() {
		return (exportImageResult != null)?exportImageResult.clone():new byte[0];
	}

	public void setExportImageResult(byte[] value) {
		this.exportImageResult = (value == null)?null:value.clone();
	}
}
