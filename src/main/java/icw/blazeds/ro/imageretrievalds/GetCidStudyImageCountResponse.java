/**
 * 
 */
package icw.blazeds.ro.imageretrievalds;

/**
 * @author YKF491
 *
 */
public class GetCidStudyImageCountResponse {

	private Integer imageCount;

	public Integer getImageCount() {
		return imageCount;
	}

	public void setImageCount(Integer imageCount) {
		this.imageCount = imageCount;
	}

	public int getGetCidStudyImageCountResult() {
		// TODO Auto-generated method stub
		return imageCount;
	}
	
}
