package icw.blazeds.ro.securitymanagerds;

import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetApplicationControlResult" type="{http://cid.ha.org.hk/cid}ApplicationControl" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getApplicationControlResult" })
@XmlRootElement(name = "getApplicationControlResponse")
public class GetApplicationControlResponse {

	@XmlElement(name = "getApplicationControlResult")
	protected WsApplicationControl getApplicationControlResult;

	/**
	 * Gets the value of the getApplicationControlResult property.
	 * 
	 * @return possible object is {@link WsApplicationControl }
	 */
	public WsApplicationControl getGetApplicationControlResult() {
		return getApplicationControlResult;
	}

	/**
	 * Sets the value of the getApplicationControlResult property.
	 * 
	 * @param value
	 *            allowed object is {@link WsApplicationControl }
	 */
	public void setGetApplicationControlResult(WsApplicationControl value) {
		this.getApplicationControlResult = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((getApplicationControlResult == null) ? 0
						: getApplicationControlResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetApplicationControlResponse other = (GetApplicationControlResponse) obj;
		if (getApplicationControlResult == null) {
			if (other.getApplicationControlResult != null)
				return false;
		} else if (!getApplicationControlResult
				.equals(other.getApplicationControlResult))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GetApplicationControlResponse [getApplicationControlResult="
				+ getApplicationControlResult + "]";
	}

	
}
