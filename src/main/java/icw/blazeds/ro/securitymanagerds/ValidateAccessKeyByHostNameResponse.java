/**
 * 
 */
package icw.blazeds.ro.securitymanagerds;

/**
 * @author YKF491
 *
 */
public class ValidateAccessKeyByHostNameResponse {
	private Boolean isvalid;

	public Boolean getIsValid() {
		return isvalid;
	}

	public void setIsValid(Boolean isvalid) {
		this.isvalid = isvalid;
	}
	
}
