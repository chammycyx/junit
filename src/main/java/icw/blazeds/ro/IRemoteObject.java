/**
 * 
 */
package icw.blazeds.ro;

/**
 * @author YKF491
 *
 */
public interface IRemoteObject {
	public Object[] getParameters() throws IllegalArgumentException, IllegalAccessException ;
	public String getOperation();
}
