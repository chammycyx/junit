/**
 * 
 */
package icw.blazeds.ro;

import java.beans.Introspector;
import java.lang.reflect.Field;

/**
 * @author YKF491
 *
 */
public abstract class AbstractRemoteObject implements IRemoteObject {
	
	public Object[] getParameters() throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = this.getClass().getDeclaredFields();
		Object[] params = new Object[fields.length];
		for(int i = 0; i < fields.length; i++) {
			params[i] = fields[i].get(this);
		}
		return params;
	}
	
	public String getOperation() {
		return Introspector.decapitalize(this.getClass().getSimpleName());
	}
}
