/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

import icw.blazeds.ro.AbstractRemoteObject;

/**
 * @author YKF491
 *
 */
public class GetAccessionNo extends AbstractRemoteObject {
	
	// fields ordering has to be the same as parameter ordering with public modifier
	public String hospCde;
	public String deptCde;
	public String userId;
	public String workstationId;
	public String requestSys;

	public GetAccessionNo() {
		super();
	}
	
	public GetAccessionNo(String hospCde, String deptCde, String userId, String workstationId, String requestSys) {
		super();
		this.hospCde = hospCde;
		this.deptCde = deptCde;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	public String getHospCde() {
		return hospCde;
	}
	public void setHospCde(String hospCde) {
		this.hospCde = hospCde;
	}
	public String getDeptCde() {
		return deptCde;
	}
	public void setDeptCde(String deptCde) {
		this.deptCde = deptCde;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

}

