/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

import icw.blazeds.ro.AbstractRemoteObject;

/**
 * @author YKF491
 *
 */
public class UploadMetaDataEx extends AbstractRemoteObject {

	// fields ordering has to be the same as parameter ordering with public modifier
	public String ha7Message;
	public String userId;
	public String workstationId;
	public String requestSys;
	
	public UploadMetaDataEx() {
		super();
	}
	
	public UploadMetaDataEx(String ha7Message, String userId, String workstationId, String requestSys) {
		super();
		this.ha7Message = ha7Message;
		this.userId = userId;
		this.workstationId = workstationId;
		this.requestSys = requestSys;
	}
	
	public String getHa7Message() {
		return ha7Message;
	}
	public void setHa7Message(String ha7Message) {
		this.ha7Message = ha7Message;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	
}
