/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

/**
 * @author YKF491
 *
 */
public class GetAccessionNoResponse {
	
	private String accessionNo;

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}
	
}
