/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

/**
 * @author YKF491
 *
 */
public class UploadImageResponse {

	private String uploadImageResult;

	public String getUploadImageResult() {
		return uploadImageResult;
	}

	public void setUploadImageResult(String uploadImageResult) {
		this.uploadImageResult = uploadImageResult;
	}
}
