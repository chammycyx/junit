/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

import icw.blazeds.ro.AbstractRemoteObject;

/**
 * @author YKF491
 *
 */
public class UploadImage extends AbstractRemoteObject {

	// fields ordering has to be the same as parameter ordering with public modifier
	public byte[] imgBinaryArray;
	public String requestSys;
	public String filename;
	public String patientKey;
	public String studyId;
	public String seriesNo;
	public String userId;
	public String workstationId;

	public UploadImage() {
		super();
	}
	
	public UploadImage(byte[] imgBinaryArray, String requestSys, String filename, String patientKey, String studyId,
			String seriesNo, String userId, String workstationId) {
		super();
		this.imgBinaryArray = imgBinaryArray;
		this.requestSys = requestSys;
		this.filename = filename;
		this.patientKey = patientKey;
		this.studyId = studyId;
		this.seriesNo = seriesNo;
		this.userId = userId;
		this.workstationId = workstationId;
	}
	
	public byte[] getImgBinaryArray() {
		return imgBinaryArray;
	}
	public void setImgBinaryArray(byte[] imgBinaryArray) {
		this.imgBinaryArray = imgBinaryArray;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPatientKey() {
		return patientKey;
	}
	public void setPatientKey(String patientKey) {
		this.patientKey = patientKey;
	}
	public String getStudyId() {
		return studyId;
	}
	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	public String getSeriesNo() {
		return seriesNo;
	}
	public void setSeriesNo(String seriesNo) {
		this.seriesNo = seriesNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	
	
}
