/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

/**
 * @author YKF491
 *
 */
public class DeleteStudyResponse {

	private String deleteStudyResult;

	public String getDeleteStudyResult() {
		return deleteStudyResult;
	}

	public void setDeleteStudyResult(String deleteStudyResult) {
		this.deleteStudyResult = deleteStudyResult;
	}
	
}
