/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

/**
 * @author YKF491
 *
 */
public class UploadMetaDataResponse {

	private Integer uploadMetaDataResult;

	public Integer getUploadMetaDataResult() {
		return uploadMetaDataResult;
	}

	public void setUploadMetaDataResult(Integer uploadMetaDataResult) {
		this.uploadMetaDataResult = uploadMetaDataResult;
	}
	
}
