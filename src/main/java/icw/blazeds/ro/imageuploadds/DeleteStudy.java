/**
 * 
 */
package icw.blazeds.ro.imageuploadds;

import icw.blazeds.ro.AbstractRemoteObject;

/**
 * @author YKF491
 *
 */
public class DeleteStudy extends AbstractRemoteObject {

	// fields ordering has to be the same as parameter ordering with public modifier
	public String patientKey;
    public String hospCde;
    public String caseNo;
    public String accessionNo;
    public String studyID;

	public DeleteStudy() {
		super();
	}
	
	public DeleteStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String studyID) {
		super();
		this.patientKey = patientKey;
		this.hospCde = hospCde;
		this.caseNo = caseNo;
		this.accessionNo = accessionNo;
		this.studyID = studyID;
	}
	
	public String getPatientKey() {
		return patientKey;
	}
	public void setPatientKey(String patientKey) {
		this.patientKey = patientKey;
	}
	public String getHospCde() {
		return hospCde;
	}
	public void setHospCde(String hospCde) {
		this.hospCde = hospCde;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getAccessionNo() {
		return accessionNo;
	}
	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}
	public String getStudyID() {
		return studyID;
	}
	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}
    
}
