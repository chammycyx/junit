//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.19 at 12:24:27 PM CST 
//


package icw.wsdl.xjc.epr.cid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deLinkERSRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deLinkERSRecord">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="patient_key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inst_cd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="case_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accession_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="study_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { //@XmlType(name = "deLinkERSRecord", propOrder = {
    "patientKey",
    "instCd",
    "caseNo",
    "accessionNo",
    "studyId"
})
@XmlRootElement(name = "deLinkERSRecord") //deLinkERSRecord
public class DeLinkERSRecord {

    @XmlElement(name = "patient_key")
    protected String patientKey;
    @XmlElement(name = "inst_cd")
    protected String instCd;
    @XmlElement(name = "case_no")
    protected String caseNo;
    @XmlElement(name = "accession_no")
    protected String accessionNo;
    @XmlElement(name = "study_id")
    protected String studyId;

    public DeLinkERSRecord() {
    	super();
    }
    
    public DeLinkERSRecord(String patientKey, String instCd, String caseNo, String accessionNo, String studyId) {
		super();
		this.patientKey = patientKey;
		this.instCd = instCd;
		this.caseNo = caseNo;
		this.accessionNo = accessionNo;
		this.studyId = studyId;
	}

	/**
     * Gets the value of the patientKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatientKey() {
        return patientKey;
    }

    /**
     * Sets the value of the patientKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatientKey(String value) {
        this.patientKey = value;
    }

    /**
     * Gets the value of the instCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstCd() {
        return instCd;
    }

    /**
     * Sets the value of the instCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstCd(String value) {
        this.instCd = value;
    }

    /**
     * Gets the value of the caseNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNo() {
        return caseNo;
    }

    /**
     * Sets the value of the caseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNo(String value) {
        this.caseNo = value;
    }

    /**
     * Gets the value of the accessionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessionNo() {
        return accessionNo;
    }

    /**
     * Sets the value of the accessionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessionNo(String value) {
        this.accessionNo = value;
    }

    /**
     * Gets the value of the studyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStudyId() {
        return studyId;
    }

    /**
     * Sets the value of the studyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStudyId(String value) {
        this.studyId = value;
    }

}
