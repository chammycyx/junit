/**
 *
 */
package org.ha.cd2.isg.icw.client.upload;

import icw.wsdl.xjc.pojo.DeleteStudy;
import icw.wsdl.xjc.pojo.DeleteStudyResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>deleteCidStudy</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class DeleteStudyClient extends WebServiceGatewaySupport {

	private String deleteStudySoapAction;

	/**
	 * Invoke the <tt>deleteCidStudy</tt>.
	 *
	 * @param request the {@link UploadMeta}
	 * @return a {@link UploadMetaResponse}
	 */
	public DeleteStudyResponse deleteStudy(final DeleteStudy request) {

		return (DeleteStudyResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(deleteStudySoapAction)
		);
	}

	/**
	 * @param deleteCidStudySoapAction the soap action of <tt>deleteCidStudy</tt> to set
	 */
	public void setDeleteCidStudySoapAction(String deleteStudySoapAction) {
		this.deleteStudySoapAction = deleteStudySoapAction;
	}

}
