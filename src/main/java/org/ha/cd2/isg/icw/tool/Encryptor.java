package org.ha.cd2.isg.icw.tool;

import org.springframework.security.crypto.codec.Base64;

public class Encryptor {
	
	public static void main(String[] arg) {
		byte[]   bytesEncoded = Base64.encode("axonmnuser".getBytes());
		System.out.println("ecncoded value is " + new String(bytesEncoded ));

		// Decode data on other side, by processing encoded data
		byte[] valueDecoded= Base64.decode(bytesEncoded );
		System.out.println("Decoded value is " + new String(valueDecoded));
		
	}
	
	
	public static String encode(String magic){
		byte[] bytesEncoded = Base64.encode(magic.getBytes());
		return new String(bytesEncoded);
	}
	
	public static String decode(String encryptedStr){
		byte[] valueDecoded= Base64.decode(encryptedStr.getBytes());
		return new String(valueDecoded);
	}
}


