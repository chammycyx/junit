package org.ha.cd2.isg.icw.tool;

import java.io.File;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CidStorePathUtil {

	private static final Logger LOG = LoggerFactory.getLogger(CidStorePathUtil.class);

	private static final String INDEX_FILE_NAME = "index.xml";
	private static final String SEPARATOR = "/";

	public static File generateIndexFilePath(File cidRootDir,
			String studyInstanceUID) {
		File studyPath = generateStudyPath(cidRootDir, studyInstanceUID);
		return new File(studyPath, INDEX_FILE_NAME);
	}

	public static File generateSopPath(File cidRootDir,
			String studyInstanceUID, String seriesInstanceUID, String fileName) {
		File seriesPath = generateSeriesPath(cidRootDir, studyInstanceUID,
				seriesInstanceUID);
		return new File(seriesPath, fileName);
	}

	public static File generateSeriesPath(File cidRootDir,
			String studyInstanceUID, String seriesInstanceUID) {
		if (seriesInstanceUID == null)
			return null;
		File studyPath = generateStudyPath(cidRootDir, studyInstanceUID);
		return new File(studyPath, seriesInstanceUID);
	}

	public static File generateStudyPath(File cidRootDir,
			String studyInstanceUID) {
		if (studyInstanceUID == null)
			return null;
		String seriesFolder = generateStudyPath(studyInstanceUID);
		LOG.info("to store Path:" + seriesFolder);
		File seriesFile = new File(cidRootDir, seriesFolder);
		seriesFile.mkdirs();
		return seriesFile;
	}

	public static String generateStudyPath(String studyInstanceUID) {
		String digestedStudyInstanceUID = DigestUtils.md5Hex(studyInstanceUID);
		String first = digestedStudyInstanceUID.substring(0, 3);
		String second = digestedStudyInstanceUID.substring(3, 6);
		String third = digestedStudyInstanceUID.substring(6, 9);
		String forth = digestedStudyInstanceUID.substring(9, 12);
		return first + SEPARATOR + second + SEPARATOR
				+ third + SEPARATOR + forth + SEPARATOR
				+ studyInstanceUID + SEPARATOR;
	}
}
