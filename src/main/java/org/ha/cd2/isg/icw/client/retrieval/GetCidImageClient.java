package org.ha.cd2.isg.icw.client.retrieval;


import icw.wsdl.xjc.pojo.GetCidImage;
import icw.wsdl.xjc.pojo.GetCidImageResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class GetCidImageClient extends WebServiceGatewaySupport {
	
	private String getCidImageSoapAction;

	/**
	 * Invoke the <tt>getCidImage</tt>.
	 *
	 * @param request the {@link GetCidImage}
	 * @return a {@link GetCidImageResponse}
	 */
	public GetCidImageResponse getCidImage(final GetCidImage request) {

		return (GetCidImageResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidImageSoapAction)
		);
	}

	/**
	 * @param getCidImageSoapAction the soap action of <tt>getCidImage</tt> to set
	 */
	public void setGetCidImageSoapAction(String getCidImageSoapAction) {
		this.getCidImageSoapAction = getCidImageSoapAction;
	}

}
