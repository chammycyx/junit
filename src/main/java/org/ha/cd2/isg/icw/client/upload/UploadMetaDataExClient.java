/**
 *
 */
package org.ha.cd2.isg.icw.client.upload;

import icw.wsdl.xjc.pojo.UploadMetaDataEx;
import icw.wsdl.xjc.pojo.UploadMetaDataExResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>uploadMetaData</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class UploadMetaDataExClient extends WebServiceGatewaySupport {

	private String uploadMetaDataExSoapAction;

	/**
	 * Invoke the <tt>uploadMetaData</tt>.
	 *
	 * @param request the {@link UploadMeta}
	 * @return a {@link UploadMetaResponse}
	 */
	public UploadMetaDataExResponse uploadMetaDataEx(final UploadMetaDataEx request) {

		return (UploadMetaDataExResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(uploadMetaDataExSoapAction)
		);
	}

	/**
	 * @param uploadMetaDataSoapAction the soap action of <tt>uploadMetaData</tt> to set
	 */
	public void setUploadMetaDataExSoapAction(String uploadMetaDataExSoapAction) {
		this.uploadMetaDataExSoapAction = uploadMetaDataExSoapAction;
	}

}
