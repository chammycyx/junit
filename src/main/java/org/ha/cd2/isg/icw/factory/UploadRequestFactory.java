/**
 *
 */
package org.ha.cd2.isg.icw.factory;

//import icw.wsdl.xjc.pojo.GetAccessionNo;
import icw.wsdl.xjc.pojo.ObjectFactory;
import icw.wsdl.xjc.pojo.UploadImage;
import icw.wsdl.xjc.pojo.UploadMetaData;
//import icw.wsdl.xjc.pojo.ValidateAccessionNo;



/**
 * The class is a factory class for creating different types of request.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class UploadRequestFactory {

	private static UploadRequestFactory INSTANCE = new UploadRequestFactory();

	private ObjectFactory objectFactory;

	/**
	 * Create a {@link UploadRequestFactory}.
	 *
	 * @return a {@link UploadRequestFactory}
	 */
	public static UploadRequestFactory createInstance() {
		return INSTANCE;
	}

	public void setObjectFactory(ObjectFactory objectFactory) {
		this.objectFactory = objectFactory;
	}
	/**
	 * Create a {@link GetCidStudy}.
	 *
	 * @return a {@link GetCidStudy}
	 */
//	public GetAccessionNo createGetAccessionNoRequest() {
//		return objectFactory.createGetAccessionNo();
//	}

	public UploadImage createUploadImageRequest(){
		return objectFactory.createUploadImage();
	}
	
//	public ValidateAccessionNo createValidateAccessionNoRequest(){
//		return objectFactory.createValidateAccessionNo();
//	}

	public UploadMetaData createUploadMetaDataRequest(){
		return objectFactory.createUploadMetaData();
	}
}
