/**
 *
 */
package org.ha.cd2.isg.icw.client.upload;

import icw.wsdl.xjc.pojo.UploadMetaData;
import icw.wsdl.xjc.pojo.UploadMetaDataResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>uploadMetaData</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class UploadMetaDataClient extends WebServiceGatewaySupport {

	private String uploadMetaDataSoapAction;

	/**
	 * Invoke the <tt>uploadMetaData</tt>.
	 *
	 * @param request the {@link UploadMeta}
	 * @return a {@link UploadMetaResponse}
	 */
	public UploadMetaDataResponse uploadMetaData(final UploadMetaData request) {

		return (UploadMetaDataResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(uploadMetaDataSoapAction)
		);
	}

	/**
	 * @param uploadMetaDataSoapAction the soap action of <tt>uploadMetaData</tt> to set
	 */
	public void setUploadMetaDataSoapAction(String uploadMetaDataSoapAction) {
		this.uploadMetaDataSoapAction = uploadMetaDataSoapAction;
	}

}
