/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidScaledImage;
import icw.wsdl.xjc.pojo.GetCidScaledImageResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidScaledImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidScaledImageClient extends WebServiceGatewaySupport {

	private String getCidScaledImageSoapAction;

	/**
	 * Invoke the <tt>getCidScaledImage</tt>.
	 *
	 * @param request the {@link UploadMeta}
	 * @return a {@link UploadMetaResponse}
	 */
	public GetCidScaledImageResponse getCidScaledImage(final GetCidScaledImage request) {

		return (GetCidScaledImageResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidScaledImageSoapAction)
		);
	}

	/**
	 * @param getCidScaledImageSoapAction the soap action of <tt>getCidScaledImage</tt> to set
	 */
	public void setGetCidScaledImageSoapAction(String getCidScaledImageSoapAction) {
		this.getCidScaledImageSoapAction = getCidScaledImageSoapAction;
	}

}
