/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImageEx;
import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImageExResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidStudyFirstLastImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidStudyFirstLastImageExClient extends WebServiceGatewaySupport {

	private String getCidStudyFirstLastImageExSoapAction;

	/**
	 * Invoke the <tt>getCidStudyFirstLastImage</tt>.
	 *
	 * @param request the {@link GetCidStudyFirstLastImage}
	 * @return a {@link GetCidStudyFirstLastImageResponse}
	 */
	public GetCidStudyFirstLastImageExResponse getCidStudyFirstLastImageEx(final GetCidStudyFirstLastImageEx request) {

		return (GetCidStudyFirstLastImageExResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidStudyFirstLastImageExSoapAction)
		);
	}

	/**
	 * @param getCidStudyFirstLastImageSoapAction the soap action of <tt>getCidStudyFirstLastImage</tt> to set
	 */
	public void setGetCidStudyFirstLastImageExSoapAction(String getCidStudyFirstLastImageExSoapAction) {
		this.getCidStudyFirstLastImageExSoapAction = getCidStudyFirstLastImageExSoapAction;
	}

}
