package org.ha.cd2.isg.icw.epr.client;


import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import epr.wsdl.xjc.pojo.SaveCID;
import epr.wsdl.xjc.pojo.SaveCIDResponse;

public class SaveCIDClient extends WebServiceGatewaySupport {
	
	private String saveCIDSoapAction;

	/**
	 * Invoke the <tt>getCidImage</tt>.
	 *
	 * @param request the {@link GetCidImage}
	 * @return a {@link GetCidImageResponse}
	 */
	public SaveCIDResponse saveCID(final SaveCID request) {

		return (SaveCIDResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(saveCIDSoapAction)
		);
	}

	/**
	 * @param saveCIDSoapAction the soap action of <tt>getCidImage</tt> to set
	 */
	public void setSaveCIDSoapAction(String saveCIDSoapAction) {
		this.saveCIDSoapAction = saveCIDSoapAction;
	}

}
