/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidStudyEx;
import icw.wsdl.xjc.pojo.GetCidStudyExResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidStudy</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidStudyExClient extends WebServiceGatewaySupport {

	private String getCidStudyExSoapAction;

	/**
	 * Invoke the <tt>getCidStudy</tt>.
	 *
	 * @param request the {@link GetCidStudy}
	 * @return a {@link GetCidStudyResponse}
	 */
	public GetCidStudyExResponse getCidStudyEx(final GetCidStudyEx request) {

		return (GetCidStudyExResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidStudyExSoapAction)
		);
	}

	/**
	 * @param getCidStudySoapAction the soap action of <tt>getCidStudy</tt> to set
	 */
	public void setGetCidStudyExSoapAction(String getCidStudyExSoapAction) {
		this.getCidStudyExSoapAction = getCidStudyExSoapAction;
	}

}
