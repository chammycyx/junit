package org.ha.cd2.isg.icw.tool;

import icw.wsdl.xjc.pojo.CIDAck;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class Ha7AckMarshaller {
	
	private Jaxb2Marshaller marshaller;
	private Jaxb2Marshaller unmarshaller;
	
	
	public Jaxb2Marshaller getMarshaller() {
		return marshaller;
	}
	public void setMarshaller(Jaxb2Marshaller marshaller) {
		this.marshaller = marshaller;
	}
	public Jaxb2Marshaller getUnmarshaller() {
		return unmarshaller;
	}
	public void setUnmarshaller(Jaxb2Marshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}
	
	public CIDAck unmarshal(String xml){
		Source source = new StreamSource(new StringReader(xml));
		CIDAck data = (CIDAck)marshaller.unmarshal(source);
		return data;
	}
	
	public String marshal(CIDAck data){
		
		StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(data, result);
		
		return writer.toString();
	}
}
