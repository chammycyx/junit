/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidStudyImageCount;
import icw.wsdl.xjc.pojo.GetCidStudyImageCountResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidStudyImageCount</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidStudyImageCountClient extends WebServiceGatewaySupport {

	private String getCidStudyImageCountSoapAction;

	/**
	 * Invoke the <tt>getCidStudyImageCount</tt>.
	 *
	 * @param request the {@link GetCidStudyImageCountClient}
	 * @return a {@link GetCidStudyImageCountClientResponse}
	 */
	public GetCidStudyImageCountResponse getCidStudyImageCount(final GetCidStudyImageCount request) {

		return (GetCidStudyImageCountResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidStudyImageCountSoapAction)
		);
	}

	/**
	 * @param getCidStudyImageCountSoapAction the soap action of <tt>getCidStudyImageCount</tt> to set
	 */
	public void setGetCidStudyImageCountSoapAction(String getCidStudyImageCountSoapAction) {
		this.getCidStudyImageCountSoapAction = getCidStudyImageCountSoapAction;
	}

}
