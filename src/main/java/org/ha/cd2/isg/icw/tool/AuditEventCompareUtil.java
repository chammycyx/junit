package org.ha.cd2.isg.icw.tool;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.ha.cid.ias.model.AuditEventDto;
import org.junit.Assert;

/**
 * @author LSM131
 * 
 */
public class AuditEventCompareUtil {
	public static void assertAuditEventsSuccess(List<AuditEventDto> auditEventDtos, List<String> auditEventNames) {
		if(auditEventDtos == null) {
			Assert.fail("No audit events were found.");
		}
		
		auditEventDtos.sort(new AuditEventDtoComparator());
		
		Iterator<AuditEventDto> iterator = auditEventDtos.iterator();
		
		while(iterator.hasNext()) {
			AuditEventDto auditEventDto = iterator.next();
			System.out.println("[AuditEventCompareUtil] " + auditEventDto.getDate() + ", " + auditEventDto.getEvent() + ", " + auditEventDto.getStatus());
			Assert.assertTrue(auditEventNames.contains(auditEventDto.getEvent()));
			Assert.assertEquals("Success", auditEventDto.getStatus());
		}
	}
	
	private static class AuditEventDtoComparator implements Comparator<AuditEventDto> {
		@Override
		public int compare(AuditEventDto object1, AuditEventDto object2) {
			return object1.getDate().compareTo(object2.getDate());
		}
	}
}