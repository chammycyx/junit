/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */

package org.ha.cd2.isg.icw.tool;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * The utility class to establish SSH session with remote machine
 *
 * <pre>
 * Last commit:-
 * $Rev: 9090 $
 * $Author: cft545 $
 * $Date: 2016-04-18 11:20:57 +0800 (Mon, 18 Apr 2016) $
 * </pre>
 *
 */
public class SshUtil {

    /**
     * Create SSH session of remote machine
     * 
     * @param host Host name of remote machine
     * @param username User name of remote machine
     * @param password Password of remote machine
     * @return SSH session
     * @throws IOException
     */
    public static Session createSession(String host, String username,
            String password) throws IOException {

        Session session = null;
        JSch jsch = new JSch();

        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");

        try {
            session = jsch.getSession(username, host, 22);
            session.setConfig(config);
            session.setPassword(password);
            session.connect();

        } catch (JSchException e) {
            if (session != null) {
                session.disconnect();
            }
            throw new IOException("Failed to create SSH session", e);
        }
        return session;
    }

    /**
     * Check if a file exists in remote machine
     * 
     * @param session SSH session
     * @param filePath File path in remote machine
     * @return true if exist
     * @throws IOException
     */
    public static boolean isFileExist(Session session, String filePath) throws IOException {
    	boolean result = true;
    	Channel channel = null;
        try {

        	channel = session.openChannel("sftp");
			channel.connect();
			
			try {
				((ChannelSftp) channel).ls(filePath);
	        } catch (SftpException e) {
	        	result = false;
	        }
			
            channel.disconnect();
        } catch (JSchException e) {
        	throw new IOException("Fail to connect SSH channel", e);
        } finally {

            if (channel != null) {
                channel.disconnect();
            }

        }
        return result;
    }
    
    /**
     * Get MD5 checksum of a file in remote machine
     * 
     * @param session SSH session
     * @param filePath File path in remote machine
     * @return MD5 checksum
     * @throws IOException
     */
    public static String getMd5(Session session, String filePath) throws IOException{
        
        Channel channel = null;
        StringBuilder outputBuffer = new StringBuilder();
        try {

            channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(String.format("md5sum \"%s\" |awk '{print $1}'", filePath));

            BufferedInputStream in = new BufferedInputStream(channel.getInputStream());
            channel.connect();

            int a;
            while((a = in.read()) != 0xffffffff){
                outputBuffer.append((char)a);
            }

            channel.disconnect();
            if(in != null){
                in.close();
            }
        } catch (JSchException e) {
        	throw new IOException("Fail to connect SSH channel", e);
        } finally {

            if (channel != null) {
                channel.disconnect();
            }

        }
        return outputBuffer.toString().replace("\n", "");
    }
}
