/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.ExportImage;
import icw.wsdl.xjc.pojo.ExportImageResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>exportImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class ExportImageClient extends WebServiceGatewaySupport {

	private String exportImageSoapAction;

	/**
	 * Invoke the <tt>exportImage</tt>.
	 *
	 * @param request the {@link ExportImage}
	 * @return a {@link ExportImageResponse}
	 */
	public ExportImageResponse exportImage(final ExportImage request) {

		return (ExportImageResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(exportImageSoapAction)
		);
	}

	/**
	 * @param exportImageSoapAction the soap action of <tt>exportImage</tt> to set
	 */
	public void setExportImageSoapAction(String exportImageSoapAction) {
		this.exportImageSoapAction = exportImageSoapAction;
	}

}
