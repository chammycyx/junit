package org.ha.cd2.isg.icw.tool;


import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;

public class IndexUtil {

	public static int getCIDStudioImagePosVersion(int ImagePos, int baseVersion){
		// 2nd Image, Verison 2 = seq 5    return 4
		// 4th Image, Version 3 = seq 12   return 11
		// 4th Image, Version 2 = seq 11   return 10
		
		int multiple = 3;
		int offset = multiple * (ImagePos - 1) + baseVersion; 
		
		return offset - 1;
	}
	
	
	// get the imageDtl object by in List<ImageDtl> by index number start from 0
	public static ImageDtl getImageDtl(CIDData data, int index){
		return data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().get(index);
	}
	
	public static void main(String[] args) {
		System.out.println(IndexUtil.getCIDStudioImagePosVersion(1, 2));
		System.out.println(getCIDStudioImagePosVersion(2, 2));
		System.out.println(getCIDStudioImagePosVersion(4, 3));
		System.out.println(getCIDStudioImagePosVersion(4, 2));
		
		System.out.println(getRTCStudioImagePosVersion(2, 2));
		System.out.println(getRTCStudioImagePosVersion(4, 3));
		System.out.println(getRTCStudioImagePosVersion(4, 2));
	}

	public static int getRTCStudioImagePosVersion(int ImagePos, int baseVersion) {
		// 2nd Image, Verison 2 = seq 4    return 3
		// 4th Image, Version 3 = seq 12   return 11 xxx
		// 4th Image, Version 2 = seq 8   return 7
		
		int multiple = 2;
		int offset = multiple * (ImagePos - 1) + baseVersion; 
		
		return offset - 1;
	}
}
