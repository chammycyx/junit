package org.ha.cd2.isg.icw.tool;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.MessageDtl;
import icw.wsdl.xjc.pojo.CIDData.PatientDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl;
import icw.wsdl.xjc.pojo.CIDData.VisitDtl;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Assert;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This Ha7 Utility is used to compare CIDData and HA7 with/without transaction information
 *
 * @author YKF491
 *
 */
public class Ha7Util {

	public static boolean isCIDDataEqual(CIDData data1, CIDData data2)
	{
		return data1.equals(data2);
	}

	public static boolean isCIDDataSkipTransactionDataEqual(CIDData data1, CIDData data2)
	{
		return data1.getPatientDtl().equals(data2.getPatientDtl()) &&
				data1.getVisitDtl().equals(data2.getVisitDtl()) &&
				data1.getStudyDtl().equals(data2.getStudyDtl()) &&
				data1.getMessageDtl().getSendingApplication().equals(data2.getMessageDtl().getSendingApplication()) &&
				data1.getMessageDtl().getServerHosp().equals(data2.getMessageDtl().getServerHosp()) &&
				data1.getMessageDtl().getTransactionCode().equals(data2.getMessageDtl().getTransactionCode());
	}

	public static boolean isHa7Equal(String Ha7Message1, String Ha7Message2)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		CIDData data1 = marshaller1.unmarshal(Ha7Message1);

		Ha7Marshaller marshaller2 = getMarshaller();
		CIDData data2 = marshaller2.unmarshal(Ha7Message2);

		return isCIDDataEqual(data1, data2);
	}

	public static boolean isHa7SkipTransactionDataEqual(String Ha7Message1, String Ha7Message2)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		CIDData data1 = marshaller1.unmarshal(Ha7Message1);

		Ha7Marshaller marshaller2 = getMarshaller();
		CIDData data2 = marshaller2.unmarshal(Ha7Message2);

		return isCIDDataSkipTransactionDataEqual(data1, data2);
	}

	public static void assertCIDDataEqual(CIDData data1, CIDData data2)
	{
		Assert.assertEquals(data1, data2);
	}

	public static void assertCIDDataSkipTransactionDataEqual(CIDData expected, CIDData actual)
	{
		Assert.assertEquals(expected.getPatientDtl(), actual.getPatientDtl());
		Assert.assertEquals(expected.getVisitDtl(),actual.getVisitDtl());
		Assert.assertEquals(expected.getStudyDtl(),actual.getStudyDtl());
		Assert.assertEquals(expected.getMessageDtl().getSendingApplication(),actual.getMessageDtl().getSendingApplication());
		Assert.assertEquals(expected.getMessageDtl().getServerHosp(),actual.getMessageDtl().getServerHosp());
		Assert.assertEquals(expected.getMessageDtl().getTransactionCode(),actual.getMessageDtl().getTransactionCode());
	}

	public static void assertCIDAckSkipTransactionDataEqual(CIDAck expected, CIDAck actual)
	{
		Assert.assertEquals(expected.getAckDtl().getAccessionNo(), actual.getAckDtl().getAccessionNo());
		Assert.assertEquals(expected.getAckDtl().getStudyID(),actual.getAckDtl().getStudyID());
		Assert.assertEquals(expected.getAckDtl().getActionStatus(),actual.getAckDtl().getActionStatus());
		Assert.assertEquals(expected.getAckDtl().getStudyDtm(),actual.getAckDtl().getStudyDtm());
		Assert.assertEquals(expected.getAckDtl().getActionRemarks(),actual.getAckDtl().getActionRemarks());
		Assert.assertEquals(expected.getMessageDtl().getSendingApplication(),actual.getMessageDtl().getSendingApplication());
		Assert.assertEquals(expected.getMessageDtl().getServerHosp(),actual.getMessageDtl().getServerHosp());
		Assert.assertEquals(expected.getMessageDtl().getTransactionCode(),actual.getMessageDtl().getTransactionCode());
	}

	public static void assertHa7Equal(String expected, String actual)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		CIDData data1 = marshaller1.unmarshal(expected);

		Ha7Marshaller marshaller2 = getMarshaller();
		CIDData data2 = marshaller2.unmarshal(actual);

		assertCIDDataEqual(data1, data2);
	}

	public static void assertHa7SkipTransactionDataEqual(String expected, String actual)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		CIDData data1 = marshaller1.unmarshal(expected);

		Ha7Marshaller marshaller2 = getMarshaller();
		CIDData data2 = marshaller2.unmarshal(actual);

		assertCIDDataSkipTransactionDataEqual(data1, data2);
	}

	public static CIDData convertToCIDData(String ha7Message)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		CIDData data = marshaller1.unmarshal(ha7Message);

		return data;
	}

	public static String convertToHa7xml(CIDData cidData)
	{
		Ha7Marshaller marshaller1 = getMarshaller();
		String ha7Xml = marshaller1.marshal(cidData);
//		System.out.println("Convert check UTF: " + ha7Xml);
		return StringEscapeUtils.unescapeXml(ha7Xml);
	}

	public static CIDAck convertToCIDAck(String ha7AckMessage)
	{
		Ha7AckMarshaller marshaller1 = getHa7AckMarshaller();
		CIDAck data = marshaller1.unmarshal(ha7AckMessage);

		return data;
	}

	public static String convertToHa7AckXml(CIDAck cidAck)
	{
		Ha7AckMarshaller marshaller1 = getHa7AckMarshaller();
		String ha7AckXml = marshaller1.marshal(cidAck);

		return ha7AckXml;
	}

	// ******* Some value to value data assertion ********
	public static void assertCIDData(CIDData expected, CIDData actual){
		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		assertMessageDtlEqual(expected.getMessageDtl(), actual.getMessageDtl());
		assertVisitDtlEqual(expected.getVisitDtl(), actual.getVisitDtl());
		assertPatientDtlEqual(expected.getPatientDtl(), actual.getPatientDtl());
		assertStudyDtlEqual(expected.getStudyDtl(), actual.getStudyDtl());
	}

	public static void assertMessageDtlEqual(MessageDtl expected, MessageDtl actual){
		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getTransactionCode(), actual.getTransactionCode());
		Assert.assertEquals(expected.getServerHosp(), actual.getServerHosp());
		Assert.assertEquals(expected.getSendingApplication(), actual.getSendingApplication());
	}


	public static void assertVisitDtlEqual(VisitDtl expected, VisitDtl actual){
		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getCaseNum(), actual.getCaseNum());
		Assert.assertEquals(expected.getVisitHosp(), actual.getVisitHosp());
		Assert.assertEquals(expected.getPayCode(), actual.getPayCode());
		Assert.assertEquals(expected.getAdtDtm(), actual.getAdtDtm());
		Assert.assertEquals(expected.getVisitSpec(), actual.getVisitSpec());
		Assert.assertEquals(expected.getVisitWardClinic(), actual.getVisitWardClinic());
	}

	public static void assertPatientDtlEqual(PatientDtl expected, PatientDtl actual){
		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getDeathIndicator(), actual.getDeathIndicator());
		Assert.assertEquals(expected.getPatSex(), actual.getPatSex());
		Assert.assertEquals(expected.getPatName(), actual.getPatName());
		Assert.assertEquals(expected.getPatKey(), actual.getPatKey());
		Assert.assertEquals(expected.getHKID(), actual.getHKID());
		Assert.assertEquals(expected.getPatDOB(), actual.getPatDOB());
	}

	public static void assertStudyDtlEqual(StudyDtl expected, StudyDtl actual){

		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getAccessionNo(), actual.getAccessionNo());
		Assert.assertEquals(expected.getStudyID(), actual.getStudyID());
		Assert.assertEquals(expected.getStudyType(), actual.getStudyType());
		Assert.assertEquals(expected.getRemark(), actual.getRemark());
		Assert.assertEquals(XmlFormatter.stripCDATA(expected.getStudyKeyword()), actual.getStudyKeyword());

		// SeriesDtl
		Assert.assertNotNull(actual.getSeriesDtls());
		List<SeriesDtl> expectedSeriesDtlList = expected.getSeriesDtls().getSeriesDtl();
		List<SeriesDtl> actualSeriesDtlList = actual.getSeriesDtls().getSeriesDtl();

		Assert.assertNotNull(expectedSeriesDtlList);

		for (int i = 0 ; i < expectedSeriesDtlList.size(); i++){
			assertSeriesDtlEqual(expectedSeriesDtlList.get(i), actualSeriesDtlList.get(i));
		}

	}

	public static void assertSeriesDtlEqual(SeriesDtl expected, SeriesDtl actual) {

		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getSeriesNo(), actual.getSeriesNo());
		Assert.assertEquals(expected.getExamType(), actual.getExamType());
		Assert.assertEquals(expected.getExamDtm(), actual.getExamDtm());
		Assert.assertEquals(expected.getEntityID(), actual.getEntityID());
		Assert.assertEquals(XmlFormatter.stripCDATA(expected.getSeriesKeyword()), actual.getSeriesKeyword());

		// ImageDtl
		Assert.assertNotNull(actual.getImageDtls());
		List<ImageDtl> expectedImageDtlList = expected.getImageDtls().getImageDtl();
		List<ImageDtl> actualImageDtlList = actual.getImageDtls().getImageDtl();

		Assert.assertNotNull(actualImageDtlList);

		for (int i = 0 ; i < expectedImageDtlList.size() ; i++){
			assertImageDtlEqaul(expectedImageDtlList.get(i), actualImageDtlList.get(i));
		}
	}

	public static void assertImageDtlEqaul(ImageDtl expected, ImageDtl actual) {

		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getImageFormat(), actual.getImageFormat());
		Assert.assertEquals(expected.getImageFile(), actual.getImageFile());
		Assert.assertEquals(expected.getImageID(), actual.getImageID());
		Assert.assertEquals(expected.getImageVersion(), actual.getImageVersion());
		Assert.assertEquals(expected.getImageSequence(), actual.getImageSequence());
		Assert.assertEquals(XmlFormatter.stripCDATA(expected.getImageKeyword()), actual.getImageKeyword());
		Assert.assertEquals(expected.getImageHandling(), actual.getImageHandling());
		Assert.assertEquals(expected.getImageType(), actual.getImageType());
		Assert.assertEquals(expected.getImageStatus(), actual.getImageStatus());

		if(expected.getAnnotationDtls() != null && expected.getAnnotationDtls().getAnnotationDtl() != null){
			Assert.assertNotNull(actual.getAnnotationDtls());
			Assert.assertNotNull(actual.getAnnotationDtls().getAnnotationDtl());
			for(int i = 0 ; i < expected.getAnnotationDtls().getAnnotationDtl().size() ; i++){
				assertAnnotationDtlEqual(expected.getAnnotationDtls().getAnnotationDtl().get(i), actual.getAnnotationDtls().getAnnotationDtl().get(i));
			}
		}
	}

	public static void assertAnnotationDtlEqual(AnnotationDtl expected, AnnotationDtl actual) {

		Assert.assertNotNull(actual);
		Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getAnnotationCoordinate(), actual.getAnnotationCoordinate());
		Assert.assertEquals(expected.getAnnotationEditable(), actual.getAnnotationEditable());
		Assert.assertEquals(expected.getAnnotationText(), actual.getAnnotationText());
		Assert.assertEquals(expected.getAnnotationStatus(), actual.getAnnotationStatus());
		Assert.assertEquals(expected.getAnnotationSeq(), actual.getAnnotationSeq());
		Assert.assertEquals(expected.getAnnotationType(), actual.getAnnotationType());
		Assert.assertEquals(expected.getAnnotationUpdDtm(), actual.getAnnotationUpdDtm());
	}

	public static CIDData cloneCIDData(CIDData cidData) {
		return convertToCIDData(convertToHa7xml(cidData));
	}

	public static CIDAck cloneCIDAck(CIDAck cidAck) {
		return convertToCIDAck(convertToHa7AckXml(cidAck));
	}

	private static Ha7Marshaller getMarshaller() {
		return (Ha7Marshaller) getApplicationContext().getBean("ha7marshaller");
	}

	private static Ha7AckMarshaller getHa7AckMarshaller() {
		return (Ha7AckMarshaller) getApplicationContext().getBean("ha7AckMarshaller");
	}

	private static ApplicationContext getApplicationContext() {
		return new ClassPathXmlApplicationContext(new String[] {"bean-context.xml"});
	}
}

