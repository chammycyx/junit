package org.ha.cd2.isg.icw.epr.client;


import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import epr.wsdl.xjc.pojo.DeLinkERSRecord;
import epr.wsdl.xjc.pojo.DeLinkERSRecordResponse;


public class DeLinkERSRecordClient extends WebServiceGatewaySupport {
	
	private String deLinkERSRecordSoapAction;

	/**
	 * Invoke the <tt>deLinkERSRecord</tt>.
	 *
	 * @param request the {@link DeLinkERSRecord}
	 * @return a {@link DeLinkERSRecordRecord}
	 */
	public DeLinkERSRecordResponse deLinkERSRecord(final DeLinkERSRecord request) {

		return (DeLinkERSRecordResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(deLinkERSRecordSoapAction)
		);
	}

	/**
	 * @param deLinkERSRecordSoapAction the soap action of <tt>deLinkERSRecord</tt> to set
	 */
	public void setDeLinkERSRecordSoapAction(String deLinkERSRecordSoapAction) {
		this.deLinkERSRecordSoapAction = deLinkERSRecordSoapAction;
	}

}
