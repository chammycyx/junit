/**
 *
 */
package org.ha.cd2.isg.icw.factory;

import icw.wsdl.xjc.epr.cid.DeLinkERSRecord;
import icw.wsdl.xjc.pojo.DeleteStudy;
import icw.wsdl.xjc.pojo.ExportImage;
//import icw.wsdl.xjc.pojo.GetAccessionNo;
//import icw.wsdl.xjc.pojo.GetBufferedCidImage;
import icw.wsdl.xjc.pojo.GetCidImage;
import icw.wsdl.xjc.pojo.GetCidImageThumbnail;
import icw.wsdl.xjc.pojo.GetCidScaledImage;
import icw.wsdl.xjc.pojo.GetCidStudy;
import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImage;
import icw.wsdl.xjc.pojo.GetCidStudyImageCount;
import icw.wsdl.xjc.pojo.ObjectFactory;
import icw.wsdl.xjc.pojo.UploadImage;
import icw.wsdl.xjc.pojo.UploadMetaData;
//import icw.wsdl.xjc.pojo.ValidateAccessionNo;

/**
 * The class is a factory class for creating different types of request.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class RequestFactory {

	private static RequestFactory INSTANCE = new RequestFactory();

	private ObjectFactory objectFactory;

	/**
	 * Create a {@link RequestFactory}.
	 *
	 * @return a {@link RequestFactory}
	 */
	public static RequestFactory createInstance() {
		return INSTANCE;
	}

	/**
	 * Create a {@link GetCidStudy}.
	 *
	 * @return a {@link GetCidStudy}
	 */
	public GetCidStudy createGetCidStudyRequest() {
		return objectFactory.createGetCidStudy();
	}

	
	/**
	 * @return
	 */
	public GetCidImage createGetCidImageRequest(){
		return objectFactory.createGetCidImage();
	}
	
	/**
	 * @param objectFactory the {@link ObjectFactory} to set
	 */
	public void setObjectFactory(ObjectFactory objectFactory) {
		this.objectFactory = objectFactory;
	}

//	public GetBufferedCidImage createGetBufferedCidImageRequest() {
//		return objectFactory.createGetBufferedCidImage();
//	}
	
	public GetCidImageThumbnail createGetBufferedCidImageThumbnailRequest() {
		return objectFactory.createGetCidImageThumbnail();
	}
	
	public ExportImage createExportImageRequest() {
		return objectFactory.createExportImage();
	}
	
	public GetCidStudyFirstLastImage createGetCidStudyFirstLastImageRequest(){
		return objectFactory.createGetCidStudyFirstLastImage();
	}
	
	public GetCidStudyImageCount createGetCidStudyImageCountRequest(){
		return objectFactory.createGetCidStudyImageCount();
	}

//	public GetAccessionNo createGetAccessionNoRequest() {
//		return objectFactory.createGetAccessionNo();
//	}

	public UploadImage createUploadImageRequest(){
		return objectFactory.createUploadImage();
	}
	
//	public ValidateAccessionNo createValidateAccessionNoRequest(){
//		return objectFactory.createValidateAccessionNo();
//	}

	public UploadMetaData createUploadMetaDataRequest(){
		return objectFactory.createUploadMetaData();
	}
	
	public GetCidScaledImage createGetCidScaledImageRequest(){
		return objectFactory.createGetCidScaledImage();
	}
	
	public DeleteStudy createDeleteStudyRequest(){
		return objectFactory.createDeleteStudy();
	}

	public DeLinkERSRecord createDeLinkERSRecordRequest(){
		return objectFactory.createDeLinkERSRecord();
	}

	public GetCidImageThumbnail createGetCidImageThumbnailRequest() {
		return objectFactory.createGetCidImageThumbnail();
	}
}
