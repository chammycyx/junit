/**
 *
 */
package org.ha.cd2.isg.icw.client.upload;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import icw.wsdl.xjc.pojo.UploadImage;
import icw.wsdl.xjc.pojo.UploadImageResponse;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>uploadImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class UploadImageClient extends WebServiceGatewaySupport {

	private String uploadImageSoapAction;

	/**
	 * Invoke the <tt>uploadImage</tt>.
	 *
	 * @param request the {@link UploadImage}
	 * @return a {@link UploadImageResponse}
	 */
	public UploadImageResponse uploadImage(final UploadImage request) {

		return (UploadImageResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(uploadImageSoapAction)
		);
	}

	/**
	 * @param uploadImageSoapAction the soap action of <tt>uploadImage</tt> to set
	 */
	public void setUploadImageSoapAction(String uploadImageSoapAction) {
		this.uploadImageSoapAction = uploadImageSoapAction;
	}

}
