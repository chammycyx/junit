/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImage;
import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImageResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidStudyFirstLastImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidStudyFirstLastImageClient extends WebServiceGatewaySupport {

	private String getCidStudyFirstLastImageSoapAction;

	/**
	 * Invoke the <tt>getCidStudyFirstLastImage</tt>.
	 *
	 * @param request the {@link GetCidStudyFirstLastImage}
	 * @return a {@link GetCidStudyFirstLastImageResponse}
	 */
	public GetCidStudyFirstLastImageResponse getCidStudyFirstLastImage(final GetCidStudyFirstLastImage request) {

		return (GetCidStudyFirstLastImageResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidStudyFirstLastImageSoapAction)
		);
	}

	/**
	 * @param getCidStudyFirstLastImageSoapAction the soap action of <tt>getCidStudyFirstLastImage</tt> to set
	 */
	public void setGetCidStudyFirstLastImageSoapAction(String getCidStudyFirstLastImageSoapAction) {
		this.getCidStudyFirstLastImageSoapAction = getCidStudyFirstLastImageSoapAction;
	}

}
