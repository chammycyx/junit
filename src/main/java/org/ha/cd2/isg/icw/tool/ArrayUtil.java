package org.ha.cd2.isg.icw.tool;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LSM131
 * 
 */
public class ArrayUtil {
	@SafeVarargs
	public static List<String> addAll(List<String>... lists) {
		List<String> results = new ArrayList<String>();
		for(List<String> list : lists){
			results.addAll(list);
		}
		return results;
	}
}
