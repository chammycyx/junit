package org.ha.cd2.isg.icw.tool;

import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SftpUtil {

	public static Session createSession(String host, String username,
			String password) throws JSchException {

		Session session = null;
		JSch jsch = new JSch();

		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");

		try {
			session = jsch.getSession(username, host, 22);
			session.setConfig(config);
			session.setPassword(password);
			session.connect();

		} catch (JSchException e1) {
			if (session != null) {
				session.disconnect();
			}
			throw e1;
		}
		return session;
	}

	public static ChannelSftp getChannel(Session session) throws JSchException {

		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;

		} catch (JSchException e) {
			if (channel != null) {
				channel.disconnect();
			}
			throw e;
		}
		return channelSftp;
	}

	public static void cd(ChannelSftp channelSftp, List<String> dirList)
			throws SftpException {
		for (int index = 0; index < dirList.size(); index++) {
			String dir = dirList.get(index);

			try {
				// change directory, if failed, make directory
				channelSftp.cd(dir);
			} catch (SftpException e) {
				boolean hasChangedDir = false;
				try {
					channelSftp.mkdir(dir);
				} catch (SftpException e1) {
					try {
						// if failed to make directory, retry to change
						// directory
						channelSftp.cd(dir);
						hasChangedDir = true;
					} catch (SftpException e2) {
						throw e2;
					}
				} finally {
					if (!hasChangedDir) {
						channelSftp.cd(dir);
					}
				}
			}
		}
	}

	public static void removeAllFiles(ChannelSftp channelSftp, String dirPath)
			throws SftpException {
		try {
			channelSftp.cd(dirPath);
		} catch (SftpException e) {
			return;
		}

		@SuppressWarnings("unchecked")
		Vector<LsEntry> list = channelSftp.ls("*");
		for (LsEntry entry : list) {
			if (!entry.getAttrs().isDir()) {
				channelSftp.rm(entry.getFilename());
			}
		}
		for (LsEntry entry : list) {
			if (entry.getAttrs().isDir()) {
				removeAllFiles(channelSftp,
						dirPath + File.separator + entry.getFilename());
			}
		}
		channelSftp.rmdir(dirPath);
	}
	
	
//	//Debug used
	public static void checkAllFiles(ChannelSftp channelSftp, String dirPath)
			throws SftpException {
		try {
			System.out.println("--------dirPath-------------" + dirPath);
			channelSftp.cd(dirPath);
			
		} catch (SftpException e) {
			System.out.println("--------Cant list dirPath--------");
			throw e;
		}

		@SuppressWarnings("unchecked")
		Vector<LsEntry> list = channelSftp.ls("*");
		for (LsEntry entry : list) {
			if (!entry.getAttrs().isDir()) {
				System.out.println("----File---"+ entry.getFilename());
			}
		}
		for (LsEntry entry : list) {
			if (entry.getAttrs().isDir()) {
				checkAllFiles(channelSftp,
						dirPath + File.separator + entry.getFilename());
			}
		}
	}
	
}
