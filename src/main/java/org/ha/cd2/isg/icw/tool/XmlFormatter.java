package org.ha.cd2.isg.icw.tool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.w3c.dom.Node;

public class XmlFormatter {

	public static String stripCDATA(String s) {
	    if(s != null){
	    	s = s.trim();
		    if (s.startsWith("<![CDATA[")) {
		      s = s.substring(9);
		      int i = s.indexOf("]]>");
		      if (i == -1) {
		        throw new IllegalStateException(
		            "argument starts with <![CDATA[ but cannot find pairing ]]&gt;");
		      }
		      s = s.substring(0, i);
		    }
//		    System.out.println("test:" + s);
		    return s;
	    } else {
	    	return null;
	    }

	  }

    public static String format(String xml) {

        ByteArrayOutputStream s = null;

        try {
            final InputStream src = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
            final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src, "UTF-8").getDocumentElement();

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            s = new ByteArrayOutputStream();
            t.transform(new DOMSource(document),new StreamResult(s));

            return new String(s.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        String unformattedXml =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><QueryMessage\n" +
                        "        xmlns=\"http://www.SDMX.org/resources/SDMXML/schemas/v2_0/message\"\n" +
                        "        xmlns:query=\"http://www.SDMX.org/resources/SDMXML/schemas/v2_0/query\">\n" +
                        "    <Query>\n" +
                        "        <query:CategorySchemeWhere>\n" +
                        "   \t\t\t\t\t         <query:AgencyID>ECB\n\n\n\n</query:AgencyID>\n" +
                        "        </query:CategorySchemeWhere>\n" +
                        "    </Query>\n\n\n\n\n" +
                        "</QueryMessage>";
        System.out.println(getTag("a", unformattedXml));
//        System.out.println(new XmlFormatter().format(unformattedXml));
    }

    public static String getSOAPMessageAsString(SOAPMessage soapMessage) {
        try {

           TransformerFactory tff = TransformerFactory.newInstance();
           Transformer tf = tff.newTransformer();

           // Set formatting

           tf.setOutputProperty(OutputKeys.INDENT, "yes");
           tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                 "2");

           Source sc = soapMessage.getSOAPPart().getContent();

           ByteArrayOutputStream streamOut = new ByteArrayOutputStream();
           StreamResult result = new StreamResult(streamOut);
           tf.transform(sc, result);

           String strMessage = streamOut.toString();
           return strMessage;
        } catch (Exception e) {
           System.out.println("Exception in getSOAPMessageAsString "
                 + e.getMessage());
           return null;
        }

    }

    public static Object getTag(String tag, String xml){
    	Document doc = Jsoup.parse(xml);
    	Element tagEle = doc.select(tag).first();

    	return tagEle;
    }
}
