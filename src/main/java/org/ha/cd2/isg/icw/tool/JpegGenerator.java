/**
 *
 */
package org.ha.cd2.isg.icw.tool;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Random;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.IOUtils;

/**
 * @author LTM548
 *
 */
public class JpegGenerator {

	public static String generate(String prefix) throws Exception {

		Random random = new Random(Calendar.getInstance().getTimeInMillis());
		int x = Math.abs(random.nextInt()) % 500;
		int y = Math.abs(random.nextInt()) % 500;
		int PIX_SIZE = 5;

		int X = 100;
		int Y = 100;

		BufferedImage bi = new BufferedImage(PIX_SIZE * x, PIX_SIZE * y, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g = (Graphics2D) bi.getGraphics();

		String filename = prefix + String.valueOf(random.nextInt()) + ".jpg";

		for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {

            	x = i * PIX_SIZE;
                y = j * PIX_SIZE;

                Color color = ((i * j) % 6 == 0 ) ? Color.GRAY : ((i + j) % 5 == 0) ? Color.BLUE : Color.WHITE;
                g.setColor(color);
                g.fillRect(y, x, PIX_SIZE, PIX_SIZE);
            }
        }

        g.dispose();
        saveToFile(bi, new File(filename));

        return filename;
    }

	public static byte[] generateImage() throws Exception {

		
		InputStream input = JpegGenerator.class.getResourceAsStream("/blazeds/imageuploadds/BRONCH1.JPG");
		return IOUtils.toByteArray(input);
		
		/*
		Random random = new Random(Calendar.getInstance().getTimeInMillis());
		int x = Math.abs(random.nextInt()) % 500;
		int y = Math.abs(random.nextInt()) % 500;
		int PIX_SIZE = 5;

		int X = 100;
		int Y = 100;

		BufferedImage bi = new BufferedImage(PIX_SIZE * x, PIX_SIZE * y, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g = (Graphics2D) bi.getGraphics();

//		String filename = prefix + String.valueOf(random.nextInt()) + ".jpg";

		for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {

            	x = i * PIX_SIZE;
                y = j * PIX_SIZE;

                Color color = ((i * j) % 6 == 0 ) ? Color.GRAY : ((i + j) % 5 == 0) ? Color.BLUE : Color.WHITE;
                g.setColor(color);
                g.fillRect(y, x, PIX_SIZE, PIX_SIZE);
            }
        }

        g.dispose();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( bi, "jpg", baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;
        */
    }
	
	
	public static void saveToFile(BufferedImage img, File file) throws IOException {

        ImageWriter writer = null;
        java.util.Iterator<?> iter = ImageIO.getImageWritersByFormatName("jpg");

        if (iter.hasNext()) {
            writer = (ImageWriter)iter.next();
        }

        ImageOutputStream ios = ImageIO.createImageOutputStream(file);
        writer.setOutput(ios);
        ImageWriteParam param = new JPEGImageWriteParam(java.util.Locale.getDefault());
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT) ;
        param.setCompressionQuality(0.98f);
        writer.write(null, new IIOImage(img, null, null), param);
    }

	public static void main(String args[]) throws Exception {

		JpegGenerator.generate("test-");
		System.out.println("image created");
	}

	public static byte[] generateLabelImage(String label) throws IOException {
        //image block size in pixels, 1 is 1px, use smaller values for 
        //greater granularity
        int PIX_SIZE = 5;

        //image size in pixel blocks
        int x = 100;
        int y = 100;

        BufferedImage bi = new BufferedImage(PIX_SIZE * x, PIX_SIZE * y,
                BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D g = (Graphics2D) bi.getGraphics();

        g.drawString(label, x, y);
        g.dispose();

        //
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( bi, "jpg", baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;

    }//end method
}
