package org.ha.cd2.isg.icw.tool;

import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.MessageDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl.AnnotationDtls.AnnotationDtl;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;

import org.ha.cid.ias.model.AnnotationDto;
import org.ha.cid.ias.model.ImageDto;
import org.ha.cid.ias.model.SeriesDto;
import org.ha.cid.ias.model.StudyDto;
import org.junit.Assert;

/**
 * @author LSM131
 * 
 */
public class StudyCompareUtil {
	public static void assertStudyAndCidDataEquals(StudyDto studyDto, CIDData cidData) {

		// Assert Study

		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getAccessionNo(), cidData.getStudyDtl().getAccessionNo());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getAdtDtm(), cidData.getVisitDtl().getAdtDtm());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getApplicationType(), cidData.getMessageDtl().getSendingApplication());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getCaseNo(), cidData.getVisitDtl().getCaseNum());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getPatDob(), cidData.getPatientDtl().getPatDOB());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getPatHkid(), cidData.getPatientDtl().getHKID());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getPatKey(), cidData.getPatientDtl().getPatKey());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getPatName(), cidData.getPatientDtl().getPatName());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getPatSex(), cidData.getPatientDtl().getPatSex());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getRemark(), cidData.getStudyDtl().getRemark());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getSendingApplication(), cidData.getMessageDtl().getSendingApplication());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getStudyDate(), cidData.getStudyDtl().getStudyDtm());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getStudyId(), cidData.getStudyDtl().getStudyID());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getStudyType(), cidData.getStudyDtl().getStudyType());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getTransactionCode(), cidData.getMessageDtl().getTransactionCode());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getVisitHosp(), cidData.getVisitDtl().getVisitHosp());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getVisitPayCode(), cidData.getVisitDtl().getPayCode());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getVisitSpec(), cidData.getVisitDtl().getVisitSpec());
		assertEqualsIgnoreCompareNullToNonemptyStr(studyDto.getVisitWardClinic(), cidData.getVisitDtl().getVisitWardClinic());

		// Assert List of Series

		List<SeriesDto> seriesDtos = studyDto.getSeriesDtos();
		List<SeriesDtl> seriesDtls = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl();

		if(seriesDtos.size() != seriesDtls.size())
			Assert.fail("No of records of Series not match");

		seriesDtos.sort(new SeriesDtoComparator());
		seriesDtls.sort(new SeriesDtlComparator());

		for(int i=0; i<seriesDtos.size(); i++) {
			assertSeriesAndSeriesDtlEquals(seriesDtos.get(i), seriesDtls.get(i), cidData.getMessageDtl());
		}
	}

	private static void assertSeriesAndSeriesDtlEquals(SeriesDto seriesDto, SeriesDtl seriesDtl, MessageDtl messageDtl) {

		// Assert Series

		assertEqualsIgnoreCompareNullToNonemptyStr(seriesDto.getModality(), seriesDtl.getExamType());
		assertEqualsIgnoreCompareNullToNonemptyStr(seriesDto.getSeriesDescription(), seriesDtl.getExamType());
		assertEqualsIgnoreCompareNullToNonemptyStr(seriesDto.getSeriesInsatnceUid(), seriesDtl.getSeriesNo());
		assertEqualsIgnoreCompareNullToNonemptyStr(seriesDto.getEntityId(), seriesDtl.getEntityID());
		assertEqualsIgnoreCompareNullToNonemptyStr(seriesDto.getSeriesDate(), seriesDtl.getExamDtm());

		// Assert List of Images

		List<ImageDtl> imageDtls = seriesDtl.getImageDtls().getImageDtl();
		List<ImageDto> imageDtos = seriesDto.getImageDtos();

		// If images were edited, old records will also be retrieved.
		// So imageDtos size can be equal to or larger than imageDtls size, but not less than imageDtls size
		if(imageDtls.size() > imageDtos.size())
			Assert.fail("Some image records missing");

		imageDtls.sort(new ImageDtlComparator());
		imageDtos.sort(new ImageDtoComparator());

		int i = 0;
		int j = 0;

		for(i=0; i<imageDtls.size(); i++) {
			while(j<imageDtos.size()) {
				ImageDtl dtl = imageDtls.get(i);
				ImageDto dto = imageDtos.get(j);
				if(dtl.getImageID().equals(dto.getImageId()) && 
						dtl.getImageVersion().equals(dto.getImageVersion()) && 
						dtl.getImageSequence().equals(dto.getImageSequence())) {
					assertImageAndImageDtlEquals(dto, dtl);
					break;
				} else {
					j++;
				}
			}
		}
	}

	private static void assertImageAndImageDtlEquals(ImageDto imageDto, ImageDtl imageDtl) {

		// Assert Image

		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageFile(), imageDtl.getImageFile());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageFormat(), imageDtl.getImageFormat());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageHandling(), imageDtl.getImageHandling());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageId(), imageDtl.getImageID());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageKeyword(), imageDtl.getImageKeyword());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageType(), imageDtl.getImageType());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageVersion(), imageDtl.getImageVersion());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageSequence(), imageDtl.getImageSequence());
		assertEqualsIgnoreCompareNullToNonemptyStr(imageDto.getImageStatus(), imageDtl.getImageStatus());

		// Assert List of Annotations

		List<AnnotationDtl> annotationDtls = imageDtl.getAnnotationDtls().getAnnotationDtl();
		List<AnnotationDto> annotationDtos = imageDto.getAnnotationDtos();

		if(annotationDtls.size() > annotationDtos.size())
			Assert.fail("Some annotation records missing");

		annotationDtls.sort(new AnnotationDtlComparator());
		annotationDtos.sort(new AnnotationDtoComparator());

		int i = 0;
		int j = 0;

		for(i=0; i<annotationDtls.size(); i++) {
			while(j<annotationDtos.size()) {
				AnnotationDtl dtl = annotationDtls.get(i);
				AnnotationDto dto = annotationDtos.get(j);
				if(dtl.getAnnotationSeq().equals(dto.getSeq())) {
					assertAnnotationAndAnnotationDtlEquals(dto, dtl);
					break;
				} else {
					j++;
				}
			}
		}
	}

	private static void assertAnnotationAndAnnotationDtlEquals(AnnotationDto annotationDto, AnnotationDtl annotationDtl) {

		// Assert Annotation

		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getCoordinate(), annotationDtl.getAnnotationCoordinate());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getEditable(), annotationDtl.getAnnotationEditable());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getStatus(), annotationDtl.getAnnotationStatus());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getText(), annotationDtl.getAnnotationText());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getType(), annotationDtl.getAnnotationType());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getUpdtm(), annotationDtl.getAnnotationUpdDtm());
		assertEqualsIgnoreCompareNullToNonemptyStr(annotationDto.getSeq(), annotationDtl.getAnnotationSeq());

	}

	// ======================== Assert Util ========================

	private static void assertEqualsIgnoreCompareNullToNonemptyStr(Object obj1, Object obj2) {
		// if dto field is null, may be no such field in DB
		// e.g. ES Study has no "SendingApplication" field, then null is compared to "CID"
		if(obj1==null && (obj2 instanceof String && obj2!="")) {
			return;
		}

		assertEqualsTreatNullAsEmptyStr(obj1, obj2);
	}

	private static void assertEqualsTreatNullAsEmptyStr(Object obj1, Object obj2) {
		if( (obj1 instanceof String || obj2 instanceof String) ) { // No need to check null since "null instanceof AnyClass" will always be false
			String obj1Str = obj1==null ? "" : String.valueOf(obj1);
			String obj2Str = obj2==null ? "" : String.valueOf(obj2);
			Assert.assertEquals(obj1Str, obj2Str);
		} else {
			Assert.assertEquals(obj1, obj2);
		}
	}

	// ======================== Comparators ========================

	private static class AnnotationDtoComparator implements Comparator<AnnotationDto> {
		@Override
		public int compare(AnnotationDto object1, AnnotationDto object2) {
			BigInteger seq1 = object1.getSeq();
			BigInteger seq2 = object2.getSeq();

			return seq1.compareTo(seq2);
		}
	}

	private static class AnnotationDtlComparator implements Comparator<AnnotationDtl> {
		@Override
		public int compare(AnnotationDtl object1, AnnotationDtl object2) {
			BigInteger seq1 = object1.getAnnotationSeq();
			BigInteger seq2 = object2.getAnnotationSeq();

			return seq1.compareTo(seq2);
		}
	}

	private static class ImageDtoComparator implements Comparator<ImageDto> {
		@Override
		public int compare(ImageDto object1, ImageDto object2) {
			int result = object1.getImageId().compareTo(object2.getImageId());

			if(result==0)
				result = object1.getImageVersion().compareTo(object2.getImageVersion());

			return result;
		}
	}

	private static class ImageDtlComparator implements Comparator<ImageDtl>  {
		@Override
		public int compare(ImageDtl object1, ImageDtl object2) {
			int result = object1.getImageID().compareTo(object2.getImageID());

			if(result==0)
				result = object1.getImageVersion().compareTo(object2.getImageVersion());

			return result;
		}
	}

	private static class SeriesDtoComparator implements Comparator<SeriesDto> {
		@Override
		public int compare(SeriesDto object1, SeriesDto object2) {
			return object1.getSeriesInsatnceUid().compareTo(object2.getSeriesInsatnceUid());
		}
	}

	private static class SeriesDtlComparator implements Comparator<SeriesDtl> {
		@Override
		public int compare(SeriesDtl object1, SeriesDtl object2) {
			return object1.getSeriesNo().compareTo(object2.getSeriesNo());
		}
	}
}