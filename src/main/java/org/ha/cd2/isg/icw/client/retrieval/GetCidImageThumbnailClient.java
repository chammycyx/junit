package org.ha.cd2.isg.icw.client.retrieval;


import icw.wsdl.xjc.pojo.GetCidImage;
import icw.wsdl.xjc.pojo.GetCidImageResponse;
import icw.wsdl.xjc.pojo.GetCidImageThumbnail;
import icw.wsdl.xjc.pojo.GetCidImageThumbnailResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class GetCidImageThumbnailClient extends WebServiceGatewaySupport {
	
	private String getCidImageThumbnailSoapAction;

	/**
	 * Invoke the <tt>getCidImageThumbnail</tt>.
	 *
	 * @param request the {@link GetCidImage}
	 * @return a {@link GetCidImageResponse}
	 */
	public GetCidImageThumbnailResponse getCidImageThumbnail(final GetCidImageThumbnail request) {

		return (GetCidImageThumbnailResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidImageThumbnailSoapAction)
		);
	}

	/**
	 * @param getCidImageThumbnailSoapAction 
	 * @param getCidImageThumbnailSoapAction the soap action of <tt>getCidImageThumbnail</tt> to set
	 */
	public void setGetCidImageThumbnailSoapAction(String getCidImageThumbnailSoapAction) {
		this.getCidImageThumbnailSoapAction = getCidImageThumbnailSoapAction;
	}

}
