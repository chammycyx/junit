package org.ha.cd2.isg.icw.tool;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class Ha7Marshaller {
	
	private Jaxb2Marshaller marshaller;
	private Jaxb2Marshaller unmarshaller;
	private Resource schema = new ClassPathResource("HA7.xsd");
	
	public Jaxb2Marshaller getMarshaller() {
		return marshaller;
	}
	public void setMarshaller(Jaxb2Marshaller marshaller) {
		this.marshaller = marshaller;
	}
	public Jaxb2Marshaller getUnmarshaller() {
		return unmarshaller;
	}
	public void setUnmarshaller(Jaxb2Marshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}
	
	public CIDData unmarshal(String xml) {
		
		marshaller.setSchema(schema);
		try {
			marshaller.afterPropertiesSet();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Source source = new StreamSource(new StringReader(xml));
		CIDData data= (CIDData)marshaller.unmarshal(source);	
		
		return data;
	}
	
	public String marshal(CIDData data){
		marshaller.setSchema(schema);
		StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(data, result);
		
		return writer.toString();
	}
	
	public CIDAck unmarshalAck(String xml){
		Source source = new StreamSource(new StringReader(xml));
		CIDAck data = (CIDAck)marshaller.unmarshal(source);
		return data;
	}
	
	public String marshalAck(CIDAck data){
		
		StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(data, result);
		
		return writer.toString();
	}
}
