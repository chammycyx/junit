/**
 * 
 */
package org.ha.cd2.isg.icw.tool;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

/**
 * @author YKF491
 * 
 */
public class ImageUtil {

	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";

	public static String getImageFormat(byte[] image) {
		String format = "";
		InputStream is = new ByteArrayInputStream(image);
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(is);
			Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
			if (iter.hasNext()) {
				ImageReader reader = iter.next();
				format = reader.getFormatName();
				iis.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return format;
	}
	
	public static BufferedImage appendVersion(byte[] image, int version) {
		InputStream is = new ByteArrayInputStream(image);
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(is);
			Graphics g = bufferedImage.getGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(15, 75, 90, 30);
			g.setColor(Color.RED);
			g.setFont(g.getFont().deriveFont(30f));
		    g.drawString("V" + String.valueOf(version), 20, 90);
		    g.dispose();
		    is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bufferedImage;
	}
	
	public static BufferedImage appendText(byte[] image, String text) {
		InputStream is = new ByteArrayInputStream(image);
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(is);
			Graphics g = bufferedImage.getGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(15, 75, 90, 30);
			g.setColor(Color.RED);
			g.setFont(g.getFont().deriveFont(30f));
		    g.drawString(text, 20, 90);
		    g.dispose();
		    is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bufferedImage;
	}
	
	public static BufferedImage resizeImg(BufferedImage img, int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION
						  ,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
		g.dispose();
		return dimg;
	}

	public static byte[] toByteAry(RenderedImage bi) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write( bi, "jpg", baos );
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;
	}
	
	public static BufferedImage toBufferedImage(byte[] img) throws IOException{
		InputStream in = new ByteArrayInputStream(img);
		return  ImageIO.read(in);
	}
	
	public static Map<String, Integer> getDimension(byte[] image) {
		
		Map<String, Integer> dimension = new HashMap<String, Integer>();
		InputStream is = new ByteArrayInputStream(image);
		
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(is);
			BufferedImage bImage = ImageIO.read(iis);
			
			dimension.put(HEIGHT, bImage.getHeight());
			dimension.put(WIDTH, bImage.getWidth());
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return dimension;
	}
	
}
