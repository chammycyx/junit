/**
 *
 */
package org.ha.cd2.isg.icw.client.upload;

import icw.wsdl.xjc.epr.cid.DeLinkERSRecord;
import icw.wsdl.xjc.epr.cid.DeLinkERSRecordResponse;



import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>delinkERSRecord</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class DeLinkERSRecordClient extends WebServiceGatewaySupport {

	private String deLinkERSRecordSoapAction;

	/**
	 * Invoke the <tt>delinkERSRecord</tt>.
	 *
	 * @param request the {@link UploadMeta}
	 * @return a {@link UploadMetaResponse}
	 */
	public DeLinkERSRecordResponse deLinkERSRecord(final DeLinkERSRecord request) {

		return (DeLinkERSRecordResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(deLinkERSRecordSoapAction)
		);
	}

	/**
	 * @param deLinkERSRecordSoapAction the soap action of <tt>delinkERSRecord</tt> to set
	 */
	public void setDeLinkERSRecordSoapAction(String deLinkERSRecordSoapAction) {
		this.deLinkERSRecordSoapAction = deLinkERSRecordSoapAction;
	}

}
