/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.GetCidStudy;
import icw.wsdl.xjc.pojo.GetCidStudyResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>getCidStudy</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class GetCidStudyClient extends WebServiceGatewaySupport {

	private String getCidStudySoapAction;

	/**
	 * Invoke the <tt>getCidStudy</tt>.
	 *
	 * @param request the {@link GetCidStudy}
	 * @return a {@link GetCidStudyResponse}
	 */
	public GetCidStudyResponse getCidStudy(final GetCidStudy request) {

		return (GetCidStudyResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(getCidStudySoapAction)
		);
	}

	/**
	 * @param getCidStudySoapAction the soap action of <tt>getCidStudy</tt> to set
	 */
	public void setGetCidStudySoapAction(String getCidStudySoapAction) {
		this.getCidStudySoapAction = getCidStudySoapAction;
	}

}
