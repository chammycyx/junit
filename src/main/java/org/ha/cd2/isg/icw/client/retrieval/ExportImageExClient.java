/**
 *
 */
package org.ha.cd2.isg.icw.client.retrieval;

import icw.wsdl.xjc.pojo.ExportImageEx;
import icw.wsdl.xjc.pojo.ExportImageExResponse;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * The class which extends the {@link WebServiceGatewaySupport} to invoke the <tt>exportImage</tt>.
 *
 * @author LTM548
 * @revision $Revision$ @lastModifiedDate $Date$ @lastModifier $Author$
 *
 */
public class ExportImageExClient extends WebServiceGatewaySupport {

	private String exportImageExSoapAction;

	/**
	 * Invoke the <tt>exportImage</tt>.
	 *
	 * @param request the {@link ExportImage}
	 * @return a {@link ExportImageResponse}
	 */
	public ExportImageExResponse exportImageEx(final ExportImageEx request) {

		return (ExportImageExResponse) getWebServiceTemplate().marshalSendAndReceive(
			request,
			new SoapActionCallback(exportImageExSoapAction)
		);
	}

	/**
	 * @param exportImageSoapAction the soap action of <tt>exportImage</tt> to set
	 */
	public void setExportImageExSoapAction(String exportImageExSoapAction) {
		this.exportImageExSoapAction = exportImageExSoapAction;
	}

}
