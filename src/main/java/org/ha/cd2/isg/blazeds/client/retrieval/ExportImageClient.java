/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.ExportImage;
import icw.blazeds.ro.imageretrievalds.ExportImageResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class ExportImageClient extends RemoteObjectClient {

	public ExportImageResponse exportImage(ExportImage request) throws Exception {
		byte[] exportImageResponse = (byte[]) executeForResult(request);
		ExportImageResponse response = new ExportImageResponse();
		response.setExportImageResult(exportImageResponse);
		return response;
		
	}
}
