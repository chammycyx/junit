/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.upload;

import icw.blazeds.ro.imageuploadds.UploadImage;
import icw.blazeds.ro.imageuploadds.UploadImageResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class UploadImageClient extends RemoteObjectClient {

	public UploadImageResponse uploadImage(UploadImage request) throws Exception {
		String uploadImageResult = (String) executeForResult(request);
		UploadImageResponse response = new UploadImageResponse();
		response.setUploadImageResult(uploadImageResult);
		return response;
		
	}
}
