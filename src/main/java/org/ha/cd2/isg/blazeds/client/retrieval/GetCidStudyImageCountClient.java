/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidStudyImageCount;
import icw.blazeds.ro.imageretrievalds.GetCidStudyImageCountResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidStudyImageCountClient extends RemoteObjectClient {

	public GetCidStudyImageCountResponse getCidStudyImageCount(GetCidStudyImageCount request) throws Exception {
		int imageCount = (Integer) executeForResult(request);
		GetCidStudyImageCountResponse response = new GetCidStudyImageCountResponse();
		response.setImageCount(imageCount);
		return response;
	}
}
