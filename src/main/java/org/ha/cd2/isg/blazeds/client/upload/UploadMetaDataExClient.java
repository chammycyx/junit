/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.upload;

import icw.blazeds.ro.imageuploadds.UploadMetaDataEx;
import icw.blazeds.ro.imageuploadds.UploadMetaDataExResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class UploadMetaDataExClient extends RemoteObjectClient {

	public UploadMetaDataExResponse uploadMetaDataEx(UploadMetaDataEx request) throws Exception {
		Integer uploadMetaDataResult = (Integer) executeForResult(request);
		UploadMetaDataExResponse response = new UploadMetaDataExResponse();
		response.setUploadMetaDataExResult(uploadMetaDataResult);
		return response;
		
	}
}
