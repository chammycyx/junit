/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.ExportImageEx;
import icw.blazeds.ro.imageretrievalds.ExportImageExResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class ExportImageExClient extends RemoteObjectClient {

	public ExportImageExResponse exportImageEx(ExportImageEx request) throws Exception {
		byte[] exportImageResponse = (byte[]) executeForResult(request);
		ExportImageExResponse response = new ExportImageExResponse();
		response.setExportImageExResult(exportImageResponse);
		return response;
		
	}
}
