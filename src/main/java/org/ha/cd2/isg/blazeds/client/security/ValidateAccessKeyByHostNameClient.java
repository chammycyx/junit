/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.security;

import icw.blazeds.ro.securitymanagerds.ValidateAccessKeyByHostName;
import icw.blazeds.ro.securitymanagerds.ValidateAccessKeyByHostNameResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class ValidateAccessKeyByHostNameClient extends RemoteObjectClient {

	public ValidateAccessKeyByHostNameResponse validateAccessKeyByHostName(ValidateAccessKeyByHostName request) throws Exception {
		Boolean isValid = (Boolean) executeForResult(request);
		ValidateAccessKeyByHostNameResponse response = new ValidateAccessKeyByHostNameResponse();
		 response.setIsValid(isValid);
		return response;
		
	}
}
