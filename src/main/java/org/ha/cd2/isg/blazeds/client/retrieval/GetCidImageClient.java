/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidImage;
import icw.blazeds.ro.imageretrievalds.GetCidImageResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidImageClient extends RemoteObjectClient {

	public GetCidImageResponse getCidImage(GetCidImage request) throws Exception {
		byte[] getCidImageResult = (byte[]) executeForResult(request);
		GetCidImageResponse response = new GetCidImageResponse();
		response.setGetCidImageResult(getCidImageResult);
		return response;
		
	}
}
