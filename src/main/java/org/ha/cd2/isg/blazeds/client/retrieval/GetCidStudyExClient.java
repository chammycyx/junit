/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidStudyEx;
import icw.blazeds.ro.imageretrievalds.GetCidStudyExResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidStudyExClient extends RemoteObjectClient {

	public GetCidStudyExResponse getCidStudyEx(GetCidStudyEx request) throws Exception {
		String ha7Result = (String) executeForResult(request);
		GetCidStudyExResponse response = new GetCidStudyExResponse();
		response.setHa7Result(ha7Result);
		return response;
	}
}
