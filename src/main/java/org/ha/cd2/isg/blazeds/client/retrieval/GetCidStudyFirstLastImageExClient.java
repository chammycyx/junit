/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidStudyFirstLastImageEx;
import icw.blazeds.ro.imageretrievalds.GetCidStudyFirstLastImageExResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidStudyFirstLastImageExClient extends RemoteObjectClient {

	public GetCidStudyFirstLastImageExResponse getCidStudyFirstLastImageEx(GetCidStudyFirstLastImageEx request) throws Exception {
		String ha7Result = (String) executeForResult(request);
		GetCidStudyFirstLastImageExResponse response = new GetCidStudyFirstLastImageExResponse();
		response.setHa7Result(ha7Result);
		return response;
	}
}
