/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidStudyFirstLastImage;
import icw.blazeds.ro.imageretrievalds.GetCidStudyFirstLastImageResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidStudyFirstLastImageClient extends RemoteObjectClient {

	public GetCidStudyFirstLastImageResponse getCidStudyFirstLastImage(GetCidStudyFirstLastImage request) throws Exception {
		String ha7Result = (String) executeForResult(request);
		GetCidStudyFirstLastImageResponse response = new GetCidStudyFirstLastImageResponse();
		response.setHa7Result(ha7Result);
		return response;
	}
}
