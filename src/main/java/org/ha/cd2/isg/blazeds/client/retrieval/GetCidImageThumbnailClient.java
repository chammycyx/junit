/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidImageThumbnail;
import icw.blazeds.ro.imageretrievalds.GetCidImageThumbnailResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidImageThumbnailClient extends RemoteObjectClient {

	public GetCidImageThumbnailResponse getCidImageThumbnail(GetCidImageThumbnail request) throws Exception {
		byte[] getCidImageThumbnailResult = (byte[]) executeForResult(request);
		GetCidImageThumbnailResponse response = new GetCidImageThumbnailResponse();
		response.setGetCidImageThumbnailResult(getCidImageThumbnailResult);
		return response;
	}
}
