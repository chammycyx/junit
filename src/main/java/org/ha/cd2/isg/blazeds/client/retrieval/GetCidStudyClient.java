/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidStudy;
import icw.blazeds.ro.imageretrievalds.GetCidStudyResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidStudyClient extends RemoteObjectClient {

	public GetCidStudyResponse getCidStudy(GetCidStudy request) throws Exception {
		String ha7Result = (String) executeForResult(request);
		GetCidStudyResponse response = new GetCidStudyResponse();
		response.setHa7Result(ha7Result);
		return response;
	}
}
