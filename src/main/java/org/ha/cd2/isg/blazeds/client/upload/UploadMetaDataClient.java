/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.upload;

import icw.blazeds.ro.imageuploadds.UploadMetaData;
import icw.blazeds.ro.imageuploadds.UploadMetaDataResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class UploadMetaDataClient extends RemoteObjectClient {

	public UploadMetaDataResponse uploadMetaData(UploadMetaData request) throws Exception {
		Integer uploadMetaDataResult = (Integer) executeForResult(request);
		UploadMetaDataResponse response = new UploadMetaDataResponse();
		response.setUploadMetaDataResult(uploadMetaDataResult);
		return response;
		
	}
}
