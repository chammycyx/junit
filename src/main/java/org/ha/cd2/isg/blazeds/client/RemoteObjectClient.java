/**
 * 
 */
package org.ha.cd2.isg.blazeds.client;

import flex.messaging.io.amf.client.AMFConnection;
import flex.messaging.io.amf.client.exceptions.ClientStatusException;
import flex.messaging.messages.AcknowledgeMessage;
import flex.messaging.messages.CommandMessage;
import flex.messaging.messages.Message;
import flex.messaging.messages.RemotingMessage;
import flex.messaging.util.Base64;
import flex.messaging.util.UUIDUtils;
import icw.blazeds.ro.IRemoteObject;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * @author YKF491
 *
 */
public class RemoteObjectClient {
	
	private String endpoint;
	private String credential;
	private String destination;
	
	public void setEndpoint(String enpoint) {
		this.endpoint = enpoint;
	}
	
	public void setCredential(String credential) {
		this.credential = credential;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	@SuppressWarnings("unchecked")
	protected Object executeForResult(IRemoteObject remoteObject) throws Exception {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(endpoint);
		AMFConnection amfConnection = new AMFConnection();
		AcknowledgeMessage callAck = null;

		try { // get Cookie with JSESSIONID

			Header cookieHeader = httpClient.execute(request).getHeaders(AMFConnection.SET_COOKIE)[0];
			amfConnection.addHttpRequestHeader(AMFConnection.COOKIE, cookieHeader.getValue());
			amfConnection.addHttpRequestHeader("Content-type", "application/x-amf");
			amfConnection.connect(endpoint);
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (ClientStatusException e) {
			e.printStackTrace();
		}

		try { // ping to get DSID, login

			CommandMessage pingCommandMessage = new CommandMessage(CommandMessage.CLIENT_PING_OPERATION);
			pingCommandMessage.setMessageId(UUIDUtils.createUUID());
			pingCommandMessage.setHeader(Message.FLEX_CLIENT_ID_HEADER, "nil");

			amfConnection.call(null, pingCommandMessage);

			AcknowledgeMessage pingAck = (AcknowledgeMessage) amfConnection.call(null, pingCommandMessage);
			String dsId = pingAck.getHeader(Message.FLEX_CLIENT_ID_HEADER).toString();
			//System.out.println("$$ dsId = " + dsId.toString());
			//System.out.println(">> ping successfully!!");

			// set encoder for authentication
			Base64.Encoder encoder = new Base64.Encoder(credential.length());
			encoder.encode(credential.getBytes());

			CommandMessage loginCommandMessage = new CommandMessage(CommandMessage.LOGIN_OPERATION);
			loginCommandMessage.setDestination("auth");
			loginCommandMessage.setBody(encoder.drain());
			loginCommandMessage.setMessageId(UUIDUtils.createUUID());
			loginCommandMessage.setHeader(Message.FLEX_CLIENT_ID_HEADER, dsId);

			amfConnection.call(null, loginCommandMessage);
			//System.out.println(">> login successfully");

			// need to use RemotingMessage instead of CommandMessage for a specific operation
			// simply follow the attributes which are intercepted by Charles proxy
			RemotingMessage remotingMessage = new RemotingMessage();
			remotingMessage.setOperation(remoteObject.getOperation());
			remotingMessage.getHeaders().put(Message.FLEX_CLIENT_ID_HEADER, dsId);
			remotingMessage.setMessageId(UUIDUtils.createUUID());
			remotingMessage.setDestination(destination);
			remotingMessage.setBody((Object[]) remoteObject.getParameters());

			callAck = (AcknowledgeMessage) amfConnection.call(null, remotingMessage);
			//System.out.println("$$ Accession No = " + callAck.getBody().toString());
		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			amfConnection.close();
		}
		return callAck == null ? null : callAck.getBody();
	}
}
