/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.security;

import hk.org.ha.service.biz.image.endpoint.security.type.WsApplicationControl;
import icw.blazeds.ro.securitymanagerds.GetApplicationControl;
import icw.blazeds.ro.securitymanagerds.GetApplicationControlResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetApplicationControlClient extends RemoteObjectClient {

	public GetApplicationControlResponse getApplicationControl(GetApplicationControl request) throws Exception {
		WsApplicationControl applicationControlResult = (WsApplicationControl) executeForResult(request);
		GetApplicationControlResponse response = new GetApplicationControlResponse();
		response.setGetApplicationControlResult(applicationControlResult);
		return response;
		
	}
}
