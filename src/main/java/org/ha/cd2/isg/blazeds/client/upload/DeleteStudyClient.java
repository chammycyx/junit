/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.upload;

import icw.blazeds.ro.imageuploadds.DeleteStudy;
import icw.blazeds.ro.imageuploadds.DeleteStudyResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class DeleteStudyClient extends RemoteObjectClient {

	public DeleteStudyResponse deleteStudy(DeleteStudy request) throws Exception {
		 String deleteStudyResult = (String) executeForResult(request);
		 DeleteStudyResponse response = new DeleteStudyResponse();
		 response.setDeleteStudyResult(deleteStudyResult);
		return response;
		
	}
}
