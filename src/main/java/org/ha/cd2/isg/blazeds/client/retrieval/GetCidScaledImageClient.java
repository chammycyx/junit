/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.retrieval;

import icw.blazeds.ro.imageretrievalds.GetCidScaledImage;
import icw.blazeds.ro.imageretrievalds.GetCidScaledImageResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetCidScaledImageClient extends RemoteObjectClient {

	public GetCidScaledImageResponse getCidScaledImage(GetCidScaledImage request) throws Exception {
		byte[] image = (byte[]) executeForResult(request);
		GetCidScaledImageResponse response = new GetCidScaledImageResponse();
		response.setCidScaledImageResult(image);
		return response;
	}
}
