/**
 * 
 */
package org.ha.cd2.isg.blazeds.client.upload;

import icw.blazeds.ro.imageuploadds.GetAccessionNo;
import icw.blazeds.ro.imageuploadds.GetAccessionNoResponse;

import org.ha.cd2.isg.blazeds.client.RemoteObjectClient;

/**
 * @author YKF491
 *
 */
public class GetAccessionNoClient extends RemoteObjectClient {
	
	public GetAccessionNoResponse getAccessionNo(GetAccessionNo request) throws Exception {
		 String accessionNo = (String) executeForResult(request);
		 GetAccessionNoResponse response = new GetAccessionNoResponse();
		 response.setAccessionNo(accessionNo);
		return response;
		
	}
}