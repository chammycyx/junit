package org.ha.cd2.isg.esb.client.receiveEsbHl7;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import esb.wsdl.xjc.pojo.ReceiveEsbHl7;
import esb.wsdl.xjc.pojo.ReceiveEsbHl7Response;

public class ReceiveEsbHl7Client extends WebServiceGatewaySupport {
	private String receiveEsbHl7SoapAction;
	
	public ReceiveEsbHl7Response receiveEsbHl7(final ReceiveEsbHl7 request){
		return (ReceiveEsbHl7Response) getWebServiceTemplate().marshalSendAndReceive(
				request,
				new SoapActionCallback(receiveEsbHl7SoapAction)
			);
	}

	public void setReceiveEsbHl7SoapAction(String receiveEsbHl7SoapAction) {
		this.receiveEsbHl7SoapAction = receiveEsbHl7SoapAction;
	}

}
