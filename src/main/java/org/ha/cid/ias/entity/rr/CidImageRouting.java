/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.rr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The entity class of <tt>T_CID_IMAGE_ROUTING</tt> that denotes a <tt>CID</tt> image is needed to be routed
 * from one node to another.
 *
 * <pre>
 * Last commit:-
 * $Rev: 10433 $
 * $Author: ltm548 $
 * $Date: 2016-09-14 17:13:17 +0800 (Wed, 14 Sep 2016) $
 * </pre>
 */
@Entity
@Table(
	name = "T_CID_IMAGE_ROUTING",
	indexes = {
		@Index(
			name = "IDX_CID_IMAGE_ROUTING_1",
			columnList = "IMAGE_ROUTING_UID, SOURCE_NODE_NO, DEST_NODE_NO", // composition of a unique route per HA7
			unique = false
		)
	}
)
@NamedQueries({
	@NamedQuery(
		name = CidImageRouting.JPQL_NAME_FIND_BY_IMAGE_ROUTING_UID,
		query = CidImageRouting.JPQL_QUERY_FIND_BY_IMAGE_ROUTING_UID
	),
	@NamedQuery(
		name = CidImageRouting.JPQL_NAME_FIND_BY_ROUTING_UID_PER_UNIUQE_ROUTE,
		query = CidImageRouting.JPQL_QUERY_FIND_BY_ROUTING_UID_PER_UNIQUE_ROUTE
	)
})
public class CidImageRouting implements Serializable {

	/**
	 * The name of the query which is used for finding any image routing per <tt>Image Routing UID</tt>.
	 */
	public static final String JPQL_NAME_FIND_BY_IMAGE_ROUTING_UID =
			"CidImageRouting.findByImageRoutingUid";

	/**
	 * The actual query which is used for finding any image routing per <tt>Image Routing UID</tt>.
	 */
	public static final String JPQL_QUERY_FIND_BY_IMAGE_ROUTING_UID =
			"SELECT r FROM CidImageRouting r WHERE r.imageRoutingUid = ?1";

	/**
	 * The name of the query which is used for finding any image routing per unique route.
	 */
	public static final String JPQL_NAME_FIND_BY_ROUTING_UID_PER_UNIUQE_ROUTE =
			"CidImageRouting.findByRoutingUidPerUniqueRoute";

	/**
	 * The actual query which is used for finding any image routing per unique route.
	 */
	public static final String JPQL_QUERY_FIND_BY_ROUTING_UID_PER_UNIQUE_ROUTE =
			"SELECT r FROM CidImageRouting r WHERE r.imageRoutingUid = ?1 " +
			"AND r.sourceNodeNumber = ?2 AND r.destinationNodeNumber = ?3";

	/**
	 * This is the generated serial version UID.
	 */
	private static final long serialVersionUID = -6802664468978488384L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cidImageRoutingTableIdGenerator")
	@SequenceGenerator(name = "cidImageRoutingTableIdGenerator", sequenceName = "SEQ_CID_IMAGE_ROUTING", allocationSize = 1)
	private long id;

	@ManyToOne
	@JoinColumn(name="FK_STUINDEX_NO")
	private Study study;

	@Column(name="IMAGE_ROUTING_UID")
	private String imageRoutingUid;

	@Column(name="IMAGE_FILE")
	private String imageFile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INSERT_DATE")
	private Date insertDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	@Column(name="SOURCE_NODE_NO")
	private int sourceNodeNumber;

	@Column(name="DEST_NODE_NO")
	private int destinationNodeNumber;

	@Column(name="ROUTED_STATE")
	@Convert(converter=CidImageRoutedStateConverter.class)
	private CidImageRoutedState routedState;

	@Column(name="HA7_TX_ID")
	private String ha7TransactionId;

	@PrePersist
    protected void onCreate() {

        insertDate = new Date();
        updateDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {

        updateDate = new Date();
    }
	/**
	 * @return the <tt>ID</tt>
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the <tt>ID</tt> to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the {@link Study}
	 */
	public Study getStudy() {
		return study;
	}

	/**
	 * @param study the {@link Study} to set
	 */
	public void setStudy(Study study) {
		this.study = study;
	}

	/**
	 * @return the value of <tt>IMAGE_ROUTING_UID</tt>
	 */
	public String getImageRoutingUid() {
		return imageRoutingUid;
	}

	/**
	 * @param imageRoutingUid the value of <tt>IMAGE_ROUTING_UID</tt> to set
	 */
	public void setImageRoutingUid(String imageRoutingUid) {
		this.imageRoutingUid = imageRoutingUid;
	}

	/**
	 * @return the value of <tt>IMAGE_FILE</tt>
	 */
	public String getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile the value of <tt>IMAGE_FILE</tt> to set
	 */
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * @return the value of <tt>INSERT_DATE</tt>
	 */
	public Date getInsertDate() {
		return insertDate;
	}

	/**
	 * @param insertDate the value of <tt>INSERT_DATE</tt> to set
	 */
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	/**
	 * @return the value of <tt>UPDATE_DATE</tt>
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the value of <tt>UPDATE_DATE</tt> to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the value of <tt>SOURCE_NODE_NO</tt>
	 */
	public int getSourceNodeNumber() {
		return sourceNodeNumber;
	}

	/**
	 * @param sourceNodeNumber the value of <tt>SOURCE_NODE_NO</tt> to set
	 */
	public void setSourceNodeNumber(int sourceNodeNumber) {
		this.sourceNodeNumber = sourceNodeNumber;
	}

	/**
	 * @return the value of <tt>DEST_NODE_NO</tt>
	 */
	public int getDestinationNodeNumber() {
		return destinationNodeNumber;
	}

	/**
	 * @param destinationNodeNumber the value of <tt>DEST_NODE_NO</tt> to set
	 */
	public void setDestinationNodeNumber(int destinationNodeNumber) {
		this.destinationNodeNumber = destinationNodeNumber;
	}

	/**
	 * @return the value of <tt>ROUTED_STATE</tt>
	 */
	public CidImageRoutedState getRoutedState() {
		return routedState;
	}

	/**
	 * @param routedState the value of <tt>ROUTED_STATE</tt> to set
	 */
	public void setRoutedState(CidImageRoutedState routedState) {
		this.routedState = routedState;
	}

	/**
	 * @return the value of <tt>HA7_TX_ID</tt>
	 */
	public String getHa7TransactionId() {
		return ha7TransactionId;
	}

	/**
	 * @param ha7TranactionId the value of <tt>HA7_TX_ID</tt> to set
	 */
	public void setHa7TransactionId(String ha7TranactionId) {
		this.ha7TransactionId = ha7TranactionId;
	}

}
