/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.rr;

/**
 * The enum which defines the <strong>routed state</strong> of an image routing, from Cache Node to Storage Node.
 *
 * <pre>
 * Last commit:-
 * $Rev: 10433 $
 * $Author: ltm548 $
 * $Date: 2016-09-14 17:13:17 +0800 (Wed, 14 Sep 2016) $
 * </pre>
 */
public enum CidImageRoutedState {

	/**
	 * It denotes an image routing is pending.
	 */
	PENDING("P"),

	/**
	 * It denotes an image routing is completed.
	 */
	COMPLETE("C"),

	/**
	 * It denotes a failure of image routing.
	 */
	ERROR("E"),

	/**
	 * It denotes a retry is being processed of image routing. 
	 */
	RETRYING("R"),

	/**
	 * It denotes the image is purged.
	 */
	PURGED("X");

	private String _name;

	private CidImageRoutedState(String name) {
		this._name = name;
	}

	public String toString() {
		return this._name;
	}

}
