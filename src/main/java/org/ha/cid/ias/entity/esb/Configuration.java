/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The entity class is used for persisting configuration set for ESB module.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9632 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:02:49 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_configuration")
@NamedQueries({
	@NamedQuery(
		name = Configuration.NAMED_QUERY_FIND_BY_KEY,
		query = "SELECT c FROM Configuration c WHERE c.key = ?1"
	)
})
public class Configuration implements Serializable {

	public final static String NAMED_QUERY_FIND_BY_KEY = "findByKey";

    private static final long serialVersionUID = -1172287990035316352L;

    @Id
    @Column(name = "configuration_id")
    private long configurationId;

    private String key;

    private String value;

    private String description;

	/**
	 * @return the ID of a configuration parameter
	 */
	public long getConfigurationId() {
		return configurationId;
	}

	/**
	 * @param configurationId the ID of a configuration parameter to set
	 */
	public void setConfigurationId(long configurationId) {
		this.configurationId = configurationId;
	}

	/**
	 * @return the key of a configuration parameter
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key of a configuration parameter to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value of a configuration parameter
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value of a configuration parameter to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the description of a configuration parameter
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description of a configuration parameter to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
