/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ha.cid.ias.entity.rr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 *
 * @author developer
 */
@Entity
@Table(name = "t_cid_image", uniqueConstraints = @UniqueConstraint(columnNames = {"image_id","image_version","image_sequence", "fk_series_no"}))
@NamedQueries({
    @NamedQuery(name = "Image.queryByImageID",
            query = "SELECT t FROM Image t WHERE t.imageID = :imageID"),
    @NamedQuery(name = "Image.queryByImageIDAndVersionAndSequence",
            query = "SELECT t FROM Image t WHERE t.imageID = :imageID AND t.imageVersion = :imageVersion AND t.imageSequence = :imageSequence"),
    @NamedQuery(name = "Image.queryBySeriesAndImageIdAndImageSeqNoAndVersionNo",
            query = "SELECT t FROM Image t WHERE t.series = :series AND t.imageID = :imageID AND t.imageVersion = :imageVersion AND t.imageSequence = :imageSequence AND t.imageStatus = :imageStatus")
})
public class Image implements java.io.Serializable {

    public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	private static final long serialVersionUID = 1843335926579848550L;

    @Id
    @SequenceGenerator(name = "SEQ_CID_IMAGE", sequenceName = "SEQ_CID_IMAGE",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CID_IMAGE")
    @Column(name = "ID")
    private long id;
    
    @Version
    @Column(name = "version")
    private int version;
    @Column(name = "image_format")
    private String imageFormat;
    @Column(name = "image_path")
    private String imagePath;
    @Column(name = "image_file")
    private String imageFile;
    @Column(name = "image_id")
    private String imageID;
    @Column(name = "image_version")
    private String imageVersion;
    @Column(name = "image_sequence")
    private int imageSequence;
    @Lob
    @Column(name = "image_keyword")
    private String imageKeyword;
    @Column(name = "image_handling")
    private String imageHandling;
    @Column(name = "image_type")
    private String imageType;
    @Column(name = "image_status")
    private int imageStatus;
    @Column(name = "insert_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "update_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_series_no")
    private Series series;
    @Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "metadata")
    @Lob
    private String metadata;
    
    @OneToMany(mappedBy = "image", orphanRemoval = true, cascade = { CascadeType.ALL })
	private List<Annotation> annotations = new ArrayList<>();
    
    //default 0 or null
    //if replaced then 1
    @Column(name = "replaced")
    private int replaced;

	public int getReplaced() {
		return replaced;
	}

	public void setReplaced(int replaced) {
		this.replaced = replaced;
	}

	@PrePersist
    protected void onCreate() {
        insertDate = new Date();
        updateDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateDate = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(String imageFormat) {
        if(imageFormat != null) {
        	imageFormat = imageFormat.trim();
        }
    	this.imageFormat = imageFormat;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
    	if(imagePath != null) {
    		imagePath = imagePath.trim();
        }
    	this.imagePath = imagePath;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
    	if(imageFile != null) {
    		imageFile = imageFile.trim();
        }
    	this.imageFile = imageFile;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
    	if(imageID != null) {
    		imageID = imageID.trim();
        }
    	this.imageID = imageID;
    }

    public String getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(String imageVersion) {
    	if(imageVersion != null) {
    		imageVersion = imageVersion.trim();
        }
    	this.imageVersion = imageVersion;
    }

    public int getImageSequence() {
        return imageSequence;
    }

    public void setImageSequence(int imageSequence) {
        this.imageSequence = imageSequence;
    }

    public String getImageKeyword() {
        return imageKeyword;
    }

    public void setImageKeyword(String imageKeyword) {
    	if(imageKeyword != null) {
    		imageKeyword = imageKeyword.trim();
        }
    	this.imageKeyword = imageKeyword;
    }

    public String getImageHandling() {
        return imageHandling;
    }

    public void setImageHandling(String imageHandling) {
    	if(imageHandling != null) {
    		imageHandling = imageHandling.trim();
        }
    	this.imageHandling = imageHandling;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
    	if(imageType != null) {
    		imageType = imageType.trim();
        }
    	this.imageType = imageType;
    }

    public int getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(int imageStatus) {
        this.imageStatus = imageStatus;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

//    public static Image buildFromMetadata(CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl imageDtl) {
//        Image newImage = null;
//        if (imageDtl == null) {
//            return newImage;
//        }
//        newImage = new Image();
//        newImage.setImageFile(imageDtl.getImageFile());
//        newImage.setImageFormat(imageDtl.getImageFormat());
//        newImage.setImageHandling(imageDtl.getImageHandling());
//        newImage.setImageID(imageDtl.getImageID());
//        newImage.setImageKeyword(imageDtl.getImageKeyword());
//        newImage.setImagePath(imageDtl.getImagePath());
//        newImage.setImageSequence(imageDtl.getImageSequence().intValue());
//        newImage.setImageStatus(imageDtl.getImageStatus().intValue());
//        newImage.setImageType(imageDtl.getImageType());
//        newImage.setImageVersion(imageDtl.getImageVersion());
//        
//        return newImage;
//    }
}
