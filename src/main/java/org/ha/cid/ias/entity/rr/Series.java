/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ha.cid.ias.entity.rr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

import org.ha.cid.ias.enumeration.SeriesStatus;

/**
 *
 * @author developer
 */
@Entity
@Table(name = "TG_SERINDEX", uniqueConstraints = @UniqueConstraint(columnNames = {"SERIES_UID", "FK_STUINDEX_NO"}))
@NamedQueries({
    @NamedQuery(name = "Series.queryBySeriesInstanceUid",
            query = "SELECT t FROM Series t WHERE t.seriesInsatnceUid = :seriesInsatnceUid"),
    @NamedQuery(name = "Series.queryByStudyAndSeriesInstanceUid",
            query = "SELECT t FROM Series t WHERE t.seriesInsatnceUid = :seriesInsatnceUid AND t.study.stuNo = :studyID")
})
public class Series implements java.io.Serializable {
    private static final long serialVersionUID = 2479528316186032077L;

    @Id
    @SequenceGenerator(name = "SQ_SERINDEX_NO", sequenceName = "SQ_SERINDEX_NO",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_SERINDEX_NO")
    @Column(name = "SERINDEX_NO")
    private long id;
    @Column(name = "SERIES_UID",unique = true)
    private String seriesInsatnceUid;
    @Column(name = "SERIES_DESC")
    private String seriesDescription;
    @Column(name = "modality")
    private String modality;
    @Column(name = "SERIES_NUMBER")
    private String seriesNumber;
    @Column(name = "entity_id")
    private String entityID;
    @Column(name = "EXAM_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date seriesDate;
    @Column(name = "insert_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "update_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "SERIES_FILL", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private SeriesStatus seriesStatus = SeriesStatus.IN_PROGRESS;
    
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "fk_patindex_no")
//    private Patient patient;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FK_STUINDEX_NO")
    private Study study;
    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval=true, mappedBy = "series")
    private List<PathIndex> pathIndexs = new ArrayList<PathIndex>();
    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "series")
    private List<Image> images = new ArrayList<Image>();
//    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "series")
//    private List<GridSubmitSeries> submitSeries = new ArrayList<GridSubmitSeries>();
    
    @Column(name = "SERIES_IMG_NUM")
    private int seriesImageNum;

	/**
	 * From the cid system.
	 */
	@Column(name = "series_keyword")
    @Lob
	private String seriesKeyword;
	
	@PrePersist
    protected void onCreate() {
        insertDate = new Date();
        updateDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateDate = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSeriesInsatnceUid() {
        return seriesInsatnceUid;
    }

    public void setSeriesInsatnceUid(String seriesInsatnceUid) {
    	if(seriesInsatnceUid != null) {
    		seriesInsatnceUid = seriesInsatnceUid.trim();
		}
		this.seriesInsatnceUid = seriesInsatnceUid;
    }

    public String getSeriesDescription() {
        return seriesDescription;
    }

    public void setSeriesDescription(String seriesDescription) {
    	if(seriesDescription != null) {
    		seriesDescription = seriesDescription.trim();
		}
		this.seriesDescription = seriesDescription;
    }

    public String getModality() {
        return modality;
    }

    public void setModality(String modality) {
    	if(modality != null) {
    		modality = modality.trim();
		}
		this.modality = modality;
    }

    public String getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(String seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public Date getSeriesDate() {
        return seriesDate;
    }

    public void setSeriesDate(Date seriesDate) {
        this.seriesDate = seriesDate;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public SeriesStatus getSeriesStatus() {
        return seriesStatus;
    }

    public void setSeriesStatus(SeriesStatus seriesStatus) {
        this.seriesStatus = seriesStatus;
    }

//    public Patient getPatient() {
//        return patient;
//    }
//
//    public void setPatient(Patient patient) {
//        this.patient = patient;
//    }

    public Study getStudy() {
        return study;
    }

    public void setStudy(Study study) {
        this.study = study;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<PathIndex> getPathIndexs() {
        return pathIndexs;
    }

    public void setPathIndexs(List<PathIndex> pathIndexs) {
        this.pathIndexs = pathIndexs;
    }
    
    public int getSeriesImageNum() {
		return seriesImageNum;
	}

	public void setSeriesImageNum(int seriesImageNum) {
		this.seriesImageNum = seriesImageNum;
	}

	public String getEntityID() {
		return entityID;
	}

	public void setEntityID(String entityID) {
		if(entityID != null) {
			entityID = entityID.trim();
		}
		this.entityID = entityID;
	}

	public String getSeriesKeyword() {
		return seriesKeyword;
	}

	public void setSeriesKeyword(String seriesKeyword) {
		if(seriesKeyword != null) {
			seriesKeyword = seriesKeyword.trim();
		}
		this.seriesKeyword = seriesKeyword;
	}

//    public List<GridSubmitSeries> getSubmitSeries() {
//        return submitSeries;
//    }
//
//    public void setSubmitSeries(List<GridSubmitSeries> submitSeries) {
//        this.submitSeries = submitSeries;
//    }
//
//    public static Series buildFromMetadata(CIDData.StudyDtl.SeriesDtls.SeriesDtl seriesDtl) {
//        Series newSeries = null;
//        if (seriesDtl == null) {
//            return newSeries;
//        }
//        newSeries = new Series();
//        newSeries.setModality(seriesDtl.getExamType());
//        newSeries.setSeriesInsatnceUid(seriesDtl.getSeriesNo());
//        newSeries.setSeriesDescription(seriesDtl.getExamType());
//        newSeries.setSeriesNumber(String.valueOf(seriesDtl.getEntityID()));
//        if (seriesDtl.getExamDtm() != null) {
//            /*
//             YYYYMMDDHHMMSS
//             20100131123456
//             */
//            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
//            try {
//                newSeries.setSeriesDate(df.parse(seriesDtl.getExamDtm()));
//            } catch (ParseException ex) {
//                Logger.getLogger(Series.class.getName()).log(Level.SEVERE, "Input the series date:{0}", seriesDtl.getExamDtm());
//            }
//        }
//        return newSeries;
//    }
}
