package org.ha.cid.ias.entity.rr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.ha.cid.ias.enumeration.StudyStatusEnum;

@Entity
@Table(name = "tg_stuindex")
@NamedNativeQueries({
	@NamedNativeQuery(name = Study.UPDATE_PATIENT_BY_CASE_NO, query = "UPDATE tg_stuindex SET fk_patindex_no = ?1 WHERE case_number = ?2"),
	@NamedNativeQuery(name = Study.UPDATE_PATIENT_BY_CASE_NO_AND_HOSPITAL_CODE, query = "UPDATE tg_stuindex SET fk_patindex_no = ?1 WHERE case_number = ?2 and hospital_code=?3"),
	@NamedNativeQuery(name = Study.UPDATE_PATIENT_BY_PAT_NO, query = "UPDATE tg_stuindex SET fk_patindex_no = ?1 WHERE fk_patindex_no = ?2"),
	@NamedNativeQuery(name = Study.UPDATE_PATIENT_BY_PAT_NO_AND_STUDY, query = "UPDATE tg_stuindex SET fk_patindex_no = ?1 WHERE fk_patindex_no = ?2 and stuindex_no = ?3")}
	)
@NamedQueries({
	@NamedQuery(name = Study.FIND_BY_ACCESION_NUMBER, query = "SELECT p FROM Study p WHERE p.accessionNumber = ?1"),
	@NamedQuery(name = Study.FIND_BY_CASE_NO, query = "SELECT p FROM Study p WHERE p.caseNo = ?1"),
	@NamedQuery(name = Study.FIND_BY_CASE_NO_AND_HOSPITAL_CODE, query = "SELECT p FROM Study p WHERE p.caseNo = ?1 and p.hospitalCode=?2"),
	@NamedQuery(name = Study.FIND_BY_PAT_NO, query = "SELECT p FROM Study p WHERE p.patient.patNo = ?1"),
	@NamedQuery(name = Study.FIND_BY_PAT_NO_AND_TYPE, query = "SELECT p FROM Study p WHERE p.patient.patNo = ?1 and p.studyType = ?2"),
	@NamedQuery(name = Study.FIND_BY_HOSPITAL_CODE_AND_CASE_NO_AND_ACCESSION_NO_AND_STUDY_ID, query = "SELECT s FROM Study s WHERE s.hospitalCode = ?1 AND s.caseNo =?2 AND s.accessionNumber = ?3 AND s.studyId = ?4"),
	@NamedQuery(name = "Study.findByPatientKeyAndHospCdeAndCaseNoAndAccessionNo", 
	query = "SELECT t FROM Study t WHERE t.patient.patNo = :patientNo AND t.hospitalCode = :hospitalCode AND t.caseNo =:caseNumber AND t.accessionNumber = :accessionNo")
	})
public class Study implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6497010852377757365L;
	
	public static final String UPDATE_PATIENT_BY_CASE_NO = "Study.updatePatientByCaseNo";
	public static final String FIND_BY_HOSPITAL_CODE_AND_CASE_NO_AND_ACCESSION_NO_AND_STUDY_ID = "Study.findByHospCdeAndCaseNoAndAccessionNoAndStudyID";
	
	public static final String UPDATE_PATIENT_BY_CASE_NO_AND_HOSPITAL_CODE = "Study.updatePatientByCaseNoAndHospitalCode";

	public static final String UPDATE_PATIENT_BY_PAT_NO = "Study.updatePatientByPatNo";
	public static final String UPDATE_PATIENT_BY_PAT_NO_AND_STUDY = "Study.updatePatientByPatNoAndStudy";
	
	public static final String FIND_BY_CASE_NO = "Study.findByCaseNo";
	
	public static final String FIND_BY_CASE_NO_AND_HOSPITAL_CODE = "Study.findByCaseNoAndHospitalCode";
	
	public static final String FIND_BY_PAT_NO = "Study.findByPatNo";
	
	public static final String FIND_BY_PAT_NO_AND_TYPE = "Study.findByPatNoAndType";

	public static final String FIND_BY_ACCESION_NUMBER = "Study.findByAccessionNumber";
	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE, generator = "STUDYNOGenerator")
	@SequenceGenerator(name = "STUDYNOGenerator", sequenceName = "SQ_STUINDEX_NO", allocationSize = 1)
	@Column(name = "stuindex_no")
	private int stuNo;
	@Column(name = "study_uid")
	private String studyUid;
	@Column(name = "study_id")
	private String studyId;
	@Column(name = "study_value")
	private String studyValue;
	@Column(name = "study_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date studyDate;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "study_desc")//, columnDefinition = "CLOB")
	private String studyDesc;
	@Column(name = "access_number")
	private String accessionNumber;
	@Column(name = "study_otherid")
	private String studyOtherId;
	@Column(name="study_fill")
	private String studyFill;

	/**
	 * From the cid system.
	 */
	@Column(name = "study_keyword")
    @Lob
	private String studyKeyword;
	//private int fkPatNo;
	@Column(name = "insert_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date insertDate;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_patindex_no")
	private Patient patient;
	//private String versionState;
	@Column(name="case_number")
	private String caseNo;
	@Column(name="txn_dtm")
	private String txnDtm;
	@Column(name = "hospital_code")
	private String hospitalCode;

    @Column(name = "STUDY_TYPE")
    private String studyType;

    /**
     * CID, ePR-ID
     */
    @Column(name = "APPLICATION_TYPE")
    private String applicationType;
    
	@Column(name = "study_status")
	@Enumerated(EnumType.STRING)
	private StudyStatusEnum studyStatus = StudyStatusEnum.PROCESSING;
	
    @Basic(optional = false, fetch = FetchType.LAZY)
    @Column(name = "remark")
    @Lob
    private String remark;

	@Column(name = "institution")
	private String transationCode;
	@Column(name = "visit_spec")
	private String visitSpec;
	@Column(name = "visit_ward_clinic")
	private String visitWardClinic;
	@Column(name = "visit_pay_code")
	private String payCode;
	@Column(name = "adt_dtm")
	private String adtDtm;
	
	@Column(name = "modality_concat")
	private String sendingApplication;

    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "study")
    private List<Series> series = new ArrayList<>();
    
    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name="FK_STUINDEX", referencedColumnName="stuindex_no")
    private List<CidRemark> remarks = new ArrayList<>();

	@Column(name = "fill1")
	private String fill1;
	
	@Lob
	@Basic(fetch=FetchType.LAZY)
	@Column(name = "ha7_msg")
	private String ha7Msg;

	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY, mappedBy = "study")
    private List<CidImageRouting> imageRoutings = new ArrayList<CidImageRouting>();

	/*private Collection<Serindex> serieses = new ArrayList<Serindex>();
	private Collection<Multindex> reports = new ArrayList<Multindex>();
		
	@OneToMany(mappedBy = "study", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<Multindex> getReports() {
		return reports;
	}

	public void setReports(Collection<Multindex> reports) {
		this.reports = reports;
	}*/

	@PrePersist
    protected void onCreate() {
        insertDate = new Date();
        updateDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updateDate = new Date();
    }

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/*@OneToMany(mappedBy = "study", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Collection<Serindex> getSerieses() {
		return serieses;
	}

	public void setSerieses(Collection<Serindex> serieses) {
		this.serieses = serieses;
	}*/


	public String getStudyValue() {
		return studyValue;
	}

	public void setStudyValue(String studyValue) {
		this.studyValue = studyValue;
	}


	public String getStudyOtherId() {
		return studyOtherId;
	}

	public void setStudyOtherId(String studyOtherId) {
		this.studyOtherId = studyOtherId;
	}


	public int getStuNo() {
		return stuNo;
	}

	public void setStuNo(int stuNo) {
		this.stuNo = stuNo;
	}


	public String getStudyUid() {
		return studyUid;
	}

	public void setStudyUid(String studyUid) {
		if(studyUid != null) {
			studyUid = studyUid.trim();
		}
		this.studyUid = studyUid;
	}


	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String studyId) {
		if(studyId != null) {
			studyId = studyId.trim();
		}
		this.studyId = studyId;
	}


	public Date getStudyDate() {
		return studyDate;
	}

	public void setStudyDate(Date studyDate) {
		this.studyDate = studyDate;
	}


	public String getStudyDesc() {
		return studyDesc;
	}

	public void setStudyDesc(String studyDesc) {
		this.studyDesc = studyDesc;
	}


	public String getAccessionNumber() {
		return accessionNumber;
	}

	public void setAccessionNumber(String accessionNumber) {
		if(accessionNumber != null) {
			accessionNumber = accessionNumber.trim();
		}
		this.accessionNumber = accessionNumber;
	}

	/*@Column(name = "fk_patindex_no")
	public int getFkPatNo() {
		return fkPatNo;
	}

	public void setFkPatNo(int fkPatNo) {
		this.fkPatNo = fkPatNo;
	}*/

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	
	/*@Column(name="version_state")
	public String getVersionState() {
		return versionState;
	}

	public void setVersionState(String versionState) {
		this.versionState = versionState;
	}*/

	public String getStudyFill() {
		return studyFill;
	}

	public void setStudyFill(String studyFill) {
		this.studyFill = studyFill;
	}
	
	
	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		if(caseNo != null) {
			caseNo = caseNo.trim();
		}
		this.caseNo = caseNo;
	}
	
	public String getTxnDtm() {
		return txnDtm;
	}

	public void setTxnDtm(String txnDtm) {
		this.txnDtm = txnDtm;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		if(hospitalCode != null) {
			hospitalCode = hospitalCode.trim();
		}
		this.hospitalCode = hospitalCode;
	}

	public String getStudyType() {
		return studyType;
	}

	public void setStudyType(String studyType) {
		if(studyType != null) {
			studyType = studyType.trim();
		}
		this.studyType = studyType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark != null) {
			remark = remark.trim();
		}
		this.remark = remark;
	}

	public String getTransationCode() {
		return transationCode;
	}

	public void setTransationCode(String transationCode) {
		if(transationCode != null) {
			transationCode = transationCode.trim();
		}
		this.transationCode = transationCode;
	}

	public String getSendingApplication() {
		return sendingApplication;
	}

	public void setSendingApplication(String sendingApplication) {
		if(sendingApplication != null) {
			sendingApplication = sendingApplication.trim();
		}
		this.sendingApplication = sendingApplication;
	}

	public List<Series> getSeries() {
		return series;
	}

	public void setSeries(List<Series> series) {
		this.series = series;
	}
	
	public List<CidRemark> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<CidRemark> remarks) {
		this.remarks = remarks;
	}

	public StudyStatusEnum getStudyStatus() {
		return studyStatus;
	}

	public void setStudyStatus(StudyStatusEnum studyStatus) {
		this.studyStatus = studyStatus;
	}

	public String getVisitSpec() {
		return visitSpec;
	}

	public void setVisitSpec(String visitSpec) {
		if(visitSpec != null) {
			visitSpec = visitSpec.trim();
		}
		this.visitSpec = visitSpec;
	}

	public String getVisitWardClinic() {
		return visitWardClinic;
	}

	public void setVisitWardClinic(String visitWardClinic) {
		if(visitWardClinic != null) {
			visitWardClinic = visitWardClinic.trim();
		}
		this.visitWardClinic = visitWardClinic;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		if(payCode != null) {
			payCode = payCode.trim();
		}
		this.payCode = payCode;
	}

	public String getAdtDtm() {
		return adtDtm;
	}

	public void setAdtDtm(String adtDtm) {
		if(adtDtm != null) {
			adtDtm = adtDtm.trim();
		}
		this.adtDtm = adtDtm;
	}

	public String getStudyKeyword() {
		return studyKeyword;
	}

	public void setStudyKeyword(String studyKeyword) {
		if(studyKeyword != null) {
			studyKeyword = studyKeyword.trim();
		}
		this.studyKeyword = studyKeyword;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getFill1() {
		return fill1;
	}

	public void setFill1(String fill1) {
		this.fill1 = fill1;
	}
	
	public String getHa7Msg() {
		return ha7Msg;
	}

	public void setHa7Msg(String ha7Msg) {
		this.ha7Msg = ha7Msg;
	}

	public List<CidImageRouting> getImageRoutings() {
		return this.imageRoutings;
	}

	public void setImageRoutings(List<CidImageRouting> imageRoutings) {
		this.imageRoutings = imageRoutings;
	}

	/**
	 * Add a {@link CidImageRouting} into the list of {@link CidImageRouting}.
	 *
	 * @param imageRouting a {@link CidImageRouting} to be added 
	 */
	public void addImageRouting(CidImageRouting imageRouting) {

		imageRouting.setStudy(this);
		this.imageRoutings.add(imageRouting);
	}

}
