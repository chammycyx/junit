/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.rr;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * The class which provides a converter for {@link CidImageRoutedState} when
 *
 * <pre>
 * Last commit:-
 * $Rev: 10433 $
 * $Author: ltm548 $
 * $Date: 2016-09-14 17:13:17 +0800 (Wed, 14 Sep 2016) $
 * </pre>
 */
@Converter
public class CidImageRoutedStateConverter implements AttributeConverter<CidImageRoutedState, String> {

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
	 */
	@Override
	public String convertToDatabaseColumn(CidImageRoutedState attribute) {

		return attribute.toString();
	}

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public CidImageRoutedState convertToEntityAttribute(String dbDataValue) {

		CidImageRoutedState routedState = null;

		for (CidImageRoutedState value : CidImageRoutedState.values()) {

			if (dbDataValue.equalsIgnoreCase(value.toString())) {

				routedState = value;
			}
		}

		return routedState;
	}

}
