package org.ha.cid.ias.entity.rr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "T_CID_ANNOTATION")
public class Annotation implements Serializable {

	/**
	 * series
	 */
	private static final long serialVersionUID = 3919041023928257454L;
	
	@Id
    @SequenceGenerator(name = "SQ_ANNOTATION_NO", sequenceName = "SQ_ANNOTATION_NO",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ANNOTATION_NO")
	@Column(name = "id")
	private long id;

	@Column(name = "seq")
	private int annotationSeq;

	@Column(name = "type")
	private String annotationType;

	@Column(name = "text")
	@Lob
	private String annotationText;

	@Column(name = "coordinate")
	private String annotationCoordinate;

	@Column(name = "status")
	private String annotationStatus;

	@Column(name = "editable")
	private String annotationEditable;

	@Column(name = "updtm")
	private String annotationUpdDtm;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_image")
	private Image image;

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getAnnotationSeq() {
		return annotationSeq;
	}

	public void setAnnotationSeq(int annotationSeq) {
		this.annotationSeq = annotationSeq;
	}

	public String getAnnotationType() {
		return annotationType;
	}

	public void setAnnotationType(String annotationType) {
		if(annotationType != null) {
			annotationType = annotationType.trim();
		}
		this.annotationType = annotationType;
	}

	public String getAnnotationText() {
		return annotationText;
	}

	public void setAnnotationText(String annotationText) {
		if(annotationText != null) {
			annotationText = annotationText.trim();
		}
		this.annotationText = annotationText;
	}

	public String getAnnotationCoordinate() {
		return annotationCoordinate;
	}

	public void setAnnotationCoordinate(String annotationCoordinate) {
		if(annotationCoordinate != null) {
			annotationCoordinate = annotationCoordinate.trim();
		}
		this.annotationCoordinate = annotationCoordinate;
	}

	public String getAnnotationStatus() {
		return annotationStatus;
	}

	public void setAnnotationStatus(String annotationStatus) {
		if(annotationStatus != null) {
			annotationStatus = annotationStatus.trim();
		}
		this.annotationStatus = annotationStatus;
	}

	public String getAnnotationEditable() {
		return annotationEditable;
	}

	public void setAnnotationEditable(String annotationEditable) {
		if(annotationEditable != null) {
			annotationEditable = annotationEditable.trim();
		}
		this.annotationEditable = annotationEditable;
	}

	public String getAnnotationUpdDtm() {
		return annotationUpdDtm;
	}

	public void setAnnotationUpdDtm(String annotationUpdDtm) {
		if(annotationUpdDtm != null) {
			annotationUpdDtm = annotationUpdDtm.trim();
		}
		this.annotationUpdDtm = annotationUpdDtm;
	}

}
