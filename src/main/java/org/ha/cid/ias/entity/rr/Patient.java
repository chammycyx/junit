package org.ha.cid.ias.entity.rr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@SqlResultSetMappings({ @SqlResultSetMapping(name = "getResourceSet", entities = {
		@EntityResult(entityClass = Patient.class),
		@EntityResult(entityClass = Study.class) }) })
@NamedNativeQueries({
		@NamedNativeQuery(name = Patient.UPDATE_PATIENT_INFO, query = "UPDATE tg_patindex SET pat_id = ?1, pat_name=?2, pat_birthdate=?3,  pat_sex=?4 WHERE pat_globalid = ?5"),
		@NamedNativeQuery(name = Patient.QUERY_PATIENT_DOCUMENT, query = "SELECT pat_globalID        as patientId\n"
				+ "    FROM tg_patindex\n"
				+ "    WHERE patindex_no = (select sak_patient from t_document_entry_index where id = ?1)"),
		@NamedNativeQuery(name = Patient.QUERY_PATIENT_FOLDER, query = "SELECT pat_globalID        as patientId\n"
				+ "    FROM tg_patindex\n"
				+ "    WHERE patindex_no = (select sak_patient from t_folder_index where id = ?1)") })
@NamedQueries({
		@NamedQuery(name = Patient.FIND_BY_PATIENTKEY, query = "SELECT p FROM Patient p WHERE p.patGlobalId = :patientKey"),
		@NamedQuery(name = Patient.FIND_BY_PATIENTNO, query = "SELECT p FROM Patient p WHERE p.patNo = :patNo"),
		@NamedQuery(name = "Patient.queryByPatKeyAndHKID", query = "SELECT t FROM Patient t WHERE t.patGlobalId = :patKey and t.patId = :hkid"),
		@NamedQuery(name = Patient.FIND_BY_PATIENT_KEY_AND_SUVIVING_KEY, query = "SELECT p FROM Patient p WHERE p.patGlobalId = ?1 and p.patSurvivingGlobalId =?2") })
@Entity
@Table(name = "tg_patindex", uniqueConstraints = { 
		@UniqueConstraint(columnNames = { "pat_globalid" }) 
})
// @TableGenerator(name = "patindex_gen", table = "tb_generator", pkColumnName =
// "gen_name", valueColumnName = "gen_value", pkColumnValue = "patindex_pk",
// allocationSize = 1)
public class Patient implements java.io.Serializable {

	public static final String QUERY_PATIENT_DOCUMENT = "Patient.findByDocumentId";
	public static final String QUERY_PATIENT_FOLDER = "Patient.findByFolderId";
	public static final String UPDATE_PATIENT_INFO = "Patient.updatePatientInfo";
	public static final String FIND_BY_PATIENTKEY = "Patient.findByPatientKey";
	public static final String FIND_BY_PATIENTNO = "Patient.findByPatientNo";
	public static final String FIND_BY_PATIENT_KEY_AND_SUVIVING_KEY = "Patient.findByPatientKeyAndSuvivingKey";

	private static final long serialVersionUID = -8547029977766233301L;
	@Id
	// @GeneratedValue(strategy = GenerationType.TABLE, generator =
	// "patindex_gen")
	@GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE, generator = "PatientGenerator")
	@SequenceGenerator(name = "PatientGenerator", sequenceName = "SQ_PATINDEX_NO", allocationSize = 1)
	@Column(name = "patindex_no")
	private int patNo;
	@Column(name = "pat_id")
	private String patId;
	@Column(name = "pat_name")
	private String patName;
	@Column(name = "pat_sex")
	private String patSex;
	@Column(name = "pat_birthdate")
	private String patBirthdate;
	@Column(name = "pat_globalid")
	private String patGlobalId;
	@Column(name = "DEATH")
	private String deathIndicator;
	@Column(name = "insert_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date insertDate;
	@Column(name = "update_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Study> studies = new ArrayList<Study>();
	@Column(name = "pat_surviving_globalid")
	private String patSurvivingGlobalId;
	//N or Y
	@Column(name = "deleted")
	private String deleted;
	//N or Y
	@Column(name = "merged")
	private String merged;
	@Column(name = "txn_dtm")
	private String txnDtm;
	@Column(name = "doubtful")
	private boolean doubtful;


	@PrePersist
	protected void onCreate() {
		Date now = new Date();
		insertDate = now;
		updateDate = now;
	}

	@PreUpdate
	protected void onUpdate() {
		updateDate = new Date();
	}

	public int getPatNo() {
		return patNo;
	}

	public void setPatNo(int patNo) {
		this.patNo = patNo;
	}

	public String getPatId() {
		return patId;
	}

	public void setPatId(String patId) {
		if(patId != null) {
			patId = patId.trim();
		}
		this.patId = patId;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		if(patName != null) {
			patName = patName.trim();
		}
		this.patName = patName;
	}

	public String getPatSex() {
		return patSex;
	}

	public void setPatSex(String patSex) {
		if(patSex != null) {
			patSex = patSex.trim();
		}
		this.patSex = patSex;
	}

	public String getPatBirthdate() {
		return patBirthdate;
	}

	public void setPatBirthdate(String patBirthdate) {
		if(patBirthdate != null) {
			patBirthdate = patBirthdate.trim();
		}
		this.patBirthdate = patBirthdate;
	}

	public String getPatGlobalId() {
		return patGlobalId;
	}

	public void setPatGlobalId(String patGlobalId) {
		if(patGlobalId != null) {
			patGlobalId = patGlobalId.trim();
		}
		this.patGlobalId = patGlobalId;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<Study> getStudies() {
		return studies;
	}

	public void setStudies(List<Study> studies) {
		this.studies = studies;
	}

	public String getPatSurvivingGlobalId() {
		return patSurvivingGlobalId;
	}

	public void setPatSurvivingGlobalId(String patSurvivingGlobalId) {
		this.patSurvivingGlobalId = patSurvivingGlobalId;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getMerged() {
		return merged;
	}

	public void setMerged(String merged) {
		this.merged = merged;
	}

	public String getTxnDtm() {
		return txnDtm;
	}

	public void setTxnDtm(String txnDtm) {
		this.txnDtm = txnDtm;
	}

	public boolean isDoubtful() {
		return doubtful;
	}

	public void setDoubtful(boolean doubtful) {
		this.doubtful = doubtful;
	}

	public String getDeathIndicator() {
		return deathIndicator;
	}

	public void setDeathIndicator(String deathIndicator) {
		if(deathIndicator != null) {
			deathIndicator = deathIndicator.trim();
		}
		this.deathIndicator = deathIndicator;
	}

	@Override
	public String toString() {
		return "Patient [patNo=" + patNo + ", patId=" + patId + ", patName="
				+ patName + ", patSex=" + patSex + ", patBirthdate="
				+ patBirthdate + ", patGlobalId=" + patGlobalId
				+ ", deathIndicator=" + deathIndicator + ", insertDate="
				+ insertDate + ", updateDate=" + updateDate + ", studies="
				+ studies + ", patSurvivingGlobalId=" + patSurvivingGlobalId
				+ ", deleted=" + deleted + ", merged=" + merged + ", txnDtm="
				+ txnDtm + ", doubtful=" + doubtful + "]";
	}

}
