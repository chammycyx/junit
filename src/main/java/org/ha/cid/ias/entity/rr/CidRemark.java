package org.ha.cid.ias.entity.rr;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "t_cid_remark", uniqueConstraints = @UniqueConstraint(columnNames = {"ID", "FK_STUINDEX"}))
public class CidRemark implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7272106278577257550L;

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE, generator = "CidRemarkIDGenerator")
	@SequenceGenerator(name = "CidRemarkIDGenerator", sequenceName = "SEQ_CID_REMARK", allocationSize = 1)
	@Column(name = "ID",unique = true)
	private long id;
	
	@Column(name = "IMAGE_REMARK")
	private String imageRemark;
	
	@Column(name = "TRANSACTION_DTM")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date transactionDtm;
	
	@ManyToOne
    @JoinColumn(name = "FK_STUINDEX", insertable=false, updatable=false, nullable=false)
    private Study study;
	
	@PrePersist
    protected void onCreate() {
		transactionDtm = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
    	transactionDtm = new Date();
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImageRemark() {
		return imageRemark;
	}

	public void setImageRemark(String imageRemark) {
		if(imageRemark != null) {
			imageRemark = imageRemark.trim();
		}
		this.imageRemark = imageRemark;
	}

	public Date getTransactionDtm() {
		return transactionDtm;
	}

	public void setTransactionDtm(Date transactionDtm) {
		this.transactionDtm = transactionDtm;
	}

	public Study getStudy() {
		return study;
	}

	public void setStudy(Study study) {
		this.study = study;
	}
	
}
