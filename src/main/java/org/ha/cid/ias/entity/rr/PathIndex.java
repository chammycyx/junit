/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ha.cid.ias.entity.rr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author developer
 */
@Entity
@Table(name = "TG_PATHINDEX", uniqueConstraints = @UniqueConstraint(columnNames = {"accession_number", "study_id"}))
public class PathIndex implements Serializable {

    private static final long serialVersionUID = 4332019775703729032L;

    @Id
    @SequenceGenerator(name = "SQ_PATHINDEX_NO", sequenceName = "SQ_PATHINDEX_NO",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PATHINDEX_NO")
    @Column(name = "PATHINDEX_NO")
    private Long id;
    @Column(name = "STORE_PATH")
    private String storePath;
    @Column(name = "SERMOTIVE_TAG")
    private String serMotiveTag = "LOCAL";
    @Column(name = "insert_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name = "update_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_SERINDEX_NO")
    private Series series;
    
    @ManyToOne
    @JoinColumn(name = "FK_NODENO")
    private EdgeNode gridEdgeNode;
    
    @PrePersist
    protected void onCreate() {
        insertDate = new Date();
        updateDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public String getSerMotiveTag() {
        return serMotiveTag;
    }

    public void setSerMotiveTag(String serMotiveTag) {
        this.serMotiveTag = serMotiveTag;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }
    
    public EdgeNode getGridEdgeNode() {
		return gridEdgeNode;
	}

	public void setGridEdgeNode(EdgeNode gridEdgeNode) {
		this.gridEdgeNode = gridEdgeNode;
	}

    @Override
    public String toString() {
        return "GridPathIndex{" + "id=" + id + ", storePath=" + storePath + ", serMotiveTag=" + serMotiveTag + ", insertDate=" + insertDate + "updateDate=" + updateDate + '}';
    }

}
