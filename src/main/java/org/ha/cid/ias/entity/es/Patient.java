package org.ha.cid.ias.entity.es;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class Patient implements java.io.Serializable {
	private static final long serialVersionUID = 8286837373805788912L;

	/**
	 * PatientKey
	 */
	@Column(name = "pat_key")
	private String patientKey;
	
	/**
	 * 0010,0010	PatientName
	 */
	@Column(name = "pat_name")
	private String patientName;

	/**
	 * 0010,0020	PatientID
	 */
	@Column(name = "pat_id")
	private String patientID;
	
	/**
	 * 0010,1000	OtherPatientIDs
	 */
	@Column(name = "other_pat_ids")
	private String OtherPatientIDs;
	
	/**
	 * 0010,0021	IssuerOfPatientID
	 */
	@Column(name = "issuer_pat_ID")
	private String issuerOfPatientID;
	
	/**
	 * 0010,0030	PatientBirthDate
	 */
	@Column(name = "pat_BirthDate")
	private String patientBirthDate;

	/**
	 * 0010,0040	PatientSex
	 */
	@Column(name = "pat_sex")
	private String patientSex;

	@Column(name = "pat_custom1")
	private String patientCustomAttribute1;

	@Column(name = "pat_custom2")
	private String patientCustomAttribute2;

	@Column(name = "pat_custom3")
	private String patientCustomAttribute3;
	
	@Transient
	private int studyCount;

	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String patientID) {
		if(patientID != null) {
			patientID = patientID.trim();
		}
		this.patientID = patientID;
	}

	public String getIssuerOfPatientID() {
		return issuerOfPatientID;
	}

	public void setIssuerOfPatientID(String issuerOfPatientID) {
		this.issuerOfPatientID = issuerOfPatientID;
	}

	public String getPatientBirthDate() {
		return patientBirthDate;
	}

	public void setPatientBirthDate(String patientBirthDate) {
		if(patientBirthDate != null) {
			patientBirthDate = patientBirthDate.trim();
		}
		this.patientBirthDate = patientBirthDate;
	}

	public String getPatientSex() {
		return patientSex;
	}

	public void setPatientSex(String patientSex) {
		if(patientSex != null) {
			patientSex = patientSex.trim();
		}
		this.patientSex = patientSex;
	}

	public String getPatientCustomAttribute1() {
		return patientCustomAttribute1;
	}

	public void setPatientCustomAttribute1(String patientCustomAttribute1) {
		this.patientCustomAttribute1 = patientCustomAttribute1;
	}

	public String getPatientCustomAttribute2() {
		return patientCustomAttribute2;
	}

	public void setPatientCustomAttribute2(String patientCustomAttribute2) {
		this.patientCustomAttribute2 = patientCustomAttribute2;
	}

	public String getPatientCustomAttribute3() {
		return patientCustomAttribute3;
	}

	public void setPatientCustomAttribute3(String patientCustomAttribute3) {
		this.patientCustomAttribute3 = patientCustomAttribute3;
	}


	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		if(patientName != null) {
			patientName = patientName.trim();
		}
		this.patientName = patientName;
	}

	public String getPatientKey() {
		return patientKey;
	}

	public void setPatientKey(String patientKey) {
		if(patientKey != null) {
			patientKey = patientKey.trim();
		}
		this.patientKey = patientKey;
	}

	public String getOtherPatientIDs() {
		return OtherPatientIDs;
	}

	public void setOtherPatientIDs(String otherPatientIDs) {
		OtherPatientIDs = otherPatientIDs;
	}

	public int getStudyCount() {
		return studyCount;
	}

	public void setStudyCount(int studyCount) {
		this.studyCount = studyCount;
	}

	@Override
	public String toString() {
		return "Patient [patientKey=" + patientKey + ", patientName="
				+ patientName + ", patientID=" + patientID
				+ ", OtherPatientIDs=" + OtherPatientIDs
				+ ", issuerOfPatientID=" + issuerOfPatientID
				+ ", patientBirthDate=" + patientBirthDate + ", patientSex="
				+ patientSex + ", patientCustomAttribute1="
				+ patientCustomAttribute1 + ", patientCustomAttribute2="
				+ patientCustomAttribute2 + ", patientCustomAttribute3="
				+ patientCustomAttribute3 + "]";
	}

}