package org.ha.cid.ias.entity.esb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 *
 * <pre>
 * Last commit:-
 * $Rev$
 * $Author$ ymm698
 * $Date$ @date Aug 14, 2017 4:01:38 PM 
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_patient_update_history")
@NamedQueries({
	@NamedQuery(
		name = PatientUpdateHistory.NAMED_QUERY_FIND_BY_PATIENT_KEYS,
		query = "SELECT c FROM PatientUpdateHistory c WHERE c.patientKey IN ?1"
	)
})
public class PatientUpdateHistory implements Serializable {
	
	private static final long serialVersionUID = 6003958202636007932L;

	public final static String NAMED_QUERY_FIND_BY_PATIENT_KEYS = "findByPatientKeys";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "history_id")
	private long historyId;
	
	@Column(name = "patient_key")
	private String patientKey;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_esb_dt")
	private Date lastEsbDt;
	

	/**
	 * 
	 * @return the ID of patient history
	 */
	public long getHistoryId() {
		return historyId;
	}

	/**
	 * 
	 * @param historyId history Id of patient update history record
	 */
	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	/**
	 * 
	 * @return patient key of patient update history record
	 */
	public String getPatientKey() {
		return patientKey;
	}

	/**
	 * 
	 * @param patientKey patient key of patient update history record
	 */
	public void setPatientKey(String patientKey) {
		this.patientKey = patientKey;
	}

	/**
	 * 
	 * @return latest update date time of a patient
	 */
	public Date getLastEsbDt() {
		return lastEsbDt;
	}

	/**
	 * 
	 * @param latestUpdateDt the latest update date time of a patient
	 */
	public void setLastEsbDt(Date latestUpdateDt) {
		this.lastEsbDt = latestUpdateDt;
	}
	
}
