package org.ha.cid.ias.entity.es;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Version;

@Entity
@Table(name = "t_audit_event")
@NamedQueries({
    @NamedQuery(name = AuditEvent.FIND_BY_ACCESSION_NO, 
    	query = " SELECT t FROM AuditEvent t WHERE t.accessionNo = :accessionNo ORDER BY t.receivedTime DESC")
})
public class AuditEvent implements Serializable{

	public static final String FIND_BY_ACCESSION_NO = "AuditEvent.findByAccessionNo";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7381939453010636316L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Version
	@Column(name = "version")
	private long version;

	@Column(name = "dt_insert", updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdTime;

	@Column(name = "dt_update")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedTime;
	
	@Column(name = "dt_received")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date receivedTime;
	
	@Column(name = "dt_responsed")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date responsedTime;
	
	@Column(name = "event_name")
	private String eventName;
	
	@Column(name = "operation")
	private String operation;
	
	@Column(name = "request")
	private String request;
	
	@Column(name = "request_channel")
	private String requestChannel;
	
	@Column(name = "request_sys")
	private String requestSys;
	
	@Column(name = "response")
	private String response;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "workstation_id")
	private String workstationId;
	
	@Column(name = "client_ip")
	private String clientIP;
	
	@Column(name = "accession_no")
	private String accessionNo;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "pat_id")
	private String patId;
	
	@Column(name = "hospital_code")
	private String hospitalCode;
	
	@Column(name = "image_quality")
	private String imageQuality;
	
	@Column(name = "pat_key")
	private String patKey;
	
	@Column(name = "row_guid")
	private String rowGuid;
	
	@Column(name = "study_uid")
	private String studyUid;
	
	@Column(name = "source_server_id")
	private String sourceServerId;
	
	@Column(name = "target_server_id")
	private String targetServerId;
	
	@Column(name = "group_name")
	private String groupName;
	
	@Column(name = "display_name")
	private String displayName;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "case_number")
	private String caseNumber;
	@Column(name = "pat_name")
	private String patName;
	@Column(name = "pat_sex")
	private String patSex;
	@Column(name = "pat_dob")
	private String patDob;
	@Column(name = "pat_surviving_key")
	private String patSurvivingKey;
	@Column(name = "pat_deathindicator")
	private String deathIndicator;

    @Column(name = "exception")
    @Lob
    private String exception;
    @Column(name = "change")
    @Lob
    private String change;
    
	@Column(name = "sent")
	private int sent;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	public Date getReceivedTime() {
		return receivedTime;
	}
	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}
	public Date getResponsedTime() {
		return responsedTime;
	}
	public void setResponsedTime(Date responsedTime) {
		this.responsedTime = responsedTime;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getRequestChannel() {
		return requestChannel;
	}
	public void setRequestChannel(String requestChannel) {
		this.requestChannel = requestChannel;
	}
	public String getRequestSys() {
		return requestSys;
	}
	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWorkstationId() {
		return workstationId;
	}
	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getAccessionNo() {
		return accessionNo;
	}
	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPatId() {
		return patId;
	}
	public void setPatId(String patId) {
		this.patId = patId;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getImageQuality() {
		return imageQuality;
	}
	public void setImageQuality(String imageQuality) {
		this.imageQuality = imageQuality;
	}
	public String getPatKey() {
		return patKey;
	}
	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}
	public String getRowGuid() {
		return rowGuid;
	}
	public void setRowGuid(String rowGuid) {
		this.rowGuid = rowGuid;
	}
	public String getStudyUid() {
		return studyUid;
	}
	public void setStudyUid(String studyUid) {
		this.studyUid = studyUid;
	}
	public String getSourceServerId() {
		return sourceServerId;
	}
	public void setSourceServerId(String sourceServerId) {
		this.sourceServerId = sourceServerId;
	}
	public String getTargetServerId() {
		return targetServerId;
	}
	public void setTargetServerId(String targetServerId) {
		this.targetServerId = targetServerId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@PrePersist
	public void onPrePersist() {
		Date now = new Date();
		createdTime = now;
		updatedTime = now;
	}
	
    @PreUpdate
    protected void onUpdate() {
    	updatedTime = new Date();
    }
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getPatName() {
		return patName;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getPatSex() {
		return patSex;
	}
	public void setPatSex(String patSex) {
		this.patSex = patSex;
	}
	public String getPatDob() {
		return patDob;
	}
	public void setPatDob(String patDob) {
		this.patDob = patDob;
	}
	public String getDeathIndicator() {
		return deathIndicator;
	}
	public void setDeathIndicator(String deathIndicator) {
		this.deathIndicator = deathIndicator;
	}
	public String getPatSurvivingKey() {
		return patSurvivingKey;
	}
	public void setPatSurvivingKey(String patSurvivingKey) {
		this.patSurvivingKey = patSurvivingKey;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	
	public int getSent() {
		return sent;
	}
	public void setSent(int sent) {
		this.sent = sent;
	}
	public String getChange() {
		return change;
	}
	public void setChange(String change) {
		this.change = change;
	}
	
	
	
}
