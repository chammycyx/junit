/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.ha.cid.ias.enumeration.StudySnapshotCategory;
import org.ha.cid.ias.persistence.converter.StudySnapshotCategoryConverter;


/**
 * The entity class which is used for persisting an update of <tt>Study</tt> per each <tt>Queue</tt>.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9637 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:04:27 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_subscription_study_snapshot")
public class SubscriptionStudySnapshot implements Serializable {

	private static final long serialVersionUID = 6751429795853524028L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "snapshot_id")
	private long snapshotId;

	@Column(name = "accession_number")
	private String accessionNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "processed_dt")
	private Date processedDt;

	@Column(name = "category")
	@Convert(converter = StudySnapshotCategoryConverter.class)
	private StudySnapshotCategory category;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "subscription_id", nullable = false)
	private Subscription subscription;

	/**
	 * @return the value of <tt>snapshot_id</tt>
	 */
	public long getSnapshotId() {
		return snapshotId;
	}

	/**
	 * @param snapshotId the <tt>snapshot_id</tt> to set
	 */
	public void setSnapshotId(long snapshotId) {
		this.snapshotId = snapshotId;
	}

	/**
	 * @return the value of <tt>accession_number</tt>
	 */
	public String getAccessionNumber() {
		return accessionNumber;
	}

	/**
	 * @param accessionNumber the <tt>accession_number</tt> to set
	 */
	public void setAccessionNumber(String accessionNumber) {
		this.accessionNumber = accessionNumber;
	}

	/**
	 * @return the value of <tt>processed_dt</tt>
	 */
	public Date getProcessedDt() {
		return getDate(processedDt);
	}

	/**
	 * @param processedDt the <tt>processed_dt</tt> to set
	 */
	public void setProcessedDt(Date processedDt) {
		this.processedDt = getDate(processedDt);
	}

	/**
	 * @return the value of <tt>category</tt>
	 */
	public StudySnapshotCategory getCategory() {
		return category;
	}

	/**
	 * @param category the <tt>category</tt> to set
	 */
	public void setCategory(StudySnapshotCategory category) {
		this.category = category;
	}

	/**
	 * @return the {@link Subscription}
	 */
	public Subscription getSubscription() {
		return subscription;
	}

	/**
	 * @param subscription the {@link Subscription} to set
	 */
	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	private Date getDate(Date value){
		return value==null?null:new Date(value.getTime());
	}
	
}
