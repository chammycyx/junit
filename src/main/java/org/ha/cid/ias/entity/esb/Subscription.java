/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


/**
 * The entity class which is used for persisting a transaction of PAS-ESB subscription.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9635 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:03:59 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_subscription")
public class Subscription implements Serializable {

	private static final long serialVersionUID = -6739842581435831827L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subscription_id")
	private long subscriptionId;

	@Version
	@Column(name = "version")
	private long version;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "published_dt")
	private Date publishedDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "received_dt")
	private Date receivedDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "completed_dt")
	private Date completedDt;

	@ManyToOne
	@JoinColumn(name = "staging_message_id", nullable = false)
	private Staging staging;

	@ManyToOne
	@JoinColumn(name = "queue_id", nullable = false)
	private SubscriptionQueue subscriptionQueue;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "subscription", orphanRemoval=true)
	private Set<SubscriptionStudySnapshot> subscriptionStudySnapshots = new HashSet<>();

	/**
	 * Add a {@link SubscriptionStudySnapshot}.
	 *
	 * @param subscriptionStudySnapshot the {@link SubscriptionStudySnapshot} to add
	 */
	public void addSubscriptionStudySnapshot(SubscriptionStudySnapshot subscriptionStudySnapshot) {

		subscriptionStudySnapshot.setSubscription(this);
		this.subscriptionStudySnapshots.add(subscriptionStudySnapshot);
	}

	/**
	 * @return a {@link Set} of {@link SubscriptionStudySnapshot}
	 */
	public Set<SubscriptionStudySnapshot> getSubscriptionStudySnapshots() {
		return this.subscriptionStudySnapshots;
	}

	/**
	 * @return the value of <tt>subscription_id</tt>
	 */
	public long getSubscriptionId() {
		return subscriptionId;
	}

	/**
	 * @param subscriptionId the <tt>subscription_id</tt> to set
	 */
	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	/**
	 * @return the value of <tt>version</tt>
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the <tt>version</tt> to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the value of <tt>published_dt</tt>
	 */
	public Date getPublishedDt() {
		return getDate(publishedDt);
	}

	/**
	 * @param publishedDt the <tt>published_dt</tt> to set
	 */
	public void setPublishedDt(Date publishedDt) {
		this.publishedDt = getDate(publishedDt);
	}

	/**
	 * @return the value of <tt>received_dt</tt>
	 */
	public Date getReceivedDt() {
		return getDate(receivedDt);
	}

	/**
	 * @param receivedDt the <tt>received_dt</tt> to set
	 */
	public void setReceivedDt(Date receivedDt) {
		this.receivedDt = getDate(receivedDt);
	}

	/**
	 * @return the value of <tt>completed_dt</tt>
	 */
	public Date getCompletedDt() {
		return getDate(completedDt);
	}

	/**
	 * @param completedDt the <tt>completed_dt</tt> to set
	 */
	public void setCompletedDt(Date completedDt) {
		this.completedDt = getDate(completedDt);
	}

	/**
	 * @return the {@link Staging}
	 */
	public Staging getStaging() {
		return staging;
	}

	/**
	 * @param staging the {@link Staging} to set
	 */
	public void setStaging(Staging staging) {
		this.staging = staging;
	}

	/**
	 * @return the {@link SubscriptionQueue}
	 */
	public SubscriptionQueue getSubscriptionQueue() {
		return subscriptionQueue;
	}

	/**
	 * @param subscriptionQueue the {@link SubscriptionQueue} to set
	 */
	public void setSubscriptionQueue(SubscriptionQueue subscriptionQueue) {
		this.subscriptionQueue = subscriptionQueue;
	}
	
	private Date getDate(Date value){
		return value==null?null:new Date(value.getTime());
	}

}
