package org.ha.cid.ias.entity.es;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "t_series", uniqueConstraints = @UniqueConstraint(columnNames = {
		"series_iuid", "fk_study" }))
public class Series implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8409275013298882205L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Version
	@Column(name = "version")
	private long version;

	@Column(name = "created_time", updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdTime;

	@Column(name = "updated_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedTime;
	/**
	 * From the cid system.
	 */
	@Column(name = "series_keyword")
    private String seriesKeyword;
	

	/**
	 * 0020,000E SeriesInstanceUID
	 */
	@Column(name = "series_iuid", updatable = false)
	private String seriesInstanceUID;

	/**
	 * 0040,0032 EntityID
	 */
	@Column(name = "entity_id")
	private String entityId;

	/**
	 * 0008,103E SeriesDescription
	 */
	@Column(name = "series_desc")
	private String seriesDescription;

	/**
	 * 0008,0060 Modality
	 */
	// for the dicom file, this attribute is from 0008,0060. For the CID, this
	// attribute is from ExamType
	@Column(name = "modality")
	private String modality;

	/**
	 * 0008,0021 SeriesDate
	 */
	// for the dicom file, this attribute is from 0008,0021. For the CID, this
	// attribute is from ExamDtm
	@Column(name = "series_date")
	private String seriesDate;

	/**
	 * 0008,0031 SeriesTime
	 */
	@Column(name = "series_time")
	private String seriesTime;

	/**
	 * 0008,1040 InstitutionalDepartmentName
	 */
	@Column(name = "department")
	private String institutionalDepartmentName;

	/**
	 * 0008,0080 InstitutionName
	 */
	@Column(name = "institution")
	private String institutionName;

	/**
	 * 0008,1010 StationName
	 */
	@Column(name = "station_name")
	private String stationName;

	/**
	 * 0018,0015 BodyPartExamined
	 */
	@Column(name = "body_part")
	private String bodyPartExamined;

	/**
	 * 0018,1030 ProtocolName
	 */
	@Column(name = "protocol_name")
	private String protocolName;

	/**
	 * 0020,0052 FrameOfReferenceUID
	 */
	@Column(name = "frame_reference_uid")
	private String frameOfReferenceUID;

	/**
	 * 0020,0060 Laterality
	 */
	@Column(name = "laterality")
	private String laterality;

	/**
	 * 0040,0244 PerformedProcedureStepStartDate
	 */
	@Column(name = "pps_start_date")
	private String performedProcedureStepStartDate;

	/**
	 * 0040,0245 PerformedProcedureStepStartTime
	 */
	@Column(name = "pps_start_time")
	private String performedProcedureStepStartTime;

	@Column(name = "pps_iuid")
	private String performedProcedureStepInstanceUID;

	@Column(name = "pps_cuid")
	private String performedProcedureStepClassUID;

	@Column(name = "series_custom1")
	private String seriesCustomAttribute1;

	@Column(name = "series_custom2")
	private String seriesCustomAttribute2;

	@Column(name = "series_custom3")
	private String seriesCustomAttribute3;

	@Column(name = "num_instances")
	private int numberOfInstances = -1;

	@Column(name = "num_instances_a")
	private int numberOfInstancesA = -1;

	@Column(name = "perf_phys_name")
	private String performingPhysicianName;

	 @ManyToOne
	 @JoinColumn(name = "fk_study")
	 private Study study;
//	@Column(name = "fk_study")
//	private long studyId;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "series", orphanRemoval = true, cascade = { CascadeType.ALL })
	@OrderBy("imageID, imageSequence, imageVersion ASC")
	private List<Image> images = new ArrayList<>();

	@PrePersist
	public void onPrePersist() {
		Date now = new Date();
		createdTime = now;
		updatedTime = now;
	}

	@PreUpdate
	protected void onUpdate() {
		updatedTime = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getSeriesInstanceUID() {
		return seriesInstanceUID;
	}

	public void setSeriesInstanceUID(String seriesInstanceUID) {
		if(seriesInstanceUID != null) {
			seriesInstanceUID = seriesInstanceUID.trim();
		}
		this.seriesInstanceUID = seriesInstanceUID;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		if(entityId != null) {
			entityId = entityId.trim();
		}
		this.entityId = entityId;
	}

	public String getSeriesDescription() {
		return seriesDescription;
	}

	public void setSeriesDescription(String seriesDescription) {
		if(seriesDescription != null) {
			seriesDescription = seriesDescription.trim();
		}
		this.seriesDescription = seriesDescription;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		if(modality != null) {
			modality = modality.trim();
		}
		this.modality = modality;
	}

	public String getInstitutionalDepartmentName() {
		return institutionalDepartmentName;
	}

	public void setInstitutionalDepartmentName(
			String institutionalDepartmentName) {
		this.institutionalDepartmentName = institutionalDepartmentName;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getBodyPartExamined() {
		return bodyPartExamined;
	}

	public void setBodyPartExamined(String bodyPartExamined) {
		this.bodyPartExamined = bodyPartExamined;
	}

	public String getLaterality() {
		return laterality;
	}

	public void setLaterality(String laterality) {
		this.laterality = laterality;
	}

	public String getPerformedProcedureStepStartDate() {
		return performedProcedureStepStartDate;
	}

	public void setPerformedProcedureStepStartDate(
			String performedProcedureStepStartDate) {
		this.performedProcedureStepStartDate = performedProcedureStepStartDate;
	}

	public String getPerformedProcedureStepStartTime() {
		return performedProcedureStepStartTime;
	}

	public void setPerformedProcedureStepStartTime(
			String performedProcedureStepStartTime) {
		this.performedProcedureStepStartTime = performedProcedureStepStartTime;
	}

	public String getPerformedProcedureStepInstanceUID() {
		return performedProcedureStepInstanceUID;
	}

	public void setPerformedProcedureStepInstanceUID(
			String performedProcedureStepInstanceUID) {
		this.performedProcedureStepInstanceUID = performedProcedureStepInstanceUID;
	}

	public String getPerformedProcedureStepClassUID() {
		return performedProcedureStepClassUID;
	}

	public void setPerformedProcedureStepClassUID(
			String performedProcedureStepClassUID) {
		this.performedProcedureStepClassUID = performedProcedureStepClassUID;
	}

	public String getSeriesCustomAttribute1() {
		return seriesCustomAttribute1;
	}

	public void setSeriesCustomAttribute1(String seriesCustomAttribute1) {
		this.seriesCustomAttribute1 = seriesCustomAttribute1;
	}

	public String getSeriesCustomAttribute2() {
		return seriesCustomAttribute2;
	}

	public void setSeriesCustomAttribute2(String seriesCustomAttribute2) {
		this.seriesCustomAttribute2 = seriesCustomAttribute2;
	}

	public String getSeriesCustomAttribute3() {
		return seriesCustomAttribute3;
	}

	public void setSeriesCustomAttribute3(String seriesCustomAttribute3) {
		this.seriesCustomAttribute3 = seriesCustomAttribute3;
	}

	public int getNumberOfInstances() {
		return numberOfInstances;
	}

	public void setNumberOfInstances(int numberOfInstances) {
		this.numberOfInstances = numberOfInstances;
	}

	public int getNumberOfInstancesA() {
		return numberOfInstancesA;
	}

	public void setNumberOfInstancesA(int numberOfInstancesA) {
		this.numberOfInstancesA = numberOfInstancesA;
	}

	public String getPerformingPhysicianName() {
		return performingPhysicianName;
	}

	public void setPerformingPhysicianName(String performingPhysicianName) {
		this.performingPhysicianName = performingPhysicianName;
	}

	// public void setInstances(Collection<Instance> instances) {
	// this.instances = instances;
	// }

	public Study getStudy() {
		return study;
	}

	public String getSeriesKeyword() {
		return seriesKeyword;
	}

	public void setSeriesKeyword(String seriesKeyword) {
		if(seriesKeyword != null) {
			seriesKeyword = seriesKeyword.trim();
		}
		this.seriesKeyword = seriesKeyword;
	}

	public void setStudy(Study study) {
		this.study = study;
	}


	public String getSeriesDate() {
		return seriesDate;
	}

	public void setSeriesDate(String seriesDate) {
		if(seriesDate != null) {
			seriesDate = seriesDate.trim();
		}
		this.seriesDate = seriesDate;
	}

	public String getSeriesTime() {
		return seriesTime;
	}

	public void setSeriesTime(String seriesTime) {
		this.seriesTime = seriesTime;
	}

	public String getProtocolName() {
		return protocolName;
	}

	public void setProtocolName(String protocolName) {
		this.protocolName = protocolName;
	}

	public String getFrameOfReferenceUID() {
		return frameOfReferenceUID;
	}

	public void setFrameOfReferenceUID(String frameOfReferenceUID) {
		this.frameOfReferenceUID = frameOfReferenceUID;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	
}