package org.ha.cid.ias.entity.es;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "t_study", uniqueConstraints = {
	@UniqueConstraint(columnNames = { "study_iuid" }),
	@UniqueConstraint(columnNames = { "accession_no","study_id" }) })
@NamedQueries({
	@NamedQuery(name = Study.FIND_BY_ACCESSION_NO, query = " SELECT t FROM Study t WHERE t.accessionNo = :accessionNo "),
	@NamedQuery(name = Study.FIND_ACCESSION_NO_BY_UPDATED_TIME, query = " SELECT t.accessionNo FROM Study t WHERE t.updatedTime >= :fromDtm and t.updatedTime < :toDtm "),
	@NamedQuery(name = Study.FIND_BY_PATIENT_KEY, query = " SELECT t FROM Study t WHERE t.patient.patientKey = :patientKey ")
})
public class Study implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7672164936691540079L;

	public static final String STUDY_STATUS_MATCHED = "MATCHED";
	public static final String STUDY_STATUS_DELETED = "DELETED";
	
	public static final String FIND_BY_ACCESSION_NO = "Study.findByAccessionNo";
	public static final String FIND_ACCESSION_NO_BY_UPDATED_TIME = "Study.findAccessionNoByUpdatedTime";
	public static final String FIND_BY_PATIENT_KEY = "Study.findByPatientKey";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Version
	@Column(name = "version")
	private long version;

	@Column(name = "created_time", updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdTime;

	@Column(name = "updated_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedTime;

	/**
	 * 0020,000D StudyInstanceUID
	 */
	@Column(name = "study_iuid", nullable = false)
	private String studyInstanceUID;

	/**
	 * 0020,000D Orignal StudyInstanceUID
	 */
	@Column(name = "original_study_iuid")
	private String originalStudyInstanceUID;

	/**
	 * 0020,0010 StudyID
	 */
	@Column(name = "study_id")
	private String studyID;

	/**
	 * 0020,1000 SeriesInStudy
	 */
	@Column(name = "series_in_study")
	private String seriesInStudy;

	/**
	 * 0008,0020 StudyDate
	 */
	@Column(name = "study_date")
	private String studyDate;

	/**
	 * 0008,0030 StudyTime
	 */
	@Column(name = "study_time")
	private String studyTime;

	/**
	 * 0008,0050 AccessionNumber
	 */
	@Column(name = "accession_no")
	private String accessionNo;

	@Column(name = "case_no")
	private String caseNumber;

	/**
	 * 0008,1030 StudyDescription
	 */
	@Column(name = "study_desc")
	private String studyDescription;

	/**
	 * From the cid system.
	 */
	@Column(name = "remark")
	private String remark;
	/**
	 * From the cid system.
	 */
	@Column(name = "study_keyword")
	private String studyKeyword;
	/**
	 * From the cid system.
	 */
	@Column(name = "study_type")
	private String studyType;
	/**
	 * 0008,0080 InstitutionName
	 */
	@Column(name = "institution_name")
	private String institutionName;

	@Column(name = "hospital_code")
	private String hospitalCode;

	/**
	 * store the study update version, used to as StudyToken
	 */
	@Column(name = "study_custom1")
	private String studyCustomAttribute1;

	@Column(name = "study_custom2")
	private String studyCustomAttribute2;

	@Column(name = "study_custom3")
	private String studyCustomAttribute3;

	@Column(name = "access_control_id")
	private String accessControlID;

	@Column(name = "num_series")
	private int numberOfSeries = -1;

	@Column(name = "num_instances")
	private int numberOfInstances = -1;

	@Column(name = "examed_bodies_in_study")
	private String examedBodiesInStudy;

	@Column(name = "study_status")
	private String studyStatus;

	@Column(name = "study_storage_status")
	private String studyStorageStatus;

	/**
	 * 0032,000A StudyStatusID, 'MATCHED', 'DELETED'
	 */
	@Column(name = "study_status_id")
	private String studyStatusID;

	/**
	 * 0032,1030 ReasonForStudy
	 */
	@Column(name = "reason_for_study")
	private String reasonForStudy;

	/**
	 * 0008,0061 ModalitiesInStudy
	 */
	@Column(name = "mods_in_study")
	private String modalitiesInStudy;

	/**
	 * 0008,0090 ReferringPhysicianName
	 */
	@Column(name = "ref_phys_name")
	private String referringPhysicianName;

	/**
	 * 0008,1050 PerformingPhysicianName
	 */
	@Column(name = "per_phys_name")
	private String performingPhysicianName;

	/**
	 * 0008,1010 StationName
	 */
	@Column(name = "station_name")
	private String stationName;

	/**
	 * 0008,1040 InstitutionalDepartmentName
	 */
	@Column(name = "inst_depart_name")
	private String institutionalDepartmentName;

	@Embedded
	private Patient patient;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "study", orphanRemoval = true, cascade = { CascadeType.ALL })
	private List<Series> serieses = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getStudyInstanceUID() {
		return studyInstanceUID;
	}

	public void setStudyInstanceUID(String studyInstanceUID) {
		if(studyInstanceUID != null) {
			studyInstanceUID = studyInstanceUID.trim();
		}
		this.studyInstanceUID = studyInstanceUID;
	}

	public String getStudyID() {
		return studyID;
	}

	public void setStudyID(String studyID) {
		if(studyID != null) {
			studyID = studyID.trim();
		}
		this.studyID = studyID;
	}

	public String getStudyDate() {
		return studyDate;
	}

	public void setStudyDate(String studyDate) {
		if(studyDate != null) {
			studyDate = studyDate.trim();
		}
		this.studyDate = studyDate;
	}

	public String getStudyTime() {
		return studyTime;
	}

	public void setStudyTime(String studyTime) {
		this.studyTime = studyTime;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		if(accessionNo != null) {
			accessionNo = accessionNo.trim();
		}
		this.accessionNo = accessionNo;
	}

	public String getStudyDescription() {
		return studyDescription;
	}

	public void setStudyDescription(String studyDescription) {
		this.studyDescription = studyDescription;
	}

	public String getStudyCustomAttribute1() {
		return studyCustomAttribute1;
	}

	public void setStudyCustomAttribute1(String studyCustomAttribute1) {
		if(studyCustomAttribute1 != null) {
			studyCustomAttribute1 = studyCustomAttribute1.trim();
		}
		this.studyCustomAttribute1 = studyCustomAttribute1;
	}

	public String getStudyCustomAttribute2() {
		return studyCustomAttribute2;
	}

	public void setStudyCustomAttribute2(String studyCustomAttribute2) {
		this.studyCustomAttribute2 = studyCustomAttribute2;
	}

	public String getStudyCustomAttribute3() {
		return studyCustomAttribute3;
	}

	public void setStudyCustomAttribute3(String studyCustomAttribute3) {
		this.studyCustomAttribute3 = studyCustomAttribute3;
	}

	public String getAccessControlID() {
		return accessControlID;
	}

	public void setAccessControlID(String accessControlID) {
		this.accessControlID = accessControlID;
	}

	public int getNumberOfSeries() {
		return numberOfSeries;
	}

	public void setNumberOfSeries(int numberOfSeries) {
		this.numberOfSeries = numberOfSeries;
	}

	public int getNumberOfInstances() {
		return numberOfInstances;
	}

	public void setNumberOfInstances(int numberOfInstances) {
		this.numberOfInstances = numberOfInstances;
	}

	public String getModalitiesInStudy() {
		return modalitiesInStudy;
	}

	public void setModalitiesInStudy(String modalitiesInStudy) {
		this.modalitiesInStudy = modalitiesInStudy;
	}

	public String getStudyStorageStatus() {
		return studyStorageStatus;
	}

	public void setStudyStorageStatus(String studyStorageStatus) {
		this.studyStorageStatus = studyStorageStatus;
	}

	public String getReferringPhysicianName() {
		return referringPhysicianName;
	}

	public void setReferringPhysicianName(String referringPhysicianName) {
		this.referringPhysicianName = referringPhysicianName;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getStudyStatus() {
		return studyStatus;
	}

	public void setStudyStatus(String studyStatus) {
		this.studyStatus = studyStatus;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public String getExamedBodiesInStudy() {
		return examedBodiesInStudy;
	}

	public void setExamedBodiesInStudy(String examedBodiesInStudy) {
		this.examedBodiesInStudy = examedBodiesInStudy;
	}

	public void setInstitutionName(String institutionName) {
		if(institutionName != null) {
			institutionName = institutionName.trim();
		}
		this.institutionName = institutionName;
	}

	public String getStudyKeyword() {
		return studyKeyword;
	}

	public void setStudyKeyword(String studyKeyword) {
		if(studyKeyword != null) {
			studyKeyword = studyKeyword.trim();
		}
		this.studyKeyword = studyKeyword;
	}

	public String getStudyStatusID() {
		return studyStatusID;
	}

	public void setStudyStatusID(String studyStatusID) {
		this.studyStatusID = studyStatusID;
	}

	public String getSeriesInStudy() {
		return seriesInStudy;
	}

	public void setSeriesInStudy(String seriesInStudy) {
		this.seriesInStudy = seriesInStudy;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		if(caseNumber != null) {
			caseNumber = caseNumber.trim();
		}
		this.caseNumber = caseNumber;
	}

	public String getReasonForStudy() {
		return reasonForStudy;
	}

	public void setReasonForStudy(String reasonForStudy) {
		this.reasonForStudy = reasonForStudy;
	}

	public String getPerformingPhysicianName() {
		return performingPhysicianName;
	}

	public void setPerformingPhysicianName(String performingPhysicianName) {
		this.performingPhysicianName = performingPhysicianName;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getInstitutionalDepartmentName() {
		return institutionalDepartmentName;
	}

	public void setInstitutionalDepartmentName(
			String institutionalDepartmentName) {
		if(institutionalDepartmentName != null) {
			institutionalDepartmentName = institutionalDepartmentName.trim();
		}
		this.institutionalDepartmentName = institutionalDepartmentName;
	}

	public String getOriginalStudyInstanceUID() {
		return originalStudyInstanceUID;
	}

	public void setOriginalStudyInstanceUID(String originalStudyInstanceUID) {
		this.originalStudyInstanceUID = originalStudyInstanceUID;
	}

	public List<Series> getSerieses() {
		return serieses;
	}

	public void setSerieses(ArrayList<Series> serieses) {
		this.serieses = serieses;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark != null) {
			remark = remark.trim();
		}
		this.remark = remark;
	}

	public String getStudyType() {
		return studyType;
	}

	public void setStudyType(String studyType) {
		if(studyType != null) {
			studyType = studyType.trim();
		}
		this.studyType = studyType;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		if(hospitalCode != null) {
			hospitalCode = hospitalCode.trim();
		}
		this.hospitalCode = hospitalCode;
	}
	
}