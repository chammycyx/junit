/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;


/**
 * The enum which defines the processing status of PAS-ESB message.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9633 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:03:10 +0800 (週三, 15 六月 2016) $
 * </pre>
 */
public enum MessageProcessStatus {

	/**
	 * A duplicate message is received.
	 */
	DUPLICATE("D"),

	/**
	 * A message is waiting for its processing in the staging queue.
	 */
	INITIAL("A"),

	/**
	 * A message is processed with an exception but it is not a duplicate message; disordered.
	 */
	OTHERS("E"),

	/**
	 * A message is being processed.
	 */
	PROCESSING("I"),

	/**
	 * A message is processed successfully without any exception.
	 */
	SUCCESS("C");

	private String _name;

	private MessageProcessStatus(String name) {
		this._name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this._name;
	}

}
