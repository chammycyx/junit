package org.ha.cid.ias.entity.es;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(name = "t_image", uniqueConstraints = @UniqueConstraint(columnNames = {
		"sop_iuid", "fk_series" }))
public class Image implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3919041023928257454L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Version
	@Column(name = "version")
	private long version;

	@Column(name = "created_time", updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdTime;

	@Column(name = "updated_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedTime;

	// The following is for the DICOM image
	/**
	 * 0002,0010 TransferSyntaxUID
	 */
	@Column(name = "transfer_syntax_uid")
	private String transferSyntaxUID;

	/**
	 * 0008,0016 SOPClassUID
	 */
	@Column(name = "sop_cuid", updatable = false)
	private String sopClassUID;

	/**
	 * 0008,0018 SOPInstanceUID
	 */
	@Column(name = "sop_iuid", updatable = false)
	private String sopInstanceUID;

	/**
	 * 0020,0013 InstanceNumber
	 */
	@Column(name = "inst_no")
	private String instanceNumber;

	/**
	 * 0008,0008 ImageType
	 */
	// for the dicom file,this attribute is from the
	// 0008,0008(ORIGINAL/DERIVED). For the CID images, the default is JPEG
	@Column(name = "image_type")
	private String imageType;

	/**
	 * 0028,0008 NumberOfFrames
	 */
	@Column(name = "number_of_frames")
	private String numberOfFrames;

	/**
	 * 0028,0010 Rows
	 */
	@Column(name = "rows")
	private int rows;

	/**
	 * 0028,0011 Columns
	 */
	@Column(name = "columns")
	private int columns;

	/**
	 * 0028,0100 BitsAllocated
	 */
	@Column(name = "bits_allocated")
	private int bitsAllocated;

	/**
	 * 0008,0023 ContentDate
	 */
	@Column(name = "content_date")
	private String contentDate;

	/**
	 * 0008,0033 ContentTime
	 */
	@Column(name = "content_time")
	private String contentTime;

	@Column(name = "image_custom1")
	private String imageCustomAttribute1;

	@Basic(optional = false)
	@Column(name = "image_custom2")
	private String imageCustomAttribute2;

	@Column(name = "image_custom3")
	private String imageCustomAttribute3;

	@Column(name = "replaced")
	private boolean replaced = false;

	@Column(name = "archived")
	private boolean archived;

	// @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true, cascade = {
	// CascadeType.ALL })
	// @JoinColumn(name = "fk_file_ref")
	// private FileRef fileRef;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_series")
	private Series series;

	// the following attributes is for CID
	@Column(name = "image_format")
	private String imageFormat;
	@Column(name = "image_path")
	private String imagePath;
	@Column(name = "image_file")
	private String imageFile;
	@Column(name = "image_id")
	private String imageID;
	@Column(name = "image_version")
	private String imageVersion;
	@Column(name = "image_sequence")
	private int imageSequence;
	@Column(name = "image_keyword")
	private String imageKeyword;
	@Column(name = "image_handling")
	private String imageHandling;
	@Column(name = "image_status")
	private int imageStatus;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "image", orphanRemoval = true, cascade = { CascadeType.ALL })
	@OrderBy("annotationSeq ASC")
	private List<Annotation> annotations = new ArrayList<>();

	@PrePersist
	public void onPrePersist() {
		Date now = new Date();
		createdTime = now;
		updatedTime = now;
	}

	@PreUpdate
	protected void onUpdate() {
		updatedTime = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getSopInstanceUID() {
		return sopInstanceUID;
	}

	public void setSopInstanceUID(String sopInstanceUID) {
		this.sopInstanceUID = sopInstanceUID;
	}

	public String getSopClassUID() {
		return sopClassUID;
	}

	public void setSopClassUID(String sopClassUID) {
		this.sopClassUID = sopClassUID;
	}

	public String getInstanceNumber() {
		return instanceNumber;
	}

	public void setInstanceNumber(String instanceNumber) {
		this.instanceNumber = instanceNumber;
	}

	public String getContentDate() {
		return contentDate;
	}

	public void setContentDate(String contentDate) {
		this.contentDate = contentDate;
	}

	public String getContentTime() {
		return contentTime;
	}

	public void setContentTime(String contentTime) {
		this.contentTime = contentTime;
	}

	public String getTransferSyntaxUID() {
		return transferSyntaxUID;
	}

	public void setTransferSyntaxUID(String transferSyntaxUID) {
		this.transferSyntaxUID = transferSyntaxUID;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		if(imageType != null) {
			imageType = imageType.trim();
		}
		this.imageType = imageType;
	}

	public String getNumberOfFrames() {
		return numberOfFrames;
	}

	public void setNumberOfFrames(String numberOfFrames) {
		this.numberOfFrames = numberOfFrames;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getBitsAllocated() {
		return bitsAllocated;
	}

	public void setBitsAllocated(int bitsAllocated) {
		this.bitsAllocated = bitsAllocated;
	}

	public String getImageCustomAttribute1() {
		return imageCustomAttribute1;
	}

	public void setImageCustomAttribute1(String imageCustomAttribute1) {
		this.imageCustomAttribute1 = imageCustomAttribute1;
	}

	public String getImageCustomAttribute2() {
		return imageCustomAttribute2;
	}

	public void setImageCustomAttribute2(String imageCustomAttribute2) {
		this.imageCustomAttribute2 = imageCustomAttribute2;
	}

	public String getImageCustomAttribute3() {
		return imageCustomAttribute3;
	}

	public void setImageCustomAttribute3(String imageCustomAttribute3) {
		this.imageCustomAttribute3 = imageCustomAttribute3;
	}

	public String getImageFormat() {
		return imageFormat;
	}

	public void setImageFormat(String imageFormat) {
		if(imageFormat != null) {
			imageFormat = imageFormat.trim();
		}
		this.imageFormat = imageFormat;
	}

	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		if(imageFile != null) {
			imageFile = imageFile.trim();
		}
		this.imageFile = imageFile;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		if(imageID != null) {
			imageID = imageID.trim();
		}
		this.imageID = imageID;
	}

	public String getImageVersion() {
		return imageVersion;
	}

	public void setImageVersion(String imageVersion) {
		if(imageVersion != null) {
			imageVersion = imageVersion.trim();
		}
		this.imageVersion = imageVersion;
	}

	public int getImageSequence() {
		return imageSequence;
	}

	public void setImageSequence(int imageSequence) {
		this.imageSequence = imageSequence;
	}

	public String getImageKeyword() {
		return imageKeyword;
	}

	public void setImageKeyword(String imageKeyword) {
		if(imageKeyword != null) {
			imageKeyword = imageKeyword.trim();
		}
		this.imageKeyword = imageKeyword;
	}

	public String getImageHandling() {
		return imageHandling;
	}

	public void setImageHandling(String imageHandling) {
		if(imageHandling != null) {
			imageHandling = imageHandling.trim();
		}
		this.imageHandling = imageHandling;
	}

	public int getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(int imageStatus) {
		this.imageStatus = imageStatus;
	}

	public boolean isReplaced() {
		return replaced;
	}

	public void setReplaced(boolean replaced) {
		this.replaced = replaced;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	// public FileRef getFileRef() {
	// return fileRef;
	// }
	//
	// public void setFileRef(FileRef fileRef) {
	// this.fileRef = fileRef;
	// }

	public Series getSeries() {
		return series;
	}

	public void setSeries(Series series) {
		this.series = series;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		if(imagePath != null) {
			imagePath = imagePath.trim();
		}
		this.imagePath = imagePath;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

}
