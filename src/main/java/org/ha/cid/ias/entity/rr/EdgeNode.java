package org.ha.cid.ias.entity.rr;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "TGC_EDGENODE_LIST", uniqueConstraints = @UniqueConstraint(columnNames = {"NODEID"}))
@NamedQueries({
    @NamedQuery(name = "EdgeNode.queryByNodeID",
            query = "SELECT t FROM EdgeNode t WHERE t.nodeid = :nodeid")
})
public class EdgeNode implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 380212761148583836L;

	@Id
    @SequenceGenerator(name = "SQ_NODENO", sequenceName = "SQ_NODENO",allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_NODENO")
    @Column(name = "NODENO")
    private Long id;
	
    @Column(name = "NODEID")
    private String nodeid;
    
    @OneToMany(mappedBy = "gridEdgeNode")
    private Set<PathIndex> gridPathIndexList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNodeid() {
		return nodeid;
	}

	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}

	public Set<PathIndex> getGridPathIndexList() {
		return gridPathIndexList;
	}

	public void setGridPathIndexList(Set<PathIndex> gridPathIndexList) {
		this.gridPathIndexList = gridPathIndexList;
	}
	
	@Override
	public String toString() {
		return "GridEdgeNodeList [id=" + id + ", nodeid=" + nodeid
				+ ", gridPathIndexList=" + gridPathIndexList + "]";
	}
}
