/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.entity.esb;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.LockModeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.ha.cid.ias.model.StagingQuery;
import org.ha.cid.ias.persistence.converter.MessageProcessStatusConverter;


/**
 * The entity class is used for persisting a PAS-ESB message.
 *
 * <pre>
 * Last commit:-
 * $Rev: 11885 $
 * $Author: ymm698 $
 * $Date: 2017-07-24 15:18:25 +0800 (週一, 24 七月 2017) $
 * </pre>
 *
 */
@Entity
@Table(name = "t_esb_staging")
@NamedQueries({
	@NamedQuery(
		name = StagingQuery.NAMED_QUERY_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID,
		query = StagingQuery.JPQL_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID
	),
	@NamedQuery(
		name = StagingQuery.NAMED_QUERY_FIND_BY_SUBSCRIPTION_ID,
		query = StagingQuery.JPQL_FIND_BY_SUBSCRIPTION_ID,
		lockMode = LockModeType.PESSIMISTIC_WRITE // pessimistic lock
	),
	@NamedQuery(
		name = StagingQuery.NAMED_QUERY_FIND_PRECEDING_DUPLICATE_MESSAGE_TRANSACTION_ID,
		query = StagingQuery.JPQL_FIND_PRECEDING_DUPLICATE_BY_MESSAGE_TRANSACTION_ID
	),
	@NamedQuery(
		name = StagingQuery.NAMED_QUERY_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT,
		query = StagingQuery.JPQL_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER,
			query = StagingQuery.JPQL_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_FIND_STAGING_AFTER_ORDERING_IDENTIFIER,
			query = StagingQuery.JPQL_FIND_STAGING_AFTER_ORDERING_IDENTIFIER
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_FIND_STAGING_BY_TXN_ID,
			query = StagingQuery.JPQL_FIND_STAGING_BY_TXN_ID
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_DELETE_STAGING_BY_STATUS,
			query = StagingQuery.JPQL_DELETE_STAGING_BY_STATUS
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_UPDATE_STAGING_TO_ERR_BY_STATUS,
			query = StagingQuery.JPQL_UPDATE_STAGING_TO_ERR_BY_STATUS
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_UPDATE_STAGING_RECEIVED_DT,
			query = StagingQuery.JPQL_UPDATE_STAGING_RECEIVED_DT
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_FIND_STAGING_BY_ORDERING_IDENTIFIER,
			query = StagingQuery.JPQL_FIND_STAGING_BY_ORDERING_IDENTIFIER
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_DELETE_STAGING_BY_MSG_ID_RANGE,
			query = StagingQuery.JPQL_DELETE_STAGING_BY_MSG_ID_RANGE
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_FIND_MINIMUM_ORDERING_IDENTIFIER,
			query = StagingQuery.JPQL_FIND_MINIMUM_ORDERING_IDENTIFIER
	),
	@NamedQuery(
			name = StagingQuery.NAMED_QUERY_UPDATE_PROCESS_STATUS,
			query = StagingQuery.JPQL_UPDATE_PROCESS_STATUS
	)
	
})
public class Staging implements Serializable {

	private static final long serialVersionUID = 3967889199765707096L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "staging_message_id")
	private long stagingMessageId;

	@Version
	@Column(name = "version")
	private long version;

	@Column(name = "message_transaction_id", unique = true)
	private String messageTransactionId;

	@Column(name = "message_code")
	private String messageCode;

	@Column(name = "message_dt_string")
	private String messageDtString;

	@Column(name = "message_payload")
	private String messagePayload;

	@Column(name = "process_status")
	@Convert(converter = MessageProcessStatusConverter.class)
	private MessageProcessStatus processStatus;

	@Temporal(TemporalType.TIMESTAMP) // injected because of java.util.Date
	@Column(name = "received_dt")
	private Date receivedDt; // TODO Change to LocalDate

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "staging_start_dt")
	private Date stagingStartDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "staging_end_dt")
	private Date stagingEndDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "acknowledged_dt")
	private Date acknowledgedDt;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "staging", orphanRemoval=true)
	private Set<Subscription> subscriptions;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ordering_identifier")
	private Date orderingIdentifier;

	/**
	 * Add a {@link Subscription}.
	 *
	 * @param subscription the {@link Subscription} to add
	 */
	public void addSubscription(Subscription subscription) {

		subscription.setStaging(this);
		this.subscriptions.add(subscription);
	}

	/**
	 * @return a {@link Set} of {@link Subscription}
	 */
	public Set<Subscription> getSubscriptions() {
		return this.subscriptions;
	}
	
	/**
	 * @param subscriptions a {@link Set} of {@link Subscription} to set
	 */
	public void setSubscriptions(Set<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	/**
	 * @return the value of value of <tt>staging_message_id</tt>
	 */
	public long getStagingMessageId() {
		return stagingMessageId;
	}

	/**
	 * @param stagingMessageId the <tt>staging_message_id</tt> to set
	 */
	public void setStagingMessageId(long stagingMessageId) {
		this.stagingMessageId = stagingMessageId;
	}

	/**
	 * @return the value of <tt>version</tt>
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the <tt>version</tt> to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the value of <tt>message_transaction_id</tt>
	 */
	public String getMessageTransactionId() {
		return messageTransactionId;
	}

	/**
	 * @param messageTransactionId the <tt>message_transaction_id</tt> to set
	 */
	public void setMessageTransactionId(String messageTransactionId) {
		this.messageTransactionId = messageTransactionId;
	}

	/**
	 * @return the value of <tt>message_code</tt>
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * @param messageCode the <tt>message_code</tt> to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	/**
	 * @return the value of <tt>message_dt_string</tt>
	 */
	public String getMessageDtString() {
		return messageDtString;
	}

	/**
	 * @param messageDtString the <tt>message_dt_string</tt> to set
	 */
	public void setMessageDtString(String messageDtString) {
		this.messageDtString = messageDtString;
	}

	/**
	 * @return the value of <tt>message_payload</tt>
	 */
	public String getMessagePayload() {
		return messagePayload.trim();
	}

	/**
	 * @param messagePayload the <tt>message_payload</tt> to set
	 */
	public void setMessagePayload(String messagePayload) {
		this.messagePayload = messagePayload;
	}

	/**
	 * @return the value of <tt>process_status</tt> in <code>enum</code>
	 */
	public MessageProcessStatus getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus the <tt>process_status</tt> to set
	 */
	public void setProcessStatus(MessageProcessStatus processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * @return the value of <tt>received_dt</tt>
	 */
	public Date getReceivedDt() {
		return getDate(receivedDt);
	}

	/**
	 * @param receivedDt the <tt>received_dt</tt> to set
	 */
	public void setReceivedDt(Date receivedDt) {
		this.receivedDt = getDate(receivedDt);
	}

	/**
	 * @return the value of <tt>staging_start_dt</tt>
	 */
	public Date getStagingStartDt() {
		return getDate(stagingStartDt);
	}

	/**
	 * @param stagingStartDt the <tt>staging_start_dt</tt> to set
	 */
	public void setStagingStartDt(Date stagingStartDt) {
		this.stagingStartDt = getDate(stagingStartDt);
	}

	/**
	 * @return the value of <tt>staging_end_dt</tt>
	 */
	public Date getStagingEndDt() {
		return getDate(stagingEndDt);
	}

	/**
	 * @param stagingEndDt the <tt>staging_end_dt</tt> to set
	 */
	public void setStagingEndDt(Date stagingEndDt) {
		this.stagingEndDt = getDate(stagingEndDt);
	}

	/**
	 * @return the value of <tt>acknowledged_dt</tt>
	 */
	public Date getAcknowledgedDt() {
		return getDate(acknowledgedDt);
	}

	/**
	 * @param acknowledgedDt the <tt>acknowledged_dt</tt> to set
	 */
	public void setAcknowledgedDt(Date acknowledgedDt) {
		this.acknowledgedDt = getDate(acknowledgedDt);
	}

	/**
	 * @return the value of <tt>ordering_identifier</tt>
	 */
	public Date getOrderingIdentifier() {
		return orderingIdentifier;
	}

	/**
	 * @param orderingIdentifier the <tt>ordering_identifier</tt> to set
	 */
	public void setOrderingIdentifier(Date orderingIdentifier) {
		this.orderingIdentifier = orderingIdentifier;
	}
	
	private Date getDate(Date value){
		return value==null?null:new Date(value.getTime());
	}

}
