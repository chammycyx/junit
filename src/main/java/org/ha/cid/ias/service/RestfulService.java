package org.ha.cid.ias.service;

public interface RestfulService {
	
	public String getRsResponse(String jsonString, String endpointURL) throws Exception; 
	public void changeCredentials(String username, String password);
}

