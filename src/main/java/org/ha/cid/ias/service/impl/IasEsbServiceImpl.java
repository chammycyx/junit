package org.ha.cid.ias.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.ha.cd2.isg.esb.client.receiveEsbHl7.ReceiveEsbHl7Client;
import org.ha.cid.ias.dao.esb.ConfigurationDao;
import org.ha.cid.ias.dao.esb.ConfigurationKeys;
import org.ha.cid.ias.dao.esb.PatientUpdateHistoryDao;
import org.ha.cid.ias.dao.esb.StagingDao;
import org.ha.cid.ias.entity.esb.Configuration;
import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.entity.esb.PatientUpdateHistory;
import org.ha.cid.ias.entity.esb.Staging;
import org.ha.cid.ias.enumeration.EsbMessageType;
import org.ha.cid.ias.model.EsbEpisode;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.EsbPatientDemographics;
import org.ha.cid.ias.model.StagingDto;
import org.ha.cid.ias.service.IasEsbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import esb.wsdl.xjc.pojo.ReceiveEsbHl7;
import esb.wsdl.xjc.pojo.ReceiveEsbHl7Response;

public class IasEsbServiceImpl implements IasEsbService {
	
	final static public String A08_TEMPLATE = "src/test/resources/wsclient/ias/esb/hl7/A08Template.xml";
	final static public String A40_TEMPLATE = "src/test/resources/wsclient/ias/esb/hl7/A40Template.xml";
	final static public String A45_TEMPLATE = "src/test/resources/wsclient/ias/esb/hl7/A45Template.xml";
	final static public String A47_TEMPLATE = "src/test/resources/wsclient/ias/esb/hl7/A47Template.xml";
	final static public String ESB_HL7_TAG_TXN_ID = "${messageTransactionId}";
	final static public String ESB_HL7_TAG_EVN2 = "${evn.2}";
	final static public String ESB_HL7_TAG_EVN6 = "${evn.6}";
	final static public String ESB_HL7_TAG_TO_PAT_KEY = "${toPatientKey}";
	final static public String ESB_HL7_TAG_FROM_PAT_KEY = "${fromPatientKey}";
	final static public String ESB_HL7_TAG_PAT_NAME = "${toPatientName}";
	final static public String ESB_HL7_TAG_PAT_DOB = "${toPatientDob}";
	final static public String ESB_HL7_TAG_PAT_HKID = "${toPatientHkid}";
	final static public String ESB_HL7_TAG_PAT_SEX = "${toPatientSex}";
	final static public String ESB_HL7_TAG_PAT_CaseNum = "${fromPatientCaseNum}";
	final static public String ESB_HL7_TAG_PAT_HOSP_CODE = "${fromPatientHospCode}";
	
	@Autowired
	private ReceiveEsbHl7Client receiveEsbHl7Client;
	
	@Autowired
	private ConfigurationDao configurationDao;
	
	@Autowired
	private StagingDao stagingDao;
	
	@Autowired
	private PatientUpdateHistoryDao patientUpdateHistoryDao;
	
	
	public ReceiveEsbHl7Client getReceiveEsbHl7Client() {
		return receiveEsbHl7Client;
	}

	public void setReceiveEsbHl7Client(ReceiveEsbHl7Client receiveEsbHl7Client) {
		this.receiveEsbHl7Client = receiveEsbHl7Client;
	}

	@Override
	public int sendEsbMsg(EsbMessageHeader esbMessageHeader,EsbMessagePatient esbMessagePatient, EsbEpisode esbEpisode) {
		ReceiveEsbHl7 receiveEsbHl7 = new ReceiveEsbHl7();
			receiveEsbHl7.setEsbHl7(createEsbHl7(esbMessageHeader, esbMessagePatient, esbEpisode));
		ReceiveEsbHl7Response receiveEsbHl7Response = receiveEsbHl7Client.receiveEsbHl7(receiveEsbHl7);
		
		return receiveEsbHl7Response.getReceiveEsbHl7Result();
	}
	
	public String createEsbHl7(EsbMessageHeader esbMessageHeader,EsbMessagePatient esbMessagePatient, EsbEpisode esbEpisode){
		String esbHl7 = "";
		if(esbMessageHeader.getMessageType().equals(EsbMessageType.A08)){
			try {
				esbHl7 = FileUtils.readFileToString(new File(A08_TEMPLATE));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(esbMessageHeader.getMessageType().equals(EsbMessageType.A40)){
			try {
				esbHl7 = FileUtils.readFileToString(new File(A40_TEMPLATE));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(esbMessageHeader.getMessageType().equals(EsbMessageType.A45)){
			try {
				esbHl7 = FileUtils.readFileToString(new File(A45_TEMPLATE));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else if(esbMessageHeader.getMessageType().equals(EsbMessageType.A47)){
			try {
				esbHl7 = FileUtils.readFileToString(new File(A47_TEMPLATE));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		EsbPatientDemographics toPatient = esbMessagePatient.getToPatient();
		EsbPatientDemographics fromPatient = esbMessagePatient.getFromPatient();
		
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_TXN_ID, esbMessageHeader.getTransactionId());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_EVN2, esbMessageHeader.getEvn2Dt());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_TO_PAT_KEY, toPatient.getPatientKey());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_NAME, toPatient.getPatientName());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_DOB, toPatient.getDob());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_HKID, toPatient.getHkid());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_SEX, toPatient.getSex());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_FROM_PAT_KEY, fromPatient==null?"":fromPatient.getPatientKey());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_CaseNum, esbEpisode==null?"":esbEpisode.getCaseNumber());
		esbHl7 = esbHl7.replace(ESB_HL7_TAG_PAT_HOSP_CODE, esbEpisode==null?"":esbEpisode.getHospitalCode());
		
		return esbHl7;
	}

	@Override
	public int sendUnknowTypeEsbMsg() throws IOException {
//		final String A08_PAS_FILE = "src/test/resources/hl7/uncertain.xml";
		final String A08_PAS_FILE = "src/test/resources/hl7/A08.xml";
//		try {
//			JAXBContext context = JAXBContext.newInstance(ReceiveEsbHl7.class, ReceiveEsbHl7Response.class);
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		ReceiveEsbHl7 receiveEsbHl7 = new ReceiveEsbHl7();
			receiveEsbHl7.setEsbHl7(FileUtils.readFileToString(new File(A08_PAS_FILE)));
		ReceiveEsbHl7Response receiveEsbHl7Response = receiveEsbHl7Client.receiveEsbHl7(receiveEsbHl7);
		
		return receiveEsbHl7Response.getReceiveEsbHl7Result();
	}

	@Transactional
	@Override
	public void enableDuplicationChecker(String value) {
		String key = ConfigurationKeys.DUPLICATION_CHECKER_ENABLE;
		setConfiguration(key, value);
	}

	@Transactional
	@Override
	public void enableDisorderChecker(String value) {
		String key = ConfigurationKeys.DISORDER_CHECKER_ENABLE;
		setConfiguration(key, value);
	}

	@Transactional
	@Override
	public void enableStagingPoller(String value) {
		String key = ConfigurationKeys.STAGING_POLLER_ENABLE;
		setConfiguration(key, value);
	}

	@Transactional
	@Override
	public void setDuplicationDayback(String value) {
		String key = ConfigurationKeys.DUPLICATION_DAYBACK;
		setConfiguration(key, value);
	}

	@Transactional
	@Override
	public void setCutoffTime(String value) {
		String key = ConfigurationKeys.CUTOFF_TIME;
		setConfiguration(key, value);
	}
	
	private void setConfiguration(String key, String value){
		Configuration configuration = configurationDao.findByKey(key);
		if(configuration==null){
			configuration = new Configuration();
			configuration.setKey(key);
			configuration.setValue(value);
			configurationDao.create(configuration);
		}else{
			configuration.setValue(value);
			configurationDao.update(configuration);
		}
	}

	@Transactional
	@Override
	public boolean isStagingExists(String txnId) {
		List<Staging> stagings = stagingDao.findByTxnId(txnId);
		return stagings.size()>0;
	}

	@Transactional
	@Override
	public StagingDto findStaging(String txnId) {
		List<Staging> stagings = stagingDao.findByTxnId(txnId);
		return stagings.size()==0?null:StagingDto.buildFrom(stagings.get(0));
	}

	@Transactional
	@Override
	public void removeStagingWithStatus(List<MessageProcessStatus> status) {
		stagingDao.deleteByStatus(status);
	}

	@Transactional
	@Override
	public void markStagingToErrWithStatus(List<MessageProcessStatus> status) {
		stagingDao.markStagingToErrWithStatus(status);
	}

	@Transactional
	@Override
	public long findNumStagingWithStatus(List<MessageProcessStatus> status) {
		return stagingDao.findNumStagingWithStatus(status);
	}

	@Transactional
	@Override
	public List<StagingDto> findStagingOrderByReceivedDtAsc(String txnId) {
		List<Staging> stagings = stagingDao.findByTxnId(txnId);
		
		Collections.sort(stagings, new Comparator<Staging>() {
		    @Override
		    public int compare(Staging o1, Staging o2) {
		        return o1.getReceivedDt().compareTo(o2.getReceivedDt());
		    }
		});
		
		List<StagingDto> stagingDtos = new ArrayList<>();
		for(Staging staging:stagings){
			stagingDtos.add(StagingDto.buildFrom(staging));
		}
		
		return stagingDtos;
	}

	@Transactional
	@Override
	public void setReceivedDt(long stagingMessageId, Date time) {
		stagingDao.setReceivedDt(stagingMessageId, time);
	}

	@Transactional
	@Override
	public StagingDto findStagingByMsgId(long stagingMessageId) {
		return StagingDto.buildFrom(stagingDao.find(stagingMessageId));
	}

	@Transactional
	@Override
	public List<StagingDto> findStagingByOrderingIdentifierSortByReceivedDtAsc(Date time) {
		List<Staging> stagings = stagingDao.findStagingByOrderingIdentifier(time);
		
		Collections.sort(stagings, new Comparator<Staging>() {
		    @Override
		    public int compare(Staging o1, Staging o2) {
		        return o1.getReceivedDt().compareTo(o2.getReceivedDt());
		    }
		});
		
		List<StagingDto> stagingDtos = new ArrayList<>();
		for(Staging staging:stagings){
			stagingDtos.add(StagingDto.buildFrom(staging));
		}
		
		return stagingDtos;
	}

	@Transactional
	@Override
	public void removeStagingByMsgId(long msgId) {
		Staging staging = stagingDao.find(msgId);
		if(staging!=null){
			stagingDao.delete(staging);
		}
	}

	@Transactional
	@Override
	public void removeStagingByMsgIdRange(long fromMsgId, long toMsgId) {
		stagingDao.removeStagingByMsgIdRange(fromMsgId, toMsgId);
	}

	@Transactional
	@Override
	public Date findMinimumOrderingIdentifier() {
		return stagingDao.findMinimumOrderingIdentifier();
	}

	@Transactional
	@Override
	public void setOrderingIdentifierNull(Date orderingIdentifier) {
		stagingDao.setOrderingIdentifierNull(orderingIdentifier);
	}

	@Transactional
	@Override
	public void enableNullOrderingIdentifierStagingPolling(String value) {
		String key = ConfigurationKeys.NULL_ORDERING_IDENTIFIER_STAGING_POLLING_ENABLE;
		setConfiguration(key, value);
	}

	@Transactional
	@Override
	public void createPatient(String testingFromPatientKey, String testingToPatientKey) {
		Long fromPatientKey = Long.valueOf(testingFromPatientKey);
		Long toPatientKey = Long.valueOf(testingToPatientKey);
		Calendar now = Calendar.getInstance();
		
		for(Long i=fromPatientKey;i<=toPatientKey;i++){
			PatientUpdateHistory patientUpdateHistory = new PatientUpdateHistory();
				patientUpdateHistory.setPatientKey(i.toString());
				patientUpdateHistory.setLastEsbDt(now.getTime());
			try{
				patientUpdateHistoryDao.create(patientUpdateHistory);
			}catch(Exception ex){
				//by-pass the exception during creating patient entity
			}
		}
		
	}

	@Transactional
	@Override
	public void setStagingStatus(Date evn2,MessageProcessStatus messageProcessStatus) {
		stagingDao.setStagingStatus(evn2, messageProcessStatus);
	}

	@Transactional
	@Override
	public StagingDto findStagingByOrderingIdentifier(Date evn2) {
		List<Staging> stagings = stagingDao.findStagingByOrderingIdentifier(evn2);
		return stagings.size()==0?null:StagingDto.buildFrom(stagings.get(0));
	}


}
