package org.ha.cid.ias.service.impl;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.zip.DataFormatException;
import java.util.zip.ZipException;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.ha.cd2.isg.icw.tool.ImageUtil;
import org.ha.cid.ias.service.ResourceService;

import de.idyl.winzipaes.AesZipFileDecrypter;
import de.idyl.winzipaes.impl.AESDecrypterBC;
import de.idyl.winzipaes.impl.ExtZipEntry;

public class ResourceServiceImpl implements ResourceService {

	private static final String FILE_FORMAT_JPG = "jpg";
	
	@Override
	public String loadFile(String filePath) throws Exception {
		InputStream is = getClass().getResourceAsStream(filePath);
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) {
			throw new Exception("Failed to load file: " + filePath, e);
		} finally {
			if (is != null) {
				IOUtils.closeQuietly(is);
			}
			if (br != null) {
				IOUtils.closeQuietly(br);
			}
		}
		
		return sb.toString();
	}
	
	@Override
	public byte[] loadFileAsByteArray(String filePath) throws Exception {
		InputStream is = getClass().getResourceAsStream(filePath);
		byte[] bytes = null;
		
		try {
			bytes = IOUtils.toByteArray(is);
		} catch (Exception e) {
			throw new Exception("Failed to load file: " + filePath, e);
		} finally {
			if (is != null) {
				IOUtils.closeQuietly(is);
			}
		}
		
		return bytes;
	}

	@Override
	public void createTempImage(String tempFile, String templateFile, String text) throws Exception {
		InputStream input = getClass().getResourceAsStream(templateFile);
		try {
			BufferedImage image = ImageUtil.appendText(IOUtils.toByteArray(input), text);
			URL url = this.getClass().getResource(tempFile);
			ImageIO.write(image, FILE_FORMAT_JPG, new File(url.toURI()));
			input.close();
		} catch (IOException e) {
			throw new Exception(e);
		} catch (URISyntaxException e) {
			throw new Exception(e);
		}
	}

	@Override
	public void createTempImage(String tempFile, String templateFile, int version) throws Exception {
		InputStream input = getClass().getResourceAsStream(templateFile);
		try {
			BufferedImage image = ImageUtil.appendVersion(IOUtils.toByteArray(input), version);
			URL url = this.getClass().getResource(tempFile);
			ImageIO.write(image, FILE_FORMAT_JPG, new File(url.toURI()));
			input.close();
		} catch (IOException e) {
			throw new Exception(e);
		} catch (URISyntaxException e) {
			throw new Exception(e);
		}
	}
	
	public byte[] extractZipFile(String zipFilePath, byte[] data, String filename, String password) throws Exception {
		URL url = getClass().getResource(zipFilePath);
		FileOutputStream fos;
		File file = null;
		try {
			file = new File(url.toURI());
			fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
		} catch (FileNotFoundException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (URISyntaxException e) {
			throw new Exception(e);
		}
		
		byte[] extractedImage = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			AESDecrypterBC decrypter = new AESDecrypterBC();
			AesZipFileDecrypter azfd = new AesZipFileDecrypter(file,
					decrypter);
			ExtZipEntry entry = azfd.getEntry(filename);
			azfd.extractEntry(entry, os, password);
			extractedImage = os.toByteArray();

		} catch (ZipException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (DataFormatException e) {
			throw new Exception(e);
		}
		
		return extractedImage;
	}
}
