package org.ha.cid.ias.service;

public interface ResourceService {

	public String loadFile(String filePath) throws Exception;

	public byte[] loadFileAsByteArray(String filePath) throws Exception;
	
	public void createTempImage(String tempFile, String templateFile, String text) throws Exception;
	
	public void createTempImage(String tempFile, String templateFile, int version) throws Exception;
	
	public byte[] extractZipFile(String zipFilePath, byte[] data, String filename, String password) throws Exception;

	
}
