package org.ha.cid.ias.service.impl;

import org.ha.cd2.isg.icw.tool.SftpUtil;
import org.ha.cid.ias.service.RemoteFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class RemoteFileServiceImpl implements RemoteFileService {

	private static final Logger LOG = LoggerFactory.getLogger(RemoteFileServiceImpl.class);

	private String host;
	private String username;
	private String password;
	private String rootDir;

	private Session session;
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRootDir() {
		return rootDir;
	}

	public void setRootDir(String rootDir) {
		this.rootDir = rootDir;
	}

	protected void initSession() throws Exception {
		if (session == null || !session.isConnected()) {
			session = SftpUtil.createSession(host, username, password);
		}
	}
	
	protected void closeSession() throws Exception {
		if (session != null) {
			session.disconnect();
		}
	}

	@Override
	public void deleteDirectory(String path) throws Exception {
		LOG.info("Start remove all files from directory: {}", path);
		ChannelSftp channel = null;
		try {
			initSession();
			channel = SftpUtil.getChannel(session);
			SftpUtil.removeAllFiles(channel, rootDir + path);
		} catch (SftpException e) {
			throw new Exception("Fail to delete directory", e);
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			closeSession();
		}
	}
	
	
	@Override
	public boolean checkFileExist(String path) throws Exception {
		LOG.info("Start check all files from directory: {}", path);
		ChannelSftp channel = null;
		try {
			initSession();
			channel = SftpUtil.getChannel(session);
			SftpUtil.checkAllFiles(channel, rootDir + path);
			return true;
		} catch (SftpException e) {
			return false;
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			closeSession();
		}
	}
}
