package org.ha.cid.ias.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.ha.cid.ias.service.RestfulService;
import org.springframework.http.HttpStatus;

public class RestfulServiceImpl implements RestfulService {

	
	protected String username;
	protected String password;
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRsResponse(String jsonString, String endpointURL) throws Exception {

		
		System.out.println("=====jsonString=======" + jsonString);
		
		String result = null;
		
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		StringEntity params = new StringEntity(jsonString);
		
		byte[] credentials = Base64.getEncoder().encode((username + ":" + password).getBytes(StandardCharsets.UTF_8));

		HttpPost request = new HttpPost(endpointURL);
		request.addHeader("Content-Type", "application/json");
		request.addHeader("Cache-Control", "no-cache");
		request.setHeader("Authorization", "Basic " + new String(credentials, StandardCharsets.UTF_8));

		
		request.setEntity(params);
		CloseableHttpResponse response = httpClient.execute(request);
		result = EntityUtils.toString(response.getEntity());
		int responseStatus = response.getStatusLine().getStatusCode();
		
		response.close();
		httpClient.close();

		if (HttpStatus.OK.value() == responseStatus) {
			return result;
		}else{
			
			throw new HttpResponseException(responseStatus, StringEscapeUtils.unescapeHtml4(result));
		}

	}

	@Override
	public void changeCredentials(String username, String password) {
		this.username = username;
		this.password = password;
				
	}








}

