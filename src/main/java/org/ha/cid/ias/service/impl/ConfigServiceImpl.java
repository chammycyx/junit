package org.ha.cid.ias.service.impl;

import org.ha.cid.ias.service.ConfigService;

public class ConfigServiceImpl implements ConfigService {
	
	private String ha7TemplatePath;
	private String ha7AckTemplatePath;
	
	private String requestSystem;
	private String requestUserId;
	private String requestWorkstationId;
	
	public String getHa7TemplatePath() {
		return ha7TemplatePath;
	}
	public void setHa7TemplatePath(String ha7TemplatePath) {
		this.ha7TemplatePath = ha7TemplatePath;
	}
	public String getHa7AckTemplatePath() {
		return ha7AckTemplatePath;
	}
	public void setHa7AckTemplatePath(String ha7AckTemplatePath) {
		this.ha7AckTemplatePath = ha7AckTemplatePath;
	}
	public String getRequestSystem() {
		return requestSystem;
	}
	public void setRequestSystem(String requestSystem) {
		this.requestSystem = requestSystem;
	}
	public String getRequestUserId() {
		return requestUserId;
	}
	public void setRequestUserId(String requestUserId) {
		this.requestUserId = requestUserId;
	}
	public String getRequestWorkstationId() {
		return requestWorkstationId;
	}
	public void setRequestWorkstationId(String requestWorkstationId) {
		this.requestWorkstationId = requestWorkstationId;
	}
	
}
