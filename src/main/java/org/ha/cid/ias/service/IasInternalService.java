package org.ha.cid.ias.service;

import java.util.Date;
import java.util.List;

import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.model.PatientDto;
import org.ha.cid.ias.model.StudyDto;

public interface IasInternalService {

	public List<AuditEventDto> getAuditEvent(String accessionNo) throws Exception;
	
	public void removeStudy(String accessionNo) throws Exception;
	
	public StudyDto getStudy(String accessionNo) throws Exception;
	
	public PatientDto getPatientByPatientKey(String patientKey) throws Exception;
	
	public void createPatient(PatientDto patientDto) throws Exception;
	
	public void removePatient(String patientKey) throws Exception;

	public void updateStudyWithPatient(String accessionNo, String patientKey);

	public StudyDto getRefreshedStudy(String accessionNo);

	public void updateStudyWithPatientEpisode(String accessionNo, String patientKey, String hospCode, String caseNum);

	public void setDelay(int i);

	public List<AuditEventDto> getAuditEvent(String accessionNo, String auditEventName) throws Exception;
	
	public PathIndex findPathIndex(String accessionNo, String serMotiveTag) throws Exception ;
	
	public void updatePathIndex(PathIndex pathIndex, Date updateDate) throws Exception ;
}
