package org.ha.cid.ias.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.entity.esb.Staging;
import org.ha.cid.ias.model.EsbEpisode;
import org.ha.cid.ias.model.EsbMessageHeader;
import org.ha.cid.ias.model.EsbMessagePatient;
import org.ha.cid.ias.model.StagingDto;

public interface IasEsbService {

	int sendEsbMsg(EsbMessageHeader esbMessageHeader,EsbMessagePatient esbMessagePatient, EsbEpisode esbEpisode);
	int sendUnknowTypeEsbMsg() throws IOException;
	void enableDuplicationChecker(String value);
	void enableDisorderChecker(String value);
	void enableStagingPoller(String value);
	void setDuplicationDayback(String value);
	void setCutoffTime(String value);
	boolean isStagingExists(String txnId);
	StagingDto findStaging(String txnId);
	void removeStagingWithStatus(List<MessageProcessStatus> list);
	void markStagingToErrWithStatus(List<MessageProcessStatus> status);
	long findNumStagingWithStatus(List<MessageProcessStatus> status);
	List<StagingDto> findStagingOrderByReceivedDtAsc(String txnId);
	void setReceivedDt(long stagingMessageId, Date time);
	StagingDto findStagingByMsgId(long stagingMessageId);
	List<StagingDto> findStagingByOrderingIdentifierSortByReceivedDtAsc(Date time);
	void removeStagingByMsgId(long msgId);
	void removeStagingByMsgIdRange(long fromMsgId, long toMsgId);
	Date findMinimumOrderingIdentifier();
	void setOrderingIdentifierNull(Date orderingIdentifier);
	void enableNullOrderingIdentifierStagingPolling(String value);
	void createPatient(String fromPatientKey, String toPatientKey);
	void setStagingStatus(Date evn2, MessageProcessStatus messageProcessStatus);
	StagingDto findStagingByOrderingIdentifier(Date evn2TxnId1);
}