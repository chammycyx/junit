package org.ha.cid.ias.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.ha.cd2.isg.icw.tool.CidStorePathUtil;
import org.ha.cid.ias.dao.EsAuditEventDao;
import org.ha.cid.ias.dao.EsStudyDao;
import org.ha.cid.ias.entity.es.AuditEvent;
import org.ha.cid.ias.entity.es.Patient;
import org.ha.cid.ias.entity.es.Study;
import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.model.AuditEventDto;
import org.ha.cid.ias.model.PatientDto;
import org.ha.cid.ias.model.StudyDto;
import org.ha.cid.ias.service.IasInternalService;
import org.ha.cid.ias.service.RemoteFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

public class IasEsInternalServiceImpl implements IasInternalService {

	private static final Logger LOG = LoggerFactory.getLogger(IasEsInternalServiceImpl.class);

	@Autowired
	private EsStudyDao esStudyDao;

	@Autowired
	private EsAuditEventDao esAuditEventDao;
	
	@Autowired
	@Qualifier("esRemoteFileService")
	private RemoteFileService esRemoteFileService;

	private int delay;
	private boolean purgeImage = false;
	
	public void setDelay(int delay) {
		this.delay = delay;
	}

	public void setPurgeImage(boolean purgeImage) {
		this.purgeImage = purgeImage;
	}

	private void delay(long seconds) {
		try {
			LOG.info("Start delay for {} seconds...", seconds);
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<AuditEventDto> getAuditEvent(String accessionNo) throws Exception {
		delay(delay);
		
		List<AuditEvent> auditEvents = esAuditEventDao.findByAccessionNo(accessionNo);
		
		if(auditEvents == null || auditEvents.size() == 0)
			return null;
		
		List<AuditEventDto> auditEventDtos = new ArrayList<AuditEventDto>();
		Iterator<AuditEvent> iterator = auditEvents.iterator();
		while(iterator.hasNext()) {
			AuditEvent auditEvent = iterator.next();
			auditEventDtos.add(AuditEventDto.buildFrom(auditEvent));
		}
		return auditEventDtos;
	}

	@Override
	@Transactional("esTransactionManager")
	public void removeStudy(String accessionNo) throws Exception {
		delay(delay);
		
		Study study = esStudyDao.findStudyByAccessionNo(accessionNo);
		esStudyDao.deleteStudy(study);
		
		if (purgeImage) {
			esRemoteFileService.deleteDirectory(CidStorePathUtil.generateStudyPath(accessionNo));
		} else {
			LOG.info("Skip delete image from directory: {}", CidStorePathUtil.generateStudyPath(accessionNo));
		}
	}

	@Override
	@Transactional("esTransactionManager")
	public StudyDto getStudy(String accessionNo) throws Exception {
		delay(delay);
		Study study = esStudyDao.findStudyByAccessionNo(accessionNo);
		
		if(study != null)
			return StudyDto.buildFrom(study);
		else
			return null;
	}

	@Override
	public PatientDto getPatientByPatientKey(String patientKey) throws Exception {
		Study study = esStudyDao.findStudyByPatientKey(patientKey);
		
		Patient patient = study.getPatient();
		if(patient != null){
			return PatientDto.buildFrom(study.getPatient());
		}
		return null;
	}

	@Override
	public void createPatient(PatientDto patientDto) throws Exception {
		throw new Exception("This is a dummy method as no patient table exist in ES");
	}

	@Override
	public void removePatient(String patientKey) throws Exception {
		throw new Exception("This is a dummy method as no patient table exist in ES");
	}

	@Override
	@Transactional("esTransactionManager")
	public void updateStudyWithPatient(String accessionNo, String patientKey) {
		Study study = esStudyDao.findStudyByAccessionNo(accessionNo);
			study.getPatient().setPatientKey(patientKey);
		esStudyDao.update(study);
	}

	@Override
	@Transactional("esTransactionManager")
	public StudyDto getRefreshedStudy(String accessionNo) {
		Study study = esStudyDao.findStudyByAccessionNo(accessionNo);
			study = esStudyDao.refresh(study);
		
		if(study != null)
			return StudyDto.buildFrom(study);
		else
			return null;
	}

	@Override
	@Transactional("esTransactionManager")
	public void updateStudyWithPatientEpisode(String accessionNo,String patientKey, String hospCode, String caseNum) {
		Study study = esStudyDao.findStudyByAccessionNo(accessionNo);
			study.getPatient().setPatientKey(patientKey);
			study.setCaseNumber(caseNum);
			study.setHospitalCode(hospCode);
		esStudyDao.update(study);
	}
	
	@Override
	public List<AuditEventDto> getAuditEvent(String accessionNo, String auditEventName) throws Exception {
		List<AuditEventDto> auditEvents = getAuditEvent(accessionNo);
			auditEvents.removeIf(a->!a.getEvent().equals(auditEventName));
		return auditEvents;
	}

	@Override
	public PathIndex findPathIndex(String accessionNo, String serMotiveTag) throws Exception {
		//dummy method
		return null;
	}

	@Override
	public void updatePathIndex(PathIndex pathIndex, Date updateDate) throws Exception{
		// dummy method
	}
	
	
	
}
