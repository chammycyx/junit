package org.ha.cid.ias.service.impl;

import icw.wsdl.xjc.epr.cid.DeLinkERSRecord;
import icw.wsdl.xjc.epr.cid.DeLinkERSRecordResponse;
import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls;
import icw.wsdl.xjc.pojo.CIDData.StudyDtl.SeriesDtls.SeriesDtl.ImageDtls.ImageDtl;
import icw.wsdl.xjc.pojo.DeleteStudy;
import icw.wsdl.xjc.pojo.DeleteStudyResponse;
import icw.wsdl.xjc.pojo.ExportImage;
import icw.wsdl.xjc.pojo.GetCidImage;
import icw.wsdl.xjc.pojo.GetCidImageThumbnail;
import icw.wsdl.xjc.pojo.GetCidScaledImage;
import icw.wsdl.xjc.pojo.GetCidStudy;
import icw.wsdl.xjc.pojo.GetCidStudyFirstLastImage;
import icw.wsdl.xjc.pojo.GetCidStudyImageCount;
import icw.wsdl.xjc.pojo.UploadImage;
import icw.wsdl.xjc.pojo.UploadMetaData;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.ha.cd2.isg.icw.client.retrieval.ExportImageClient;
import org.ha.cd2.isg.icw.client.retrieval.GetCidImageClient;
import org.ha.cd2.isg.icw.client.retrieval.GetCidImageThumbnailClient;
import org.ha.cd2.isg.icw.client.retrieval.GetCidStudyClient;
import org.ha.cd2.isg.icw.client.retrieval.GetCidStudyFirstLastImageClient;
import org.ha.cd2.isg.icw.client.upload.DeLinkERSRecordClient;
import org.ha.cd2.isg.icw.client.upload.DeleteStudyClient;
import org.ha.cd2.isg.icw.tool.Ha7Util;
import org.ha.cid.ias.constant.IasConstants;
import org.ha.cid.ias.service.ConfigService;
import org.ha.cid.ias.service.IasClientService;
import org.ha.cid.ias.service.ResourceService;
import org.ha.cid.ias.service.RestfulService;

import com.google.gson.Gson;

/**
 * @author CFT545
 *
 */
public class IasRsServiceImpl implements IasClientService {

	
	protected String retrievalEndpoint;
	protected String uploadEndpoint;
	
	private static final String EX_UPLOAD_IMAGE = "Upload image failed";
	private static final String EX_UPLOAD_METADATA = "Upload metadata failed";

	private static final String GET_CID_STUDY ="/getCidStudy";
	private static final String GET_CID_STUDY_FIRST_LAST_IMAGE ="/getCidStudyFirstLastImage";
	private static final String GET_CID_IMAGE_THUMBNAIL ="/getCidImageThumbnail";
	private static final String EXPORT_IMAGE ="/exportImage";
	private static final String GET_CID_IMAGE ="/getCidImage";
	private static final String UPLOAD_IMAGE ="/uploadImage";
	private static final String UPLOAD_META_DATA ="/uploadMetaData";
	private static final String GET_CID_SCALED_IMAGE ="/getCidScaledImage";
	private static final String GET_CID_STUDY_IMAGE_COUNT ="/getCidStudyImageCount";

	protected ConfigService configService;
	protected ResourceService resourceService;
	protected RestfulService restfulService;

	protected DeleteStudyClient deleteStudyClient;
	protected DeLinkERSRecordClient deLinkERSRecordClient;

	protected GetCidImageClient getCidImageClient;
	protected GetCidStudyClient getCidStudyClient;
	protected GetCidImageThumbnailClient getCidImageThumbnailClient;
	protected GetCidStudyFirstLastImageClient getCidStudyFirstLastImageClient;
	protected ExportImageClient exportImageClient;
	
	public void setRetrievalEndpoint(String retrievalEndpoint) {
		this.retrievalEndpoint = retrievalEndpoint;
	}
	
	public void setUploadEndpoint(String uploadEndpoint) {
		this.uploadEndpoint = uploadEndpoint;
	}
	
	private Gson gson = new Gson();

	public ConfigService getConfigService() {
		return configService;
	}

	public void setConfigService(ConfigService configService) {
		this.configService = configService;
	}

	public ResourceService getResourceService() {
		return resourceService;
	}

	public RestfulService getRestfulService() {
		return restfulService;
	}

	public void setRestfulService(RestfulService restfulService) {
		this.restfulService = restfulService;
	}

	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}


	public DeleteStudyClient getDeleteStudyClient() {
		return deleteStudyClient;
	}

	public void setDeleteStudyClient(DeleteStudyClient deleteStudyClient) {
		this.deleteStudyClient = deleteStudyClient;
	}

	public DeLinkERSRecordClient getDeLinkERSRecordClient() {
		return deLinkERSRecordClient;
	}

	public void setDeLinkERSRecordClient(DeLinkERSRecordClient deLinkERSRecordClient) {
		this.deLinkERSRecordClient = deLinkERSRecordClient;
	}

	public GetCidImageClient getGetCidImageClient() {
		return getCidImageClient;
	}

	public void setGetCidImageClient(GetCidImageClient getCidImageClient) {
		this.getCidImageClient = getCidImageClient;
	}

	public GetCidStudyClient getGetCidStudyClient() {
		return getCidStudyClient;
	}

	public void setGetCidStudyClient(GetCidStudyClient getCidStudyClient) {
		this.getCidStudyClient = getCidStudyClient;
	}


	public GetCidImageThumbnailClient getGetCidImageThumbnailClient() {
		return getCidImageThumbnailClient;
	}

	public void setGetCidImageThumbnailClient(
			GetCidImageThumbnailClient getCidImageThumbnailClient) {
		this.getCidImageThumbnailClient = getCidImageThumbnailClient;
	}

	public GetCidStudyFirstLastImageClient getGetCidStudyFirstLastImageClient() {
		return getCidStudyFirstLastImageClient;
	}

	public void setGetCidStudyFirstLastImageClient(
			GetCidStudyFirstLastImageClient getCidStudyFirstLastImageClient) {
		this.getCidStudyFirstLastImageClient = getCidStudyFirstLastImageClient;
	}


	public ExportImageClient getExportImageClient() {
		return exportImageClient;
	}

	public void setExportImageClient(ExportImageClient exportImageClient) {
		this.exportImageClient = exportImageClient;
	}



	protected UploadImage prepareUploadImage(String patientKey, String studyId, String seriesNo, String imageID, String version, String sequence, String imageFile) throws Exception {

		System.out.println("prepareUploadImage");
		System.out.println("=======================");
		System.out.println(seriesNo);
		System.out.println(imageID);
		System.out.println(version);
		System.out.println(sequence);
		System.out.println(imageFile);
		System.out.println("=======================");

		UploadImage uploadImage = new UploadImage();
		InputStream input = getClass().getResourceAsStream(imageFile);

		try {
			uploadImage.setImgBinaryArray(IOUtils.toByteArray(input));
			input.close();
		} catch (IOException e) {
			throw new Exception(e);
		}
		uploadImage.setRequestSys(configService.getRequestSystem());
		uploadImage.setFilename(sequence + "." + imageID + "_v" + version + ".JPEG");
		uploadImage.setPatientKey(patientKey);
		uploadImage.setStudyId(studyId);
		uploadImage.setSeriesNo(seriesNo);
		uploadImage.setUserId(configService.getRequestUserId());
		uploadImage.setWorkstationId(configService.getRequestWorkstationId());

		return uploadImage;
	}

	protected CIDData prepareCIDData(String ha7File) throws Exception {
		String content = resourceService.loadFile(ha7File);
		return Ha7Util.convertToCIDData(content);
	}

	protected CIDAck prepareCIDAck(String ha7AckFile) throws Exception {
		String content = resourceService.loadFile(ha7AckFile);
		return Ha7Util.convertToCIDAck(content);
	}

	protected ImageDtl createImageDtl(String imageID, String sequence, String version, String imageHandling, String imageType) {
		return createImageDtl(imageID, sequence, version , imageHandling, imageType, IasConstants.IMAGE_STATUS_ACTIVE.toString());
	}

	protected ImageDtl createImageDtl(String imageID, String sequence, String version, String imageHandling, String imageType, String imageStatus) {
		ImageDtl imageDtl = new ImageDtl();
		imageDtl.setImageFormat("IM");
		imageDtl.setImagePath("");
		imageDtl.setImageFile(sequence + "." + imageID + "_v" + version + ".JPEG");
		imageDtl.setImageID(imageID);
		imageDtl.setImageVersion(version);
		imageDtl.setImageSequence(BigInteger.valueOf(Long.parseLong(sequence)));
		imageDtl.setImageKeyword("Name:|");
		imageDtl.setImageHandling(imageHandling);
		imageDtl.setImageType(imageType);
		imageDtl.setImageStatus(BigInteger.valueOf(Long.parseLong(imageStatus)));
		imageDtl.setAnnotationDtls(new ImageDtl.AnnotationDtls());
		return imageDtl;
	}

	protected UploadMetaData prepareUploadMetaData(CIDData cidData) {

		cidData.getMessageDtl().setTransactionID(UUID.randomUUID().toString());

		String Ha7Message = Ha7Util.convertToHa7xml(cidData);
		System.out.println();
		System.out.println("prepareUploadMetaData");
		System.out.println(Ha7Message);
		System.out.println("Accession No. : " );
		System.out.println(cidData.getStudyDtl().getAccessionNo());
		System.out.println();

		UploadMetaData uploadMetaData = new UploadMetaData();
		uploadMetaData.setHa7Message(Ha7Message);

		uploadMetaData.setUserId(configService.getRequestUserId());
		uploadMetaData.setWorkstationId(configService.getRequestWorkstationId());
		uploadMetaData.setRequestSys(configService.getRequestSystem());

		return uploadMetaData;
	}

	protected GetCidStudy prepareGetCidStudy(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) {

		if(cidData == null){

			return null;
			
		}else{
			GetCidStudy getCidStudy = new GetCidStudy();
			getCidStudy.setPatientKey(cidData.getPatientDtl().getPatKey());
			getCidStudy.setHospCde(cidData.getVisitDtl().getVisitHosp());
			getCidStudy.setCaseNo(cidData.getVisitDtl().getCaseNum());
			getCidStudy.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
			getCidStudy.setSeriesNo(seriesNo);
			getCidStudy.setImageSeqNo(imageSeqNo);
			getCidStudy.setVersionNo(versionNo);

			getCidStudy.setUserId(configService.getRequestUserId());
			getCidStudy.setWorkstationId(configService.getRequestWorkstationId());
			getCidStudy.setRequestSys(configService.getRequestSystem());
			return getCidStudy;
		}
	}

	protected GetCidImageThumbnail prepareGetCidImageThumbnail(CIDData cidData, String imageSeqNo, String versionNo, String imageId) {
		
		if(cidData == null){

			return null;
			
		}else{
			GetCidImageThumbnail getCidImageThumbnail = new GetCidImageThumbnail();

			getCidImageThumbnail.setPatientKey(cidData.getPatientDtl().getPatKey());
			getCidImageThumbnail.setHospCde(cidData.getVisitDtl().getVisitHosp());
			getCidImageThumbnail.setCaseNo(cidData.getVisitDtl().getCaseNum());
			getCidImageThumbnail.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
			getCidImageThumbnail.setSeriesNo(cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo());
			getCidImageThumbnail.setImageSeqNo(imageSeqNo);
			getCidImageThumbnail.setVersionNo(versionNo);
			getCidImageThumbnail.setImageId(imageId);
			getCidImageThumbnail.setUserId(configService.getRequestUserId());
			getCidImageThumbnail.setWorkstationId(configService.getRequestWorkstationId());
			getCidImageThumbnail.setRequestSys(configService.getRequestSystem());

			return getCidImageThumbnail;

		}


	}

	protected GetCidScaledImage prepareGetCidScaledImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId, int width, int height) {
		GetCidScaledImage getCidScaledImage = new GetCidScaledImage();
		getCidScaledImage.setPatientKey(cidData.getPatientDtl().getPatKey());
		getCidScaledImage.setHospCde(cidData.getVisitDtl().getVisitHosp());
		getCidScaledImage.setCaseNo(cidData.getVisitDtl().getCaseNum());
		getCidScaledImage.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		getCidScaledImage.setSeriesNo(cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo());
		getCidScaledImage.setImageSeqNo(imageSeqNo);
		getCidScaledImage.setVersionNo(versionNo);
		getCidScaledImage.setImageId(imageId);
		getCidScaledImage.setWidth(width);
		getCidScaledImage.setHeight(height);
		getCidScaledImage.setUserId(configService.getRequestUserId());
		getCidScaledImage.setWorkstationId(configService.getRequestWorkstationId());
		getCidScaledImage.setRequestSys(configService.getRequestSystem());
		return getCidScaledImage;
	}

	protected GetCidImage prepareGetCidImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId) {
		if( cidData == null){

			return null;
			
		}else{

			GetCidImage getCidImage = new GetCidImage();
			getCidImage.setPatientKey(cidData.getPatientDtl().getPatKey());
			getCidImage.setHospCde(cidData.getVisitDtl().getVisitHosp());
			getCidImage.setCaseNo(cidData.getVisitDtl().getCaseNum());
			getCidImage.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
			getCidImage.setSeriesNo(cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo());
			getCidImage.setImageSeqNo(imageSeqNo);
			getCidImage.setVersionNo(versionNo);
			getCidImage.setImageId(imageId);

			getCidImage.setUserId(configService.getRequestUserId());
			getCidImage.setWorkstationId(configService.getRequestWorkstationId());
			getCidImage.setRequestSys(configService.getRequestSystem());

			return getCidImage;
		}


	}

	protected ExportImage prepareExportImage(String ha7Msg, String password) {
		
		if(ha7Msg == null){
			
			return null;
			
		}else{
			
			ExportImage exportImage = new ExportImage();
			CIDData cidData = Ha7Util.convertToCIDData(ha7Msg);
			exportImage.setHa7Msg(toJson(cidData));
			exportImage.setPassword(password);
			exportImage.setUserId(configService.getRequestUserId());
			exportImage.setWorkstationId(configService.getRequestWorkstationId());
			exportImage.setRequestSys(configService.getRequestSystem());
			return exportImage;
		}
		
	}

	protected ExportImage prepareExportImage(CIDData cidData, String password) {

		String Ha7Message = Ha7Util.convertToHa7xml(cidData);
		System.out.println();
		System.out.println("prepareExportImage");
		System.out.println(Ha7Message);
		System.out.println();

		ExportImage exportImage = new ExportImage();
		exportImage.setHa7Msg(Ha7Message);
		exportImage.setPassword(password);
		return exportImage;
	}

	protected GetCidStudyImageCount prepareGetCidStudyImageCount(CIDData cidData, boolean isPrintedCount, String imageSeqNo, String versionNo) {

		GetCidStudyImageCount getCidStudyImageCount = new GetCidStudyImageCount();

		getCidStudyImageCount.setPatientKey(cidData.getPatientDtl().getPatKey());
		getCidStudyImageCount.setHospCde(cidData.getVisitDtl().getVisitHosp());
		getCidStudyImageCount.setCaseNo(cidData.getVisitDtl().getCaseNum());
		getCidStudyImageCount.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
		getCidStudyImageCount.setSeriesNo(cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo());
		getCidStudyImageCount.setImageSeqNo(imageSeqNo);
		getCidStudyImageCount.setVersionNo(versionNo);
		getCidStudyImageCount.setIsPrintedCount(isPrintedCount);

		getCidStudyImageCount.setUserId(configService.getRequestUserId());
		getCidStudyImageCount.setWorkstationId(configService.getRequestWorkstationId());
		getCidStudyImageCount.setRequestSys(configService.getRequestSystem());

		return getCidStudyImageCount;
	}

	protected GetCidStudyFirstLastImage prepareGetCidStudyFirstLastImage(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) {

		if(cidData == null ){

			return null;

		}else{

			GetCidStudyFirstLastImage getCidStudyFirstLastImage = new GetCidStudyFirstLastImage();
			getCidStudyFirstLastImage.setPatientKey(cidData.getPatientDtl().getPatKey());
			getCidStudyFirstLastImage.setHospCde(cidData.getVisitDtl().getVisitHosp());
			getCidStudyFirstLastImage.setCaseNo(cidData.getVisitDtl().getCaseNum());
			getCidStudyFirstLastImage.setAccessionNo(cidData.getStudyDtl().getAccessionNo());
			getCidStudyFirstLastImage.setSeriesNo(seriesNo);
			getCidStudyFirstLastImage.setImageSeqNo(imageSeqNo);
			getCidStudyFirstLastImage.setVersionNo(versionNo);

			getCidStudyFirstLastImage.setUserId(configService.getRequestUserId());
			getCidStudyFirstLastImage.setWorkstationId(configService.getRequestWorkstationId());
			getCidStudyFirstLastImage.setRequestSys(configService.getRequestSystem());
			return getCidStudyFirstLastImage;
		}


	}

	protected DeleteStudy prepareDeleteStudy(CIDData cidData, String accessionNo, String studyID) {
		DeleteStudy deleteStudy = new DeleteStudy();
		deleteStudy.setPatientKey(cidData.getPatientDtl().getPatKey());
		deleteStudy.setHospCde(cidData.getVisitDtl().getVisitHosp());
		deleteStudy.setCaseNo(cidData.getVisitDtl().getCaseNum());
		deleteStudy.setAccessionNo(accessionNo);
		deleteStudy.setStudyID(studyID);
		return deleteStudy;
	}

	protected DeLinkERSRecord prepareDelinkERSRecord(CIDData cidData, String accessionNo, String studyID) {
		DeLinkERSRecord deleteStudy = new DeLinkERSRecord();
		deleteStudy.setPatientKey(cidData.getPatientDtl().getPatKey());
		deleteStudy.setInstCd(cidData.getVisitDtl().getVisitHosp());
		deleteStudy.setCaseNo(cidData.getVisitDtl().getCaseNum());
		deleteStudy.setAccessionNo(accessionNo);
		deleteStudy.setStudyId(studyID);
		return deleteStudy;
	}

	@Override
	public CIDData uploadImagesWithTemplateFiles(String templateFile, String tempFile, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandlings) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		int imageIndex = 0;
		String lastImageID = "";
		for(int i = 0; i < versions.length; i++) {
			if(!lastImageID.equals(imageIDs[i])) {
				lastImageID = imageIDs[i];
				imageIndex++;
			}
			String sequence = String.valueOf(i + 1);
			resourceService.createTempImage(tempFile, templateFile, imageIndex + "_" + versions[i]);
			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, tempFile);

			
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}

			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG);
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);

		}

		return cidData;
	}

	@Override
	public CIDData reuploadImagesWithTemplateFiles(CIDData cidData, String templateFile, String tempFile, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception {

		String studyID = cidData.getStudyDtl().getStudyID();
		String seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		String examType = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getExamType();

		CIDData data = prepareCIDData(configService.getHa7TemplatePath());
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
		data.getStudyDtl().setAccessionNo(accessionNo);
		data.getStudyDtl().setStudyID(studyID);

		int imageIndex = 0;
		String lastImageID = "";
		for(int i = 0; i < versions.length; i++) {
			if(!lastImageID.equals(imageIDs[i])) {
				lastImageID = imageIDs[i];
				imageIndex++;
			}
			String version = versions[i];
			if(!"1".equals(version)) {
				version = String.valueOf(Integer.valueOf(version).intValue() + 1);
				versions[i] = version;
			}

			String sequence = String.valueOf(i + 1);
			resourceService.createTempImage(tempFile, templateFile, imageIndex + "_" + version);
			UploadImage uploadImageRequest = prepareUploadImage(data.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], version, sequence, tempFile);

			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}

			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, version, imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG);
			data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);

		}

		return data;
	}

	@Override
	public String uploadImage(byte[] imgBinaryArray, String requestSys, String fileName, String patientKey,
			String studyId, String seriesNo, String userId, String workstationId) throws Exception {
		UploadImage uploadImage = new UploadImage(imgBinaryArray, requestSys, fileName, patientKey, 
				studyId, seriesNo, userId, workstationId);
		return uploadImageByRs(uploadImage);
	}

	@Override
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();
		return uploadImages(imageFiles, versions, imageIDs, accessionNo, imageHandling, studyID, seriesNo);
	}

	@Override
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String[] sequences) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequences[i], versions[i], imageHandling[i], IasConstants.IMAGE_TYPE_JPEG);
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return cidData;
	}

	@Override
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String studyID) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		return uploadImages(imageFiles, versions, imageIDs, accessionNo, imageHandling, studyID, seriesNo);
	}

	@Override
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String studyID, String seriesNo) throws Exception {

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());

		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandling[i], IasConstants.IMAGE_TYPE_JPEG);
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return cidData;
	}

	@Override
	public CIDData uploadImagesWithImageType(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String imageType) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandling[i], imageType);
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return cidData;
	}

	@Override
	public CIDData uploadImagesWithImagePath(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			String result = uploadImageByRs(uploadImageRequest);
			if (StringUtils.isEmpty(result)) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandling[i], IasConstants.IMAGE_TYPE_JPEG);
			imageDtl.setImagePath(result);
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return cidData;
	}

	@Override
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, int seriesCount) throws Exception {

		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		SeriesDtl seriesDtlTemplate = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0);
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().remove(0);
		for(int i = 0; i < seriesCount; i++) {
			SeriesDtl seriesDtl = new SeriesDtl();
			seriesDtl.setExamType(seriesDtlTemplate.getExamType());
			seriesDtl.setExamDtm(seriesDtlTemplate.getExamDtm());
			seriesDtl.setEntityID(seriesDtlTemplate.getEntityID());
			seriesDtl.setSeriesNo(UUID.randomUUID().toString());
			seriesDtl.setImageDtls(new ImageDtls());
			cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().add(seriesDtl);
		}
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		List<SeriesDtl> seriesDtlList = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl();

		int sequenceNo = 0;
		for(int seriesIndex = 0; seriesIndex < seriesDtlList.size(); seriesIndex++) {

			SeriesDtl seriesDtl = seriesDtlList.get(seriesIndex);
			String seriesNo = seriesDtl.getSeriesNo();
			for(int i = 0; i < imageFiles.length; i++) {
				sequenceNo++;
				String sequence = String.valueOf(sequenceNo);
				UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
				if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
					throw new Exception(EX_UPLOAD_IMAGE);
				}
				ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandling[i], IasConstants.IMAGE_TYPE_JPEG);
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(seriesIndex).getImageDtls().getImageDtl().add(imageDtl);

			}
		}

		return cidData;
	}

	@Override
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings, String[] sequences) throws Exception {

		String studyID = cidData.getStudyDtl().getStudyID();
		String seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		String examType = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getExamType();

		CIDData data = prepareCIDData(configService.getHa7TemplatePath());
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
		data.getStudyDtl().setAccessionNo(accessionNo);
		data.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			UploadImage uploadImageRequest = prepareUploadImage(data.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequences[i], imageFiles[i]);
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequences[i], versions[i], imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG);
			data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return data;
	}

	@Override
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings, String[] sequences, String[] status) throws Exception {

		String studyID = cidData.getStudyDtl().getStudyID();
		String seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		String examType = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getExamType();

		CIDData data = prepareCIDData(configService.getHa7TemplatePath());
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
		data.getStudyDtl().setAccessionNo(accessionNo);
		data.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			if (IasConstants.IMAGE_STATUS_ACTIVE.toString().equals(status[i])) {
				UploadImage uploadImageRequest = prepareUploadImage(data.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequences[i], imageFiles[i]);
				if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
					throw new Exception(EX_UPLOAD_IMAGE);
				}
			}

			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequences[i], versions[i], imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG, status[i]);
			data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return data;
	}


	@Override
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception {

		String studyID = cidData.getStudyDtl().getStudyID();
		String seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		String examType = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getExamType();

		CIDData data = prepareCIDData(configService.getHa7TemplatePath());
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
		data.getStudyDtl().setAccessionNo(accessionNo);
		data.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(data.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			if (uploadImageByRs(uploadImageRequest) == null) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG);
			data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
		}

		return data;
	}

	@Override
	public CIDData reuploadImagesWithImagePath(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception {

		String studyID = cidData.getStudyDtl().getStudyID();
		String seriesNo = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		String accessionNo = cidData.getStudyDtl().getAccessionNo();
		String examType = cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getExamType();

		CIDData data = prepareCIDData(configService.getHa7TemplatePath());
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
		data.getStudyDtl().setAccessionNo(accessionNo);
		data.getStudyDtl().setStudyID(studyID);

		for(int i = 0; i < imageFiles.length; i++) {
			String sequence = String.valueOf(i + 1);
			UploadImage uploadImageRequest = prepareUploadImage(data.getPatientDtl().getPatKey(), studyID, seriesNo, imageIDs[i], versions[i], sequence, imageFiles[i]);
			String result = uploadImageByRs(uploadImageRequest);
			if (StringUtils.isEmpty(result)) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
			ImageDtl imageDtl = createImageDtl(imageIDs[i], sequence, versions[i], imageHandlings[i], IasConstants.IMAGE_TYPE_JPEG);
			imageDtl.setImagePath(result);
			data.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);

		}

		return data;
	}

	@Override
	public CIDData repeatUploadImages(String templateFile, String[] tempFiles, String accessionNo, 
			int uploadCount, String examType) throws Exception {
		resourceService.createTempImage(tempFiles[0], templateFile, 1);

		String[] imageHandlings = {	IasConstants.IMAGE_HANDLING_NO, IasConstants.IMAGE_HANDLING_NO, IasConstants.IMAGE_HANDLING_NO };

		String imageID1 = UUID.randomUUID().toString();

		String[] imageIDs = { imageID1, imageID1, imageID1 };

		CIDData cidData = null;
		for(int i = 0; i < uploadCount; i++) {
			resourceService.createTempImage(tempFiles[1], templateFile, i + 2);
			resourceService.createTempImage(tempFiles[2], templateFile, i + 3);
			String[] versions = new String[3];
			versions[0] = "1";
			versions[1] = String.valueOf(i + 2);
			versions[2] = String.valueOf(i + 3);

			Integer result = 0;
			if(i == 0) {
				cidData = uploadImages(tempFiles, versions, imageIDs, accessionNo, imageHandlings);
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
				result = uploadMetaData(cidData);
			} else {
				cidData = reuploadImages(cidData, tempFiles, versions, imageIDs, imageHandlings);
				result = uploadMetaData(cidData);
			}

			if (result != 0) {
				throw new Exception(EX_UPLOAD_METADATA);
			}
		}
		return cidData;
	}

	@Override
	public CIDData repeatUploadImages(String templateFile, String tempFile, String accessionNo,
			int imageCount, int versionCount, int uploadCount, String examType) throws Exception {

		List<String> imageHandlingList = new ArrayList<String>();
		List<String> imageVersionList = new ArrayList<String>();
		List<String> imageIDList = new ArrayList<String>();

		for(int i = 0; i < imageCount; i++) {
			String imageID = UUID.randomUUID().toString();
			for(int j = 0; j < versionCount; j++) {
				imageVersionList.add(String.valueOf(j+1));
				imageHandlingList.add(IasConstants.IMAGE_HANDLING_NO);
				imageIDList.add(imageID);
			}
		}

		String[] versions = imageVersionList.toArray(new String[imageVersionList.size()]);
		String[] imageIDs = imageIDList.toArray(new String[imageIDList.size()]);
		String[] imageHandlings = imageHandlingList.toArray(new String[imageHandlingList.size()]);

		CIDData cidData = null;
		for(int i = 0; i < uploadCount; i++) {
			Integer result = 0;
			if(i == 0) {
				cidData = uploadImagesWithTemplateFiles(templateFile, tempFile, versions, imageIDs, accessionNo, imageHandlings);
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setExamType(examType);
				result = uploadMetaData(cidData);
			} else {
				cidData = reuploadImagesWithTemplateFiles(cidData, templateFile, tempFile, versions, imageIDs, imageHandlings);
				result = uploadMetaData(cidData);
			}
			if (result != 0) {
				throw new Exception(EX_UPLOAD_METADATA);
			}
		}
		return cidData;
	}

	@Override
	public CIDData deleteImages(CIDData cidData, String[] imageIDs, String[] imageFiles, String remarks) throws Exception {

		CIDData cidDataClone = Ha7Util.cloneCIDData(cidData);

		String seriesNo = cidDataClone.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getSeriesNo();
		cidDataClone.getStudyDtl().setRemark(remarks);

		Iterator<ImageDtl> iterator = cidDataClone.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().iterator();
		while(iterator.hasNext()) {
			ImageDtl imageDtl = iterator.next();
			for (String imageID : imageIDs) {
				if(imageID.equals(imageDtl.getImageID())) {
					imageDtl.setImageStatus(IasConstants.IMAGE_STATUS_INACTIVE);
					imageDtl.setImageFile("");
				}
			}
		}

		List<ImageDtl> imageDtls = cidDataClone.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl();

		for(int i = 0; i < imageDtls.size(); i++) {
			ImageDtl imageDtl = imageDtls.get(i);

			if(imageDtl.getImageStatus().longValue() == 0) {
				continue;
			}

			UploadImage uploadImageRequest = prepareUploadImage(cidData.getPatientDtl().getPatKey(), cidData.getStudyDtl().getStudyID(), seriesNo, imageDtl.getImageID(), imageDtl.getImageVersion(), imageDtl.getImageSequence().toString(), imageFiles[i]);
			if (StringUtils.isEmpty(uploadImageByRs(uploadImageRequest))) {
				throw new Exception(EX_UPLOAD_IMAGE);
			}
		}

		Integer result = uploadMetaData(cidDataClone);

		if (result != 0) {
			throw new Exception(EX_UPLOAD_METADATA);
		}

		return cidDataClone;
	}

	@Override
	public Integer uploadMetaData(CIDData cidData) throws Exception {
		UploadMetaData uploadMetaData = prepareUploadMetaData(cidData);
		return uploadMetaDataByRs(uploadMetaData);
	}

	@Override
	public Integer uploadMetaData(String ha7Msg, String userId, String workstationId, String requestSys)
			throws Exception {
		UploadMetaData uploadMetaData = new UploadMetaData(ha7Msg, userId, workstationId, requestSys);
		return uploadMetaDataByRs(uploadMetaData);
	}

	@Override
	public int getCidStudyImageCount(CIDData cidData, boolean isPrintedCount) throws Exception {
		GetCidStudyImageCount request = prepareGetCidStudyImageCount(cidData, isPrintedCount, "", "");
		return getCidStudyImageCountByRs(request);
	}

	@Override
	public int getCidStudyImageCount(CIDData cidData, boolean isPrintedCount, String imageSeqNo, String versionNo) throws Exception {
		GetCidStudyImageCount request = prepareGetCidStudyImageCount(cidData, isPrintedCount, imageSeqNo, versionNo);
		return getCidStudyImageCountByRs(request);
	}

	@Override
	public int getCidStudyImageCount(String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, Boolean isPrintedCount, String userId,
			String workstationId, String requestSys) throws Exception {
		GetCidStudyImageCount request = new GetCidStudyImageCount(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, isPrintedCount, userId, workstationId, requestSys);
		return getCidStudyImageCountByRs(request);
	}

	@Override
	public String getCidStudyFirstLastImage(CIDData cidData) throws Exception {
		GetCidStudyFirstLastImage getCidStudyFirstLastImage = prepareGetCidStudyFirstLastImage(cidData, "", "", "");
		return getCidStudyFirstLastImageByRs(getCidStudyFirstLastImage);
	}


	@Override
	public String getCidStudyFirstLastImage(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) throws Exception {
		GetCidStudyFirstLastImage getCidStudyFirstLastImage = prepareGetCidStudyFirstLastImage(cidData, seriesNo, imageSeqNo, versionNo);
		return getCidStudyFirstLastImageByRs(getCidStudyFirstLastImage);
	}

	@Override
	public String getCidStudyFirstLastImage(String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, String userId, String workstationId,
			String requestSys) throws Exception {
		GetCidStudyFirstLastImage getCidStudyFirstLastImage = new GetCidStudyFirstLastImage(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, userId, workstationId, requestSys);
		return getCidStudyFirstLastImageByRs(getCidStudyFirstLastImage);
	}


	
	@Override
	public String getCidStudy(CIDData cidData) throws Exception {
		GetCidStudy getCidStudy = prepareGetCidStudy(cidData, "", "", "");
		return getCidStudyByRs(getCidStudy);

	}

	@Override
	public String getCidStudy(CIDData cidData, String seriesNo) throws Exception {
		GetCidStudy getCidStudy = prepareGetCidStudy(cidData, seriesNo, "", "");
		return getCidStudyByRs(getCidStudy);
	}

	@Override
	public String getCidStudy(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) throws Exception {
		GetCidStudy getCidStudy = prepareGetCidStudy(cidData, seriesNo, imageSeqNo, versionNo);
		return getCidStudyByRs(getCidStudy);
	}

	@Override
	public String getCidStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String userId, String workstationId, String requestSys)
					throws Exception {
		GetCidStudy getCidStudy = new GetCidStudy(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, userId, workstationId, requestSys);
		return getCidStudyByRs(getCidStudy);
	}
	


	@Override
	public byte[] getCidImageThumbnail(CIDData cidData, String imageSeqNo, String versionNo, String imageId) throws Exception {
		GetCidImageThumbnail getCidImageThumbnail = prepareGetCidImageThumbnail(cidData, imageSeqNo, versionNo, imageId);
		return getCidImageThumbnailByRs(getCidImageThumbnail );
	}

	@Override
	public byte[] getCidImageThumbnail(String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, String imageId, String userId, String workstationId,
			String requestSys) throws Exception {
		GetCidImageThumbnail getCidImageThumbnail = new GetCidImageThumbnail(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, imageId, userId, workstationId, requestSys);
		return getCidImageThumbnailByRs(getCidImageThumbnail );
	}

	

	@Override
	public byte[] getCidImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId) throws Exception {
		GetCidImage getCidImage = prepareGetCidImage(cidData, imageSeqNo, versionNo, imageId);
		return getCidImageByRs(getCidImage);
	}

	@Override
	public byte[] getCidImage(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo,
			String imageSeqNo, String versionNo, String imageId, String userId, String workstationId, String requestSys)
					throws Exception {
		GetCidImage getCidImage = new GetCidImage(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, imageId, userId, workstationId, requestSys);
		return getCidImageByRs(getCidImage);
	}

	


	@Override
	public byte[] exportImage(String ha7Msg, String password) throws Exception {
		ExportImage exportImage = prepareExportImage(ha7Msg, password);
		return exportImageByRs(exportImage);
	}

	@Override
	public byte[] exportImage(String ha7Msg, String password, String userId, String workstationId, String requestSys)
			throws Exception {
		CIDData cidData = null;
		ExportImage exportImage = null;
		try {
    		if(!StringUtils.isEmpty(ha7Msg)){
    			cidData = Ha7Util.convertToCIDData(ha7Msg);
    			exportImage = new ExportImage(toJson(cidData), password, userId, workstationId, requestSys);
    		} else {
    		    exportImage = new ExportImage(ha7Msg, password, userId, workstationId, requestSys);
    		}
		} catch (Exception e) {
		    exportImage = new ExportImage(ha7Msg, password, userId, workstationId, requestSys);
		}
		return exportImageByRs(exportImage);
	}

	


	@Override
	public byte[] getCidScaledImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId, int width, int height) throws Exception {
		GetCidScaledImage getCidScaledImage = prepareGetCidScaledImage(cidData, imageSeqNo, versionNo, imageId, width, height);
		return getCidScaledImageByRs(getCidScaledImage);
	}

	@Override
	public byte[] getCidScaledImage(String patientKey, String hospCde, String caseNo, String accessionNo,
			String seriesNo, String imageSeqNo, String versionNo, String imageId, Integer width, Integer height,
			String userId, String workstationId, String requestSys) throws Exception {
		GetCidScaledImage getCidScaledImage = new GetCidScaledImage(patientKey, hospCde, caseNo, 
				accessionNo, seriesNo, imageSeqNo, versionNo, imageId, width, height, 
				userId, workstationId, requestSys);
		return getCidScaledImageByRs(getCidScaledImage);
	}

	@Override
	public String deleteStudy(CIDData cidData, String accessionNo, String studyID) throws Exception {
		DeleteStudy deleteStudy = prepareDeleteStudy(cidData, accessionNo, studyID);
		DeleteStudyResponse result = null;
		result = deleteStudyClient.deleteStudy(deleteStudy);
		return result.getDeleteStudyResult();
	}

	@Override
	public String deleteStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String studyId)
			throws Exception {
		DeleteStudy deleteStudy = new DeleteStudy(patientKey, hospCde, caseNo, accessionNo, studyId);
		DeleteStudyResponse result = null;
		result = deleteStudyClient.deleteStudy(deleteStudy);
		return result.getDeleteStudyResult();
	}

	@Override
	public String delinkStudy(CIDData cidData, String accessionNo, String studyID) throws Exception {
		DeLinkERSRecord deLinkERSRecord = prepareDelinkERSRecord(cidData, accessionNo, studyID);
		DeLinkERSRecordResponse result = null;
		result = deLinkERSRecordClient.deLinkERSRecord(deLinkERSRecord);
		return result.getReturn();
	}

	@Override
	public String delinkStudy(String patientKey, String instCd, String caseNo, String accessionNo, String studyId)
			throws Exception {
		DeLinkERSRecord deLinkERSRecord = new DeLinkERSRecord(patientKey, instCd, caseNo, accessionNo, studyId);
		DeLinkERSRecordResponse result = null;
		result = deLinkERSRecordClient.deLinkERSRecord(deLinkERSRecord);
		return result.getReturn();
	}

	@Override
	public CIDData generateCIDData(String accessionNo, int imageCount, int versionCount) throws Exception {

		String seriesNo = UUID.randomUUID().toString();
		String studyID = UUID.randomUUID().toString();

		CIDData cidData = prepareCIDData(configService.getHa7TemplatePath());
		cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).setSeriesNo(seriesNo);
		cidData.getStudyDtl().setAccessionNo(accessionNo);
		cidData.getStudyDtl().setStudyID(studyID);

		int sequence = 0;
		for(int i = 0; i < imageCount; i++) {
			String imageID = UUID.randomUUID().toString();
			for(int j = 0; j < versionCount; j++) {
				ImageDtl imageDtl = createImageDtl(imageID, String.valueOf(++sequence), String.valueOf(j+1), 
						IasConstants.IMAGE_HANDLING_NO, IasConstants.IMAGE_TYPE_JPEG);
				cidData.getStudyDtl().getSeriesDtls().getSeriesDtl().get(0).getImageDtls().getImageDtl().add(imageDtl);
			}
		}
		return cidData;
	}

	@Override
	public CIDAck generateCIDAck() throws Exception {
		return prepareCIDAck(configService.getHa7AckTemplatePath());
	}
	
	
	
	private String getCidStudyByRs(GetCidStudy getCidStudy) throws Exception{
		String result = restfulService.getRsResponse(toJson(getCidStudy), retrievalEndpoint + GET_CID_STUDY);
		return Ha7Util.convertToHa7xml(gson.fromJson(result, CIDData.class));

	}
	
	
	private String uploadImageByRs(UploadImage uploadImage) throws Exception{
		if(uploadImage.getImgBinaryArray() != null){
			uploadImage.setImageBase64(Base64.getEncoder().encodeToString(uploadImage.getImgBinaryArray()));
		}
		
		uploadImage.setImgBinaryArray(null);
		String result = restfulService.getRsResponse(toJson(uploadImage), uploadEndpoint+ UPLOAD_IMAGE);
		
		return result;
	}
	
	private Integer uploadMetaDataByRs(UploadMetaData uploadMetaData) throws Exception{
		String result = restfulService.getRsResponse(toJson(uploadMetaData), uploadEndpoint+UPLOAD_META_DATA);
		
		return Integer.valueOf(result);
	}
	
	
	private Integer getCidStudyImageCountByRs(GetCidStudyImageCount getCidStudyImageCount) throws Exception{
		String result = restfulService.getRsResponse(toJson(getCidStudyImageCount), retrievalEndpoint+GET_CID_STUDY_IMAGE_COUNT);
	
		return Integer.valueOf(result);
	}
	
	protected byte[] getCidScaledImageByRs(GetCidScaledImage getCidScaledImage ) throws Exception{
		String result = restfulService.getRsResponse(toJson(getCidScaledImage), retrievalEndpoint+GET_CID_SCALED_IMAGE);
		return Base64.getDecoder().decode(result);
	}
	
	protected String getCidStudyFirstLastImageByRs(GetCidStudyFirstLastImage getCidStudyFirstLastImage) throws Exception {
		String result = restfulService.getRsResponse(toJson(getCidStudyFirstLastImage), retrievalEndpoint + GET_CID_STUDY_FIRST_LAST_IMAGE);
		return Ha7Util.convertToHa7xml(gson.fromJson(result, CIDData.class));

	}
	
	protected byte[] getCidImageThumbnailByRs(GetCidImageThumbnail getCidImageThumbnail ) throws Exception{
		String result = restfulService.getRsResponse(toJson(getCidImageThumbnail), retrievalEndpoint + GET_CID_IMAGE_THUMBNAIL);
		return Base64.getDecoder().decode(result);
	}
	protected byte[] getCidImageByRs(GetCidImage getCidImage) throws Exception{
		String result = restfulService.getRsResponse(toJson(getCidImage), retrievalEndpoint + GET_CID_IMAGE);
		return Base64.getDecoder().decode(result);
	}
	protected byte[] exportImageByRs(ExportImage exportImage) throws Exception {

		String result = restfulService.getRsResponse(toJson(exportImage), retrievalEndpoint + EXPORT_IMAGE);
		return Base64.getDecoder().decode(result);

	}
	
	
	
	protected String toJson(Object obj){
		if(obj == null){
			
			return "{}";
		}else{
			
			return gson.toJson(obj);
		}
		
	}


}

