package org.ha.cid.ias.service;

public interface RemoteFileService {

	public void deleteDirectory(String path) throws Exception;
	
	public boolean checkFileExist(String path) throws Exception;
	
}
