package org.ha.cid.ias.service;

import icw.wsdl.xjc.pojo.CIDAck;
import icw.wsdl.xjc.pojo.CIDData;

public interface IasClientService {
	
	public CIDData uploadImagesWithTemplateFiles(String templateFile, String tempFile, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandlings) throws Exception;
	
	public CIDData reuploadImagesWithTemplateFiles(CIDData cidData, String templateFile, String tempFile, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception;
	
	public String uploadImage(byte[] imgBinaryArray, String requestSys, String fileName, String patientKey, String studyId, String seriesNo, String userId, String workstationId) throws Exception;
	
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling) throws Exception;
	
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String[] sequences) throws Exception;
	
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String studyID) throws Exception;
	
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String studyID, String seriesNo) throws Exception;
	
	public CIDData uploadImagesWithImageType(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, String imageType) throws Exception;
	
	public CIDData uploadImagesWithImagePath(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling) throws Exception;
	
	public CIDData uploadImages(String[] imageFiles, String[] versions, String[] imageIDs, String accessionNo, String[] imageHandling, int seriesCount) throws Exception;
	
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings, String[] sequences) throws Exception;
	
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings, String[] sequences, String[] status) throws Exception;
	
	public CIDData reuploadImages(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception;
	
	public CIDData reuploadImagesWithImagePath(CIDData cidData, String[] imageFiles, String[] versions, String[] imageIDs, String[] imageHandlings) throws Exception;
	
	public CIDData repeatUploadImages(String templateFile, String[] tempFiles, String accessionNo, int uploadCount, String examType) throws Exception;
	
	public CIDData repeatUploadImages(String templateFile, String tempFile, String accessionNo, 
			int imageCount, int versionCount, int uploadCount, String examType) throws Exception;
	
	public CIDData deleteImages(CIDData cidData, String[] imageIDs, String[] imageFiles, String remark) throws Exception;
	
	public Integer uploadMetaData(CIDData cidData) throws Exception;

	public Integer uploadMetaData(String ha7Msg, String userId, String workstationId, String requestSys) throws Exception;
	
	public int getCidStudyImageCount(CIDData cidData, boolean isPrintedCount) throws Exception;
	
	public int getCidStudyImageCount(CIDData cidData, boolean isPrintedCount, String imageSeqNo, String versionNo) throws Exception;

	public int getCidStudyImageCount(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, Boolean isPrintedCount, String userId, String workstationId, String requestSys) throws Exception;
	
	public String getCidStudyFirstLastImage(CIDData cidData) throws Exception;
	
	public String getCidStudyFirstLastImage(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) throws Exception;

	public String getCidStudyFirstLastImage(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, String userId, String workstationId, String requestSys) throws Exception;
	
	public String getCidStudy(CIDData cidData) throws Exception;
	
	public String getCidStudy(CIDData cidData, String seriesNo) throws Exception;
	
	public String getCidStudy(CIDData cidData, String seriesNo, String imageSeqNo, String versionNo) throws Exception;

	public String getCidStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, String userId, String workstationId, String requestSys) throws Exception;
	
	public byte[] getCidImageThumbnail(CIDData cidData, String imageSeqNo, String versionNo, String imageId) throws Exception;

	public byte[] getCidImageThumbnail(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, String imageId, String userId, String workstationId, String requestSys) throws Exception;
	
	public byte[] getCidScaledImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId, int width, int height) throws Exception;

	public byte[] getCidScaledImage(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, String imageId, Integer width, Integer height, String userId, String workstationId, String requestSys) throws Exception;
	
	public byte[] getCidImage(CIDData cidData, String imageSeqNo, String versionNo, String imageId) throws Exception;

	public byte[] getCidImage(String patientKey, String hospCde, String caseNo, String accessionNo, String seriesNo, String imageSeqNo, 
			String versionNo, String imageId, String userId, String workstationId, String requestSys) throws Exception;
	
	public byte[] exportImage(String ha7Msg, String password) throws Exception;

	public byte[] exportImage(String ha7Msg, String password, String userId, String workstationId, String requestSys) throws Exception;
	
	public String deleteStudy(CIDData cidData, String accessionNo, String studyID) throws Exception;

	public String deleteStudy(String patientKey, String hospCde, String caseNo, String accessionNo, String studyId) throws Exception;

	public String delinkStudy(CIDData cidData, String accessionNo, String studyID) throws Exception;

	public String delinkStudy(String patientKey, String instCd, String caseNo, String accessionNo, String studyId) throws Exception;

	public CIDData generateCIDData(String accessionNo, int imageCount, int versionCount) throws Exception;

	public CIDAck generateCIDAck() throws Exception;
	
}
