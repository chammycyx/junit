package org.ha.cid.ias.service;

public interface ConfigService {
	
	public String getHa7TemplatePath();
	
	public void setHa7TemplatePath(String ha7TemplatePath);
	
	public String getHa7AckTemplatePath();
	
	public void setHa7AckTemplatePath(String ha7AckTemplatePath);
	
	public String getRequestSystem();
	
	public void setRequestSystem(String requestSystem);
	
	public String getRequestUserId();
	
	public void setRequestUserId(String requestUserId);
	
	public String getRequestWorkstationId();
	
	public void setRequestWorkstationId(String requestWorkstationId);
	
}
