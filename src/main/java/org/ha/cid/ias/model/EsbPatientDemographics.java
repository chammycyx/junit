/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;

// import net.sourceforge.cobertura.CoverageIgnore;

/**
 * The class which provides a <strong>value object</strong> of patient demographics.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9916 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:41 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public class EsbPatientDemographics {

	private String _dob;
	private String _hkid;
	private String _patientName;
	private String _patientKey;
	private String _sex;

	/**
	 * @return the date of birth
	 */
	public String getDob() {
		return _dob;
	}

	/**
	 * @param dob the date of birth to set
	 */
	public void setDob(String dob) {
		this._dob = dob;
	}

	/**
	 * @return the HKID
	 */
	public String getHkid() {
		return _hkid;
	}

	/**
	 * @param hkid the HKID to set
	 */
	public void setHkid(String hkid) {
		this._hkid = hkid;
	}

	/**
	 * @return the patient name
	 */
	public String getPatientName() {
		return _patientName;
	}

	/**
	 * @param patientName the patient name to set
	 */
	public void setPatientName(String patientName) {
		this._patientName = patientName;
	}

	/**
	 * @return the patient key
	 */
	public String getPatientKey() {
		return _patientKey;
	}

	/**
	 * @param patientKey the patient key to set
	 */
	public void setPatientKey(String patientKey) {
		this._patientKey = patientKey;
	}

	/**
	 * @return the _sex
	 */
	public String getSex() {
		return _sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this._sex = sex;
	}

}
