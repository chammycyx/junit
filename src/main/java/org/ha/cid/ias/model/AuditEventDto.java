/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;

import java.util.Arrays;
import java.util.List;

import org.ha.cd2.isg.icw.tool.DateUtils;

/**
 * This DTO is the model for the following entities:
 * org.ha.cid.ias.entity.es.AuditEvent
 * org.ha.cid.ias.entity.rr.AuditEvent
 *
 * @author LSM131
 *
 */
public class AuditEventDto {
	private String event;
	private String date;
	private String requestChannel;
	private String requestSys;
	private String status;
	private String request;
	private String response;
	private String exception;
	private String userId;
	private String workStationId;
	private String clientIp;
	private String patKey;
	private String studyUid;
	private String patName;
	private String patDob;
	private String hkid;
	private String accessionNo;
	
	public static final String STATUS_SUCCESS = "Success";
	public static final String STATUS_FAILURE = "Failure";
	
	public static final List<String> UPLOAD_ES_EVENT_NAME = Arrays.asList(
			"uploadImage",
			"searchHKPMIPatientByCaseNo",
			"uploadMetaData",
			"saveCID",
			"ITI-41",
			"imageRouting",
			"patientOutmodedAsHA7",
			"ExtSubImageUpdateFilter",
			"synchronizeStudy");
	
	public static final List<String> UPLOAD_RR_EVENT_NAME = Arrays.asList(
			"ReceivingXDSSubmitInRR",
			"ReplicationToOtherRR",
			"XRemoteITIReplicationInRR",
			"RoutingScheduleInRR",
			"RouteImageToSNInRR",
			"UploadStudyOutSyncInRR");
	
	public static final List<String> DELETE_ES_EVENT_NAME = Arrays.asList(
			"deLinkERSRecord",
			"deLinkERSRecordToEpr",
			"imageRouting",
			"SendDeleteStudy",
			"deleteStudy");

	public static final List<String> DELETE_RR_EVENT_NAME = Arrays.asList(
			"deleteStudyInRR",
			"ReplicationToOtherRR",
			"XRemoteCIDStudyMarkDeletedInRR");
	
	public static final List<String> RETRIEVE_ES_EVENT_NAME = Arrays.asList(
			"searchHKPMIPatientByCaseNo",
			"exportImage",
			"exportImageEx",
			"getCidStudy",
			"getCidStudyEx",
			"getCidStudyFirstLastImage",
			"getCidStudyFirstLastImageEx",
			"getCidStudyImageCount",
			"getCidImage",
			"getCidScaledImage",
			"getCidImageThumbnail",
			"DownloadMetadata"
			);
	
	public static final List<String> RETRIEVE_RR_EVENT_NAME = Arrays.asList(
			"exportImageInRR",
			"exportImageExInRR",
			"getCidImageInRR",
			"getCidScaledImageInRR",
			"getCidImageThumbnailInRR",
			"getCidStudyInRR",
			"getCidStudyExInRR",
			"getCidStudyFirstLastImageInRR",
			"getCidStudyFirstLastImageExInRR",
			"getCidStudyImageCountInRR",
			"ReturnMetaDataInRR");
	
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRequestChannel() {
		return requestChannel;
	}

	public void setRequestChannel(String requestChannel) {
		this.requestChannel = requestChannel;
	}

	public String getRequestSys() {
		return requestSys;
	}

	public void setRequestSys(String requestSys) {
		this.requestSys = requestSys;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWorkStationId() {
		return workStationId;
	}

	public void setWorkStationId(String workStationId) {
		this.workStationId = workStationId;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getStudyUid() {
		return studyUid;
	}

	public void setStudyUid(String studyUid) {
		this.studyUid = studyUid;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}
	
	public String getPatDob() {
		return patDob;
	}

	public void setPatDob(String patDob) {
		this.patDob = patDob;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}

	public static AuditEventDto buildFrom(org.ha.cid.ias.entity.es.AuditEvent audit) {
		if (audit == null) {
			return null;
		}
		AuditEventDto newAuditEventDto = new AuditEventDto();
		newAuditEventDto.setEvent(audit.getEventName());

		if (audit.getReceivedTime() != null) {
			newAuditEventDto.setDate(DateUtils.format(audit.getReceivedTime(), DateUtils.FORMAT_TIMESTAMP_SHORT));
		}

		newAuditEventDto.setRequestChannel(audit.getRequestChannel());
		newAuditEventDto.setRequestSys(audit.getRequestSys());
		newAuditEventDto.setStatus(audit.getStatus());
		newAuditEventDto.setUserId(audit.getUserId());
		newAuditEventDto.setWorkStationId(audit.getWorkstationId());
		newAuditEventDto.setClientIp(audit.getClientIP());
		newAuditEventDto.setRequest(audit.getRequest());
		newAuditEventDto.setResponse(audit.getResponse());
		newAuditEventDto.setException(audit.getException());
		newAuditEventDto.setPatKey(audit.getPatKey());
		newAuditEventDto.setStudyUid(audit.getStudyUid());
		newAuditEventDto.setPatName(audit.getPatName());
		newAuditEventDto.setPatDob(audit.getPatDob());
		newAuditEventDto.setHkid(audit.getPatId());
		newAuditEventDto.setAccessionNo(audit.getAccessionNo());
		return newAuditEventDto;
	}

	public static AuditEventDto buildFrom(org.ha.cid.ias.entity.rr.AuditEvent audit) {
		if (audit == null) {
			return null;
		}
		AuditEventDto newAuditEventDto = new AuditEventDto();
		newAuditEventDto.setEvent(audit.getEventName());

		if (audit.getReceivedTime() != null) {
			newAuditEventDto.setDate(DateUtils.format(audit.getReceivedTime(), DateUtils.FORMAT_TIMESTAMP_SHORT));
		}

		newAuditEventDto.setRequestChannel(audit.getRequestChannel());
		newAuditEventDto.setRequestSys(audit.getRequestSys());
		newAuditEventDto.setStatus(audit.getStatus());
		newAuditEventDto.setUserId(audit.getUserId());
		newAuditEventDto.setWorkStationId(audit.getWorkstationId());
		newAuditEventDto.setClientIp(audit.getClientIP());
		newAuditEventDto.setRequest(audit.getRequest());
		newAuditEventDto.setResponse(audit.getResponse());
		newAuditEventDto.setException(audit.getException());
		newAuditEventDto.setPatKey(audit.getPatKey());
		newAuditEventDto.setStudyUid(audit.getStudyUid());
		newAuditEventDto.setPatName(audit.getPatName());
		newAuditEventDto.setPatDob(audit.getPatDob());
		newAuditEventDto.setHkid(audit.getPatId());
		newAuditEventDto.setAccessionNo(audit.getAccessionNo());
		return newAuditEventDto;
	}

	@Override
	public String toString() {
		return "AuditEventDto [event=" + event + ", date=" + date
				+ ", requestChannel=" + requestChannel + ", requestSys="
				+ requestSys + ", status=" + status + ", request=" + request
				+ ", response=" + response + ", exception=" + exception
				+ ", userId=" + userId + ", workStationId=" + workStationId
				+ ", clientIp=" + clientIp + ", patKey=" + patKey
				+ ", studyUid=" + studyUid + ", patName=" + patName
				+ ", patDob=" + patDob + ", hkid=" + hkid + ", accessionNo="
				+ accessionNo + "]";
	}
	
	
	
}
