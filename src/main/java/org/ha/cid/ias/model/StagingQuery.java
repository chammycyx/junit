/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;


/**
 * The class which keep the queries with respect to the <code>Staging</code> entity. It is recommended to specify
 * the <strong>query name</strong> and <strong>JPQL</strong> in pair.
 *
 * <pre>
 * Last commit:-
 * $Rev: 11885 $
 * $Author: ymm698 $
 * $Date: 2017-07-24 15:18:25 +0800 (Mon, 24 Jul 2017) $
 * </pre>
 *
 */
public final class StagingQuery {

	public final static String NAMED_QUERY_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID =
			"findByProcessStatusOrderByStagingMessageId";

	public final static String JPQL_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID =
			"SELECT s FROM Staging s " +
			"WHERE s.processStatus = ?1 " +
			"ORDER BY s.stagingMessageId";

	public final static String NAMED_QUERY_FIND_BY_SUBSCRIPTION_ID =
			"findBySubscriptionId";

	public final static String JPQL_FIND_BY_SUBSCRIPTION_ID =
			"SELECT st FROM Staging st " +
			"INNER JOIN st.subscriptions su " +
			"WHERE su.subscriptionId = ?1";

	public final static String NAMED_QUERY_FIND_PRECEDING_DUPLICATE_MESSAGE_TRANSACTION_ID =
			"findPrecedingDuplicateByMessageTransactionId";

	public final static String JPQL_FIND_PRECEDING_DUPLICATE_BY_MESSAGE_TRANSACTION_ID =
			"SELECT s FROM Staging s " +
			"WHERE s.stagingMessageId < ?1 " +
			"AND s.messageTransactionId = UPPER(?2) ORDER BY s.receivedDt DESC";

	public final static String NAMED_QUERY_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT =
			"findPrecedingProcessedBeforeCurrent";

	public static final String JPQL_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT =
			"SELECT s FROM Staging s " +
			"WHERE s.stagingMessageId < ?1 " +
			"AND (s.processStatus != ?2 AND s.processStatus != ?3) " +
			"ORDER BY s.stagingMessageId DESC";
	
	public final static String NAMED_QUERY_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER =
			"findByProcessStatusWithCutoffTimeOrderByOrderingIdentifier";
	
	public static final String JPQL_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER =
			"SELECT s FROM Staging s " +
			"WHERE s.processStatus = :status " +
			"AND s.orderingIdentifier <= (SELECT MAX(o.orderingIdentifier) FROM Staging o WHERE o.processStatus = :status and o.receivedDt < :cutoffDt) " +
			"ORDER BY s.orderingIdentifier ASC";
	
	public final static String NAMED_QUERY_FIND_STAGING_AFTER_ORDERING_IDENTIFIER=
			"findStagingAfterOrderingIdentifier";
	
	public static final String JPQL_FIND_STAGING_AFTER_ORDERING_IDENTIFIER =
			"SELECT s FROM Staging s " +
			"WHERE s.orderingIdentifier > ?1 " + 
			"AND s.processStatus NOT IN ?2";
	
	public final static String NAMED_QUERY_FIND_STAGING_BY_TXN_ID=
			"findStagingByTxnId";
	
	public static final String JPQL_FIND_STAGING_BY_TXN_ID =
			"SELECT s FROM Staging s " +
			"WHERE s.messageTransactionId = ?1 ";
	
	public final static String NAMED_QUERY_DELETE_STAGING_BY_STATUS=
			"deleteStagingByStatus";
	
	public static final String JPQL_DELETE_STAGING_BY_STATUS =
			"DELETE FROM Staging s " +
			"WHERE s.processStatus IN ?1 ";
	
	public final static String NAMED_QUERY_UPDATE_STAGING_TO_ERR_BY_STATUS=
			"updateStagingToErrByStatus";
	
	public static final String JPQL_UPDATE_STAGING_TO_ERR_BY_STATUS =
			"UPDATE Staging s " +
			"SET s.processStatus = ?1 " + 
			"WHERE s.processStatus IN ?2";
	
	public final static String NAMED_QUERY_UPDATE_STAGING_RECEIVED_DT=
			"updateStagingReceivedDt";
	
	public static final String JPQL_UPDATE_STAGING_RECEIVED_DT =
			"UPDATE Staging s " +
			"SET s.receivedDt = ?2 " + 
			"WHERE s.stagingMessageId = ?1";

	public final static String NAMED_QUERY_FIND_STAGING_BY_ORDERING_IDENTIFIER=
			"findStagingByOrderingIdentifier";
	
	public static final String JPQL_FIND_STAGING_BY_ORDERING_IDENTIFIER =
			"SELECT s FROM Staging s " +
			"WHERE s.orderingIdentifier = ?1 ";
	
	public final static String NAMED_QUERY_DELETE_STAGING_BY_MSG_ID_RANGE=
			"deleteStagingByMsgIdRange";
	
	public static final String JPQL_DELETE_STAGING_BY_MSG_ID_RANGE =
			"DELETE FROM Staging s " +
			"WHERE s.stagingMessageId BETWEEN ?1 AND ?2 ";
	
	public final static String NAMED_QUERY_FIND_MINIMUM_ORDERING_IDENTIFIER=
			"findMinimumOrderingIdentifier";
	
	public static final String JPQL_FIND_MINIMUM_ORDERING_IDENTIFIER =
			"SELECT MIN(s.orderingIdentifier) FROM Staging s ";
	
	public final static String NAMED_QUERY_UPDATE_PROCESS_STATUS=
			"updateProcessStatus";
	
	public static final String JPQL_UPDATE_PROCESS_STATUS =
			"UPDATE Staging s " +
			"SET s.processStatus = ?2 " + 
			"WHERE s.messageTransactionId = ?1";
	
}
