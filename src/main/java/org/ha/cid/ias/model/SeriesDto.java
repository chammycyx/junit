package org.ha.cid.ias.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ha.cd2.isg.icw.tool.DateUtils;

/**
 * This DTO is the model for the following entities:
 * org.ha.cid.ias.entity.es.Series
 * org.ha.cid.ias.entity.rr.Series
 *
 * @author LSM131
 *
 */
public class SeriesDto {
	private String seriesInsatnceUid;
	private String seriesDescription;
	private String modality;
	private BigInteger entityId;
	private String seriesDate;

	private List<ImageDto> imageDtos;

	public String getSeriesInsatnceUid() {
		return seriesInsatnceUid;
	}

	public void setSeriesInsatnceUid(String seriesInsatnceUid) {
		this.seriesInsatnceUid = seriesInsatnceUid;
	}

	public String getSeriesDescription() {
		return seriesDescription;
	}

	public void setSeriesDescription(String seriesDescription) {
		this.seriesDescription = seriesDescription;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public BigInteger getEntityId() {
		return entityId;
	}

	public void setEntityId(BigInteger entityId) {
		this.entityId = entityId;
	}

	public String getSeriesDate() {
		return seriesDate;
	}

	public void setSeriesDate(String seriesDate) {
		this.seriesDate = seriesDate;
	}

	public List<ImageDto> getImageDtos() {
		return imageDtos;
	}

	public void setImageDtos(List<ImageDto> imageDtos) {
		this.imageDtos = imageDtos;
	}

	public static SeriesDto buildFrom(org.ha.cid.ias.entity.es.Series series){
		if(series == null){
			return null;
		}

		SeriesDto seriesDto = new SeriesDto();
		seriesDto.setSeriesInsatnceUid(series.getSeriesInstanceUID()); 
		seriesDto.setSeriesDescription(series.getSeriesDescription());
		seriesDto.setModality(series.getModality());
		seriesDto.setEntityId(series.getEntityId()!=null ? new BigInteger(series.getEntityId()) : null);
		seriesDto.setSeriesDate(series.getSeriesDate());

		//ImageDtos
		List<ImageDto> imageDtos = new ArrayList<ImageDto>();
		Iterator<org.ha.cid.ias.entity.es.Image> iterator = series.getImages().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.es.Image image = iterator.next();
			ImageDto imageDto = ImageDto.buildFrom(image);
			imageDtos.add(imageDto);
		}
		seriesDto.setImageDtos(imageDtos);

		return seriesDto;
	}

	public static SeriesDto buildFrom(org.ha.cid.ias.entity.rr.Series series){
		if(series == null){
			return null;
		}

		SeriesDto seriesDto = new SeriesDto();
		seriesDto.setSeriesInsatnceUid(series.getSeriesInsatnceUid());
		seriesDto.setSeriesDescription(series.getSeriesDescription());
		seriesDto.setModality(series.getModality());
		seriesDto.setEntityId(series.getEntityID()!=null ? new BigInteger(series.getEntityID()) : null);
		seriesDto.setSeriesDate(DateUtils.format(series.getSeriesDate(), DateUtils.FORMAT_TIMESTAMP_SHORT));

		//ImageDtos
		List<ImageDto> imageDtos = new ArrayList<ImageDto>();
		Iterator<org.ha.cid.ias.entity.rr.Image> iterator = series.getImages().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.rr.Image image = iterator.next();
			ImageDto imageDto = ImageDto.buildFrom(image);
			imageDtos.add(imageDto);
		}
		seriesDto.setImageDtos(imageDtos);

		return seriesDto;
	}
}