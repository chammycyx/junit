package org.ha.cid.ias.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.entity.esb.Staging;
import org.ha.cid.ias.entity.esb.Subscription;

public class StagingDto {
	private long stagingMessageId;
	private String messageTransactionId;
	private String messageCode;
	private MessageProcessStatus processStatus;
	private Date receivedDt;
	private Date stagingStartDt;
	private Date stagingEndDt;
	private Date orderingIdentifier;
	private List<SubscriptionDto> subscriptions;
	
	public String getMessageTransactionId() {
		return messageTransactionId;
	}

	public void setMessageTransactionId(String messageTransactionId) {
		this.messageTransactionId = messageTransactionId;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public MessageProcessStatus getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(MessageProcessStatus processStatus) {
		this.processStatus = processStatus;
	}

	public Date getReceivedDt() {
		return receivedDt;
	}

	public void setReceivedDt(Date receivedDt) {
		this.receivedDt = receivedDt;
	}

	public Date getStagingStartDt() {
		return stagingStartDt;
	}

	public void setStagingStartDt(Date stagingStartDt) {
		this.stagingStartDt = stagingStartDt;
	}

	public Date getStagingEndDt() {
		return stagingEndDt;
	}

	public void setStagingEndDt(Date stagingEndDt) {
		this.stagingEndDt = stagingEndDt;
	}

	public Date getOrderingIdentifier() {
		return orderingIdentifier;
	}

	public void setOrderingIdentifier(Date orderingIdentifier) {
		this.orderingIdentifier = orderingIdentifier;
	}

	public List<SubscriptionDto> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<SubscriptionDto> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	public static StagingDto buildFrom(Staging staging){
		if(staging == null){
			return null;
		}
		
		List<SubscriptionDto> subscriptions = new ArrayList<>();
		for(Subscription subscription:staging.getSubscriptions()){
			subscriptions.add(SubscriptionDto.buildFrom(subscription));	
		}
		
		StagingDto stagingDto = new StagingDto();
			stagingDto.setStagingMessageId(staging.getStagingMessageId());
			stagingDto.setMessageCode(staging.getMessageCode());
			stagingDto.setMessageTransactionId(staging.getMessageTransactionId());
			stagingDto.setOrderingIdentifier(staging.getOrderingIdentifier());
			stagingDto.setProcessStatus(staging.getProcessStatus());
			stagingDto.setReceivedDt(staging.getReceivedDt());
			stagingDto.setStagingEndDt(staging.getStagingEndDt());
			stagingDto.setStagingStartDt(staging.getStagingStartDt());
			stagingDto.setSubscriptions(subscriptions);
			
		return stagingDto;
	}

	public long getStagingMessageId() {
		return stagingMessageId;
	}

	public void setStagingMessageId(long stagingMessageId) {
		this.stagingMessageId = stagingMessageId;
	}
		
}
