package org.ha.cid.ias.model;

import java.math.BigInteger;


/**
 * This DTO is the model for the following entities:
 * org.ha.cid.ias.entity.es.Annotation
 * org.ha.cid.ias.entity.rr.Annotation
 *
 * @author LSM131
 *
 */
public class AnnotationDto {
	private BigInteger seq;
	private String type;
	private String text;
	private String coordinate;
	private String status;
	private String editable;
	private String updtm;

	public BigInteger getSeq() {
		return seq;
	}

	public void setSeq(BigInteger seq) {
		this.seq = seq;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getUpdtm() {
		return updtm;
	}

	public void setUpdtm(String updtm) {
		this.updtm = updtm;
	}

	public static AnnotationDto buildFrom(org.ha.cid.ias.entity.es.Annotation annotation){
		if(annotation == null){
			return null;
		}
		
		AnnotationDto annotationDto = new AnnotationDto();
		annotationDto.setCoordinate(annotation.getAnnotationCoordinate());
		annotationDto.setEditable(annotation.getAnnotationEditable());
		annotationDto.setSeq(BigInteger.valueOf(annotation.getAnnotationSeq()));
		annotationDto.setStatus(annotation.getAnnotationStatus());
		annotationDto.setText(annotation.getAnnotationText());
		annotationDto.setType(annotation.getAnnotationType());
		annotationDto.setUpdtm(annotation.getAnnotationUpdDtm());
		
		return annotationDto;
	}
	
	public static AnnotationDto buildFrom(org.ha.cid.ias.entity.rr.Annotation annotation){
		if(annotation == null){
			return null;
		}
		
		AnnotationDto annotationDto = new AnnotationDto();
		annotationDto.setCoordinate(annotation.getAnnotationCoordinate());
		annotationDto.setEditable(annotation.getAnnotationEditable());
		annotationDto.setSeq(BigInteger.valueOf(annotation.getAnnotationSeq()));
		annotationDto.setStatus(annotation.getAnnotationStatus());
		annotationDto.setText(annotation.getAnnotationText());
		annotationDto.setType(annotation.getAnnotationType());
		annotationDto.setUpdtm(annotation.getAnnotationUpdDtm());
		
		return annotationDto;
	}
}