/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.model;

// import net.sourceforge.cobertura.CoverageIgnore;

import org.apache.commons.lang3.StringUtils;

/**
 * The class which provides a <strong>value object</strong> of an <tt>Episode</tt>.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9916 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:41 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public class EsbEpisode {

	private String _caseNumber;
	private String _hospitalCode;

	/**
	 * @return the trimmed case number
	 */
	public String getCaseNumber() {
		return _caseNumber;
	}

	/**
	 * @param caseNumber the trimmed case number to set
	 */
	public void setCaseNumber(String caseNumber) {
		this._caseNumber = StringUtils.trim(caseNumber);
	}

	/**
	 * @return the hospital code
	 */
	public String getHospitalCode() {
		return _hospitalCode;
	}

	/**
	 * @param hospitalCode the hospital code to set
	 */
	public void setHospitalCode(String hospitalCode) {
		this._hospitalCode = hospitalCode;
	}

}
