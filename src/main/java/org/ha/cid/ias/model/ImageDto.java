package org.ha.cid.ias.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * This DTO is the model for the following entities:
 * org.ha.cid.ias.entity.es.Image
 * org.ha.cid.ias.entity.rr.Image
 *
 * @author LSM131
 *
 */
public class ImageDto {
	private String imageFile;
	private String imageFormat;
	private String imageHandling;
	private String imageId;
	private String imageKeyword;
	private BigInteger imageSequence;
	private BigInteger imageStatus;
	private String imageType;
	private String imageVersion;
	private boolean replaced = false;

	private List<AnnotationDto> annotationDtos;

	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	public String getImageFormat() {
		return imageFormat;
	}

	public void setImageFormat(String imageFormat) {
		this.imageFormat = imageFormat;
	}

	public String getImageHandling() {
		return imageHandling;
	}

	public void setImageHandling(String imageHandling) {
		this.imageHandling = imageHandling;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageKeyword() {
		return imageKeyword;
	}

	public void setImageKeyword(String imageKeyword) {
		this.imageKeyword = imageKeyword;
	}

	public BigInteger getImageSequence() {
		return imageSequence;
	}

	public void setImageSequence(BigInteger imageSequence) {
		this.imageSequence = imageSequence;
	}

	public BigInteger getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(BigInteger imageStatus) {
		this.imageStatus = imageStatus;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageVersion() {
		return imageVersion;
	}

	public void setImageVersion(String imageVersion) {
		this.imageVersion = imageVersion;
	}

	public boolean isReplaced() {
		return replaced;
	}

	public void setReplaced(boolean replaced) {
		this.replaced = replaced;
	}

	public List<AnnotationDto> getAnnotationDtos() {
		return annotationDtos;
	}

	public void setAnnotationDtos(List<AnnotationDto> annotationDtos) {
		this.annotationDtos = annotationDtos;
	}

	public static ImageDto buildFrom(org.ha.cid.ias.entity.es.Image image){
		if(image == null){
			return null;
		}

		ImageDto imageDto = new ImageDto();
		imageDto.setImageFile(image.getImageFile()); 
		imageDto.setImageFormat(image.getImageFormat());
		imageDto.setImageHandling(image.getImageHandling());
		imageDto.setImageId(image.getImageID());
		imageDto.setImageKeyword(image.getImageKeyword());
		imageDto.setImageSequence(BigInteger.valueOf(image.getImageSequence()));
		imageDto.setImageStatus(BigInteger.valueOf(image.getImageStatus()));
		imageDto.setImageType(image.getImageType());
		imageDto.setImageVersion(image.getImageVersion());
		imageDto.setReplaced(image.isReplaced());

		//AnnotationDtos
		List<AnnotationDto> annotationDtos = new ArrayList<AnnotationDto>();
		Iterator<org.ha.cid.ias.entity.es.Annotation> iterator = image.getAnnotations().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.es.Annotation annotation = iterator.next();
			AnnotationDto annotationDto = AnnotationDto.buildFrom(annotation);
			annotationDtos.add(annotationDto);
		}
		imageDto.setAnnotationDtos(annotationDtos);

		return imageDto;
	}

	public static ImageDto buildFrom(org.ha.cid.ias.entity.rr.Image image){
		if(image == null){
			return null;
		}

		ImageDto imageDto = new ImageDto();
		imageDto.setImageFile(image.getImageFile()); 
		imageDto.setImageFormat(image.getImageFormat());
		imageDto.setImageHandling(image.getImageHandling());
		imageDto.setImageId(image.getImageID());
		imageDto.setImageKeyword(image.getImageKeyword());
		imageDto.setImageSequence(BigInteger.valueOf(image.getImageSequence()));
		imageDto.setImageStatus(BigInteger.valueOf(image.getImageStatus()));
		imageDto.setImageType(image.getImageType());
		imageDto.setImageVersion(image.getImageVersion());
		imageDto.setReplaced(image.getReplaced() == 1);

		//AnnotationDtos
		List<AnnotationDto> annotationDtos = new ArrayList<AnnotationDto>();
		Iterator<org.ha.cid.ias.entity.rr.Annotation> iterator = image.getAnnotations().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.rr.Annotation annotation = iterator.next();
			AnnotationDto annotationDto = AnnotationDto.buildFrom(annotation);
			annotationDtos.add(annotationDto);
		}
		imageDto.setAnnotationDtos(annotationDtos);

		return imageDto;
	}
}