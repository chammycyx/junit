package org.ha.cid.ias.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.ha.cd2.isg.icw.tool.DateUtils;
/**
 * This DTO is the model for the following entities:
 * org.ha.cid.ias.entity.es.Study
 * org.ha.cid.ias.entity.rr.Study
 *
 * @author LSM131
 *
 */
public class StudyDto {
	private String studyId;
	private String caseNo;
	private String accessionNo;
	private String remark;
	private String studyType;
	private String visitHosp;
	private String studyDate;
	private String patHkid;
	private String patKey;
	private String patName;
	private String patSex;
	private String patDob;
	private String transactionCode;
	private String sendingApplication;
	private String visitSpec;
	private String visitWardClinic;
	private String visitPayCode;
	private String adtDtm;
	private String applicationType;
	private String studyStatus;
	
	private List<SeriesDto> seriesDtos;

	public String getStudyId() {
		return studyId;
	}

	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getAccessionNo() {
		return accessionNo;
	}

	public void setAccessionNo(String accessionNo) {
		this.accessionNo = accessionNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStudyType() {
		return studyType;
	}

	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}

	public String getVisitHosp() {
		return visitHosp;
	}

	public void setVisitHosp(String visitHosp) {
		this.visitHosp = visitHosp;
	}

	public String getStudyDate() {
		return studyDate;
	}

	public void setStudyDate(String studyDate) {
		this.studyDate = studyDate;
	}

	public String getPatHkid() {
		return patHkid;
	}

	public void setPatHkid(String patHkid) {
		this.patHkid = patHkid;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPatSex() {
		return patSex;
	}

	public void setPatSex(String patSex) {
		this.patSex = patSex;
	}

	public String getPatDob() {
		return patDob;
	}

	public void setPatDob(String patDob) {
		this.patDob = patDob;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getSendingApplication() {
		return sendingApplication;
	}

	public void setSendingApplication(String sendingApplication) {
		this.sendingApplication = sendingApplication;
	}

	public String getVisitSpec() {
		return visitSpec;
	}

	public void setVisitSpec(String visitSpec) {
		this.visitSpec = visitSpec;
	}

	public String getVisitWardClinic() {
		return visitWardClinic;
	}

	public void setVisitWardClinic(String visitWardClinic) {
		this.visitWardClinic = visitWardClinic;
	}

	public String getVisitPayCode() {
		return visitPayCode;
	}

	public void setVisitPayCode(String visitPayCode) {
		this.visitPayCode = visitPayCode;
	}

	public String getAdtDtm() {
		return adtDtm;
	}

	public void setAdtDtm(String adtDtm) {
		this.adtDtm = adtDtm;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getStudyStatus() {
		return studyStatus;
	}

	public void setStudyStatus(String studyStatus) {
		this.studyStatus = studyStatus;
	}

	public List<SeriesDto> getSeriesDtos() {
		return seriesDtos;
	}

	public void setSeriesDtos(List<SeriesDto> seriesDtos) {
		this.seriesDtos = seriesDtos;
	}

	public static StudyDto buildFrom(org.ha.cid.ias.entity.es.Study study){
		if(study == null){
			return null;
		}
		
		StudyDto studyDto = new StudyDto();
		
		studyDto.setAccessionNo(study.getAccessionNo());
		studyDto.setAdtDtm(null);
		studyDto.setApplicationType(null);
		studyDto.setCaseNo(study.getCaseNumber());
		
		if(study.getPatient() != null){
			org.ha.cid.ias.entity.es.Patient patient =  study.getPatient();
			studyDto.setPatHkid(patient.getPatientID());
			studyDto.setPatKey(patient.getPatientKey());
			studyDto.setPatName(patient.getPatientName());
			studyDto.setPatSex(patient.getPatientSex());
			studyDto.setPatDob(DateUtils.toFormat(patient.getPatientBirthDate(), DateUtils.FORMAT_TIMESTAMP));
		}  
		
		studyDto.setRemark(study.getRemark());
		studyDto.setSendingApplication(null);
		
		List<SeriesDto> seriesDtos = new ArrayList<SeriesDto>();
		Iterator<org.ha.cid.ias.entity.es.Series> iterator = study.getSerieses().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.es.Series series = iterator.next();
			SeriesDto seriesDto = SeriesDto.buildFrom(series);
			seriesDtos.add(seriesDto);
		}
		studyDto.setSeriesDtos(seriesDtos);
		
		studyDto.setStudyDate(study.getStudyDate());
		studyDto.setStudyId(study.getStudyID());
		studyDto.setStudyType(study.getStudyType());
		studyDto.setTransactionCode(null);
		studyDto.setVisitHosp(study.getHospitalCode());
		studyDto.setVisitPayCode(null);
		studyDto.setVisitSpec(null);
		studyDto.setVisitWardClinic(null);
		studyDto.setStudyStatus(study.getStudyStatus());

		return studyDto;
	}

	public static StudyDto buildFrom(org.ha.cid.ias.entity.rr.Study study){
		if(study == null){
			return null;
		}
		
		StudyDto studyDto = new StudyDto();
		
		studyDto.setAccessionNo(study.getAccessionNumber());
		studyDto.setAdtDtm(study.getAdtDtm());
		studyDto.setApplicationType(study.getApplicationType());
		studyDto.setCaseNo(study.getCaseNo());
		
		if(study.getPatient() != null){
			org.ha.cid.ias.entity.rr.Patient patient =  study.getPatient();
			studyDto.setPatHkid(patient.getPatId());
			studyDto.setPatKey(patient.getPatGlobalId());
			studyDto.setPatName(patient.getPatName());
			studyDto.setPatSex(patient.getPatSex());
			studyDto.setPatDob(DateUtils.toFormat(patient.getPatBirthdate(), DateUtils.FORMAT_TIMESTAMP));
		}  
		
		studyDto.setRemark(study.getRemark());
		studyDto.setSendingApplication(study.getSendingApplication());
		
		List<SeriesDto> seriesDtos = new ArrayList<SeriesDto>();
		Iterator<org.ha.cid.ias.entity.rr.Series> iterator = study.getSeries().iterator();
		while(iterator.hasNext()) {
			org.ha.cid.ias.entity.rr.Series series = iterator.next();
			SeriesDto seriesDto = SeriesDto.buildFrom(series);
			seriesDtos.add(seriesDto);
		}
		studyDto.setSeriesDtos(seriesDtos);
		
		studyDto.setStudyDate(DateUtils.format(study.getStudyDate(), DateUtils.FORMAT_TIMESTAMP_SHORT));
		studyDto.setStudyId(study.getStudyId());
		studyDto.setStudyType(study.getStudyType());
		studyDto.setTransactionCode(study.getTransationCode());
		studyDto.setVisitHosp(study.getHospitalCode());
		studyDto.setVisitPayCode(study.getPayCode());
		studyDto.setVisitSpec(study.getVisitSpec());
		studyDto.setVisitWardClinic(study.getVisitWardClinic());
		studyDto.setStudyStatus(study.getStudyStatus().getLookup());

		return studyDto;
	}

	@Override
	public String toString() {
		return "StudyDto [studyId=" + studyId + ", caseNo=" + caseNo
				+ ", accessionNo=" + accessionNo + ", remark=" + remark
				+ ", studyType=" + studyType + ", visitHosp=" + visitHosp
				+ ", studyDate=" + studyDate + ", patHkid=" + patHkid
				+ ", patKey=" + patKey + ", patName=" + patName + ", patSex="
				+ patSex + ", patDob=" + patDob + ", transactionCode="
				+ transactionCode + ", sendingApplication="
				+ sendingApplication + ", visitSpec=" + visitSpec
				+ ", visitWardClinic=" + visitWardClinic + ", visitPayCode="
				+ visitPayCode + ", adtDtm=" + adtDtm + ", applicationType="
				+ applicationType + ", studyStatus=" + studyStatus
				+ ", seriesDtos=" + seriesDtos + "]";
	}
	
	
}