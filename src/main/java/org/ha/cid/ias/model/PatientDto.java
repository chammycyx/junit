package org.ha.cid.ias.model;

import org.ha.cid.ias.entity.es.Patient;


public class PatientDto {
	private String patName;
	private String patSex;
	private String patDob;
	private String patKey;
	private String patHkid;
	
	public String getPatName() {
		return patName;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getPatSex() {
		return patSex;
	}
	public void setPatSex(String patSex) {
		this.patSex = patSex;
	}
	public String getPatDob() {
		return patDob;
	}
	public void setDob(String patBirthdate) {
		this.patDob = patBirthdate;
	}
	public String getPatKey() {
		return patKey;
	}
	public void setPatKey(String patGlobalId) {
		this.patKey = patGlobalId;
	}
	public String getPatHkid() {
		return patHkid;
	}
	public void setPatHkid(String patHkid) {
		this.patHkid = patHkid;
	}
	public static PatientDto buildFrom(Patient patient) {
		if(patient != null){
			PatientDto patientDto = new PatientDto();
				patientDto.setDob(patient.getPatientBirthDate());
				patientDto.setPatKey(patient.getPatientKey());
				patientDto.setPatName(patient.getPatientName());
				patientDto.setPatSex(patient.getPatientSex());
				patientDto.setPatHkid(patient.getPatientID());
			return patientDto;
		}
		
		return null;
	}
	public static PatientDto buildFrom(org.ha.cid.ias.entity.rr.Patient patient) {
		if(patient != null){
			PatientDto patientDto = new PatientDto();
				patientDto.setDob(patient.getPatBirthdate());
				patientDto.setPatKey(patient.getPatGlobalId());
				patientDto.setPatName(patient.getPatName());
				patientDto.setPatSex(patient.getPatSex());
				patientDto.setPatHkid(patient.getPatId());
			return patientDto;
		}
		
		return null;
	}
	
}
