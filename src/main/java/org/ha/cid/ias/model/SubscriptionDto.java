package org.ha.cid.ias.model;

import java.util.Date;

import org.ha.cid.ias.entity.esb.Subscription;

public class SubscriptionDto {
	private long version;
	private Date publishedDt;
	private Date receivedDt;
	private Date completedDt;
	private StagingDto staging;
	
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public Date getPublishedDt() {
		return publishedDt;
	}
	public void setPublishedDt(Date publishedDt) {
		this.publishedDt = publishedDt;
	}
	public Date getReceivedDt() {
		return receivedDt;
	}
	public void setReceivedDt(Date receivedDt) {
		this.receivedDt = receivedDt;
	}
	public Date getCompletedDt() {
		return completedDt;
	}
	public void setCompletedDt(Date completedDt) {
		this.completedDt = completedDt;
	}
	public StagingDto getStaging() {
		return staging;
	}
	public void setStaging(StagingDto staging) {
		this.staging = staging;
	}
	
	public static SubscriptionDto buildFrom(Subscription subscription){
		if(subscription == null){
			return null;
		}
		
		SubscriptionDto subscriptionDto = new SubscriptionDto();
			subscriptionDto.setCompletedDt(subscription.getCompletedDt());
			subscriptionDto.setPublishedDt(subscription.getPublishedDt());
			subscriptionDto.setReceivedDt(subscription.getReceivedDt());
			subscriptionDto.setVersion(subscription.getVersion());
		return subscriptionDto;
	}
	
}
