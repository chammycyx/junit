package org.ha.cid.ias.dao;

import org.ha.cid.ias.entity.es.Study;

public interface EsStudyDao {

	public Study findStudyByAccessionNo(String accessionNo);
	
	public void deleteStudy(Study study);

	public Study findStudyByPatientKey(String patientKey);

	public void update(Study study);

	public Study refresh(Study study);

}
