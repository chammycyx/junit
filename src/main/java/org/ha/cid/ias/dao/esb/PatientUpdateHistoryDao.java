package org.ha.cid.ias.dao.esb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import org.ha.cid.ias.entity.esb.PatientUpdateHistory;

public class PatientUpdateHistoryDao extends Dao<PatientUpdateHistory> {

	public PatientUpdateHistoryDao() {
		super(PatientUpdateHistory.class);
	}
	
	/**
	 * Find list of patient update history by patient's keys
	 * 
	 * @param patientKeys keys of target patients
	 * @return a list of patient update history
	 */
	public List<PatientUpdateHistory> findByPatientKeys(List<String> patientKeys){
		TypedQuery<PatientUpdateHistory> query = getEntityManager().createNamedQuery(
				PatientUpdateHistory.NAMED_QUERY_FIND_BY_PATIENT_KEYS,
	    		PatientUpdateHistory.class
	    	);

    	query.setParameter(1, patientKeys);

    	return query.getResultList();
	}

	public void flush() {
		getEntityManager().flush();
	}

}
