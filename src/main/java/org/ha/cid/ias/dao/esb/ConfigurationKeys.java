/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;


/**
 * The final class which defines the keys of {@link Configuration}.
 *
 * <pre>
 * Last commit:-
 * $Rev: 11885 $
 * $Author: ymm698 $
 * $Date: 2017-07-24 15:18:25 +0800 (週一, 24 七月 2017) $
 * </pre>
 *
 */
public final class ConfigurationKeys {

	/**
	 * The key of enabling the disorder checking of PAS-ESB message.
	 */
	public static final String DISORDER_CHECKER_ENABLE = "esb.disorder.checker.enable";

	/**
	 * The key of enabling the duplication checking of PAS-ESB message.
	 */
	public static final String DUPLICATION_CHECKER_ENABLE = "esb.duplication.checker.enable";

	/**
	 * The key of enabling the polling of PAS-ESB message.
	 */
	public static final String STAGING_POLLER_ENABLE = "esb.staging.poller.enable";

	/**
	 * The key of dayback value for checking the duplication of PAS-ESB message.
	 */
	public static final String DUPLICATION_DAYBACK = "esb.duplication.dayback";

	/**
	 * The key of login user for the receiver endpoint.
	 */
	public static final String ENDPOINT_LOGIN_USER = "esb.endpoint.login.user";

	/**
	 * The key of login password for the receiver endpoint.
	 */
	public static final String ENDPOINT_LOGIN_PASSWORD = "esb.endpoint.login.password";

	/**
	 * The key of email SMTP server address
	 */
	public static final String SMTP_SERVER_ADDRESS = "esb.smtp.server.address";

    /**
     * The key of email SMTP server port
     */
    public static final String SMTP_SERVER_PORT = "esb.smtp.server.port";

    /**
     * The key of email SMTP server password
     */
    public static final String SMTP_SERVER_PASSWORD = "esb.smtp.server.password";

    /**
     * The key of STMP email validate
     */
    public static final String SMTP_EMAIL_VALIDATE = "esb.smtp.email.validate";

    /**
     * The key of FROM email address
     */
    public static final String FROM_EMAIL_ADDRESS = "esb.email.address.from";

    /**
     * The key of TO email address
     */
    public static final String TO_EMAIL_ADDRESS = "esb.email.address.to";

    /**
     * The key of email subject (disorder)
     */
    public static final String DISORDER_EMAIL_SUBJECT = "esb.email.subject.disorder";

    /**
     * The key of email subject (duplication)
     */
    public static final String DUPLICATION_EMAIL_SUBJECT = "esb.email.subject.duplication";

    /**
     * The key of email content
     */
    public static final String EMAIL_CONTNET = "esb.email.content";

    /**
     * The key of enabling PAS web service.
     */
    public static final String PAS_WEB_SERVICE_ENABLE = "pas.ws.enable";

    /**
     * The key of PAS web service address.
     */
    public static final String PAS_WEB_SERVICE_ADDRESS = "pas.ws.address";

    /**
     * The key of PAS web service username.
     */
    public static final String PAS_WEB_SERVICE_USERNAME = "pas.ws.username";

    /**
     * The key of PAS web service password.
     */
    public static final String PAS_WEB_SERVICE_PASSWORD = "pas.ws.password";

    /**
     * The key of PAS web service connect timeout.
     */
    public static final String PAS_WEB_SERVICE_CONNECT_TIMEOUT = "pas.ws.connect.timeout";

    /**
     * The key of PAS web service request timeout
     */
    public static final String PAS_WEB_SERVICE_REQUEST_TIMEOUT = "pas.ws.request.timeout";
    
    /**
     * The key of esb processing cut-off time
     */
    public static final String CUTOFF_TIME = "esb.cutoff.time";
    
    /**
     * The key of enabling null ordering identifier data polling 
     */
    public static final String NULL_ORDERING_IDENTIFIER_STAGING_POLLING_ENABLE = "null.ordering.identifier.staging.polling.enable";

}