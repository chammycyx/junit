/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;



/**
 * The abstract class which defines the skeleton of any <tt>DaoBean</tt>.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9641 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:19:28 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
public abstract class Dao<T> {

	protected static final int SINGLE_IN_RESULTS = 1;

    @PersistenceContext(unitName = DaoProperties.PERSISTENCE_CONTEXT_UNIT_NAME)
    private EntityManager _entityManager;

    private Class<T> _clazz;

    /**
     * Get the {@link EntityManager} in the persistence context.
     *
     * @return a {@link EntityManager}
     */
    protected EntityManager getEntityManager() {
    	return _entityManager;
    }

    /**
     * Get a single entity in the entity list.
     *
     * @param objects a list of entity
     *
     * @return an entity
     */
    protected T getSingle(List<T> objects) {
    	return (objects==null||objects.size()==0) ? null : objects.get(0);
    }

    /**
     * Define a constructor by setting its entity {@link Class}.
     *
     * @param clazz the {@link Class} of entity
     */
    protected Dao(Class<T> clazz) {
        this._clazz = clazz;
    }

    /**
     * Refer to {@link #create(Object, boolean)}.
     *
     * @param entity the entity to create
     */
    public void create(T entity) {

    	create(entity, false); // don't flush
    }

    /**
     * Create an entity by using {@link EntityManager#persist(Object)}.
     *
     * @param entity the entity to create
     * @param isFlushRequired <code>true</code> if flush() is required
     */
    public void create(T entity, boolean isFlushRequired) {

    	_entityManager.persist(entity);

    	if (isFlushRequired) {

    		_entityManager.flush();
    	}
    }

    /**
     * Find an entity by using {@link EntityManager#find(Class, Object)}.
     *
     * @param id the primary key of entity
     *
     * @return a matched entity, if any
     */
    public T find(Object id) {
        return _entityManager.find(_clazz, id);
    }

    /**
     * Delete an entity by using {@link EntityManager#remove(Object)}.
     *
     * @param entity the entity to delete
     */
    public void delete(T entity) {
        _entityManager.remove(entity);
    }

    /**
     * Refer to {@link Dao#update(Object, boolean)}.
     *
     * @param entity the entity to update
     */
    public void update(T entity) {

        update(entity, false); // don't flush
    }

    /**
     * Update an entity by using {@link EntityManager#persist(Object)}.
     *
     * @param entity the entity to update
     * @param isFlushRequired <code>true</code> if flush() is required
     */
    public void update(T entity, boolean isFlushRequired) {

    	_entityManager.persist(entity);

    	if (isFlushRequired) {

    		_entityManager.flush();
    	}
    }

}
