package org.ha.cid.ias.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.ha.cid.ias.dao.EsStudyDao;
import org.ha.cid.ias.entity.es.Study;

public class EsStudyDaoImpl implements EsStudyDao {
	
	@PersistenceContext(unitName="persistence.es")
	private EntityManager em;

	@Override
	@SuppressWarnings("unchecked")
	public Study findStudyByAccessionNo(String accessionNo) {
		Query query = em.createNamedQuery(Study.FIND_BY_ACCESSION_NO);
		query.setParameter("accessionNo", accessionNo);
		List<Study> result = query.getResultList();
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public void deleteStudy(Study study) {
		em.remove(study);
	}

	@Override
	public Study findStudyByPatientKey(String patientKey) {
		Query query = em.createNamedQuery(Study.FIND_BY_PATIENT_KEY);
			query.setMaxResults(1);
			query.setParameter("patientKey", patientKey);
		List<Study> result = query.getResultList();
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public void update(Study study) {
		em.merge(study);
		em.flush();
	}

	@Override
	public Study refresh(Study study) {
		em.refresh(study);
		return study;
	}
}
