package org.ha.cid.ias.dao;

import java.util.List;

import org.ha.cid.ias.entity.es.AuditEvent;

public interface EsAuditEventDao {

	public List<AuditEvent> findByAccessionNo(String accessionNo);
	
}
