package org.ha.cid.ias.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.ha.cid.ias.dao.RrPatientDao;
import org.ha.cid.ias.entity.rr.Patient;
import org.ha.cid.ias.entity.rr.Study;

public class RrPatientDaoImpl implements RrPatientDao {
	
	@PersistenceContext(unitName="persistence.rr")
	private EntityManager em;

	@Override
	public Patient findPatientByPatientKey(String patientKey) {
		Query query = em.createNamedQuery(Patient.FIND_BY_PATIENTKEY);
		query.setParameter("patientKey", patientKey);
		List<Patient> result = query.getResultList();	
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public void insert(Patient patient) {
		em.persist(patient);
		em.flush();
	}

	@Override
	public void deletePatientByPatientKey(String patientKey) {
		Patient patient = findPatientByPatientKey(patientKey);
		if(patient!=null){
			em.remove(patient);
		}
	}

}
