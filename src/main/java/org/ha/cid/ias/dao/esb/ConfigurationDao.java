/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import org.ha.cid.ias.entity.esb.Configuration;


/**
 * The class which provides a Data Access Object for {@link Configuration}.
 *
 * <pre>
 * Last commit:-
 * $Rev: 10968 $
 * $Author: ltm548 $
 * $Date: 2016-12-20 15:38:48 +0800 (週二, 20 十二月 2016) $
 * </pre>
 *
 */
public class ConfigurationDao extends Dao<Configuration> {

	/**
	 * Define a constructor by setting the {@link Class} of {@link Configuration}.
	 */
    public ConfigurationDao() {
        super(Configuration.class);
    }

    /**
     * Invoke the named query {@link Configuration#NAMED_QUERY_FIND_BY_KEY}.
     *
     * @param key the key of a configuration parameter
     *
     * @return a {@link Configuration} if matched
     */
    public Configuration findByKey(String key) {

    	TypedQuery<Configuration> query = getEntityManager().createNamedQuery(
    		Configuration.NAMED_QUERY_FIND_BY_KEY,
    		Configuration.class
    	);

    	query.setParameter(1, key);

    	return getSingle(query.getResultList());
    }

    /**
     * Get the <tt>value</tt> by a given <tt>key</tt> of any configuration parameter.
     *
     * @param key the <tt>key</tt> of a configuration parameter
     *
     * @return the <tt>value</tt> of a configuration parameter.
     */
    public String findValueByKey(String key) {

    	final Configuration configurationParameter = findByKey(key);
    	return (configurationParameter != null) ? configurationParameter.getValue() : null;
    }

}
