package org.ha.cid.ias.dao;

import org.ha.cid.ias.entity.rr.Patient;

public interface RrPatientDao {
	Patient findPatientByPatientKey(String patientKey);

	void insert(Patient patient);

	void deletePatientByPatientKey(String patientKey);
}
