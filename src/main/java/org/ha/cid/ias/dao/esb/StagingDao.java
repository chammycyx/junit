/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.FlushModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.ha.cid.ias.entity.esb.MessageProcessStatus;
import org.ha.cid.ias.entity.esb.Staging;
import org.ha.cid.ias.entity.esb.Subscription;
import org.ha.cid.ias.model.StagingQuery;


/**
 * The class which provides a Data Access Object for {@link Staging}.
 *
 * <pre>
 * Last commit:-
 * $Rev: 11885 $
 * $Author: ymm698 $
 * $Date: 2017-07-24 15:18:25 +0800 (週一, 24 七月 2017) $
 * </pre>
 *
 */
public class StagingDao extends Dao<Staging> {

	/**
	 * Define a constructor by setting the {@link Class} of {@link Staging}.
	 */
	public StagingDao() {
		super(Staging.class);
	}

	/**
	 * Invoke the named query {@link StagingQuery#NAMED_QUERY_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID}.
	 *
	 * @param processStatus a {@link MessageProcessStatus}
	 *
	 * @return a {@link Staging} that its <tt>processStatus</tt> is matched; else <code>null</code>
	 */
	public Staging findSingleByProcessStatusOrderByStagingMessageId(MessageProcessStatus processStatus) {

		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
			StagingQuery.NAMED_QUERY_FIND_BY_PROCESS_STATUS_ORDER_BY_STAGING_MESSAGE_ID,
			Staging.class
		);

		query.setMaxResults(SINGLE_IN_RESULTS);
		query.setParameter(1, processStatus);

		return getSingle(Collections.unmodifiableList(query.getResultList()));
	}

	/**
	 * Invoke the named query {@link StagingQuery#NAMED_QUERY_FIND_BY_SUBSCRIPTION_ID}.
	 *
	 * @param subscriptionId a subscription ID
	 *
	 * @return a {@link Staging} which contains the matched {@link Subscription}; else <code>null</code>
	 */
	public Staging findBySubscriptionId(long subscriptionId) {

		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
			StagingQuery.NAMED_QUERY_FIND_BY_SUBSCRIPTION_ID,
			Staging.class
		);

		query.setParameter(1, subscriptionId);

		return getSingle(Collections.unmodifiableList(query.getResultList()));
	}

	/**
	 * Invoke the named query {@link StagingQuery#NAMED_QUERY_FIND_PRECEDING_DUPLICATE_MESSAGE_TRANSACTION_ID}
	 *
	 * @param currentStagingMessageId the current <tt>stagingMessageId</tt> of {@link Staging}
	 * @param messageTransactionId the <tt>messageTransactionId</tt> of {@link Staging}
	 *
	 * @return a {@link Staging} which is the last one with <tt>messageTransactionId</tt> matched; else <code>null</code>
	 */
	public Staging findPrecedingDuplicateByMessageTranactionID(long currentStagingMessageId, String messageTransactionId) {

		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
			StagingQuery.NAMED_QUERY_FIND_PRECEDING_DUPLICATE_MESSAGE_TRANSACTION_ID,
			Staging.class
		);

		query.setMaxResults(SINGLE_IN_RESULTS);
		query.setParameter(1, currentStagingMessageId);
		query.setParameter(2, messageTransactionId);

		return getSingle(Collections.unmodifiableList(query.getResultList()));
	}

	/**
	 * Invoke the named query {@link StagingQuery#JPQL_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT}.
	 *
	 * @param currentStagingMessageId the current <tt>stagingMessageId</tt> of {@link Staging}
	 *
	 * @return the last processed {@link Staging} before current one
	 */
	public Staging findPrecedingProcessedBeforeCurrent(long currentStagingMessageId) {

		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
			StagingQuery.NAMED_QUERY_FIND_PRECEDING_PROCESSED_BEFORE_CURRENT,
			Staging.class
		);

		query.setMaxResults(SINGLE_IN_RESULTS);
		query.setParameter(1, currentStagingMessageId);
		query.setParameter(2, MessageProcessStatus.DUPLICATE);
		query.setParameter(3, MessageProcessStatus.OTHERS);

		return getSingle(Collections.unmodifiableList(query.getResultList()));
	}

	/**
	 * Invoke the named query {@link StagingQuery#JPQL_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER}.
	 * 
	 * @param processStatus the target staging with process status
	 * @param cutoffTime the target staging with received time before cut-off time
	 * @return the stage with smallest ordering identifier
	 */
	public Staging findSingleByProcessStatusWithCutoffTimeOrderByOrderingIdentifier(MessageProcessStatus processStatus, Date cutoffTime) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_FIND_BY_PROCESS_STATUS_WITH_CUTOFF_TIME_ORDER_BY_ORDERING_IDENTIFIER,
				Staging.class
			);

			query.setMaxResults(SINGLE_IN_RESULTS);
			query.setParameter("status", processStatus);
			query.setParameter("cutoffDt", cutoffTime);

			return getSingle(Collections.unmodifiableList(query.getResultList()));
	}

	/**
	 * Find a list of {@link Staging}s after ordering identifier
	 * 
	 * @param orderingIdentifier the target {@link Staging}s after ordering identifier
	 * @return a list of {@link Staging}s
	 */
	public List<Staging> findPolledStagingAfterOrderingIdentifier(Date orderingIdentifier) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_FIND_STAGING_AFTER_ORDERING_IDENTIFIER,
				Staging.class
			);

		query.setParameter(1, orderingIdentifier);
		query.setParameter(2, Arrays.asList(MessageProcessStatus.INITIAL, MessageProcessStatus.PROCESSING));
		return Collections.unmodifiableList(query.getResultList());
	}

	public List<Staging> findByTxnId(String txnId) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_FIND_STAGING_BY_TXN_ID,
				Staging.class
			);

			query.setParameter("1", txnId);

		List result = query.getResultList();
		
		//refresh entity to discard caching from 2nd level cache
		Iterator<Staging> iterator = result.iterator();
		while(iterator.hasNext()){
			try{
				getEntityManager().refresh(iterator.next());
			}catch(EntityNotFoundException ex){
				iterator.remove();
			}
		}
			
		return result;
	}

	public void deleteByStatus(List<MessageProcessStatus> status) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_DELETE_STAGING_BY_STATUS,
				Staging.class
			);

		query.setParameter(1, status);
		query.executeUpdate();
	}

	public void markStagingToErrWithStatus(List<MessageProcessStatus> status) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_UPDATE_STAGING_TO_ERR_BY_STATUS,
				Staging.class
			);

		query.setParameter(1, MessageProcessStatus.OTHERS);
		query.setParameter(2, status);
		query.executeUpdate();
	}

	public long findNumStagingWithStatus(List<MessageProcessStatus> status) {
		Query query = getEntityManager().createNativeQuery("SELECT COUNT(*) FROM t_esb_staging s WHERE s.process_status='A' ");
		Long count = (Long) query.getSingleResult();
		return count;
	}

	public void setReceivedDt(long stagingMessageId, Date time) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_UPDATE_STAGING_RECEIVED_DT,
				Staging.class
			);

		query.setParameter(1, stagingMessageId);
		query.setParameter(2, time);
		query.executeUpdate();
	}

	public List<Staging> findStagingByOrderingIdentifier(Date time) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_FIND_STAGING_BY_ORDERING_IDENTIFIER,
				Staging.class
			);

			query.setParameter("1", time);

		List result = query.getResultList();
		
		//refresh entity to discard caching from 2nd level cache
		Iterator<Staging> iterator = result.iterator();
		while(iterator.hasNext()){
			try{
				getEntityManager().refresh(iterator.next());
			}catch(EntityNotFoundException ex){
				iterator.remove();
			}
		}
			
		return result;
	}

	public void removeStagingByMsgIdRange(long fromMsgId, long toMsgId) {
		TypedQuery<Staging> query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_DELETE_STAGING_BY_MSG_ID_RANGE,
				Staging.class
			);

		query.setParameter(1, fromMsgId);
		query.setParameter(2, toMsgId);
		query.executeUpdate();
	}

	public Date findMinimumOrderingIdentifier() {
		Query query = getEntityManager().createNamedQuery(
				StagingQuery.NAMED_QUERY_FIND_MINIMUM_ORDERING_IDENTIFIER,
				Staging.class
			);

		return (Date) query.getSingleResult();
	}

	public void setOrderingIdentifierNull(Date orderingIdentifier) {
		List<Staging> stagings = findStagingByOrderingIdentifier(orderingIdentifier);
		
		for(Staging staging:stagings){
			staging.setOrderingIdentifier(null);
//			getEntityManager().merge(staging);
			getEntityManager().persist(staging);
		}
		getEntityManager().flush();
	}

	public void setStagingStatus(Date evn2, MessageProcessStatus messageProcessStatus) {
		List<Staging> stagings = findStagingByOrderingIdentifier(evn2);
		for(Staging staging:stagings){
			staging.setProcessStatus(messageProcessStatus);
			getEntityManager().persist(staging);
		}
		getEntityManager().flush();
	}

}
