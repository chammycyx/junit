/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;


/**
 * The final class which defines the value of {@link Configuration}.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9642 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:19:34 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
public final class ConfigurationValue {

    /**
     * The value of boolean - TRUE.
     */
    public static final String BOOLEAN_TRUE = "true";

    /**
     * The value of boolean - TRUE.
     */
    public static final String BOOLEAN_FALSE = "false";
    
    /**
     * Delimiter of email address
     */
    public static final String EMAIL_ADDRESS_DELIMITER = ";";

}