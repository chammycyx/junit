package org.ha.cid.ias.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.ha.cid.ias.dao.RrStudyDao;
import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.entity.rr.Study;

public class RrStudyDaoImpl implements RrStudyDao {
	
	@PersistenceContext(unitName="persistence.rr")
	private EntityManager em;

	@Override
	@SuppressWarnings("unchecked")
	public Study findStudyByAccessionNo(String accessionNo) {
		
		
		Query query = em.createNamedQuery(Study.FIND_BY_ACCESION_NUMBER);
		query.setParameter(1, accessionNo);
		List<Study> result = query.getResultList();	
		if (result != null && result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public void deletePathIndex(long pathIndexId) {
		String sql = "delete from tg_pathindex where pathindex_no = ?1";
		Query query = em.createNativeQuery(sql);
		query.setParameter(1, pathIndexId);
		query.executeUpdate();
	}

	
	
	@Override
	public void update(Study study) {
		em.merge(study);
		em.flush();
	}

	@Override
	public Study refresh(Study study) {
		em.refresh(study);
		return study;
	}
	
	@Override
	public void updatePathIndex(PathIndex pathIndex) {
		em.merge(pathIndex);
		em.flush();
		em.clear();
	}
	
}
