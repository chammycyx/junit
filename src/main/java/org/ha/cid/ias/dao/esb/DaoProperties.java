/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.dao.esb;


/**
 * The final class which defines the properties of Data Access Object.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9640 $
 * $Author: wch076 $
 * $Date: 2016-06-15 16:19:20 +0800 (週三, 15 六月 2016) $
 * </pre>
 *
 */
public final class DaoProperties {

	/**
	 * The unit name of persistence context which is specified in the <tt>persistence.xml</tt>.
	 */
	public static final String PERSISTENCE_CONTEXT_UNIT_NAME = "persistence.esb";

}
