package org.ha.cid.ias.dao;

import java.util.List;

import org.ha.cid.ias.entity.rr.AuditEvent;

public interface RrAuditEventDao {

	public List<AuditEvent> findByAccessionNo(String accessionNo);
	
}
