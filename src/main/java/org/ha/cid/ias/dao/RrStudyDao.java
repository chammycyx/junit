package org.ha.cid.ias.dao;

import org.ha.cid.ias.entity.rr.PathIndex;
import org.ha.cid.ias.entity.rr.Study;

public interface RrStudyDao {

	public Study findStudyByAccessionNo(String accessionNo);

	public void deletePathIndex(long pathIndexId);

	public void update(Study study);

	public Study refresh(Study study);
	
	public void updatePathIndex(PathIndex pathIndex);
}
