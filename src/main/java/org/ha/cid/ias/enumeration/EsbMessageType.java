/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.enumeration;

// import net.sourceforge.cobertura.CoverageIgnore;

/**
 * The <code>enum</code> which specifies different types of ESB messages.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9916 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:41 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public enum EsbMessageType {

	A08,
	A40,
	A45,
	A47,
	UNCERTAIN

}
