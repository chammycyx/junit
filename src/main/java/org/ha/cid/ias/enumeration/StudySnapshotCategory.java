/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.enumeration;

// import net.sourceforge.cobertura.CoverageIgnore;

/**
 * The enum which defines the category of study snapshot.
 *
 * <pre>
 * Last commit:-
 * $Rev: 9915 $
 * $Author: wch076 $
 * $Date: 2016-07-08 10:10:26 +0800 (週五, 08 七月 2016) $
 * </pre>
 *
 */
// @CoverageIgnore
public enum StudySnapshotCategory {

	/**
	 * The <strong>before</strong> study snapshot of a <tt>FromPatient</tt>.
	 */
	FROM_PATIENT_BEFORE("FB"),

	/**
	 * The <strong>after</strong> study snapshot of a <tt>FromPatient</tt>.
	 */
	FROM_PATIENT_AFTER("FA"),

	/**
	 * The <strong>before</strong> study snapshot of a <tt>ToPatient</tt>.
	 */
	TO_PATIENT_BEFORE("TB"),

	/**
	 * The <strong>after</strong> study snapshot of a <tt>ToPatient</tt>.
	 */
	TO_PATIENT_AFTER("TA");

	private String _name;

	private StudySnapshotCategory(String name) {
		this._name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this._name;
	}

}
