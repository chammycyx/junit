/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ha.cid.ias.enumeration;

/**
 *
 * @author developer
 */
public enum SeriesStatus {
    IN_PROGRESS,
    SUCCESS,
    WARNING,
    FAILED;
}
