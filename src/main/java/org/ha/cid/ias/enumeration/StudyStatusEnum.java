package org.ha.cid.ias.enumeration;

public enum StudyStatusEnum {
	PROCESSING("Processing", "Study is processing"), 
	PARKED("Parked", "Study is Parked"), 
	VALIDATING("Validating", "Study is Validating"), 
	MATCHED("Matched", "The study is Matched against the QA rules"), 
	UNMATCHED("Unmatched", "The study is Unmatched against the QA rules"), 
	DELETED("Deleted", "The study is Deleted");

	private String lookup;
	private String description;

	private StudyStatusEnum(String lookup, String description) {
		this.lookup = lookup;
		this.description = description;
	}

	public String getLookup() {
		return lookup;
	}

	public String getDescription() {
		return description;
	}
}
