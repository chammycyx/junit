package org.ha.cid.ias;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	private static final String[] configLocations = {
		"classpath:bean-context.xml",
		"classpath:internal-context.xml"
	};
	
	public static void main(String[] args) {
        try (ClassPathXmlApplicationContext  context =  new ClassPathXmlApplicationContext(configLocations)) {
            Main p = context.getBean(Main.class);
            p.start(args);
        };
    }

    private void start(String[] args) {
    }
	
}
