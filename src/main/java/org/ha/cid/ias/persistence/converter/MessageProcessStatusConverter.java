/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.persistence.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.ha.cid.ias.entity.esb.MessageProcessStatus;


/**
 * The class which provides a converter for {@link MessageProcessStatus} when persisting or
 * retrieving the entity <code>Staging</code>.
 *
 * <pre>
 * Last commit:-
 * $Rev: 8230 $
 * $Author: ltm548 $
 * $Date: 2016-02-04 10:46:53 +0800 (週四, 04 二月 2016) $
 * </pre>
 *
 */
@Converter
public class MessageProcessStatusConverter implements AttributeConverter<MessageProcessStatus, String> {

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
	 */
	@Override
	public String convertToDatabaseColumn(MessageProcessStatus attribute) {

		return attribute.toString();
	}

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public MessageProcessStatus convertToEntityAttribute(String dbData) {

		MessageProcessStatus processStatus = null;

		for (MessageProcessStatus value : MessageProcessStatus.values()) {

			if (dbData.equalsIgnoreCase(value.toString())) {

				processStatus = value;
			}
		}

		return processStatus;
	}

}
