/*
 * Copyright (C) Hospital Authority - All Rights Reserved
 * Clinical Departmental Systems Team 2 (CD2): Radiology Information and Image Distribution
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */
package org.ha.cid.ias.persistence.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.ha.cid.ias.enumeration.StudySnapshotCategory;


/**
 * The class which provides a converter for {@link StudySnapshotCategory} when persisting or
 * retrieving the entity <code>SubscriptionStudySnapshot</code.
 *
 * <pre>
 * Last commit:-
 * $Rev: 8401 $
 * $Author: ltm548 $
 * $Date: 2016-02-17 12:07:43 +0800 (週三, 17 二月 2016) $
 * </pre>
 *
 */
@Converter
public class StudySnapshotCategoryConverter implements AttributeConverter<StudySnapshotCategory, String> {

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.Object)
	 */
	@Override
	public String convertToDatabaseColumn(StudySnapshotCategory attribute) {

		return attribute.toString();
	}

	/* (non-Javadoc)
	 * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.Object)
	 */
	@Override
	public StudySnapshotCategory convertToEntityAttribute(String dbData) {

		StudySnapshotCategory category = null;

		for (StudySnapshotCategory value : StudySnapshotCategory.values()) {

			if (dbData.equalsIgnoreCase(value.toString())) {

				category = value;
			}
		}

		return category;
	}

}
