package org.ha.cid.ias.constant;

import java.math.BigInteger;

public class IasConstants {

	public static final String IMAGE_TYPE_JPEG = "JPEG";
	
	public static final String IMAGE_HANDLING_YES = "Y";
	public static final String IMAGE_HANDLING_NO = "N";
	
	public static final BigInteger IMAGE_STATUS_INACTIVE = BigInteger.ZERO;
	public static final BigInteger IMAGE_STATUS_ACTIVE = BigInteger.ONE;
}
